<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_AdminController extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->checkAdminLogin();
	}
	public function checkAdminLogin()
	{
		$id = $this->session->userdata('id');	 
		if(empty($id)){
			redirect('admin/login');
		}
	}
	function _sendEmail($from,$sender_name,$to,$sub,$body)
	{						      
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;	
		$this->email->initialize($config);
		$this->email->from($from, $sender_name);
		$this->email->to($to);	
		$this->email->subject($sub);
		$this->email->message($body);	
		$res = $this->email->send();			
		return $res;	
	}
}

class MY_FundraiserController extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->checkFundraiserLogin();
	}
	public function checkFundraiserLogin()
	{
		$id = $this->session->userdata('fundraiser_id');	 
		if(empty($id)){
			redirect('admin/login');
		}
	}
	function _sendEmail($from,$sender_name,$to,$sub,$body)
	{						      
	        $this->load->library('email');
		    $config['mailtype'] = 'html';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;	
			$this->email->initialize($config);
			$this->email->from($from, $sender_name);
			$this->email->to($to);	
			$this->email->subject($sub);
			$this->email->message($body);	
			$res = $this->email->send();			
			return $res;	
	}
}

class MY_PlayerController extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->checkPlayerLogin();
	}
	public function checkPlayerLogin()
	{
		$id = $this->session->userdata('player_id');	 
		if(empty($id)){
			redirect('admin/login');
		}
	}
	function _sendEmail($from,$sender_name,$to,$sub,$body)
	{						      
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;	
		$this->email->initialize($config);
		$this->email->from($from, $sender_name);
		$this->email->to($to);	
		$this->email->subject($sub);
		$this->email->message($body);	
		$res = $this->email->send();			
		return $res;
	}
}


class MY_Controller extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->checkLogin();
	}
	public function checkLogin()
	{
		$id = $this->session->userdata('id');	 
		if(empty($id)){
			redirect('admin/login');
		}
	}
	
 
}