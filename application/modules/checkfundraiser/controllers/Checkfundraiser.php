<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkfundraiser extends CI_controller{

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$id = $this->session->userdata('fundraiser_id'); 
		if(!empty($id)){				 
		redirect($this->uri->segment(1).'/admin/home');
		}
	}
	   
    public function index($fslug='')
	{
		if($fslug!='')
		{
		redirect(base_urk('admin/login'));	
		}
		else
		{
		redirect(base_url('admin/login'));	
		}
	}
	
	public function login($fslug=''){
		redirect(base_url('admin/login'));
		$a=$this->session->userdata('err_msg');
		if(isset($a)){
		   $data['err_msg']=$a;
		   $this->session->unset_userdata('err_msg');
	    }else{
		   $data['err_msg']=0;
		}
		$data['fslug'] = $fslug;
		$this->load->view('../../includes_fundraiser/header');
		$this->load->view('view_login',$data);
		$this->load->view('../../includes_fundraiser/footer');
	}
	
	public function fundraiserlogin($fslug)
	{
		$this->load->model('fundraiser/fundraiser_model');
		$encrypt_key = $this->config->item('encryption_key');      
		$username = $this->input->post('username');

		$where=array('fund_email' => $username,'fund_slug_name' => $fslug);
		$query=$this->fundraiser_model->encry_login_check('tbl_fundraiser',$where);

		if(count($query)>0)
		{
			$dpass = $query[0]['fund_pass'];  
			if($this->input->post('password')==$this->encrypt->decode($dpass,$encrypt_key))
			{
				$urlname = $query[0]['fund_slug_name'];
				$this->session->set_userdata('fundraiser_id', $query[0]['id']);
				$this->session->set_userdata('fundraiser_fullname', $username);
				$this->session->set_userdata('fundraiser_urlname', $urlname);
				$this->session->set_userdata('fundraiser_username', $query[0]['fund_email']);
				$this->session->set_userdata('fundraiser_password', $query[0]['fund_pass']);
				$this->session->set_userdata('fundraiser_contact_email', $query[0]['fund_email']);
				//echo $this->session->userdata('id'); 
				redirect($urlname."/admin/home");
			}
			else
			{
				$this->session->set_userdata('err_msg', 1);	
				redirect(base_url("admin/login"));
			}
		}
		else
		{
			$this->session->set_userdata('err_msg', 1);	
			redirect(base_url("admin/login"));
		}
	}
}