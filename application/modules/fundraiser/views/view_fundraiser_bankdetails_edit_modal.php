<form name="frm_popup_bank" id="frm_popup_bank" method="post" enctype="multipart/form-data" action=""> 
<input type="hidden" name="hid_fid" id="hid_fid" value="<?php echo $fundraiserInfo[0]['id'];?>">
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left">
                	<h2>Edit Fundraiser Bank Detail</h2>                	
                </div>
                <div class="ms" style="display:none;"></div>
			</div>
			<div class="add_plr_wrp">
				<div class="row">
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="frm">
							<div class="form-group">
								<label for="bank-name">Bank Name</label>
								<input type="text" class="form-control required" id="fund_bank_name" name="fund_bank_name" value="<?php echo (isset($fundraiserInfo[0]['fund_bank_name']) && $fundraiserInfo[0]['fund_bank_name']!="")?$fundraiserInfo[0]['fund_bank_name']:'';?>">
							</div>
							<div class="form-group">
								<label for="routing-number">Bank Routing Number</label>
								<input type="text" class="form-control required" id="fund_routing_number" name="fund_routing_number" value="<?php echo (isset($fundraiserInfo[0]['fund_routing_number']) && $fundraiserInfo[0]['fund_routing_number']!="")?$fundraiserInfo[0]['fund_routing_number']:'';?>">
							</div>
							<div class="form-group">
								<label for="ac-number">Bank Account Number</label>
								<input type="text" class="form-control required" id="fund_account_number" name="fund_account_number"  value="<?php echo (isset( $fundraiserInfo[0]['fund_account_number']) && $fundraiserInfo[0]['fund_account_number']!="")?$fundraiserInfo[0]['fund_account_number']:'';?>">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
                    <input type="submit" name="btn_save" id="btn_save" class="btn_round" value="Save" />
					<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
			</div>
      </div>
    </div>
  </div>
 </form>
<script type="text/javascript">
$(document).ready(function ($) {
$("#frm_popup_bank").validate({
        submitHandler: function(form) {    
        $.ajax({
		url:"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/fundraiserBankDetailsEditAjax');?>", 
        type:'POST',
        data: $('#frm_popup_bank').serializeArray(),    
        dataType:"json",  
        beforeSend: function(){
           //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
        },
        success:function(results){
            if(results.msg==1){
            $('.ms').html('Successfully updated!!');
			$('.ms').show();
            setTimeout(function() {                
                location.reload();
                }, 3000);
            }
            else{
                $('.ms').html('Updation is failured');
            }            
        }
       });
      }
    }); 	
});
</script>