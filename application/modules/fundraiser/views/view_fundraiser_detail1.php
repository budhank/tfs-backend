<script src="<?php echo base_url('assets/fundraiser/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/additional-methods.js')?>"></script>
<link href="<?php echo base_url('assets/css/jquery.colorpicker.bygiro.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/jquery.colorpicker.bygiro.js'); ?>"></script>
<style>
.youtube:before {
	content: "\f04b";
	font-family: "FontAwesome";
	position: absolute;
	width: 30px;
	height: 30px;
	z-index: 10;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	margin: auto;
	color: #F00;
	font-size: 30px;
}
.rem, .fa.rem {
	position: absolute;
	right: 0;
	top: 0;
	heidght: 24px;
	width: 24px;
	line-height: 22px;
	background: #fff;
	color: #333;
	text-align: center;
	cursor: pointer;
}
.chng_up_file_wrp input[type="file"]{ display:inline-block !important; opacity:0px;}
.chng_up_file_wrp .chng_up_file {width: 70px;font-size: 12px;}
#b_content .chng_up_file_wrp{ margin-top:0px;}
#b_content .chng_up_file_wrp a{ font-family: 'roboto';}
.col-sm-1 .chng_up_file_wrp { margin-top:0px; overflow:inherit;}
/*.color-col {
	cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    height: 30px;
    width: 30px;
}*/
.color-col {
	cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    line-height: 40px;
    width: 18%;
    text-align: center;
    border-radius: 6px;
    margin-top: 4px;
}
.color-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}

.bg-black{color:#000; border:solid 4px; border-color: rgba(201, 76, 76, 0); /*box-shadow: 1px 2px 2px 0px;*/}
.bg-white{color:#fff; border:solid 4px; border-color: rgba(201, 76, 76, 0); /*box-shadow: 1px 2px 2px 0px;*/}

.color-selector input:active +.color-col{opacity: .9;}
.color-selector input:checked +.color-col{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
			border: solid 2px #12A5F4;
			/*box-shadow: 0px 0px 10px 0px #12A5F4;*/
			
}
.lt-form-sec .file-btn{ max-width:100%;}
.tab-video .tab1 ,.tab-video .tab2{
    border-radius: 5px; margin:5px;
}
.or-div {
    float: left;
    line-height: 30px;
    padding: 10px;
	font-size: 18px;
}
h4{font-size: 17px; margin:5px 0 10px}
textarea.form-control {
    height: 34px;
	 max-height: 34px;
    max-width: 400px;
}
.send-chech{ display:none;}
.lt-form-sec a.btn40 {
    padding: 5px;
    margin-top: 10px;
    line-height: 50px;

}</style> 
<?php  
$desc_fund = '<p>ABOUT US<br /> <br /> Write something about your organization here. Mention what your primary purpose is and how you help the youth and/or community. You can mention your overall organization goals, past achievements, how long you have been around, your outlook for the upcoming season/year, etc. <br /> <br /> PURPOSE OF FUNDRAISER<br /> <br /> In the next paragraph, mention the primary purpose of the fundraiser. Tell them exactly how the fundraiser will help you achieve your upcoming goals. Mention specifically what the funds will be used for whether it be for traveling to competitions, new equipment, new training facilities, uniforms, instruments, etc. <br /> <br /> OUR GIFT TO YOU<br /> <br /> Thank you in advance for taking the time to read about our organization and for supporting our cause by making a donation and/or sharing this opportunity with your personal networks. <br /> <br /> As a special thank you for donating to our cause, you will receive a free one-year subscription to the GeoCouponAlerts<sup>TM</sup> coupon app (a $20 value) that can save you $100&rsquo;s with merchants in the greater Sacramento and Salinas areas.&rdquo;</p>'; 
$fslug = $this->session->userdata('fundraiser_urlname'); 
?>
<div id="page-wrapper" class="nw-admin">
  <div class="container-fluid p-gap2-0">
    <div class="sec-title">Setup Guide</div>
  </div>
  <div class="container-fluid">
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p class="p-gap2-20">Complete the Setup Steps below to get your fundraiser up and running in as little as 15 minutes.</p>
        </div>
      </div>
    </div>
    <div class="tab-section mar_t_15 ">
      <ul class="nav tab-title">
        <li <?php echo ($active_tab=='home')? 'class="active"' : '' ?>> <a href="<?php echo base_url($fslug.'/admin/home');?>"><b>Step 1</b><span>Your Info</span></a> </li>
        <li <?php echo ($active_tab=='organization')? 'class="active"' : '' ?>> <a href="<?php echo base_url($fslug.'/admin/organization');?>"><b>Step 2</b><span>Organization</span></a> </li>
        <li <?php echo ($active_tab=='bankinfo')? 'class="active"' : '' ?>> <a href="<?php echo base_url($fslug.'/admin/bankinfo');?>"><b>Step 3</b><span>Payment Info</span></a> </li>
        <li <?php echo ($active_tab=='fundraiser')? 'class="active"' : '' ?>> <a href="<?php echo base_url($fslug.'/admin/fundraiser');?>"><b>Step 4</b><span>Fundraiser</span></a> </li>
        <li <?php echo ($active_tab=='participants')? 'class="active"' : '' ?>> <a href="<?php echo base_url($fslug.'/admin/participants');?>"><b>Step 5</b><span>Participants</span></a> </li>
        <li <?php echo ($active_tab=='startup')? 'class="active"' : '' ?>> <a href="<?php echo base_url($fslug.'/admin/startup');?>"><b>Step 6</b><span>Start</span></a> </li>
      </ul>
      <div class="bg_wht tot_pad">
        <div class="tab-content">
          <div id="step1" class="tab-pane fade <?php echo ($active_tab=='home')? 'in active' : '' ?> ">
            <div class="row">
              <div class="col-sm-8 lt-form-sec">
                <h2>Step 1 - Enter Your Contact Information</h2>
                <div class="lt-form-wrap m-gap2-15">
                  <form action="<?php echo base_url($fslug.'/admin/home');?>" id="frm_contact_info" name="frm_contact_info" method="POST">
                    <div class="form-group">
                      <label>First Name</label>
                      <input type="text" class="form-control required" id="fund_fname" name="fund_fname" value="<?php echo $fundraiserInfo[0]["fund_fname"]; ?>">
                    </div>
                    <div class="form-group">
                      <label>Last Name</label>
                      <input type="text" class="form-control required" id="fund_lname" name="fund_lname" value="<?php echo $fundraiserInfo[0]["fund_lname"]; ?>">
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control required" id="fund_email" name="fund_email" value="<?php echo $fundraiserInfo[0]["fund_email"]; ?>">
                    </div>
                    <div class="form-group">
                      <label>Phone Number</label>
                      <input type="text" class="form-control required" id="fund_contact" name="fund_contact" value="<?php echo $fundraiserInfo[0]["fund_contact"]; ?>">
                    </div>
                    <div class="form-group">
                      <label>Responsible Party</label>
                    </div>
                    <div class="form-group">
                      <label class="container">I agree to act as the Coordinator for the Fundraiser. As such, I am 
                        the party responsible for the Fundraiser and the only authorized 
                        person who can communicate with ThanksForSupporting <sub>TM</sub>on 
                        behalf of the Organization. By clicking below, I acknowledge that I 
                        have read and agree to the <a href="javascript:void(0);" onclick="displayPage('<?php echo base64_encode("11230");?>')">Terms &amp; Conditions</a>
                        <input type="checkbox" class="checkbox" id="fund_terms" name="fund_terms" value="y" <?php if($fundraiserInfo[0]["fund_terms"]=='y'){ echo 'checked'; }?>/>
                        <span class="checkmark"></span> </label>
                    </div>
                    <div class="sec-line"></div>
                    <div class="btn-gp text-center m-gap2-25">
                      <input type="submit" name="contact_submit" class="btn33" value="Next">
                    </div>
                  </form>
                </div>
              </div>
              <div  class="col-sm-4">
                <h2 class="text-center">Description</h2>
                <p>Enter your personal contact information in this form. This is the information we will use to contact you about your Fundraiser, if necessary. </p>
                <p> When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
              </div>
            </div>
          </div>
          <div id="step2" class="tab-pane fade <?php echo ($active_tab=='organization')? 'in active' : '' ?> ">
            <div class="row">
              <div class="col-sm-8 lt-form-sec">
                <h2>Step 2 - Organization/Beneficiary Information</h2>
                <div class="lt-form-wrap m-gap2-15">
                  <form action="<?php echo base_url($fslug.'/admin/organization');?>" id="frm_orgz_info" name="frm_orgz_info" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label>Organization Name</label>
                      <input type="text" class="form-control required" id="fund_username" name="fund_username" value="<?php echo $fundraiserInfo[0]["fund_username"]; ?>" placeholder="i.e. Rocky Mountain Baseball">
                    </div>
                    <div class="form-group">
                      <label>Short Description (optional)</label>
                      <input type="text" class="form-control" id="fund_slogan" name="fund_slogan" value="<?php echo $fundraiserInfo[0]["fund_slogan"]; ?>" placeholder="i.e. 2017 SIC State Champions">
                    </div>
					<div class="form-group">
                      <label>Beneficiary Tax ID (optional – helpful if donations are tax deductible)</label>
                      <input type="text" class="form-control" id="fund_beneficiary_tax_id" name="fund_beneficiary_tax_id" value="<?php echo $fundraiserInfo[0]["fund_beneficiary_tax_id"]; ?>" placeholder="Beneficiary Tax ID">
                    </div>
                    <div class="form-group">
                      <label>City</label>
                      <input type="text" class="form-control required" id="fund_city" name="fund_city" value="<?php echo $fundraiserInfo[0]["fund_city"]; ?>">
                    </div>
                    <div class="form-group">
                      <label>State</label>
                      <select id="fund_state" name="fund_state" class="form-control required">
                        <option value="">Select</option>
                        <?php                                            
						foreach($stateList as $sval) 
						{
						?>
                        <option value="<?php echo $sval['state_initial'];?>" <?php if($fundraiserInfo[0]['fund_state']==$sval['state_initial']){ echo 'selected="selected"';} ?> ><?php echo $sval['name'];?></option>
                        <?php
						}  
						?>
                      </select>
                    </div>
                    <div class="form-group file-btn ">
                      <div class="m-gap2-10 logo-gap"><b>Logo</b> (Accepted Formats: jpg, jpeg, png, gif)</div>
                      <label for="fund_image" class="file-upload text-center btn34"> Choose image from your computer</label>
                      <input type="file" id="fund_image" name="fund_image" class="do-not-ignore"/>
                      <div class="text-center">(Ideal Size: at least 100 x 100 pixels)</div>
                    </div>
					<?php
					if($fundraiserInfo[0]['fund_image']==""){
					$proimg =  base_url("assets/images/noimage-150x150.jpg");   
					}else{
					$protimg = explode(".",$fundraiserInfo[0]['fund_image']);   
					$proimg =  base_url("assets/fundraiser_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]);   
					} 
					?>
					<div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px;overflow:hidden;height:150px;"> 
					<img src="<?php echo $proimg; ?>" id="fund_logo"/> 
					</div>
                    <div class="form-group">
                      <label>Select Primary Color</label>
                      <div class="span6">
                        <div class="input-group myColorPicker"> <span class="input-group-addon myColorPicker-preview" style="background-color: <?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>;">&nbsp;</span>
                          <input type="text" class="form-control" name="selected_color" id="myselected_color" value="<?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>">
                        </div>
                      </div>
                      <div class="span6"></div>
                    </div>
					<div class="color-selector">
						<div style="margin-bottom: 15px;"><b>Select Text Color</b> (that goes over Primary Color)</div>
						<input id="black_color" type="radio" name="fund_font_color" value="#000000" <?php echo ($fundraiserInfo[0]['fund_font_color']=='#000000')?'checked':''?>/>
						<label class="color-col mybg bg-black" for="black_color" style="background-color: <?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>;">Black Text</label><br>
						<input id="white_color" type="radio" name="fund_font_color" value="#FFFFFF" <?php echo ($fundraiserInfo[0]['fund_font_color']=='#FFFFFF')?'checked':''?>/>
						<label class="color-col mybg bg-white"for="white_color" style="background-color: <?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>;">White Text</label>
					</div>
                    <div class="sec-line"></div>
                    <div class="btn-gp text-center m-gap2-25"> <a href="<?php echo base_url($fslug.'/admin/home');?>" class="btn35" >Back</a>
                      <input type="submit" name="orgz_submit" class="btn33" value="Next">
                    </div>
                  </form>
                </div>
              </div>
              <div  class="col-sm-4">
                <h2 class="text-center">Description</h2>
                <p>Enter the information of the Beneficiary Organization that will receive the Donations from the Fundraiser. </p>
                <p> Be sure to upload a current Logo and choose the color that best represents your Organization. The color you choose will appear on the system-generated invitation emails and web pages.</p>
                <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
              </div>
            </div>
          </div>
          <div id="step3" class="tab-pane fade <?php echo ($active_tab=='bankinfo')? 'in active' : '' ?> ">
            <div class="row">
              <div class="col-sm-8 lt-form-sec">
                <h2>Step 3 - Payment Information</h2>
                <div class="lt-form-wrap m-gap2-15">
                  <form action="<?php echo base_url($fslug.'/admin/bankinfo');?>" id="frm_bank_info" name="frm_bank_info" method="POST" enctype="multipart/form-data">
					<input type="hidden" class="form-control" id="fund_pay_type" name="fund_pay_type" value="<?php echo $fundraiserInfo[0]['fund_pay_type'];?>">
                      <div class="form-group">
                          <label>Description to Appear on Donor Credit Card Statement</label>
                          <input type="text" class="form-control required" id="fund_donor_statement" name="fund_donor_statement" value="<?php echo (isset($fundraiserInfo[0]['fund_donor_statement']) && $fundraiserInfo[0]['fund_donor_statement']!="")?$fundraiserInfo[0]['fund_donor_statement']:$fundraiserInfo[0]["fund_username"];?>">
                        </div>
                      <div class="form-group file-btn tab-video" style="margin-bottom:20px;">
                          <div><b>At the end of the fundraiser,how would you like to receive the funds?</b></div>
                          <div id="paypal" data-id="P" class="file-upload tab1 text-center btn36 pay-type <?php echo ($fundraiserInfo[0]['fund_pay_type']=='P')?'btn-active':'';?>">Deposit to a PayPal account</div><div class="or-div">or</div>
                          <div id="chq" data-id="C" class="file-upload tab2 text-center btn36 pay-type <?php echo ($fundraiserInfo[0]['fund_pay_type']=='C')?'btn-active':'';?>">Mail me a Check</div>
                          <div class="clearfix"></div>
                    </div>
                      <div id="paypal-info" class="p-info-w" <?php echo ($fundraiserInfo[0]['fund_pay_type']=='C')?'style="display:none;"':'';?>>
                        <div class="form-group">
                          <label>PayPal Account</label>
                          <input type="text" class="form-control" name="fund_paypal_acc" value="<?php echo (isset($fundraiserInfo[0]['fund_paypal_acc']) && $fundraiserInfo[0]['fund_paypal_acc']!="")?$fundraiserInfo[0]['fund_paypal_acc']:'';?>">
                        </div>
                      </div>
                      <div id="bank-info" class="b-info-w" <?php echo ($fundraiserInfo[0]['fund_pay_type']=='P')?'style="display:none;"':'';?>>
                      
                      <div>
                      <h4>Where should we mail the check?</h4>
                      <div class="form-group">
                          <label>Make check payable to</label>
                          <input type="text" class="form-control required" id="make_check_payable_to" name="make_check_payable_to" value="<?php echo (isset($fundraiserInfo[0]['make_check_payable_to']) && $fundraiserInfo[0]['make_check_payable_to']!="")?$fundraiserInfo[0]['make_check_payable_to']:'';?>">
                        </div>
                         <div class="form-group">
                          <label>Street Address</label>
                          <textarea class="form-control required" name="street_address" id="street_address"><?php echo (isset($fundraiserInfo[0]['street_address']) && $fundraiserInfo[0]['street_address']!="")?$fundraiserInfo[0]['street_address']:'';?></textarea>
                        </div>
                         <div class="form-group">
                          <label>City</label>
                          <input type="text" class="form-control required" id="city" name="city" value="<?php echo (isset($fundraiserInfo[0]['city']) && $fundraiserInfo[0]['city']!="")?$fundraiserInfo[0]['city']:'';?>">
                        </div>
                        <div class="form-group">
                          <label>State</label>
                          <input type="text" class="form-control required" id="state" name="state" value="<?php echo (isset($fundraiserInfo[0]['state']) && $fundraiserInfo[0]['state']!="")?$fundraiserInfo[0]['state']:'';?>">
                        </div>
                         <div class="form-group">
                          <label>Zip</label>
                          <input type="text" class="form-control required" id="zip" name="zip" value="<?php echo (isset($fundraiserInfo[0]['zip']) && $fundraiserInfo[0]['zip']!="")?$fundraiserInfo[0]['zip']:'';?>" >
                        </div>
                      </div>
                      <!--<div class="send-chech">
                        <div class="form-group">
                          <label>Bank Name</label>
                          <input type="text" class="form-control required" id="fund_bank_name" name="fund_bank_name" value="<?php echo (isset($fundraiserInfo[0]['fund_bank_name']) && $fundraiserInfo[0]['fund_bank_name']!="")?$fundraiserInfo[0]['fund_bank_name']:'';?>">
                        </div>
                        <div class="form-group">
                          <label>Bank Routing Number (9-digit number)</label>
                          <input type="text" class="form-control required" id="fund_routing_number" name="fund_routing_number" value="<?php echo (isset($fundraiserInfo[0]['fund_routing_number']) && $fundraiserInfo[0]['fund_routing_number']!="")?$fundraiserInfo[0]['fund_routing_number']:'';?>">
                        </div>
                        <div class="form-group">
                          <label>Bank Account Number</label>
                          <input type="text" class="form-control required" id="fund_account_number" name="fund_account_number"  value="<?php echo (isset( $fundraiserInfo[0]['fund_account_number']) && $fundraiserInfo[0]['fund_account_number']!="")?$fundraiserInfo[0]['fund_account_number']:'';?>">
                        </div>
                        <div class="form-group"><img src="<?php echo base_url('assets/images/routing-number.jpg')?>" /></div>
                      </div>-->
					  </div>
                    <div class="sec-line m-top-60"></div>
                    <div class="btn-gp text-center m-gap2-25"> <a href="<?php echo base_url($fslug.'/admin/organization');?>" class="btn35" >Back</a>
                      <input type="submit" name="bank_submit" class="btn33" value="Next">
                      <div class="clearfix"></div>
                      <a href="#" class="btn40">Cancel</a>
                    </div>
                  </form>
                </div>
              </div>
              <div  class="col-sm-4">
                <h2 class="text-center">Description</h2>
                <p>Enter the bank information for the Organization where the Donations will be deposited after the Fundraiser ends.</p>
                <p> It is critical that you enter the correct information here.</p>
                <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
              </div>
            </div>
          </div>
          <div id="step4" class="tab-pane fade <?php echo ($active_tab=='fundraiser')? 'in active' : '' ?> ">
            <div class="row">
              <div class="col-sm-8 lt-form-sec">
                <h2>Step 4 - Fundraiser Information</h2>
                <div class="lt-form-wrap m-gap2-15">
                  <form action="<?php echo base_url($fslug.'/admin/fundraiser');?>" id="frm_fundraiser_info" name="frm_fundraiser_info" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="media_type" id="media_type" class="form-control valid" value="I">
                    <div class="form-group">
                      <label>Overall Donations Goal ($)</label>
                      <input id="fund_org_goal" name="fund_org_goal" type="text" class="form-control" value="<?php echo number_format($fundraiserInfo[0]['fund_org_goal']);?>" placeholder="<?php echo number_format("10000");?>">
                    </div>
                    <div class="form-group">
                      <label>Donation Goal Per Participant ($)</label>
                      <input id="fund_individual_goal" name="fund_individual_goal" type="text" class="form-control" value="<?php echo number_format($fundraiserInfo[0]['fund_individual_goal']);?>" placeholder="<?php echo number_format("10000");?>">
                    </div>
                    <div class="form-group">
                      <label>Start Date</label>
                      <div class="file-btn">
                        <div class="input-group-date">
                          <input type="text" class="form-control datepicker" id="fund_start_dt" name="fund_start_dt" readonly="readonly" value="<?php echo date('m-d-Y',strtotime($fundraiserInfo[0]['fund_start_dt']));?>" placeholder="MM-DD-YYYY">
                          <span class="glyphicon glyphicon-calendar"></span> </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>End Date</label>
                      <div class="file-btn">
                        <div class="input-group-date" >
                          <input type="text" class="form-control datepicker" id="fund_end_dt" name="fund_end_dt" readonly="readonly" value="<?php echo date('m-d-Y',strtotime($fundraiserInfo[0]['fund_end_dt']));?>" placeholder="MM-DD-YYYY">
                          <span class="glyphicon glyphicon-calendar"></span> </div>
                      </div>
                    </div>
                    <div class="form-group file-btn tab-video">
                      <div> <b>Upload Image/Video (photo of team, class, etc.)</b></div>
                      <div for="file-upload" class="file-upload tab1 img_vid text-center btn36 btn-active" id="img_tab">Image</div>
                      <div for="file-upload" class="file-upload tab2 img_vid text-center btn36" id="vid_tab">Video</div>
                    </div>
                    <div class="form-group file-btn " id="img_video">
                      <div class="m-gap2-10 text-center">(Accepted Formats: jpg, jpeg, png, gif)</div>
                      <label for="feature_img" class="file-upload text-center btn34"> Choose image from your computer</label>
                      <input id="feature_img" name="feature_img" type="file" onchange="img_vid_add()"/>
                      <div class="text-center">(Ideal Size: at least 1280 x 720 pixels)</div>
                    </div>
                    <div id="img_vid_list">
                      <?php
					$str = '';
					if(count($fundraiserImgInfo)>0)
					{
						foreach($fundraiserImgInfo as $fval)
						{
							if($fval['media_type']=='I'){
								$protimg = explode(".",$fval['fund_feature_image']);   
								$proimg  = base_url()."assets/fundraiser_feature_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
								$str = '<img src="'.$proimg.'"/>';
							}else{
								$str = '<a class="youtube" href="http://youtube.com/watch?v='.$fval['fund_feature_image'].'"><img src="https://img.youtube.com/vi/'.$fval['fund_feature_image'].'/mqdefault.jpg" /></a>';
							}
						?>
                      <div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px; overflow:hidden;height:150px;"> <?php echo $str;?> <a href="javascript:void(0);" onclick="del_media(<?php echo $fval['id'];?>)" class="fa fa-times rem"></a> </div>
                      <?php
						}
					}
					?>
                    </div>					
					<div class="form-group">
						<label>Fundraiser Description</label>
						<?php 
						$settings_qry=$this->db->query("SELECT fund_des_static,fund_des_editable FROM tbl_settings");
						$fund_des_array=$settings_qry->result_array();
						if($fundraiserInfo[0]['fund_des']!="") { 
							if($fundraiserInfo[0]['fund_des_static']!=""){ ?>
								<input type="hidden" value="<?php echo $fundraiserInfo[0]['fund_des_static'];?>" name="fund_des_static">
								<div class="tab-cont"><?php echo $fundraiserInfo[0]['fund_des_static'];?></div>
							<?php } else{ ?> 
								<input type="hidden" value="<?php echo $fund_des_array[0]['fund_des_static'];?>" name="fund_des_static">
								<div class="tab-cont"><?php echo $fund_des_array[0]['fund_des_static'];?></div>
							<?php }?>
							<textarea name="fund_des"  id="fund_des" class="form-control editme" rows="5">
							<?php echo $fundraiserInfo[0]['fund_des'];?>
							</textarea>
						<?php } else {?>
							<input type="hidden" value="<?php echo $fund_des_array[0]['fund_des_static'];?>" name="fund_des_static">
							<div class="tab-cont"><?php echo $fund_des_array[0]['fund_des_static'];?></div>
							<textarea name="fund_des"  id="fund_des" class="form-control editme" rows="5">
							<?php echo $fund_des_array[0]['fund_des_editable'];?>
							</textarea>
						<?php }  ?>
					</div>					
                    <div class="sec-line m-top-60"></div>
					<div class="btn-gp text-center m-gap2-25"> <a href="<?php echo base_url($fslug.'/admin/bankinfo');?>" class="btn35" >Back</a>
					<input type="submit" name="fundraiser_submit" class="btn33" value="Next">
					</div>
                  </form>
                </div>
              </div>
              <div  class="col-sm-4">
                <h2 class="text-center">Description</h2>
                <p>Here is where you enter the details of your Fundraiser. </p>
                <p> The Overall Donations Goals is the combined amount you are hoping to raise overall.</p>
                <p>The Donation Goal Per Participant is the amount you hope each Participant is able to raise.</p>
                <p>You must upload at least one photo or YouTube video. </p>
                <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
              </div>
            </div>
          </div>
          <div id="step5" class="tab-pane fade <?php echo ($active_tab=='participants')? 'in active' : '' ?> ">
            <div class="row">
              <div class="col-sm-8 lt-form-sec">
                <h2>Step 5 - Add Participants
                  <div style=" float:right;">
					<form action="<?php echo base_url($fslug.'/admin/bulk_player_upload');?>" id="frm_upload_info" name="frm_upload_info" method="POST" enctype="multipart/form-data">
					<a href="<?php echo base_url('assets/uploads/contacts.csv');?>" class="file-upload text-center btn33" style="font-size:15px; padding:5px 40px;line-height:18px; text-decoration:none;">
					Download<span style="font-size:11px;" class="btn-block">( file format )</span>
					</a>
					<label for="bulk_player" class="file-upload text-center btn33" style="font-size:15px; padding:5px 40px;line-height:18px;">
					Upload File<span style="font-size:11px;" class="btn-block">( csv, excel )</span>
					</label>
                    <input id="bulk_player" name="bulk_player" type="file" class="do-not-ignore">
                    <input type="submit" id="bulk_submit" name="bulk_submit" value="Next" style="display:none;">
					</form>
                  </div>
                  <div style="clear:both"></div>
                </h2>
                <div class="lt-form-wrap m-gap2-15"> 
                  <div class="participants">
                  <div class="row">
                  <div class="col-sm-1"></div>
                  <div class="col-sm-3">First</div>
                  <div class="col-sm-3 row">Last</div>				  
                  <div class="col-sm-3">Email Address</div>
				  
                  <div class="col-sm-2"></div>
                  </div>
				<div class="row">
				<?php
				if($this->session->userdata('up_msg')){
				echo '<br /><p class="text-success">&nbsp;&nbsp;&nbsp;'.$this->session->userdata('up_msg').'</p>';
				$this->session->unset_userdata('up_msg');
				}
				?>
				<form action="<?php echo base_url($fslug.'/admin/participants');?>" id="frm_participants_info" name="frm_participants_info" method="POST" enctype="multipart/form-data">
				<div class="col-sm-1">
				<div style="margin:auto;">
				<span class="pic"><img id="uploadPreview1" src="<?php echo base_url('assets/images/no_image.png');?>" /></span>
				</div>
				<div class="chng_up_file_wrp">
				<input class="chng_up_file" id="uploadImage1" placeholder="Change" type="file" name="player_image" onchange="PreviewImage(1);" />
				<a href="JavaScript:Void(0);">Upload</a> 
				</div>
				</div>
				<div class="col-sm-3"><input type="text" id="player_fname" name="player_fname" class="form-control" ></div>
				<div class="col-sm-3 row"><input type="text" id="player_lname" name="player_lname" class="form-control" ></div>
				<div class="col-sm-3"><input type="email" id="player_email" name="player_email" class="form-control"></div>
				<div class="col-sm-2"><input type="submit" name="participant_submit" value="Add" class="btn34"></div>
				</form>
				</div>
                  <hr />
                      <div id="b_content">
                        <?php
							if(count($players_list)>0)
							{
							foreach($players_list as $players_li)
							{								
							if($players_li['player_image']==""){
							$proimg =  base_url()."assets/images/noimage-150x150.jpg";   
							}else{
							$protimg = explode(".",$players_li['player_image']);   
							$proimg =  base_url()."assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1];  
							} 
							$org_goal = $players_li['player_goal'];
							$raised_amt = $players_li['raised_amt'];
							$remaining_goal = ($org_goal - $raised_amt);
							?>
                        <div class="row">
                          <div class="col-sm-1"><span class="pic"><img src="<?php echo $proimg;?>"/></span></div>
                          <div class="col-sm-3"><?php echo $players_li["player_fname"] ?></div>
                          <div class="col-sm-3 row"><?php echo $players_li["player_lname"] ?></div>
                          <div class="col-sm-3">
                          <div class="cus-tip">
                            <?php echo $players_li["player_email"] ?>
                            <div class="cus-tip-sec">
                              <div class="tip-row">
                                <div class="tip-lt">Invite</div>
                                <div class="tip-rt">
                                  <?php if($players_li["invitation"]==0){?>
                                  <a href="<?php echo base_url($fslug.'/admin/players/asktoinvite/'.$players_li["player_slug_name"]);?>" class="btn33">Send</a>
                                  <?php }else{ ?>
                                  <span class="invite"></span>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="tip-row">
                                <div class="tip-lt">Reminder</div>
                                <div class="tip-rt">
                                  <?php if($players_li["reminder"]==0){?>
                                  <a href="<?php echo base_url($fslug.'/admin/players/remindtoinvite/'.$players_li["player_slug_name"]);?>" class="btn33">Send</a>
                                  <?php }else{ ?>
                                  <span class="invite"></span>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="tip-row">
                                <div class="tip-lt">2nd Reminder</div>
                                <div class="tip-rt">
                                  <?php if($players_li["sec_reminder"]==0){?>
                                  <a href="<?php echo base_url($fslug.'/admin/players/secremindtoinvite/'.$players_li["player_slug_name"]);?>" class="btn33">Send</a>
                                  <?php }else{ ?>
                                  <span class="invite"></span>
                                  <?php } ?>
                                </div>
                              </div>
                            </div></div>
                            </div>
							
							<div class="col-sm-2 text-center">
							<a href="<?php echo base_url($fslug.'/admin/delete-participant/'.$players_li["player_slug_name"]);?>">
							<span class="remove">X</span>
							</a>
							</div>
                        </div>
                        <hr />
                        <?php
							}
							}
						?>
                      </div>
                      <div id="f_content">
                        <?php
						if(count($players_list) >= 10)
						{
						?>
                       <div class="row">
                          <div class="text-center"><a class="grn2" id="ldmore" href="javascript:void(0);" onclick="loadmore();">Load more</a></div>
                         </div>
                        <?php
						}
						?>
                      </div>
                  </div>
                  <div class="sec-line m-top-60"></div>
                  <div class="btn-gp text-center m-gap2-25"> <a href="<?php echo base_url($fslug.'/admin/fundraiser');?>" class="btn35" >Back</a> <a href="<?php echo base_url($fslug.'/admin/startup');?>" class="btn33" >Next</a> </div>
                  </form>
                </div>
              </div>
              <div  class="col-sm-4">
                <h2 class="text-center">Description</h2>
                <p>Enter the names and email addresses of those who will be the Participants helping to promote the Fundraiser. </p>
                <p>Remember that children under the age of 13 are NOT permitted to use the Platform or participate in the Fundraiser. If they are part of your school, team, club or organization, invite their parent or gaurdian instead.</p>
                <p>You may uplaod a csv or spreadsheet file if you have one to save time.</p>
                <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
              </div>
            </div>
          </div>
          <div id="step6" class="tab-pane fade <?php echo ($active_tab=='startup')? 'in active' : '' ?> ">
          <div class="row">
             <div class="col-sm-8 lt-form-sec">
              <h2>Step 6 - Invite Participants</h2>
              <div class="lt-form-wrap m-gap2-15">
                <?php
				if($this->session->userdata('succ_msg')){
				?>
				  <div class="congratulations-icn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></div>
                  <div align="center"><h3>Congratulations</h3>The participants have been invited</div>
				<?php
				$this->session->unset_userdata('succ_msg');
				}else{
				?>
				  <div align="center"><img src="<?php echo base_url('assets/images/setup-complete.jpg'); ?>" /></div>
                  <div class="form-group file-btn" style="margin:auto;">
                    <div data-toggle="modal" data-target="#myParticipant" class="text-center btn34 m-gap2-25"> Start Fundraiser</div>
                  </div>
				<?php
				}
				?>
              </div>
            </div>
            <div  class="col-sm-4">
              <h2 class="text-center">Description</h2>
              <p>Click on the Start Fundraiser button to have the system send out emails to each of the Participants you entered.</p>
              <p>The email will include instructions on how they can invited their contacts to donate to your cause.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-8 lt-form-sec">
              <div class="sec-line m-gap2-15"></div>
              <div class="btn-gp text-center m-gap2-25"> <a href="<?php echo base_url($fslug.'/admin/participants');?>" class="btn35" >Back</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php
if($active_tab=='home'){
?>
<div id="terms" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content terms">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function ($){
//VALIDATE IMAGE DIMENSION
$.validator.addMethod("imagedimension", function(value, element, params)
{
	var val_height = params[0];
	var val_width = params[1];
	
    var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0]);
		formData.append('valid_height', val_height);
		formData.append('valid_width', val_width); 
		
    var eReport = ''; //error report
	var sts = '';
    $.ajax(
    {
		url:"<?php echo base_url($fslug.'/admin/checkfile');?>",
        type:'POST',
        data: formData,    
        contentType: false,            
        processData:false,     
        async:false,
        dataType:"json",
        success: function(res){
            if (res.valid !== 'true'){
				sts = false;
            }else{
               sts = true;
            }
        },
        error: function(xhr, textStatus, errorThrown){
            sts = false;
        }
    });
	return sts;

}, "Image Dimension must be greater than {0}x{1}.");

	$("#frm_contact_info").validate({
	    rules: {
	    fund_email: {
			required  : true,
			email: true,
			remote: {
				url:"<?php echo base_url($fslug.'/admin/fundraiserEmailCheckerEditAjax');?>",
				type: "post",
				data: {
					fund_email: function() {
						return $("#fund_email" ).val();
					}
				}
			}
		},
		fund_terms: {
			required  : true
		}
		},
		messages:
		{
			fund_email:
			{
			remote:"The email id is already in use!",
			},
			fund_terms: {
			required:"Please accept Terms & Conditions",
			}
		}
	});
});

function displayPage(page_id){
	var url = "<?php echo base_url('wppages/get_page');?>";
	$.ajax({
		type: 'POST',
		url: url,
		data: {page_id: page_id},
		success: function (msg) {
			$(".modal-body").html(msg);
			$("#terms").modal('show');
		}
	});
}
</script>
<?php	
}
?>
<?php
if($active_tab=='organization'){
$color_rgba = str_replace(['rgb(', 'rgba(', ')'], '',$fundraiserInfo[0]['fund_color_rgb']);
$color_dt = explode(",",$color_rgba,4);
$col_r = $color_dt[0];
$col_g = $color_dt[1];
$col_b = $color_dt[2];
$col_a = $color_dt[3];
?>
<script type="text/javascript">
$(document).ready(function ($) {
$.validator.setDefaults({ ignore: ':hidden:not(.do-not-ignore)' });
$("#frm_orgz_info").validate({
	rules: {
		fund_image:{
		extension: "png|jpe?g|gif"
		}
	},
	messages: {
		fund_image:{
		extension: "Please upload valid file type."
		}
	}
});

$("#fund_image").change(function () {
	var stss = true;
	$('.crr').remove();
	var img = $("#fund_image").val();
	if(img != ''){
		var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		$('#fund_image').after('<label for="fund_image" generated="true" class="crr error">Please upload valid file type.</label>');
		}else{
		stss = true;	
		}
	}else{
		$('#fund_image').after('<label for="fund_image" generated="true" class="crr error">This field is required.</label>');
		stss = false;
	}
	if(stss==true){
		var formData = new FormData();
		formData.append('fund_image', $('#fund_image')[0].files[0]);  	      
		
		$.ajax({
		url:"<?php echo base_url($fslug.'/admin/upload_fund_logo');?>", 
		type:'POST',
		data: formData,    
		contentType: false,            
		processData:false,     
		dataType:"json",
		beforeSend: function(){
		   // $('#div_loading').html('<div class="loading_inn">loading...</div>');
		},
		success:function(results){
			if(results.valid==1){
				$("#fund_logo").attr('src',results.src);
			}else{
				$('#fund_image').after('<label for="fund_image" generated="true" class="crr error">'+results.msg+'</label>');
			}
		}
	   });	
	}
});

$('.myColorPicker').colorPickerByGiro({
	preview: '.myColorPicker-preview',
	format: 'rgba'
});
$('#col_r').val('<?php echo $col_r; ?>');
$('#col_g').val('<?php echo $col_g; ?>');
$('#col_b').val('<?php echo $col_b; ?>');
$('#col_a').val('<?php echo $col_a; ?>');
$('.cp-hex input').val('<?php echo $fundraiserInfo[0]['fund_color_hex']; ?>');
$('.cp-hex input').trigger('keyup');
$('.cp-trigger').click(function(){
	$('.mybg').css("background-color", $('#myselected_color').val());
});
});
</script>
<?php	
}
?>
<?php
if($active_tab=='bankinfo'){
?>
<script type="text/javascript">
$(document).ready(function ($) {
	$("#frm_bank_info").validate({
		rules: {
			fund_routing_number:{
			required:function(element) { return ($('#fund_pay_type').val()!='' && $('#fund_pay_type').val()=='C')?true:false; },
			routingnumber:true
			},
			fund_paypal_acc:{
			required:function(element) { return ($('#fund_pay_type').val()!='' && $('#fund_pay_type').val()=='P')?true:false; }
			},
			fund_donor_statement:{
			required: true,
			maxlength: 22
			}
		},
		messages:{
			fund_donor_statement:{
			maxlength: 'Enter not more than 22 characters.'
			}
		}
	});
  $("#paypal").removeClass("btn-active");  
  $("#paypal-info").hide();
 $('.pay-type').click(function(){	 
		if($(this).attr('data-id')=='C'){
			$("#paypal").removeClass("btn-active");
			$("#fund_pay_type").val("C");
			$(this).addClass("btn-active");
			$("#paypal-info").hide();
			$("#bank-info").show();
		}else{
			$("#chq").removeClass("btn-active");
			$("#fund_pay_type").val("P");
			$(this).addClass("btn-active");
			$("#paypal-info").show();
			$("#bank-info").hide();
		}
	});
	
});
</script>
<?php	
}
?>
<?php
if($active_tab=='fundraiser'){
?>
<form name="hid_media_fm" id="hid_media_fm" action="" method="post">
  <input type="hidden" id="hid_fid" name="hid_fid" value="" />
  <input type="hidden" name="fslug" value="<?php echo $fslug;?>" />
</form>
<script type="text/javascript">
$(function() {
	$("#fund_start_dt").datepicker({ 
		dateFormat: 'mm-dd-yy', 
		onSelect: function(selected) 
		{
			var sdate = $(this).datepicker('getDate');
			if(sdate){sdate.setDate(sdate.getDate() + 1);}
			$("#fund_end_dt").datepicker("option","minDate", sdate)
		}     
	});
		
	$("#fund_end_dt").datepicker({ 
		dateFormat: 'mm-dd-yy', 
	});
	
	var validator = $("#frm_fundraiser_info").submit(function() {
		tinyMCE.triggerSave();// update underlying textarea before submit validation
	}).validate({
		ignore: "",
		rules: {
			fund_des: "required",
			fund_org_goal:{
			required:true,
			currency: ["$", false]
			},
			fund_individual_goal:{
			required:true,
			currency: ["$", false]
			},
			fund_start_dt:{
			required:true
			},
			fund_end_dt:{
			required:true
			}
		},
		messages:{
			fund_org_goal:{
			currency:"Please enter valid amount.",
			},
			fund_individual_goal:{
			currency:"Please enter valid amount.",
			}
		},
		errorPlacement: function(error, element) {
			// position error label after generated textarea
			if (element.is("textarea")) {
				error.insertAfter(element);
			} else {
				error.insertAfter(element)
			}
		}
	});
	validator.focusInvalid = function() {
		// put focus on tinymce on submit validation
		if (this.settings.focusInvalid) {
			try {
				var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
				if (toFocus.is("textarea")) {
					tinyMCE.get(toFocus.attr("id")).focus();
				} else {
					toFocus.filter(":visible").focus();
				}
			} catch (e) {
				// ignore IE throwing errors when focusing hidden elements
			}
		}
	}

});

	function img_vid_add(){
		var stss = true;
		var mtype = $('#media_type').val();
		if(mtype == 'V'){
			$('.crr').remove();
			var vid = $("#youtube_fil").val();
			if(vid != ''){
				stss = true;
			}else{
				$('#youtube_fil').after('<label for="youtube_fil" generated="true" class="crr error">This field is required.</label>');
				stss = false;
			}
		}else{
			$('.crr').remove();
			var img = $("#feature_img").val();
			if(img != ''){
				var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
				if ($.inArray($('#feature_img').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$('#feature_img').after('<label for="feature_img" generated="true" class="crr error">Please upload valid file type.</label>');
				}else{
				/*-----------------*/
				var val_height = '720';
				var val_width = '1280';
				
				var formData = new FormData();
				formData.append('file', $('#feature_img')[0].files[0]);
				formData.append('valid_height', val_height);
				formData.append('valid_width', val_width); 
				
				var eReport = ''; //error report
				var sts = '';
				$.ajax(
				{
					url:"<?php echo base_url($fslug.'/admin/checkfile');?>",
					type:'POST',
					data: formData,    
					contentType: false,            
					processData:false,     
					async:false,
					dataType:"json",
					success: function(res){
						if (res.valid !== 'true'){
							$('#feature_img').after('<label for="feature_img" generated="true" class="crr error">Image Dimension must be greater than '+val_width+'x'+val_height+'.</label>');
							sts = false;
						}else{
						   sts = true;
						}
					},
					error: function(xhr, textStatus, errorThrown){
						$('#feature_img').after('<label for="feature_img" generated="true" class="crr error">Image Dimension must be greater than '+val_width+'x'+val_height+'.</label>');
						sts = false;
					}
				});
				stss = sts;
				/*-----------------*/	
				}
			}else{
				stss = false;
			}
		}
		if(stss==true){
			var mtype = $('#media_type').val();
			var formData = new FormData();
			
			if(mtype=='I'){
			formData.append('feature_img', $('#feature_img')[0].files[0]);  	
			}else{
			formData.append('youtube_fil', $('#youtube_fil').val());	
			}
			formData.append('media_type', $('#media_type').val());       
			
			$.ajax({
			url:"<?php echo base_url($fslug.'/admin/upload_media');?>", 
			type:'POST',
			data: formData,    
			contentType: false,            
			processData:false,     
			dataType:"json",
			beforeSend: function(){
			   // $('#div_loading').html('<div class="loading_inn">loading...</div>');
			},
			success:function(results){
				if(results.valid==1){
					$("#img_vid_list").prepend(results.html);
					if(results.mtype=='I'){
						$('#feature_img').val('');  	
					}else{
						$('#youtube_fil').val('');	
					}
				}       
			}
		   });	
		}
	}
</script> 
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.min.js'); ?>"></script> 
<script>
tinymce.init({ 
	selector:'textarea.editme' ,
	height: 250,
	menubar: false,
	plugins: [
	'advlist autolink lists charmap preview anchor',
	'searchreplace visualblocks code fullscreen',
	'insertdatetime media contextmenu paste code'
	],
	toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect fontsizeselect',
	content_css: [
	'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
	'//www.tinymce.com/css/codepen.min.css'
	],
	onchange_callback: function(editor) {
		tinyMCE.triggerSave();
		$("#" + editor.id).valid();
	},
	encoding: "xml"
});
</script> 
<script type="text/javascript">
function del_media(v)
{
	var r = confirm("Are you sure to delete this image/video?");
	if (r == true) 
	{
		$('#hid_media_fm').attr('action', '<?php echo base_url($fslug.'/admin/delMedia');?>');
		$('#hid_fid').val(v);
		$('#hid_media_fm').submit();
	}
}
</script>
<?php	
}
?>
<?php
if($active_tab=='participants'){
?>
<div id="emailAllMailModal" class="modal fade" role="dialog"></div>
<script type="text/javascript">
$(document).ready(function ($) {
	$.validator.setDefaults({ ignore: ':hidden:not(.do-not-ignore)' });
	$("#frm_participants_info").validate({
		rules: {
			player_fname:{
				required : true
			},
			player_lname:{
				required : true
			},
			player_email:{
				required : true,
				email : true
			},
			player_image:{
				extension: "png|jpg|jpeg|gif"
			}
		},
		messages: {
			player_image:{
				extension: "Please upload valid file type."
			}
		}
	});
	$("#bulk_player").change(function () {
		$('#frm_upload_info').validate({
			rules: {
				bulk_player:{
				extension: "csv|xls|csvx|xlsx"
				}
			},
			messages:{
				bulk_player:{
				extension: "Please upload valid file type."
				}
			}
		});
		$('#bulk_submit').trigger('click');
	});
	
	$("#emailall").click(function (e) {
		$.ajax({
		type      :"POST",
		url       :"<?php echo base_url($fslug.'/admin/getEmailToAllAjax'); ?>",
		data      :"fslug=<?php echo $fslug; ?>",
		dataType  :"html",
		success: function(response){        
			$("#emailAllMailModal").html(response);
			$("#emailAllMailModal").modal('show');               
		}
		});	
	});
});
</script>
<?php	
}
?>
<?php
if($active_tab=='startup'){
?>
<div id="myParticipant" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">AGE ACKNOWLEDGEMENT</h4>
      </div>
      <div class="modal-body">
        <div class="ms" style="display:none;"></div>
        <p>Children under the age of 13 are NOT permitted to use the Platform or participate in the Fundraiser under any circumstances. By clicking below, I 
          acknowledge and agree that I have not invited anyone under the age of 13 to participate in this Fundraiser. </p>
        <p><b>NOTE: </b>If there is a member of your Organization who is under 13 years of age, please invite their parents or legal guardians who are 18 years of age or older to participate on their behalf.</p>
      </div>
      <div class="modal-footer"> <a href="<?php echo base_url($fslug.'/admin/invitationtoall');?>" id="agree_send" class="btn34">Agree & Send Invitations</a>
        <div class="clearfix"></div>
        <div class="goback-btn " data-dismiss="modal">(go back)</div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function ($) {
	/*$('#agree_send').click(function () {
		$.ajax({
			url:  "<?php echo base_url($fslug.'/admin/invitationtoall');?>",            
			type: 'POST',
			data: 'fslug="<?php echo $fslug; ?>"',
			dataType:"text",
			beforeSend: function(){
			   $('.ms').html('sending...');
			},
			success:function(results){
				$('.ms').html(results);
				$('.ms').show();
				setTimeout(function() { 
				location.reload(); 
				}, 2000);
			}
	   });
	});*/
});
</script>
<?php	
}
?>
