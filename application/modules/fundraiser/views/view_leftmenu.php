<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li class="active"><a href="#">Dashboard</a></li>
        <li><a href="fundraiser-list.html">Fundraisers</a></li>
        <li><a href="email-template.html">Email Templates</a></li>
        <li><a href="manage-payment.html">Manage Payments</a></li>
        <li><a href="settings.html">Settings</a></li>
        <li><a href="index.html">Log Out</a></li>
    </ul>
</div>
