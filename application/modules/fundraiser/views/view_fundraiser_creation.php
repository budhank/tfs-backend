<div id="page-wrapper">
    <div class="full_top_wrp bg_wht">
        <ul class="breadcrumb">
            <li>All fundraisers</li>
            <li class="active">Add New Fundraiser</li>
        </ul>
    </div>
    <div class="container-fluid">
        <div class="bg_wht mar_t_15 tot_pad">
            <div class="row">
                <div class="col-xs-12">
                    <form name="form_fundraiser_creation" id="form_fundraiser_creation" action="<?php echo base_url()?>admin/create_fundraiser" method="post">
                        <div class="add_plr_wrp">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 style="border-bottom:1px solid #ccc;padding-bottom:15px;">Add New Fundraiser</h4>
                                </div>
                                <div class="col-md-12">
                                    <h4>Fundraiser Contact</h4>
                                </div>
                                <div class="col-sm-7 col-xs-12">
                                    <div class="frm">
                                        <div class="form-group">
                                            <label for="email">Email address</label> <span>[Login User Id]</span>
                                            <input type="email" class="form-control required" id="email" name="fund_email">
                                        </div>
                                        <div class="form-group">
                                            <label for="firstname">Password</label>
                                            <input type="text" class="form-control required" id="password" name="fund_pass">
                                        </div>
                                        <div class="form-group">
                                            <label for="firstname">First Name</label>
                                            <input type="text" class="form-control required" id="firstname" name="fund_fname">
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Last Name</label>
                                            <input type="text" class="form-control required" id="lastname" name="fund_lname">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Phone</label>
                                            <input type="text" class="form-control required" id="phone" name="fund_contact">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5 col-xs-12 text-center">
                                    <label>Picture</label>
                                    <div class="upld_img" style="margin:0 auto;">
                                        <img id="uploadPreview1" src="<?php echo base_url()?>assets/admin/images/pro_pic1.jpg" />
                                    </div>
                                    <div class="chng_up_file_wrp text-center">
                                        <input class="chng_up_file" id="uploadImage1" placeholder="Change" type="file" name="fund_image" onchange="PreviewImage(1);" />
                                        <a href="JavaScript:Void(0);">Change or remove</a>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Fundraiser Goals</h4>
                                </div>
                                <div class="col-sm-7 col-xs-12">
                                    <div class="frm">
                                        <div class="form-group ico_fld">
                                            <label for="org-goal">Org Goal</label>
                                            <input id="org-goal" type="text" class="form-control" name="fund_org_goal">
                                        </div>
                                    </div>
                                    <div class="frm">
                                        <div class="form-group ico_fld">
                                            <label for="indv-goal">Individual Goal</label>
                                            <input id="indv-goal" type="text" class="form-control" name="fund_individual_goal">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Fundraiser Dates</h4>
                                </div>
                                <div class="col-sm-7 col-xs-12">
                                    <div class="frm">
                                        <div class="form-group ico_fld">
                                            <label for="strt-dt">Start Date</label>
                                            <i class="fa fa-calendar-o"></i>
                                            <input id="strt-dt" type="date" readonly placeholder="Starting Date" class="form-control datepicker required" name="fund_start_dt">
                                        </div>
                                    </div>
                                    <div class="frm">
                                        <div class="form-group ico_fld">
                                            <label for="end-dt">End Date</label>
                                            <i class="fa fa-calendar-o"></i>
                                            <input id="end-dt" type="date" class="form-control datepicker required" name="fund_end_dt">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-7 col-xs-12">
                                    <div class="frm">
                                        <div class="form-group">
                                            <label for="featr-image">Fundraiser Feature Image</label>
                                            <input id="featr-image" type="file" name="fund_feature_image">
                                        </div>
                                    </div>
                                    <div class="frm">
                                        <div class="form-group">
                                            <label for="fundr-desc">Fundraiser Description</label>
                                            <textarea id="fundr-desc" class="form-control" name="fund_des"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Bank Account Detail</h4>
                                </div>
                                <div class="col-sm-7 col-xs-12">
                                    <div class="frm">
                                        <div class="form-group ico_fld">
                                            <label for="bank-name">Bank Name</label>
                                            <input id="bank-name" type="text" class="form-control" name="fund_bank_name">
                                        </div>
                                    </div>
                                    <div class="frm">
                                        <div class="form-group ico_fld">
                                            <label for="bank-number">Bank Routing Number</label>
                                            <input id="bank-number" type="text" class="form-control" name="fund_routing_number">
                                        </div>
                                    </div>
                                    <div class="frm">
                                        <div class="form-group ico_fld">
                                            <label for="ac-number">Bank Account Number</label>
                                            <input id="ac-number" type="text" class="form-control" name="fund_account_number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="frm_btn_grp">
                                <a href="fundraiser-list.html" class="btn_round">Add</a>
                                <a href="fundraiser-list.html" class="undr_lin">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
