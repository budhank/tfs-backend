<form action="<?php echo base_url($fslug.'/admin/email_all_participants');?>" name="frm_popup_mail_des" id="frm_popup_mail_des" method="post" enctype="multipart/form-data">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tit1_sec clearfix">
          <div class="left">
            <h2>Send Email To All Participants</h2>            
          </div>
        </div>
        <div class="add_plr_wrp">
            <div class="frm">
				<div class="form-group">
                  <label for="mail_subject">Email Subject</label>                  
                  <input type="text" name="mail_subject"  id="mail_subject" class="form-control required"/>
                </div> 
                <div class="form-group">
                  <label for="mail_content">Email Content</label>                  
                  <textarea name="mail_content"  id="mail_content" class="form-control editme required" rows="5">
				  </textarea>
                </div>                
            </div>
			<hr>
			<div class="frm_btn_grp"> 
				<input type="submit" name="btn_save" id="btn_save" class="btn_round" value="Send" />
				<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a> 
			</div>
        </div>
      </div>
    </div>
  </div>
</form>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.min.js'); ?>"></script>
<script>
tinymce.init({ 
	selector:'textarea.editme',
	height: 250,
	menubar: false,
	plugins: [
	'advlist autolink lists charmap preview anchor',
	'searchreplace visualblocks code fullscreen',
	'insertdatetime media contextmenu paste code'
	],
	toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect fontsizeselect',
	content_css: [
	'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
	'//www.tinymce.com/css/codepen.min.css'
	],
	onchange_callback: function(editor) {
		tinyMCE.triggerSave();
		$("#" + editor.id).valid();
	},
	encoding: "xml"
});
</script>
<script type="text/javascript">
$(function() {
	var validator = $("#frm_popup_mail_des").submit(function() {
	// update underlying textarea before submit validation
	tinyMCE.triggerSave();
	}).validate({
		ignore: "",
		rules: {
			mail_content: "required"
		},
		errorPlacement: function(error, element) {
			// position error label after generated textarea
			if (element.is("textarea")) {
				error.insertAfter(element);
			} else {
				error.insertAfter(element)
			}
		}
	});
	validator.focusInvalid = function() {
		// put focus on tinymce on submit validation
		if (this.settings.focusInvalid) {
			try {
				var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
				if (toFocus.is("textarea")) {
					tinyMCE.get(toFocus.attr("id")).focus();
				} else {
					toFocus.filter(":visible").focus();
				}
			} catch (e) {
				// ignore IE throwing errors when focusing hidden elements
			}
		}
	}
});
</script> 