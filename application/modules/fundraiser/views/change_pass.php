<div class="main-body container">
   
    		<div class="error_msg">
					<?php
							$change_pass_msg = $this->session->userdata('change_pass_msg');	
							if(!empty($change_pass_msg) && $change_pass_msg == 1){
								echo 'Old password is not matching.';
								$this->session->unset_userdata('change_pass_msg');
							}		
							if(!empty($change_pass_msg) && $change_pass_msg == 2){
								echo 'Password changed successfully.';
								$this->session->unset_userdata('change_pass_msg');
							}
					?>
  			</div>
  
            <div class="page-heading">
              <h1>Change Password</h1>
              <a href="<?php echo base_url()?>admin/home" class="btn-right org-btn btn-back">Back</a> 
            </div>
            <?php 
			$attributes = array('class' => 'form-signin');
			  echo form_open('admin/changepassword',$attributes); 
	       ?>
            <input name="flag" type="hidden" value="1" />
            <div class="form-title">
             <div class="form-title">&nbsp;</div>
             <div class="form-fild">
               <input type="password" class="form-control" name="old_pass" placeholder="Old Password" required autofocus>
		       <?php echo form_error('old_pass', '<div class="error">', '</div>'); ?>
             </div> 
            </div>
            
             <div class="form-title">
              <div class="form-title">&nbsp;</div>
              <div class="form-fild">
               <input type="password" class="form-control" name="new_pass" placeholder="New Password" required autofocus>
		       <?php echo form_error('new_pass', '<div class="error">', '</div>'); ?>
              </div> 
            </div>
            
            <div class="form-title">
              <div class="form-title">&nbsp;</div>
              <div class="form-fild">
               <input type="password" class="form-control" name="confirm_pass" placeholder="Confirm New Password" required>
		       <?php echo form_error('confirm_pass', '<div class="error">', '</div>'); ?>
              </div> 
            </div>
            
             <!--<div class="form-wrap">-->
             <div class="fc-list-wrapper bottom-btn">
             
              <a href="<?php echo base_url()?>admin/home" class="btn-right org-btn btn-back">Back</a>
               <input class="add blue-btn" name="signin" style="width: 140px;" value="Change Password" type="submit" />
                
             </div>
           
           <?php
		     echo form_close();
		   ?> 
  
</div>