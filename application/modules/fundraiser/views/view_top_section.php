<div class="top_pro_wrp">
	<div class="clearfix">
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
			<div class="top_pro">
			<?php
			if($player_details[0]['player_image']==""){
			$proimg =  base_url()."assets/images/noimage-150x150.jpg";   
			}else{
			$protimg = explode(".",$player_details[0]['player_image']);   
			$proimg =  base_url()."assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
			} 
			?>
				<div class="top_pro_img">
				<img src="<?php echo $proimg; ?>" alt="">
				</div>
				<div class="top_pro_con">
					<h4><?php echo ucwords($player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]); ?></h4>
					<!--<p><?php //echo ucfirst($player_details[0]["player_title"]); ?></p>-->
				</div>
			</div>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<div class="top_pro_prc">
				<h4><span class="glyphicon glyphicon-usd"></span> <?php echo number_format($player_details[0]["player_goal"]); ?></h4>
				<p>Goal</p>
			</div>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<div class="top_pro_prc">
				<h4 class="grn1"><span class="glyphicon glyphicon-usd"></span> <?php echo number_format($player_details[0]["raised_amt"]); ?></h4>
				<p>Raised</p>
			</div>
		</div>
		<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
			<div class="top_pro_prc">
				<h4 class="txt-red"><span class="glyphicon glyphicon-usd"></span> <?php echo number_format(($player_details[0]["player_goal"]-$player_details[0]["raised_amt"])); ?></h4>
				<p>Remaining</p>
			</div>
		</div>
		<div class="col-lg-3 col-xs-12 text-right">
			<div class="top_pro_donation">
				<a href="javascript:void(0)" class="grn2" onclick="openProfile();">Edit profile</a>
			</div>
		</div>
	</div>
</div>