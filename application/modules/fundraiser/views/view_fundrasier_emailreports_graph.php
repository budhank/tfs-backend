<style type="text/css">
.price_sec h1{line-height: 45px;}
.price_sec ul{
	text-align:center;
	margin-bottom:15px;
	}
.price_sec ul > li{
	display:inline-block;
	width:20%;
	padding:5px 0;
	border-right:1px solid #ccc;
}
.price_sec ul > li:last-child{
	border-right:none;
	}
.price_sec ul > li p{
	padding-bottom:0;
	text-transform: capitalize;
	color:#c4c4c4;
	font-weight:bold;
	}
.price_sec ul > li.active h1{
	color:red;
	}
.price_sec .p__bar_sec{
	padding: 0 15%;
	}
.price_sec .p__bar_sec p{
	padding-bottom:0;
	font-size: 12px;
	font-weight: bold;
	}
.price_sec .p__bar_sec .progress{
	margin-bottom:0;
	height:5px;
	border-radius:10px;
	}

.price_sec .p__bar_sec .progress .progress-bar-success{
	background-color:#7ad400;
	}
.morris-area-chart_sec{
	margin-top:15px;
	position:relative;
	}
.morris-area-chart_sec .chrt__overloop{
	position:absolute;
	left:0;
	top:0;
	width:100%;
	height:100%;
	background:rgba(244,244,244,0.8);
	z-index:2222;
	}
.morris-area-chart_sec .chrt__overloop > div{
	margin-top: 120px !important;
	}

</style>
<?php
$fund_enddate		=	$fund_details[0]["fund_end_dt"];
$today_date			=	date("Y-m-d");
$goal				=	$fund_details[0]["fund_org_goal"];
if(count($total_payments)>0){
$raised				=	$total_payments[0]["raised_amt"];
}else{
$raised				=	0;	
}	

$remaining			=	$goal - $raised;

if($goal > 0){
$average_percent	=	ceil((($raised/$goal)*100));	
}else{
$average_percent	=	0;	
}
$ended_days_in 		=	ceil(strtotime($fund_enddate)-strtotime($today_date));
if($ended_days_in < 0)
{	
$campaign_msg		=	"<p>Campaign has ended</p>";
}
elseif($ended_days_in == 0)
{	
$campaign_msg		=	"<p>Campaign ends today</p>";	
}
elseif($ended_days_in == 1)
{	
$campaign_msg		=	"<p>Campaign ends tomorrow</p>";
}
else
{
$campaign_days_left =	ceil((strtotime($fund_enddate)-strtotime($today_date))/86400);
$campaign_msg		=	"<p>Campaign ends in ".$campaign_days_left." days</p>";	
}
?>
<div id="page-wrapper">
	<div class="container-fluid">
    <h5 style="margin-top:15px;">Fundraising Progress</h5>
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="price_sec">
        <ul>
          <li>
            <h1>$<?php echo number_format($goal);?></h1>
            <p>goal</p>
          </li>
          <li>
            <h1>$<?php echo number_format($raised);?></h1>
            <p>raised</p>
          </li>
          <li class="active">
			<h1>
			<?php 
			if($remaining < 0)
			{
			echo "$0";	
			}
			else
			{
			echo "$".number_format($remaining);	
			}
			?>
			</h1>
            <p>remaning</p>
          </li>
        </ul>
        <div class="p__bar_sec">
          <p><?php echo $average_percent; ?>% Funded</p>
          <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $average_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $average_percent; ?>%;"> </div>
          </div>
          <?php echo $campaign_msg; ?>
		  </div>
      </div>
    </div>
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Email Shares Report</h2></div>
				<div class="right">
                    <?php
                    if($this->session->userdata('id')!="") {
                        ?>
                        <a href="<?php echo base_url('/admin/fundraiser/'.$fslug.'/report/graph'); ?>" class="active">Graph View</a>
                        <a href="<?php echo base_url('/admin/fundraiser/'.$fslug.'/report/list'); ?>">List View</a>
                        <?php
                    }

                  if($this->session->userdata('fundraiser_id')!="" && $this->session->userdata('id')=="" )
                    {
                        ?>
                    <a href="<?php echo base_url($fslug.'/admin/report/graph');?>" class="active">Graph View</a>
                    <a href="<?php echo base_url($fslug.'/admin/report/list');?>">List View</a>
                    <?php }?>
				</div>
			</div>
			<div class="filter_sec">
				<h3>Filters:</h3>
				<div class="filter_sec_inner">
					<ul>
						<li>
							<label>From</label>
							<input type="text" class="datepicker" value="All Time" readonly>
						</li>
						<li>
							<label>To</label>
							<input type="text" class="datepicker" value="All Time" readonly>
						</li>
						<li>
							<label>Attributes</label>
							<a href="#" class="btn1">All</a>
						</li>
						<li>
							<label></label>
							<a href="#">Opened</a>
						</li>
						<li>
							<label></label>
							<a href="#">Logged in</a>
						</li>
						<li>
							<label></label>
							<a href="#">Invited</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="total_info">
				<div class="row">
				  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
					<div class="total_info_con text-center">
					  <p>Participants</p>
					  <span><?php echo $report_chart["participants"];?></span> </div>
				  </div>
				  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
					<div class="total_info_con text-center">
					  <p>Accounts</p>
					  <span><?php echo $report_chart["accounts"];?></span> </div>
				  </div>
				  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
					<div class="total_info_con text-center">
					  <p>Contacts</p>
					  <span><?php echo $report_chart["contacts"];?></span> </div>
				  </div>
				  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
					<div class="total_info_con text-center">
					  <p>Donors</p>
					  <span><?php echo $report_chart["donors"];?></span> </div>
				  </div>
				  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
					<div class="total_info_con text-center">
					  <p>Shares</p>
					  <span><?php echo $report_chart["shares"];?></span> </div>
				  </div>
				</div>
			</div>
			<div class="morris-area-chart_sec">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="morris-area-chart_inr morris-area-chart_sec">
							<?php
							if($report_chart["participants"] == 0 && $report_chart["accounts"] == 0 && $report_chart["contacts"] == 0 && $report_chart["donors"] == 0 && $report_chart["shares"] == 0){
							?>
							<div class="chrt__overloop">
							<div style="width:50%; margin:auto; text-align:center;">
							<h5>You haven't invited any of your contacts to donate yet. Invite your contacts to start receiving donations.</h5>
							<form action="<?php echo base_url($fslug.'/admin/participants'); ?>" method="POST">
							<input type="submit" name="info_submit" class="btn34" value="Invite Your Contacts" style="margin-top:12px; width:300px;padding:6px 25px;">
							</form>  
							</div>
							</div>
							<?php
							}
							?>
							<div id="morris-area-chart"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
	
	/*var jsonData ='{"cols":[{"id":"","label":"Dates","pattern":"","type":"string"},{"id":"","label":"Amounts","pattern":"","type":"number"}],"rows": [{"c":[{"v":"05/06/18","f":null},{"v":3,"f":null}]},{"c":[{"v":"06/07/18","f":null},{"v":1,"f":null}]},{"c":[{"v":"Olives","f":null},{"v":1,"f":null}]},{"c":[{"v":"Zucchini","f":null},{"v":1,"f":null}]},{"c":[{"v":"Pepperoni","f":null},{"v":2,"f":null}]}]}';*/
	
	var jsonData = $.ajax({
	url: "<?php echo base_url($fslug.'/admin/graph_json');?>",
	dataType: "json",
	async: false
	}).responseText;
	
	var options = {
	width: '100%',
	height: 400,
	pointSize: 6,
    colors: ['#3ACA6D'],
    legend: 'none',
	vAxis: {
	title: 'Donations received($)',
	gridlines:{count: 7}
	},
	chartArea: {top:50, bottom:50, left:80, right:0, width:'100%', height:'100%'},
	curveType: 'function'
	};
	
	var data = new google.visualization.DataTable(jsonData);
	var chart = new google.visualization.LineChart(document.getElementById('morris-area-chart'));
	chart.draw(data, options);
}

</script>