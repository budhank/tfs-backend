<script src="<?php echo base_url()?>assets/fundraiser/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/fundraiser/js/additional-methods.js"></script>
<?php $fslug = $this->session->userdata('fundraiser_urlname'); ?>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		<li class="active">Manage Rewards</li>
		</ul>
		<div class="tab_mnu">
		<ul>
		<li><a href="<?php echo base_url($fslug.'/admin/manage-donations')?>">DONATIONS</a></li>
		<li class="active"><a href="<?php echo base_url($fslug.'/admin/manage-rewards')?>">REWARDS</a></li>
		</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left">
					<h2>Rewards</h2>
				</div>
				<!--<a href="#" class="btn_round pull-right" data-toggle="modal" data-target="#edit_reward">Add new Record</a>-->
				<a href="javascript:void(0)" class="btn_round pull-right" onclick="openNewRec(0);">Add new Record</a>
			</div>

			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th width="25%">Reward Name</th>
							<th>Reward Info</th>
							<th width="25%" class="text-right">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php
						if(count($info)>0)
						{
							foreach($info as $val)
							{
								$str_rem_link = '';
								if($val["reward_id"]!='' AND $val["donation_id"]!='')
								{
									$str_rem_link = '<a href="javascript:void(0)" class="grn2" data-toggle="tooltip" data-placement="left" data-original-title="Delete donation records first!!">Remove</a>';
								}
								else
								{
									$str_rem_link = '<a href="javascript:void(0)" class="grn2" onclick="removeRec('.$val['id'].',\'r\');">Remove</a>';
								}
					?>
							<tr>
								<td data-th="Reward Name"><?php echo $val['reward_name'];?></td>
								<td data-th="Reward Info"><?php echo $val['reward_info'];?></td>
								<td data-th="" class="text-right">
									<a href="javascript:void(0)" class="grn2" onclick="openNewRec(<?php echo $val['id'];?>);">Edit</a>&nbsp;&nbsp;&nbsp;
									<?php echo $str_rem_link;?>
								</td>
							</tr>
					<?php
						    }
						}
						else
						{
					?>
						    <tr><td colspan="3" style="text-align: center;">No records found!!</td></tr>
					<?php
						}
					?>
					</tbody>					
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function openNewRec(flg)
{
  if(flg==0)
	{
		$.ajax({
            type      :"POST",
            url       :"<?php echo base_url($fslug.'/admin/getRewardOpenAjax'); ?>",
            data      :"fslug="+flg,
            dataType  :"html",
            beforeSend: function(){
                $("#rewAddModal").modal('show');
                $('#div_loading').show();
            },
            success: function(response){ 
			    $('#div_loading').hide();               
                $("#rewAddModal").html(response);
                $("#rewAddModal").modal('show');               
            }
    	});	
	}
	else{
		$.ajax({
            type      :"POST",
            url       :"<?php echo base_url($fslug.'/admin/getRewardOpenAjax'); ?>",
            data      :"fslug="+flg,
            dataType  :"html",
            beforeSend: function(){
                $("#rewAddModal").modal('show');
                $('#div_loading').show();
            },
            success: function(response){ 
			    $('#div_loading').hide();               
                $("#rewAddModal").html(response);
                $("#rewAddModal").modal('show');               
            }
    	});
	}
}	
function removeRec(v)
{
var r = confirm("Are you sure to change the status?");
if (r == true) 
{
	//alert(v);
	$("#hid_id").val(v);
	$("#frm_rem").submit();
}
else{}
	
}
</script>
<form name="frm_rem" id="frm_rem" action="<?php echo base_url($fslug.'/admin/manage-rewards')?>" method="post">
	<input type="hidden" id="hid_id" name="hid_id" value="">
</form>
<div id="rewAddModal" class="modal fade" role="dialog"></div>