<script src="<?php echo base_url('assets/fundraiser/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/additional-methods.js')?>"></script>
<style>
.rewards_row{
	border:1px solid red;margin:0 15px 15px;position:relative;
}
</style>
<?php $fslug = $this->session->userdata('fundraiser_urlname'); ?>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		<li class="active">Manage Rewards</li>
		</ul>
		<div class="tab_mnu">
		<ul>
		<li class="active"><a href="<?php echo base_url($fslug.'/admin/manage-donations')?>">DONATIONS</a></li>
		<li><a href="<?php echo base_url($fslug.'/admin/manage-rewards')?>">REWARDS</a></li>
		</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Donations & Rewards</h2></div>
				<a href="javascript:void(0)" class="btn_round pull-right" onclick="openNewRec(0);">Add new Donation amount</a>
			</div>
			
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th width="25%">Donation amount</th>
							<th>Reward</th>
							<th width="25%" class="text-right">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if(count($info)>0)
					{
						foreach($info as $val)
						{
					?>
						<tr>
							<td data-th="Donation amount">
							<b>$<?php echo $val['donate_start'];?> - $<?php echo $val['donate_end'];?></b>
							</td>
							<td data-th="Reward"><?php echo $val['reward_name'];?></td>
							<td data-th="" class="text-right">
								<a href="javascript:void(0)" class="grn2" onclick="openNewRec(<?php echo $val['id'];?>);">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="javascript:void(0)" class="grn2" onclick="removeRec(<?php echo $val['id'];?>);">Remove</a>
							</td>
						</tr>
					<?php
						}
					}
					else
					{
					?>
						<tr><td colspan="3" style="text-align: center;">No records found!!</td></tr>
					<?php
					}
					?>
					</tbody>
					<!--<tfoot>
						<tr>
							<td colspan="6" class="text-center"><a class="grn2" href="#">Load more</a></td>
						</tr>
					</tfoot>-->
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Add/Edit Donation popup start -->
<div id="donAddModal" class="modal fade" role="dialog"></div>
<!-- Add/Edit Donation popup ends -->
<form name="frm_rem" id="frm_rem" action="<?php echo base_url($fslug.'/admin/manage-donations')?>" method="post">
	<input type="hidden" id="hid_id" name="hid_id" value="">
</form>
<script type="text/javascript">
function openNewRec(flg)
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url($fslug.'/admin/getDonationOpenAjax'); ?>",
		data      :"fslug="+flg,
		dataType  :"html",
		beforeSend: function(){
			$("#donAddModal").modal('show');
			$('#div_loading').show();
		},
		success: function(response){ 
			$('#div_loading').hide();               
			$("#donAddModal").html(response);
			$("#donAddModal").modal('show');               
		}
	});	
}

function removeRec(v)
{
	var r = confirm("Are you sure to delete this?");
	if (r == true) 
	{
		$("#hid_id").val(v);
		$("#frm_rem").submit();
	}
}
</script>