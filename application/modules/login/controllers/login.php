<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller{
	
	function Login(){
		parent::__construct();
	   }
	
	public function index(){
		echo 'dd';die;
		redirect('login/login');
	}
	
	public function home()
	{
	    $this->load->view('../../includes/header');
		$this->load->view('welcome_message');	
		$this->load->view('../../includes/footer');	
	}
	
	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('password');
		$this->session->unset_userdata('contact_email');
		$this->session->set_userdata('logout_msg', 'Successfully Logout !');
		redirect('login/login');
	}
	
	public function changepassword()
	{    
	  $flag=$this->input->post('flag');
		if($flag==1)
		{	
		    $this->form_validation->set_rules('old_pass', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|matches[confirm_pass]|xss_clean');
		    $this->form_validation->set_rules('confirm_pass', 'Confirm New Password', 'trim|required|xss_clean');
			if ($this->form_validation->run() != FALSE)
			  {		
			      $ID = $this->session->userdata('id');	
				  $old_pass=$this->input->post('old_pass');
				  $row = $this->admin_model->view('mob_admin',array('id'=>$ID));	
				  if($row[0]['password']==md5($old_pass))
				  { 
			        $new_pass=$this->input->post('new_pass');
					$new_pass = md5($new_pass);						
					$this->admin_model->edit('mob_admin',array('password'=>$new_pass),array('id'=>$ID));		
					$this->session->set_userdata('password', $new_pass);		
					$this->session->set_userdata('change_pass_msg', 2);		
					redirect('admin/changepassword');
				 }	
				 else
				 {
				 	$this->session->set_userdata('change_pass_msg', 1);		
					redirect('admin/changepassword');
				 }							
			  }
	    }
	   $this->load->view('../../includes/header');
	   $this->load->view('change_pass');
	   $this->load->view('../../includes/footer');	
   }
   
	
	public function agreement()
	{    
	  $submit=$this->input->post('submit');
		if(isset($submit))
		{	
		    $this->form_validation->set_rules('agreement', 'Agreement', 'trim|required|xss_clean');
			if ($this->form_validation->run() != FALSE)
			  {		
					$this->admin_model->edit('mob_agreement',array('agreement_text'=>$this->input->post('agreement')),array('id'=>1));	
					$this->session->set_userdata('update_msg', 1);	
			  }
	    }
	   $agreement_row = $this->admin_model->view('mob_agreement',array('id'=>1));	
	   $data['agreement_row'] = $agreement_row;
	   $this->load->view('../../includes/header');
	   $this->load->view('agreement',$data);
	   $this->load->view('../../includes/footer');	
   }
   
  /* public function checkAllUser()
   {
	   echo $uname = $this->input->post('username');
	   //$res   = $this->apps_model->funcCheckAllUser($uname);
	   //print_r($res);
   }*/
	
}
