<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">

    

    <title></title>

    <link href="<?php echo base_url()?>assets/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/jquery-ui.css">

    <link href="<?php echo base_url()?>assets/admin/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/admin/css/custom_style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300i,400,400i,700" rel="stylesheet"> 

    

    <script src="<?php echo base_url()?>assets/admin/js/jquery-3.1.1.min.js"></script>    

    <script src="<?php echo base_url()?>assets/admin/js/jquery-ui.js"></script>

    <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url()?>assets/admin/js/custom.js"></script>

    <script  type="text/javascript">

    $(function () {

  	$('[data-toggle="tooltip"]').tooltip()

	})

	</script>

    

    <!--<script src="<?php //echo base_url()?>assets/admin/js/jquery.js"></script> -->

    <!--<script src="<?php //echo base_url()?>assets/admin/js/jquery.validate.js"></script>--> 

    <!--<script src="<?php //echo base_url()?>assets/admin/js/additional-methods.js"></script>-->   

    <!--<script src="<?php //echo base_url()?>assets/admin/js/raphael-min.js"></script> -->

    <!--<script src="<?php //echo base_url()?>assets/admin/js/morris-0.4.1.min.js"></script> -->

    



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>

    <body>

    <div id="wrapper" class="white-header">

           <!-- Navigation -->

        <nav class="navbar navbar-inverse navbar-fixed-top navigation" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->
 <div class="container2">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

                <a class="navbar-brand" href="<?php echo base_url('admin/login');?>"><img src="<?php echo base_url('assets/admin/images/logo_white.jpg')?>" /></a>

            </div>

            <!-- Top Menu Items -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-right top-nav">
			<li><a href="http://thanksforsupporting.com/">Home</a></li>
			<li><a href="http://thanksforsupporting.com/#HowItWorks">How It Works</a></li>
			<li><a href="http://thanksforsupporting.com/#FAQs">FAQ’s</a></li>
			<li><a href="http://thanksforsupporting.com/#ContactUs">Contact Us</a></li>
			<li class="signup"><a href="<?php echo base_url('start-fundraiser');?>">SIGNUP</a></li>
			<li class="login"><a href="<?php echo base_url('admin/login');?>">LOGIN</a></li>
            </ul>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            

                <?php

                if($this->session->userdata('id')!=""){

                ?>

                <ul class="nav navbar-nav side-nav">

                    <li id="act_dboard"><a href="<?php echo base_url()?>admin/home">Dashboard</a></li>

                    <li id="act_fundraiser"><a href="<?php echo base_url()?>admin/fundraisers">Fundraisers</a></li>

                    <li id="act_etemplate"><a href="<?php echo base_url()?>admin/emailtemplate">Email Templates</a></li>

                    <li id="act_payment"><a href="<?php echo base_url()?>admin/managepayment">Manage Payments</a></li>

                    <li id="act_settings"><a href="<?php echo base_url()?>admin/settings">Settings</a></li>

                    <li id="act_log"><a href="<?php echo base_url()?>admin/logout">Log Out</a></li>

                </ul> 

                <?php } ?>

            </div>

            <!-- /.navbar-collapse -->
</div>
        </nav>     
		
    