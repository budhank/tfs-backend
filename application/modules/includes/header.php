<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title></title>
    <link href="<?php echo base_url()?>assets/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/jquery-ui.css">
    <link href="<?php echo base_url()?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/admin/css/custom_style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" rel="stylesheet" type="text/css">
    
    <script src="<?php echo base_url()?>assets/admin/js/jquery-3.1.1.min.js"></script>    
    <script src="<?php echo base_url()?>assets/admin/js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/admin/js/custom.js"></script>
    <script  type="text/javascript">
    $(function () {
  	$('[data-toggle="tooltip"]').tooltip()
	})
	</script>
    
    <!--<script src="<?php //echo base_url()?>assets/admin/js/jquery.js"></script> -->
    <!--<script src="<?php //echo base_url()?>assets/admin/js/jquery.validate.js"></script>--> 
    <!--<script src="<?php //echo base_url()?>assets/admin/js/additional-methods.js"></script>-->   
    <!--<script src="<?php //echo base_url()?>assets/admin/js/raphael-min.js"></script> -->
    <!--<script src="<?php //echo base_url()?>assets/admin/js/morris-0.4.1.min.js"></script> -->
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    <body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('admin/login');?>"><img src="<?php echo base_url('assets/admin/images/logo.png')?>" /></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li><a href="#"><i class="fa fa-bell-o"></i></a></li>
                <li><a href="#"><span class="cd">CD</span></a></li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php
                if($this->session->userdata('id')!=""){
                ?>
                <ul class="nav navbar-nav side-nav">
                    <li id="act_dboard"><a href="<?php echo base_url()?>admin/home">Dashboard</a></li>
                    <li id="act_fundraiser"><a href="<?php echo base_url()?>admin/fundraisers">Fundraisers</a></li>
                    <li id="act_etemplate"><a href="<?php echo base_url()?>admin/emailtemplate">Email Templates</a></li>
                    <li id="act_payment"><a href="<?php echo base_url()?>admin/managepayment">Manage Payments</a></li>
                    <li id="act_settings"><a href="<?php echo base_url()?>admin/settings">Settings</a></li>
                    <li id="act_settings"><a href="<?php echo base_url()?>admin/cron-mail-list">CRON Emails</a></li>
                    <li id="act_log"><a href="<?php echo base_url()?>admin/logout">Log Out</a></li>
                </ul> 
                <?php } ?>
            </div>
            <!-- /.navbar-collapse -->
        </nav>     
    