<?php
class Cronmail_model extends CI_Model {
	
	public function view($table_name, $where)
    {
		$query = $this->db->get_where($table_name, $where);
		return $query->result_array(); 
    }
	public function view_all($table_name)
    {
		$query = $this->db->get($table_name);
		return $query->result_array(); 	
    }
	public function view_check($table_name,$where)
    {
		$query = $this->db->get_where($table_name,$where);
		return ($query->num_rows()>0)?$query->num_rows():0; 	
    }
	public function add($table_name, $data)
    {
		return $this->db->insert($table_name, $data); 
	}  
	public function edit($table_name, $data, $where)
    {
		return $this->db->update($table_name, $data, $where);
	}
	public function remove($table_name, $where)
    {
		return $this->db->delete($table_name, $where); 
	}
	public function last_insert_id()
	{
		return $this->db->insert_id();
	}
	public function last_qry()
	{
		echo $this->db->last_query();
	}	
	public function donations_part($fund_id)
	{
		$query = $this->db->query('SELECT tdp.*,CONCAT(tdp.donor_fname," ",tdp.donor_lname) donor_name ,CONCAT(tp.player_fname," ",tp.player_lname) participant_name 
		FROM tbl_donors_payment tdp 
		LEFT JOIN tbl_player tp ON tp.id = tdp.player_id
		WHERE tdp.fund_id = "'.$fund_id.'"');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function fundraiser_before_start()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(fr.fund_start_dt, NOW()) <=5) AND (DATEDIFF(fr.fund_start_dt, NOW()) >=0) AND tp.cron_mail_log_credential = 0');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_two_days_before_start()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(fr.fund_start_dt, NOW()) <= 2) AND (DATEDIFF(fr.fund_start_dt, NOW()) >=0) AND tp.cron_mail_reminder1 = 0 AND tp.invited = 0');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_five_days_before_end()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(fr.fund_end_dt, NOW()) <=5) AND (DATEDIFF(fr.fund_start_dt, NOW()) >=0) AND tp.cron_mail_reminder2 = 0 AND tp.invited = 0');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_five_days_after_end()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(NOW(),fr.fund_end_dt) >=5) AND tp.cron_mail_recap = 0 AND tp.invited = 1');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_invite_participant_start_date()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(fr.fund_end_dt, NOW()) <=5) AND (DATEDIFF(fr.fund_start_dt, NOW()) >=0) AND tp.cron_mail_reminder2 = 0 AND tp.invited = 0');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_invite_participant_three_days_before_end()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(fr.fund_end_dt, NOW()) <=5) AND (DATEDIFF(fr.fund_start_dt, NOW()) >=0) AND tp.cron_mail_reminder2 = 0 AND tp.invited = 0');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_invite_contacts_contact_date()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(fr.fund_end_dt, NOW()) <=5) AND (DATEDIFF(fr.fund_start_dt, NOW()) >=0) AND tp.cron_mail_reminder2 = 0 AND tp.invited = 0');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_invite_contacts_three_days_before_end()
	{
		$query = $this->db->query('SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr INNER JOIN tbl_player tp ON fr.id = tp.player_added_by WHERE (DATEDIFF(fr.fund_end_dt, NOW()) <=5) AND (DATEDIFF(fr.fund_start_dt, NOW()) >=0) AND tp.cron_mail_reminder2 = 0 AND tp.invited = 0');
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_collects($fund_id)
	{
		$sql = "SELECT tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt, tdnpc.donors, IFNULL(tpt.fundraiser_id,0) transferStatus
		FROM tbl_fundraiser fundr
		LEFT JOIN (
		SELECT id as pay_id,fund_id, IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY fund_id
		) tdnp ON tdnp.fund_id=fundr.id 
		LEFT JOIN (
		SELECT fund_id fundr_id, COUNT(fund_id) donors FROM tbl_donors_payment GROUP BY fund_id
		) tdnpc ON tdnpc.fundr_id=fundr.id 
		LEFT JOIN tbl_payment_transfer tpt ON tpt.fundraiser_id=fundr.id 
		WHERE fundr.id = '".$fund_id."' 
		GROUP BY fundr.id";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
}
?>