<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronmail extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Cronmail_model');
	}

    public function cron_mail_admin_start_end()
    {
        $email_content="";
        $query = $this->db->query('SELECT * FROM tbl_fundraiser WHERE DATE(`fund_start_dt`) = CURDATE()');
        if($query->num_rows() > 0) {
            $result = $query->result_array();

            $email_content.="The following fundraiser is starting today: ";
            foreach ($result as $key=>$value)
            {
                $email_content.="<br>".$value['fund_fname']." ".$value['fund_lname'];
            }

            $config = array();
            $config['useragent']= "CodeIgniter";
            $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
            $config['protocol'] = "mail";
           // $config['smtp_host']= "ssl://smtp.gmail.com";
            //$config['smtp_port']= "465";
            $config['mailtype'] = 'html';
            $config['charset']	= 'iso-8859-1';
            $config['newline']  = "\r\n";
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            $this->email->from('jlewis@thanksforsupporting.com','Admin');
            //$this->email->to('madank.wms@gmail.com');

            $this->email->to('jlewis@thanksforsupporting.com');
            $this->email->subject('ThanksForSupporting - Fundraiser is Starting');
            $this->email->message($email_content);
            $bval=$this->email->send();

        }




        $email_content="";
        $query = $this->db->query('SELECT * FROM tbl_fundraiser WHERE DATE(`fund_end_dt`) = CURDATE()');
        if($query->num_rows() > 0) {
            $result = $query->result_array();

            $email_content.="The following fundraiser is ending today: ";
            foreach ($result as $key=>$value)
            {
                $email_content.="<br>".$value['fund_fname']." ".$value['fund_lname'];
            }

            $config = array();
            $config['useragent']= "CodeIgniter";
            $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
            $config['protocol'] = "mail";
           // $config['smtp_host']= "ssl://smtp.gmail.com";
            //$config['smtp_port']= "465";
            $config['mailtype'] = 'html';
            $config['charset']	= 'iso-8859-1';
            $config['newline']  = "\r\n";
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            $this->email->from('jlewis@thanksforsupporting.com','Admin');
           // $this->email->to('madank.wms@gmail.com');
            $this->email->to('jlewis@thanksforsupporting.com');
            $this->email->subject('ThanksForSupporting - Fundraiser is Ending');
            $this->email->message($email_content);
            $bval=$this->email->send();

        }




    }

	public function cron_mail_log_credential()
	{
		$fund_details = $this->Cronmail_model->fundraiser_before_start();
		if(count($fund_details)>0)
		{
			$encrypt_key 	= $this->config->item('encryption_key');
			$template = $this->Cronmail_model->view('tbl_mail_template',array('template_slug'=>'asking-to-invite-donors-to-player-from-fundraiser'));
			
			foreach($fund_details as $fund)
			{
				$fund_email 		= $fund["fund_email"];
				$fund_id			= $fund['id'];
				$fund_name			= $fund['fund_username'];
				$fund_start			= date("F j, Y",strtotime($fund['fund_start_dt']));
				$fund_end			= date("F j, Y",strtotime($fund['fund_end_dt']));
				$fund_email			= $fund['fund_email'];
				$fund_no			= $fund['fund_contact'];
				$fund_goal			= $fund['fund_org_goal'];
				$player_goal		= $fund['fund_individual_goal'];
				$fundr_slog			= $fund['fund_slogan'];
				$fund_bgcolor		= $fund['fund_color_rgb'];
				$fund_fontcolor		= $fund['fund_font_color'];
				if($fund['fund_image']==""){
					$fundimg 		= base_url()."assets/images/noimage-150x150.jpg";   
				}else{
					$fundTimg 		= explode(".",$fund['fund_image']);	
					$fundimg  		= base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
				}
				
				$pslug				= $fund['player_slug_name'];
				$player_id			= $fund['player_id'];
				$player_first		= $fund['player_fname'];
				$player_last		= $fund['player_lname'];
				$player_email		= $fund['player_email'];
				$player_password	= $fund['player_pass'];
				
				$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
				$player_login_url	= base_url('admin/login');
				$start_fundraiser	= base_url("start-fundraiser");

				
				$html_header = '<!DOCTYPE html>
				<html lang="en">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>ThanksForSupporting</title>
				<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
				</head>
				<body>
				<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato; font-size: 20px;">
				<tr>
				<td align="center"><table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0">
				<tr>
				<td style="padding:0 30px;"><table border="0" width="640" cellpadding="0" cellspacing="0">
				<tr>
				<td><img src="http://thanksforsupporting.com/apps/assets/images/logo_thanks-for-supporting.png" /></td>
				<td style="padding:0 10px;"><img src="'.$fundimg.'" style="width: 100px;height: 100px;border-radius: 50%;" /></td>
				<td><strong>'.$fundraiser_name.'</strong> <br>
				<div style="font-size: 14px;">'.$fundraiser_slog.'</div></td>
				</tr>
				</table></td>
				</tr>
				</table></td>
				</tr>
				</table>';
				$html_footer = '<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;">
				<tr>
				<td style="padding:5px 30px 30px; text-align: center; color:#9f9f9f; font-size: 16px"> This email was sent to you because we want to make the world a better place.<br/>
				You can choose to opt out of future opportunities to help others by clicking here. </td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				</body>
				</html>';

				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
				$search_val			= array($player_first,$fund_start,$fund_end,$fund_goal,$player_goal,$player_email,$player_pass,$fund_name,$fund_email,$fund_no,$player_login_url,$start_fundraiser,$fund_bgcolor,$fund_fontcolor,$fund_bgcolor);
				$email_content		= $html_header.str_replace($search_key,$search_val,$template_body).$html_footer;
				
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				//$config['smtp_host']= "localhost";
				//$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com',$fund_name);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				if($bval){
					$this->Cronmail_model->edit('tbl_player',array("cron_mail_log_credential"=>1),array("id"=>$player_id));
				}
			}
		}
		redirect("admin/cron-mail-list");
	}
	
	public function cron_mail_reminder1()
	{
		$fund_details = $this->Cronmail_model->fundraiser_two_days_before_start();
		if(count($fund_details)>0)
		{
			$encrypt_key 	= $this->config->item('encryption_key');
			$template = $this->Cronmail_model->view('tbl_mail_template',array('template_slug'=>'reminder-to-invite-donors-to-player-from-fundraiser'));
			
			foreach($fund_details as $fund)
			{
				$fund_email 		= $fund["fund_email"];
				$fundraiser_id		= $fund['id'];
				$fundraiser_name	= $fund['fund_username'];
				$fundraiser_start	= $fund['fund_start_dt'];
				$fundraiser_end		= $fund['fund_end_dt'];
				$fundraiser_email	= $fund['fund_email'];
				$fundraiser_no		= $fund['fund_contact'];
				$fundraiser_goal	= $fund['fund_individual_goal'];
				
				$player_id			= $fund['player_id'];
				$player_first		= $fund['player_fname'];
				$player_last		= $fund['player_lname'];
				$player_goal		= $fund['player_goal'];
				$player_email		= $fund['player_email'];
				$player_password	= $fund['player_pass'];
				$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
				$player_login_url	= base_url('admin/login');
				$start_fundraiser	= base_url("start-fundraiser");
				$selected_color		= $fund['fund_color_rgb'];
				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[selected_color]');
				$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$selected_color);
				$email_content		= str_replace($search_key,$search_val,$template_body);

				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				//$config['smtp_host']= "localhost";
				//$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				if($bval){
				$this->Cronmail_model->edit('tbl_player',array("cron_mail_reminder1"=>1),array('id'=>$player_id));
				}
			}
		}
		redirect("admin/cron-mail-list");
	}
	
	public function cron_mail_reminder2()
	{
		$fund_details = $this->Cronmail_model->fundraiser_five_days_before_end();
		if(count($fund_details)>0)
		{
			$encrypt_key 	= $this->config->item('encryption_key');
			$template = $this->Cronmail_model->view('tbl_mail_template',array('template_slug'=>'2nd-reminder-to-invite-donors-to-player-from-fundraiser'));
			
			foreach($fund_details as $fund)
			{
				$fund_email 		= $fund["fund_email"];
				$fundraiser_id		= $fund['id'];
				$fundraiser_name	= $fund['fund_username'];
				$fundraiser_start	= $fund['fund_start_dt'];
				$fundraiser_end		= $fund['fund_end_dt'];
				$fundraiser_email	= $fund['fund_email'];
				$fundraiser_no		= $fund['fund_contact'];
				$fundraiser_goal	= $fund['fund_individual_goal'];
				
				$player_id			= $fund['player_id'];
				$player_first		= $fund['player_fname'];
				$player_last		= $fund['player_lname'];
				$player_goal		= $fund['player_goal'];
				$player_email		= $fund['player_email'];
				$player_password	= $fund['player_pass'];
				$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
				$player_login_url	= base_url('admin/login');
				$start_fundraiser	= base_url("start-fundraiser");
				$selected_color		= $fund["fund_color_rgb"];
				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[selected_color]');
				$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$selected_color);
				$email_content		= str_replace($search_key,$search_val,$template_body);

				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				//$config['smtp_host']= "localhost";
				//$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				if($bval){
				$this->Cronmail_model->edit('tbl_player',array("cron_mail_reminder2"=>1),array('id'=>$player_id));
				}
			}
		}
		redirect("admin/cron-mail-list");
	}

	public function cron_mail_recap()
	{
		$fund_details = $this->Cronmail_model->fundraiser_five_days_after_end();
		if(count($fund_details)>0)
		{
			$encrypt_key 	= $this->config->item('encryption_key');
			$template = $this->Cronmail_model->view('tbl_mail_template',array('template_slug'=>'fundraiser-is-over-to-player-from-fundraiser'));
			$settings = $this->Cronmail_model->view('tbl_settings',array('id'=>1));
			
			foreach($fund_details as $fund)
			{
				$fund_email 		= $fund["fund_email"];
				$fundraiser_id		= $fund['id'];
				$fundraiser_name	= $fund['fund_username'];
				$fundraiser_start	= $fund['fund_start_dt'];
				$fundraiser_end		= $fund['fund_end_dt'];
				$fundraiser_email	= $fund['fund_email'];
				$fundraiser_no		= $fund['fund_contact'];
				$fundraiser_org_goal= $fund['fund_org_goal'];
				$fundraiser_ind_goal= $fund['fund_individual_goal'];
				
				$selected_color	    = $fund['fund_color_rgb'];
				
				$player_id			= $fund['player_id'];
				$player_first		= $fund['player_fname'];
				$player_last		= $fund['player_lname'];
				$player_goal		= $fund['player_goal'];
				$player_email		= $fund['player_email'];
				
				$start_fundraiser	= base_url("start-fundraiser");
				$collects	= $this->Cronmail_model->fundraiser_collects($fundraiser_id);
				
				$fund_id	= $collects[0]['fund_id'];
				$deduct_amt	= $settings[0]["deductable_amt"];
				$less_per	= $settings[0]["less_percent"];
				$donations	= $collects[0]['raised_amt'];
				$collected	= $collects[0]['raised_amt'];
				$donors		= $collects[0]['donors'];
				$deductable	= ($donors*$deduct_amt);
				$lessamt	= (($collects[0]['raised_amt']*$less_per)/100);
				$deduct		= ($deductable+$lessamt);
				$deposits	= ($collected-$deduct);
				
				$goalpercent= (($donations/$fundraiser_ind_goal)*100);
				
				$participationrate	= $this->Cronmail_model->view_check('tbl_player',array('player_added_by'=>$fundraiser_id,'invited'=>1));
				$donations_part = $this->Cronmail_model->donations_part($fundraiser_id);
				$donationdetailslist= '';
				if(count($donations_part)>0)
				{
				$tot_donation	= 0;
				$tot_fees 		= 0;
				$tot_transfer	= 0;
				foreach($donations_part as $part){
					$lessamt	= (($part['donation_amt']*$less_per)/100);
					$fees		= ($deduct_amt+$lessamt);
					$transfer	= ($part['donation_amt']-$fees);
					$donationdetailslist .= '<tr>
					<td valign="top" style="padding:15px;border-bottom:1px solid #eee;">'.$part["donor_name"].'</td>
					<td valign="top" style="padding:15px 10px;border-bottom:1px solid #eee;">'.$part["participant_name"].'</td>
					<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">'.date("m-d-Y",strtotime($part["donation_date"])).'</td>
					<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$'.number_format($part["donation_amt"],2).'</td>
					<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$'.number_format($fees,2).'</td>
					<td valign="top" style="text-align:right;padding:15px;border-bottom:1px solid #eee;">$'.number_format($transfer,2).'</td>
					</tr>
					<tr>';
					$tot_donation += $part["donation_amt"];
					$tot_fees	  += $fees;
					$tot_transfer += $transfer;
				}
				$donationdetailslist .= '<td valign="top" colspan="3" style="padding:15px;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #eee;">Grand Total</td>
				<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$'.number_format($tot_donation,2).'</td>
				<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$'.number_format($tot_fees,2).'</td>
				<td valign="top" style="text-align:right;padding:15px;font-weight:bold;border-bottom:1px solid #eee;">$'.number_format($tot_transfer,2).'<span style="color:#ffb944;">**</span></td>
				</tr>';
				}
				
				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				
				$search_key = array('[campaign]','[startdate]','[enddate]','[firstname]','[goal]','[goalpercent]','[participationrate]','[donors]','[donations]','[deposits]','[donationdetailslist]','[selected_color]');

				$search_val = array($fundraiser_name,$fundraiser_start,$fundraiser_end,$player_first,$fundraiser_ind_goal,$goalpercent,$participationrate,$donors,$donations,$deposits,$donationdetailslist,$selected_color);
				
				$email_content		= str_replace($search_key,$search_val,$template_body);

				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				//$config['smtp_host']= "localhost";
				//$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				if($bval){
				$this->Cronmail_model->edit('tbl_player',array("cron_mail_recap"=>1),array('id'=>$player_id));
				}
			}
		}
		redirect("admin/cron-mail-list");
	}	
	
	public function cron_mail_invite_participant1()
	{
		/*--------fundraiser-------------*/
		$fund_details		= $this->player_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
		$fundraiser_id		= $fund_details[0]['id'];
		//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
		$fundraiser_name	= $fund_details[0]['fund_username'];
		$fundraiser_slog	= $fund_details[0]['fund_slogan'];
		$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
		$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
		
		if($fund_details[0]['fund_image']==""){
			$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
		}else{
			$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
			$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
		}
		/*-------------------------------*/
		/*-------------player------------*/
		$player_details		= $this->player_model->view('tbl_player',array('player_slug_name'=>$pslug));
		$player_id			= $player_details[0]['id'];
		$player_first		= $player_details[0]['player_fname'];
		$player_last		= $player_details[0]['player_lname'];
		$player_email		= $player_details[0]['player_email'];
		
		if($player_details[0]['player_image']==""){
		$player_img			=  base_url("assets/images/noimage-150x150.jpg");   
		}else{
		$playerTimg			= explode(".",$player_details[0]['player_image']);	
		$player_img			=  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
		}
		
		$player_name		= $player_first.' '.$player_last;
		$start_fundraiser	= base_url("start-fundraiser");
		/*------------------------------*/
		
		$template	= $this->player_model->view('tbl_mail_template',array('template_slug'=>'request-for-donation-to-donor-from-player'));
		$subject	= $template[0]["template_subject"];
		$header		= $template[0]["template_header"];
		$footer		= $template[0]["template_footer"];
		$body		= $header.html_entity_decode($template[0]["template_content"]).$footer;

		$contact_list = $this->player_model->view('tbl_player_contacts',array('contact_added_by'=>$pslug));
		foreach($contact_list as $contact_li){
			$contact_id			= $contact_li["id"];
			$email				= $contact_li["contact_email"];
			
			$indata = array(
				'from_id'		=> $player_id,
				'to_id'			=> $contact_id,
				'from_email'	=> $player_email,
				'to_email'		=> $email,
				'sent'			=> '1',
				'sent_time'		=> date('Y-m-d H:i:s')
			);
			$insert				= $this->player_model->add('tbl_email_share_reports',$indata);
			$report_id			= $this->player_model->last_insert_id();
			
			$player_url			= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
			$player_share_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
			$player_support_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
			
			$keywords			= array('[FundraiserImage]','[FundraiserName]','[FundraiserSlogan]','[PlayerImage]','[PlayerName]','[PlayerURL]','[PlayerShareLink]','[UseOfDonations]','[PlayerFirst]','[PlayerLast]','[SupportUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]');

			$values				= array($fundimg,$fundraiser_name,$fundraiser_slog,$player_img,$player_name,$player_support_url,$player_share_url,$fundraiser_name,$player_first,$player_last,$player_support_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor);

			$mail_content = str_replace($keywords,$values,$body).'<img src="'.base_url('getimage/'.$report_id).'" style="display:none;"/>';
			
			$config 			= array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "mail";
			//$config['smtp_host']= "localhost";
			//$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('support@thanksforsupporting.com',$player_name);  
			$this->email->to($email); 
			$this->email->subject('ThanksForSupporting - '.$subject); 
			$this->email->message($mail_content);
			$bval=$this->email->send();	
			if($bval){
			$this->player_model->edit('tbl_player_contacts',array("invitation"=>1),array("id"=>$contact_li["id"],'contact_added_by'=>$pslug));
			}
		}
		$this->player_model->edit('tbl_player',array("invited"=>1),array('player_slug_name'=>$pslug));
		redirect("admin/cron-mail-list");
	}

	public function cron_mail_invite_participant2()
	{
		if(count($don_details)>0)
		{
			$fslug 				= $don_details[0]['fund_slug'];
			$pslug 				= $don_details[0]['player_slug'];
			
			$fund_details		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= $fund_details[0]['fund_start_dt'];
			$fundraiser_end		= $fund_details[0]['fund_end_dt'];
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
			$selected_color		= $fund_details[0]['fund_color_rgb'];
			
			$player_details		= $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_name		= $player_first.' '.$player_last;
			$player_goal		= $player_details[0]['player_goal'];
			$player_email		= $player_details[0]['player_email'];
			$player_login_url	= base_url('admin/login');
			$player_url			= base_url($fslug.'/'.$pslug);
			$player_share_url	= base_url($fslug.'/'.$pslug.'/share');
			$start_fundraiser	= base_url("start-fundraiser");
			
			$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'reminder-request-for-donation-to-donor-from-player'));

			$template_body		= html_entity_decode($template[0]["template_content"]);
			$template_subject	= $template[0]["template_subject"];
			$search_key 		= array('[PlayerName]','[PlayerFirst]','[PlayerLast]','[FundraiserName]','[PlayerURL]','[PlayerShareLink]','[StartFundraiser]','[selected_color]');
			$search_val			= array($player_name,$player_first,$player_last,$fundraiser_name,$player_url,$player_share_url,$start_fundraiser,$selected_color);
			$email_content		= str_replace($search_key,$search_val,$template_body);

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "mail";
			//$config['smtp_host']= "localhost";
			//$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('invite@thanksforsupporting.com',$player_first.' '.$player_last);  
			$this->email->to($player_email); 
			$this->email->subject('ThanksForSupporting - '.$template_subject); 
			$this->email->message($email_content);
			$bval=$this->email->send();	
			$response = "Reminder sent";
		}
		redirect("admin/cron-mail-list");
	}

	public function cron_mail_invite_contact_donor1()
	{
		if($fslug!='' AND $pslug!='')
		{
			$this->session->unset_userdata('donation_id');
			
			$player_details 		= $this->Frontplayer_model->view('tbl_player',array("player_slug_name"=>$pslug));
			$fund_details 			= $this->Frontplayer_model->view('tbl_fundraiser',array("fund_slug_name"=>$fslug));
			//$fundraiser_name		= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name		= $fund_details[0]['fund_username'];
			$fundraiser_slog		= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor		= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor	= $fund_details[0]['fund_font_color'];
			
			if($fund_details[0]['fund_image']==""){
				$fundimg =  base_url()."assets/images/noimage-150x150.jpg";   
			}else{
				$fundTimg = explode(".",$fund_details[0]['fund_image']);	
				$fundimg  =  base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
			}
			
			if(count($player_details)>0)
			{
				$firstname	= 'ThanksForSupporting';
				$lastname	= ' ';
				
				$player_url			= base_url($fslug.'/'.$pslug);
				$player_share_url	= base_url($fslug.'/'.$pslug);
				$start_fundraiser	= base_url("start-fundraiser");

				$template = $this->Frontplayer_model->view('tbl_mail_template',array('template_slug'=>'share-request-for-donation-to-donercontact-from-donor'));
				$share_html_header = $template[0]["template_header"];
				$share_html_footer = $template[0]["template_footer"];
				$template_subject = $template[0]["template_subject"];
				$template_body	= $share_html_header.html_entity_decode(htmlspecialchars_decode($template[0]['template_content'])).$share_html_footer;
				
				$search_key 	= array('[FundraiserImage]','[FundraiserName]','[FundraiserSlogan]','[ShareFirst]','[ShareLast]','[PlayerURL]','[PlayerShareLink]','[StartFundraiser]','[UseOfDonations]','[bgcolor]','[fontcolor]','[selected_color]');
				$search_val		= array($fundimg,$fundraiser_name,$fundraiser_slog,$firstname,$lastname,$player_url,$player_share_url,$start_fundraiser,$fundraiser_name,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_bgcolor);
				$mail_content	= str_replace($search_key,$search_val,$template_body);
				$from = 'invite@thanksforsupporting.com';
				$subject = "ThanksForSupporting - ".$template_subject;
				$mail_body = $mail_content;
				
			}
		}
		redirect("admin/cron-mail-list");
	}

	public function cron_mail_invite_contact_donor2()
	{
		$fund_details = $this->Cronmail_model->fundraiser_five_days_before_end();
		if(count($fund_details)>0)
		{
			$encrypt_key 	= $this->config->item('encryption_key');
			$template = $this->Cronmail_model->view('tbl_mail_template',array('template_slug'=>'2nd-reminder-to-invite-donors-to-player-from-fundraiser'));
			
			foreach($fund_details as $fund)
			{
				$fund_email 		= $fund["fund_email"];
				$fundraiser_id		= $fund['id'];
				$fundraiser_name	= $fund['fund_username'];
				$fundraiser_start	= $fund['fund_start_dt'];
				$fundraiser_end		= $fund['fund_end_dt'];
				$fundraiser_email	= $fund['fund_email'];
				$fundraiser_no		= $fund['fund_contact'];
				$fundraiser_goal	= $fund['fund_individual_goal'];
				
				$player_id			= $fund['player_id'];
				$player_first		= $fund['player_fname'];
				$player_last		= $fund['player_lname'];
				$player_goal		= $fund['player_goal'];
				$player_email		= $fund['player_email'];
				$player_password	= $fund['player_pass'];
				$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
				$player_login_url	= base_url('admin/login');
				$start_fundraiser	= base_url("start-fundraiser");
				$selected_color		= $fund["fund_color_rgb"];
				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[selected_color]');
				$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$selected_color);
				$email_content		= str_replace($search_key,$search_val,$template_body);

				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				//$config['smtp_host']= "localhost";
				//$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				if($bval){
				$this->Cronmail_model->edit('tbl_player',array("cron_mail_reminder2"=>1),array('id'=>$player_id));
				}
			}
		}
		redirect("admin/cron-mail-list");
	}

}