<script src="<?php echo base_url()?>assets/player/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/player/js/additional-methods.js"></script>
<script src="<?php echo base_url()?>assets/player/js/raphael-min.js"></script>
<script src="<?php echo base_url()?>assets/player/js/morris-0.4.1.min.js"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		  <li class="active">Status of Invites</li>
		</ul>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Status of Invites</h2></div>
				<div class="right">
				<!-- 
					<a href="<?php //echo base_url($fslug.'/'.$pslug.'/admin/emailreport/list');?>" class="active">List View</a>
					<a href="<?php //echo base_url($fslug.'/'.$pslug.'/admin/emailreport/graph')?>">Graph View</a>
			 -->
				</div>
			</div>
			<div class="total_info">
				<div class="row">
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Total Invites sent</p>
							<span><?php echo $report_chart["sent"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Unopened Emails</p>
							<span><?php echo $report_chart["unopen"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Opened Emails</p>
							<span><?php echo $report_chart["open"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Web Visits</p>
							<span><?php echo $report_chart["visit"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Donated</p>
							<span><?php echo $report_chart["donate"];?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
					<tr>
						<th>Contact Name</th>
						<th>Invited</th>
						<th>Reminder</th>
						<th>Opened</th>
						<th>Clicked</th>
						<th>Donated</th>
						<th>Say Thanks</th>
						<th class="text-right">
							<a href="#" style="color: #333"><i class="fa fa-list-ul"></i>&nbsp;&nbsp;Sort</a>
						</th>
					</tr>
					</thead>
					<tbody>
					<?php
					$date	 = strtotime(date("Y-m-d"));
					$st_date = strtotime($start_date);

					if($date >= $st_date){
					if(count($report_list)==0){
					?>
                    <tr>
                    <td colspan="9">
                    <div style="width:50%; margin:auto; text-align:center;">
                    <h5>You haven't invited any of your contacts to donate yet. Invite your contacts to start receiving donations.</h5>
					<form action="<?php echo base_url($fslug.'/'.$pslug.'/admin/invitetoall');?>" method="POST">
					<input type="hidden" name="goto" value="step2"/>
					<input type="submit" name="info_submit" class="btn34" value="Invite Your Contacts" style="margin-top:12px; width:300px;padding:6px 25px;">
					</form>  
                    </div>
                    </td>
                    </tr>
					<?php
					}
					?>
					<?php
					if(count($report_list)>0){
						foreach($report_list as $report_li){
					?>
						<tr>
							<td><?php echo $report_li["to_email"];?></td>
							<td>
							<?php 
							if($report_li["sent"] == 1)
							{
								echo date('m-d-Y',strtotime($report_li["sent_time"]));
							}
							?>
							</td>
							<td></td>
							<td>
							<?php 
							if($report_li["sent"] == 1 AND $report_li["opened"] == 1)
							{
								echo date('m-d-Y',strtotime($report_li["opened_time"]));
							}
							?>
							</td>
							<td>
							<?php 
							if($report_li["sent"] == 1 AND $report_li["opened"] == 1 AND $report_li["visited"] == 1)
							{
								echo date('m-d-Y',strtotime($report_li["visited_time"]));
							}
							?>
							</td>
							<td>
							<?php 
							if($report_li["sent"] == 1 AND $report_li["opened"] == 1 AND $report_li["visited"] == 1 AND $report_li["donated"] == 1)
							{
								echo date('m-d-Y',strtotime($report_li["donated_time"]));
							}
							?>
							</td>
							<td>
							<?php
							if($report_li["thanks"] == 1){
								echo date("m-d-Y", strtotime($report_li["thanks_datetime"]));
							}else{
							?>
							<a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($report_li["id"]);?>" style="text-decoration:underline;">Send Email</a>
							<?php	
							}
							?>
							</td>
							<td data-th="Date" class="text-right"><small><?php //echo $date;?></small></td>
						</tr>
					<?php
						}
					}
					?>
					<?php
					}else{
					?>
					<tr>
                    <td colspan="9">
                    <div style="width:50%; margin:auto; text-align:center;">
                    <h5>You cannot invite your contacts until the campaign starts</h5>
                    </div>
                    </td>
                    </tr>
					<?php
					}
					?>
					</tbody>
					<tfoot>
					<?php 
					if(count($report_list)>0 AND $date >= $st_date){
					
					
					?>
						<tr>
							<td colspan="4" class="text-center"><span class="grn2">Thats all</span></td>
						</tr>
					<?php
					
					}
					?>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="mail_pro" class="modal fade" role="dialog"></div>
<script type="text/javascript">
function thanksent(ref)
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>player/admin/getMailfromReportsp",
		data      :"pslug=<?php echo $pslug;?>&report="+ref.rel,
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#mail_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#mail_pro").html(response);
			$("#mail_pro").modal('show');               
		}
    });
}
</script>