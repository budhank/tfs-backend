<div id="page-wrapper">
				<div class="full_top_wrp bg_wht">
					<ul class="breadcrumb">
					  <li class="active">Manage Rewards</li>
					</ul>
					<div class="tab_mnu">
						<ul>
							<li class="active"><a href="#">DONATIONS</a></li>
							<li><a href="rewards.html">REWARDS</a></li>
						</ul>
					</div>
				</div>
          		<div class="container-fluid">
          			<div class="bg_wht mar_t_15 tot_pad">
						<div class="tit1_sec clearfix">
							<div class="left"><h2>Donations & Rewards</h2></div>
							<a href="#" class="btn_round pull-right" data-toggle="modal" data-target="#add_edit_don">Add new Donation amount</a>
						</div>
       					
        				<div class="donations_tbl">
        					<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<th width="25%">Donation amount</th>
										<th>Reward</th>
										<th width="25%" class="text-right">&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td data-th="Donation amount"><b>$50</b></td>
										<td data-th="Reward">Reward 1: Dunkin Donuts 12 $ off</td>
										<td data-th="" class="text-right">
											<a href="#" class="grn2" data-toggle="modal" data-target="#add_edit_don">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="#" class="grn2">Remove</a>
										</td>
									</tr>
									<tr>
										<td data-th="Donation amount"><b>$100</b></td>
										<td data-th="Reward">Reward 1: Dunkin Donuts 12 $ off</td>
										<td data-th="" class="text-right">
											<a href="#" class="grn2" data-toggle="modal" data-target="#add_edit_don">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="#" class="grn2">Remove</a>
										</td>
									</tr>
									<tr>
										<td data-th="Donation amount"><b><50</b></td>
										<td data-th="Reward">Reward 1: Dunkin Donuts 12 $ off</td>
										<td data-th="" class="text-right">
											<a href="#" class="grn2" data-toggle="modal" data-target="#add_edit_don">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="#" class="grn2">Remove</a>
										</td>
									</tr>
									<tr>
										<td data-th="Donation amount"><b>>100</b></td>
										<td data-th="Reward">Reward 1: Dunkin Donuts 12 $ off</td>
										<td data-th="" class="text-right">
											<a href="#" class="grn2" data-toggle="modal" data-target="#add_edit_don">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="#" class="grn2">Remove</a>
										</td>
									</tr>
								</tbody>
								<!--<tfoot>
									<tr>
										<td colspan="6" class="text-center"><a class="grn2" href="#">Load more</a></td>
									</tr>
								</tfoot>-->
							</table>
        				</div>
          			</div>
				</div>
        </div>