<form name="mail_sending" id="mail_sending" method="post" enctype="multipart/form-data" action=""> 
<input type="hidden" name="hid_pid" id="hid_pid" value="<?php echo base64_encode($playerInfo[0]['id']);?>"/>
<input type="hidden" name="hid_did" id="hid_did" value="<?php echo base64_encode($donorInfo[0]['id']);?>"/>
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left">
				<h2>Thanks for Supporting</h2>
				<div class="ms" style="display:none;"></div>
				</div>
			</div>
			<div class="add_plr_wrp">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="frm">
							<p>Thank you so much for your donation. I really appreciate you thinking of me and you couldn't have chosen a more perfect present for me. Again, thanks very much.</p>
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
					<input type="submit" name="submit" value="Ok" class="btn_round"/>
					<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
			</div>
      </div>
    </div>
</div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	$("#mail_sending").validate({
        submitHandler: function(form) {
        $.ajax({
        url:'<?php echo base_url();?>player/admin/sendingThanksp',
        type:'POST',
        data: $("#mail_sending").serialize(),
        dataType:"json",
        beforeSend: function(){
           //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
        },
        success:function(results){
            if(results.msg==1){
            $('.ms').html('Mail has been successfully sent!!');
            $('.ms').show();
            setTimeout(function() {            
                location.reload();
                }, 3000);
            }
            else{
                $('.ms').html('Sending fail');
				$('.ms').show();
            }            
        }
       });
      }
	});   
});
</script>    
