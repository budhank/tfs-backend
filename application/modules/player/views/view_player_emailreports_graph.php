<script src="<?php echo base_url()?>assets/fundraiser/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/fundraiser/js/additional-methods.js"></script>
<script src="<?php echo base_url()?>assets/fundraiser/js/raphael-min.js"></script>
<script src="<?php echo base_url()?>assets/fundraiser/js/morris-0.4.1.min.js"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		  <li class="active">Status of Invites</li>
		</ul>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Status of Invites</h2></div>
				<div class="right">
					<a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/emailreport/list');?>">List View</a>
					<a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/emailreport/graph')?>" class="active">Graph View</a>
				</div>
			</div>
			<div class="filter_sec">
				<h3>Filters:</h3>
				<div class="filter_sec_inner">
					<ul>
						<li>
							<label>From</label>
							<input type="text" class="datepicker" value="All Time" readonly>
						</li>
						<li>
							<label>To</label>
							<input type="text" class="datepicker" value="All Time" readonly>
						</li>
						<li>
							<label>Attributes</label>
							<a href="#" class="btn1">All</a>
						</li>
						<li>
							<label></label>
							<a href="#">Sent</a>
						</li>
						<li>
							<label></label>
							<a href="#">Unopened</a>
						</li>
						<li>
							<label></label>
							<a href="#">Opened</a>
						</li>
						<li>
							<label></label>
							<a href="#">Donated</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="total_info">
				<div class="row">
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Total Invites sent</p>
							<span><?php echo $report_chart["sent"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Unopened Emails</p>
							<span><?php echo $report_chart["unopen"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Opened Emails</p>
							<span><?php echo $report_chart["open"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Web Visits</p>
							<span><?php echo $report_chart["visit"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Donated</p>
							<span><?php echo $report_chart["donate"];?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="morris-area-chart_sec">
				<?php
				$date	 = strtotime(date("Y-m-d"));
				$st_date = strtotime($start_date);

				if($date >= $st_date){
				if($report_chart["sent"] == 0 && $report_chart["unopen"] == 0 && $report_chart["open"] == 0 && $report_chart["visit"] == 0 && $report_chart["donate"] == 0){
				?>
				<div class="chrt__overloop">
				<div style="width:50%; margin:auto; text-align:center;">
				<h5>You haven't invited any of your contacts to donate yet. Invite your contacts to start receiving donations.</h5>
				<form action="<?php echo base_url($fslug.'/'.$pslug.'/admin/invitetoall');?>" method="POST">
				<input type="hidden" name="goto" value="step2"/>
				<input type="submit" name="info_submit" class="btn34" value="Invite Your Contacts" style="margin-top:12px; width:300px;padding:6px 25px;">
				</form>  
				</div>
				</div>
				<?php
				}}else{
				?>
				<div class="chrt__overloop">
				<div style="width:50%; margin:auto; text-align:center;">
				<h5>You cannot invite your contacts until the campaign starts</h5> 
				</div>
				</div>
				<?php
				}
				?>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="morris-area-chart_inr">
							<div id="morris-area-chart"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($){
	// Morris Line
	Morris.Line({
		element: 'morris-area-chart',
		data: [
			{ y: '2006', a: 100},
			{ y: '2007', a: 75},
			{ y: '2008', a: 50},
			{ y: '2009', a: 75},
			{ y: '2010', a: 50},
			{ y: '2011', a: 75},
			{ y: '2012', a: 100}
		],
		xkey: 'y',
		ykeys: ['a'],
		labels: ['Series A']
	});
	
});
</script>