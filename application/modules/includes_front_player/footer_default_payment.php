<!--<div class="div_loading" id="div_loading" style="display:none">loading...</div>-->
<footer class="footer2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-7 c-number">
                <p>Looking to Raise Money for Your School, Team, Youth Group or Charity?</p>
                <p class="txt-green">Call <b>800-590-4160</b> to offer the "Fundraiser that gives back"</p>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 f-logo">
                <img src="<?php echo base_url()?>assets/images/g-coupon.png" alt="Geo-coupon">
            </div>
            <div class="col-xs-12 col-sm-3 col-md-2">
                <p>Enjoy Savings...</p>
                <p class="bold text-right">...All Year</p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
