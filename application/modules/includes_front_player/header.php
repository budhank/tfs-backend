<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Thanksforsupporting</title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bs.css'); ?>" rel="stylesheet"/>
    <!-- Font-Awesome -->
    <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet"/>
    <!-- Custom -->
    <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/jquery-ui.css'); ?>" rel="stylesheet"/>
    <script src="<?php echo base_url('assets/js/jquery-3.1.1.min.js'); ?>"></script>    
    <script src="<?php echo base_url('assets/js/jquery-ui.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bs.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/additional-methods.js'); ?>"></script>
    <!--<script src="<?php //echo base_url('assets/admin/js/custom.js'); ?>"></script>-->
	<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5ad5ec17d0b9d300137e3a12&product=custom-share-buttons"></script>
</head>
<body>
<header class="site-header bg-white">
	<div class="container">
		<div class="row">
			<div class="col-xs-5 col-sm-4 col-md-3 col-lg-2">
				<img src="<?php echo base_url()?>assets/images/logo.png" class="logo" alt="logo">
				<span class="context">Thanks for<br><span class="txt-support">Supporting</span></span>
			</div>
			<div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
			<?php
			if($found_flag!=0)
			{
			?>
				<div class="top-nav pull-right">
				<?php 
				$smess=$this->session->userdata('err_msg'); 
				if(isset($smess) && $smess!="")
				{
				echo $this->session->userdata('err_msg');
				$this->session->unset_userdata('err_msg'); 
				}
				?>
				<?php
				if($this->session->userdata('player_id')=="")
				{
				?>
					<a href="<?php echo base_url('admin/login');?>" class="grn2"> Log In </a>
				<?php 
				}
				?>
				</div>
			<?php    
			} 
			?>    
			</div>
		</div>
	</div>
</header>
<?php
/*
<script type="text/javascript">
function openLogin()
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url('frontplayer/getLoginOpenAjax'); ?>",
		data      :"fslug=<?php echo $this->uri->segment(2);?>",
		dataType  :"html",
		beforeSend: function(){
			$("#fundFetModal").modal('show');
			$('#div_loading').show();
		},
		success: function(response){ 
			$('#div_loading').hide();               
			$("#loginModal").html(response);
			$("#loginModal").modal('show');               
		}
	});	
}
</script>
<div id="loginModal"  class="modal fade" role="dialog"></div>
*/
?>