<?php
class Front_model extends CI_Model {
	public function view_all($table_name)
    {
		$query = $this->db->get($table_name);
		return $query->result_array(); 	
    }
	public function view_limit($table,$where='',$limit,$offset)
    {
		$this->db->select('*');
		$this->db->from($table);
		if($where!='')
		{
		$this->db->where($where);	
		}
		$this->db->limit($offset,$limit);	
		
		$query = $this->db->get();
		return $query->result_array(); 	
    }
	public function view($table_name, $where)
    {
		$query = $this->db->get_where($table_name, $where);
		return $query->result_array(); 
    }
	public function view_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function add($table_name, $data)
    {
		return $this->db->insert($table_name, $data); 
	}  
	public function edit($table_name, $data, $where)
    {
		return $this->db->update($table_name, $data, $where);
	}
	public function remove($table_name, $where)
    {
		return $this->db->delete($table_name, $where); 
	}
	public function last_insert_id()
	{
		return $this->db->insert_id();
	}
	public function last_qry()
	{
		echo $this->db->last_query();
	}	
	public function login_check($table,$where){
		   $this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query;
	}	
	public function encry_login_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function func_common_listing($table,$field,$ord)
	{
        $this->db->from($table);
        if($field!='' && $ord!=''){
        $this->db->order_by($field, $ord);
        }
        $query = $this->db->get(); 
        return $query->result_array();
	}
	public function donor_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
}
?>