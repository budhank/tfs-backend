<!-- Green Bar start-->
<?php
if($player_details[0]['player_image']==""){
$playerimg =  base_url()."assets/images/noimage-150x150.jpg";   
}else{
$playerTimg = explode(".",$player_details[0]['player_image']);	
$playerimg  =  base_url()."assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1];   
} 
if($fund_details[0]['fund_image']==""){
$fundimg =  base_url()."assets/images/noimage-150x150.jpg";   
}else{
$fundTimg = explode(".",$fund_details[0]['fund_image']);	
$fundimg  =  base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
} 
$player_name 	= $player_details[0]['player_fname'].' '.$player_details[0]['player_lname'];
$player_title	= $player_details[0]['player_title'];
?>
<section class="bg-green subject-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-xs-3 col-md-2"><img src="<?php echo $fundimg;?>" alt="Subject Image" class="img-circle"/></div>
      <div class="col-xs-9  col-md-10 subject-title"> <span class="item-title"><?php echo $player_title;?></span>
        <!--<p class="item-info">Lorem Ipsum dolor sit amet</p>-->
      </div>
    </div>
  </div>
</section>
<!-- Green Bar ends--> 

<!-- Home Content start--> 
<!-- Row 1 starts -->
<section class="bg-white inner-content-wrapper">
		  <div class="container">
			  <div class="row">
				  <div class="col-xs-12 text-center page-title">
				  	<img src="<?php echo base_url();?>assets/images/user.png" alt="User Image"/>
					  <span>Support Jake</span>
				  </div>
				  <div class="progress-meter">
					  <span class="meter-block first active" data-title="Donate">1</span>
					  <span class="meter-block middle" data-title="Thank you">2</span>
					  <span class="meter-block last" data-title="Share">3</span>
				  </div>
				  <div class="clearfix"></div>
				  <div class="col-xs-12 col-sm-8 col-md-6 form-content-wrapper">
				  	 <form action="successful-payment.html" method="post">
					  <div class="form-group">
					  	<div class="col-xs-12 col-sm-6">
						<label for="amount">Amount</label><br>
						<input type="email" class="form-control" id="amount" placeholder="$45">
					  </div>
					  <div class="col-xs-12 col-sm-6">
						<label for="donate-avg-amount"><input type="radio" name="Donate-amount"/> Donate average amount - $45</label><br>
						<label for="donate-rest-amount"><input type="radio" name="Donate-amount"/> Donate rest amount - $250</label>
					  </div>
						  <div class="clearfix"></div>
						 </div>
					  <div class="form-group">
					  	<div class="col-xs-12">
						<label for="email">Supporter's email</label><br>
						<input type="email" class="form-control" id="email" readonly="readonly" placeholder="<?php echo $this->session->userdata('donor_email');?>">
					  </div>
					  <div class="clearfix"></div>
					  </div>
					  <div class="form-group">
					  	<div class="col-xs-12 col-sm-6">
						<label for="firstname">First Name</label><br>
						<input type="text" class="form-control" id="firstname" placeholder="Firstname" value="<?php echo $this->session->userdata('donor_fname');?>">
					  </div>
					  <div class="col-xs-12 col-sm-6">
						<label for="lastname">Last Name</label><br>
						<input type="text" class="form-control" id="lastname" placeholder="Lastname" value="<?php echo $this->session->userdata('donor_lname');?>">
					  </div>
					  <div class="clearfix"></div>
						 </div>
						 <div class="form-group">
					  	<div class="col-xs-12 col-sm-6">
						<label for="card-number">Credit card number</label><br>
						<input type="text" class="form-control" id="card-number" placeholder="4584-">
					  </div>
					  <div class="col-xs-12 col-sm-3">
						<label for="lastname">Expiration</label><br>
						  <select class="form-control">
							  <option>Month</option>
						  </select>
					  </div>
					  <div class="col-xs-12 col-sm-3">
						  <select class="form-control" style="margin-top:24px;">
							  <option>Year</option>
						  </select>
					  </div>
					  <div class="clearfix"></div>
						 </div>
						 <div class="form-group">
					  	<div class="col-xs-12 col-sm-6">
						<label for="card-address">Card address country</label><br>
						<select class="form-control">
							  <option>Please select the address</option>
						  </select>
					  </div>
					  <div class="col-xs-12 col-sm-3">
						<label for="cvc">CVC</label><br>
						 <input type="text" class="form-control" id="cvc" placeholder="CVC">
					  </div>
					  <div class="clearfix"></div>
						 </div>
						 <div class="form-group text-center">
					  <button type="submit" class="btn btn-default btn-green">Submit</button><br>
						 <a href="<?php echo base_url().'player/'.$pslug;?>" style="text-decoration: underline;">Cancel</a>
						 </div>
					</form> 
				  </div>
			  </div>
		  </div>
	  </section>
<!-- Home Content ends-->