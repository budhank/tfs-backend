<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller{
    public $data=array();
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->data['found_flag'] = 1;
		$this->load->model('Front_model');
	}

	public function index()
	{
		/*
		echo "Front View for players";
		if($this->session->userdata('player_id')!="")
		{
			redirect('player/'.$this->session->userdata('player_urlname').'/home');
		}
		else
		{
			redirect('player/login');
		}
		redirect('home');
		*/
		$this->data = array();
		//echo 'hi'; die;
		$this->load->view('../../includes_front/header',$this->data);  
		$this->load->view('view_dashboard',$this->data);
	}

	public function home()
	{
		$this->data = array();
		$this->load->view('../../includes_front/header',$this->data);  
		$this->load->view('view_dashboard',$this->data);
	}
	
	public function getTrackimage($report_id)
	{
		$updata = array('opened'=>1,'opened_time'=>date("Y-m-d H:i:s"));
		$where  = array('id'=>$report_id);
		$this->Front_model->edit('tbl_email_share_reports',$updata,$where);
		echo base_url().'assets/images/trans.png';
	}
	
	public function getTrackimagefund($player_id)
	{
		$updata = array('opened'=>1,'opened_datetime'=>date("Y-m-d H:i:s"));
		$where  = array('id'=>$player_id);
		$this->Front_model->edit('tbl_player',$updata,$where);
		echo base_url().'assets/images/trans.png';
	}
	
	public function unsubscribe($contact_id){		
	
		$contact_where = array('id'	=> $contact_id);
		$contact_update = array('status'	=> 3);
		$this->Front_model->edit('tbl_player_contacts', $contact_update, $contact_where);
		$this->load->view('../../includes_front/header',$this->data);  
		$this->load->view('form_donors_unsubscribe',$this->data);
	}
}
