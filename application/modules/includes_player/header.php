<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title></title>
    <link href="<?php echo base_url()?>assets/player/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url()?>assets/player/css/jquery-ui.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/player/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/player/css/custom_style.css" rel="stylesheet" type="text/css">
    
    <script src="<?php echo base_url()?>assets/player/js/jquery-3.1.1.min.js"></script>    
    <script src="<?php echo base_url()?>assets/player/js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>assets/player/js/custom.js"></script>
    <script src="<?php echo base_url()?>assets/player/js/bootstrap.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--[endif]-->
</head>
    <body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<?php 
				if($this->session->userdata('player_id')!=""){
					$url_home=base_url()."player/admin/".$this->session->userdata('player_urlname')."/home";
				}
				else{
					$url_home=base_url()."player/login";
				}
				?>
                <a class="navbar-brand" href="<?php echo $url_home; ?>">
				<img src="<?php echo base_url()?>assets/player/images/logo.png" alt="logo"/>
				</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li><a href="#"><i class="fa fa-bell-o"></i></a></li>
                <li><a href="#"><span class="cd">CD</span></a></li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <div class="site-tag"><i>“The Fundraiser That Gives Back™”.</i></div>
                <?php 
				$ffslug = $this->session->userdata('fundraiser_urlname');
				$ppslug = $this->session->userdata('player_urlname');
				if($this->session->userdata('player_id')!=""){
				$mn_arr = array('home','contacts','invite','checkoutweb');
				$pp_page = $this->uri->segment(4);
				?>
				<ul class="nav navbar-nav side-nav">
					<li <?php //if($pp_page=="home"){ echo "class='active'";}?>>
					<a href="<?php //echo base_url($ffslug.'/'.$ppslug.'/admin/home')?>">Dashboard</a>
					</li>
                    <!--<li <?php //if(in_array($pp_page,$mn_arr)){ echo "class='active'";}?>>
					<a href="<?php //echo base_url($ffslug.'/'.$ppslug.'/admin/home')?>">Setup Guide</a>
					</li>
                    <li <?php //if($pp_page=="donors"){ echo "class='active'";}?>>
					<a href="<?php //echo base_url($ffslug.'/'.$ppslug.'/admin/donors')?>">Donors/ Donations</a>
					</li>
                    <li <?php //if($pp_page=="emailreport"){ echo "class='active'";}?>>
					<a href="<?php //echo base_url($ffslug.'/'.$ppslug.'/admin/emailreport/list')?>">Status of Invites</a>
					</li>
					<?php /*
                    <li <?php if($pp_page=="profile"){ echo "class='active'";}?>>
					<a href="<?php echo base_url()?>player/admin/<?php echo $this->session->userdata('player_urlname'); ?>/profile">Profile</a>
					</li>
					*/ ?>
                    <li <?php //if($pp_page=="settings"){ echo "class='active'";}?>>
					<a href="<?php //echo base_url($ffslug.'/'.$ppslug.'/admin/settings')?>">Password</a>
					</li>-->
                    <li><a href="<?php echo base_url($ffslug.'/'.$ppslug.'/admin/logout')?>">Log Out</a></li>
                </ul>
                <?php } ?>
            </div>
            <!-- /.navbar-collapse -->
        </nav>     
    
