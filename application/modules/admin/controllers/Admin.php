<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_AdminController{

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('admin/admin_model');
	}

	public function index(){
		redirect('admin/login');
	}

	//*************Start of Dashboard ***********************//
	public function home(){
		$rows = $this->admin_model->get_amount_raised();
		$data['amount_raised'] = $rows[0]['amount_raised'];
		$data['count_fundraiser'] = $this->admin_model->fundraiser_email_check();
		$data['count_player'] = $this->admin_model->fundraiser_email_check('','tbl_player');
		$data['count_donors'] = $this->admin_model->fundraiser_email_check('','tbl_donors_payment');
		$order_by = ($this->uri->segment(3))?$this->uri->segment(3):'day';
		$data['donation_day'] = $this->admin_model->get_donors_payments($order_by);
		
		$data['fundraisers_list'] = $this->admin_model->view('tbl_fundraiser',array('active_fundraiser'=>'1'));
		
		$data['active_page'] = 'dboard';
		//echo '<pre>';print_r($data);die;
		$this->load->view('../../includes/header');        
		$this->load->view('view_dashboard',$data);
		$this->load->view('../../includes/footer');
	}
	//*************End of Dashboard *************************//

	//************Start of Fundraisers *********************//
	public function fundraisers()
	{
		$this->session->unset_userdata('order_by');
		$this->session->unset_userdata('title_by');
		if($this->input->post('mode')=='del'){			
			$this->admin_model->remove('tbl_player',array('player_added_by' => $this->input->post('row_id')));
			$this->admin_model->remove('tbl_payment_transfer',array('fundraiser_id' => $this->input->post('row_id')));
			$this->admin_model->remove('tbl_donors_payment',array('fund_id' => $this->input->post('row_id')));
			$this->admin_model->remove('tbl_fundraiser',array('id' => $this->input->post('row_id')));
			$succ_msg="Successfully Deleted";
			$this->session->set_userdata('succ_msg',$succ_msg);
		}
		   
		$data['active_page'] = 'fundraiser';
		$limit  = 0;
		$offset = $this->config->item('pageoffset');
			$fund_rec = $this->admin_model->fundraiser_list($limit,$offset);
	
	   
		$data['rec_listing'] = $fund_rec;
		$data['offset'] = $offset;
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraisers',$data);
		$this->load->view('../../includes/footer');
	}

    public function reportlist($fslug)
    {
        $this->load->model('fundraiser/fundraiser_model');
        $report_list = array();
        $limit 	= 0;
        $offset	= 999999999999;
        $fund_dt = $this->fundraiser_model->fundraiser_getID($fslug);
        $fund_id = $fund_dt[0]["id"];
        $report_list = $this->fundraiser_model->fundraiser_players($fund_id,$limit,$offset);
        $report_chart = $this->fundraiser_model->participants_report($fund_id);
        $total_donations = $this->fundraiser_model->total_player_fund($fslug);
        $data['total_payments'] = $total_donations;
        $data['report_list'] 	= $report_list;
        $data['report_chart'] 	= $report_chart;
        $data['fund_details'] 	= $fund_dt;
        $data["fslug"] 			= $fslug;
        /*print_r($data);
        exit;*/
        $this->load->view('../../includes/header');
        $this->load->view('fundraiser/view_fundraiser_emailreports_list',$data);
        $this->load->view('../../includes/footer');

    }
    public function reportgraph($fslug)
    {
        $this->load->model('fundraiser/fundraiser_model');
        $fund_dt = $this->fundraiser_model->fundraiser_getID($fslug);
        $fund_id = $fund_dt[0]["id"];
        $report_chart = $this->fundraiser_model->participants_report($fund_id);
        $total_donations = $this->fundraiser_model->total_player_fund($fslug);
        $data['total_payments'] = $total_donations;
        $data['report_chart'] 	= $report_chart;
        $data['fund_details'] 	= $fund_dt;
        $data["fslug"] 			= $fslug;
        $this->load->view('../../includes/header');
        $this->load->view('fundraiser/view_fundrasier_emailreports_graph',$data);
        $this->load->view('../../includes/footer');

    }

    public function reportlistinvited($fslug)
    {
        $this->load->model('fundraiser/fundraiser_model');
        $report_list = array();
        $limit 	= 0;
        $offset	= 999999999999;
        $fund_dt = $this->fundraiser_model->fundraiser_getID($fslug);
        $fund_id = $fund_dt[0]["id"];
        $report_list = $this->fundraiser_model->fundraiser_players($fund_id,$limit,$offset);
        $report_chart = $this->fundraiser_model->participants_report($fund_id);
        $total_donations = $this->fundraiser_model->total_player_fund($fslug);
        $data['total_payments'] = $total_donations;
        $data['report_list'] 	= $report_list;
        $data['report_chart'] 	= $report_chart;
        $data['fund_details'] 	= $fund_dt;
        $data["fslug"] 			= $fslug;
        /*print_r($data);
        exit;*/
        $this->load->view('../../includes/header');
        $this->load->view('fundraiser/view_fundraiser_emailreportsinvited_list',$data);
        $this->load->view('../../includes/footer');

    }
    public function reportgraphinvited($fslug)
    {
        $this->load->model('fundraiser/fundraiser_model');
        $fund_dt = $this->fundraiser_model->fundraiser_getID($fslug);
        $fund_id = $fund_dt[0]["id"];
        $report_chart = $this->fundraiser_model->participants_report($fund_id);
        $total_donations = $this->fundraiser_model->total_player_fund($fslug);
        $data['total_payments'] = $total_donations;
        $data['report_chart'] 	= $report_chart;
        $data['fund_details'] 	= $fund_dt;
        $data["fslug"] 			= $fslug;
        $this->load->view('../../includes/header');
        $this->load->view('fundraiser/view_fundrasier_emailreports_graphinvited',$data);
        $this->load->view('../../includes/footer');

    }

	public function information_tab($slug)
	{
		if(isset($_POST["fund_email"]))
		{
			$this->form_validation->set_rules('fund_fname', 'First Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_lname', 'Last Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_email', 'Email', 'trim|required|valid_email');	
			$this->form_validation->set_rules('fund_contact', 'Phone', 'trim|required|xss_clean');	
			
			if ($this->form_validation->run() != FALSE)
			{
				$fundata       = array();
				$fundata["fund_fname"]= $this->input->post('fund_fname');
				$fundata["fund_lname"]= $this->input->post('fund_lname');
				$fundata["fund_email"]= $this->input->post('fund_email');
				$fundata["fund_contact"]= $this->input->post('fund_contact');
				$fundata["fund_terms"]= $this->input->post('fund_terms');
				$return = $this->db->update('tbl_fundraiser', $fundata, array('fund_slug_name'=>$slug));
				if($return){
					redirect('admin/fundraiser/'.$slug.'/organization');
				}
			}
		}
		$data['active_tab']			= 'information'; 
		$data['active_page'] 		= 'fundraiser';
		$limit 						= 0;
		$offset 					= 10;
		$getId						= $this->admin_model->fundraiser_getID($slug);
		$fund_id 					= $getId['0']['id'];
        $data['fslug']				= $slug;
        $data['fundraiserInfo']		= $this->admin_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->admin_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->admin_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->admin_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes/footer');
	}
	
	//Added by Rupak 11/09/2018
	//-------------------------
	
	public function makeTrialAjax(){
		$fslug=$this->input->post('fslug');	
		$pslug=$this->input->post('pslug');
		$fundraiserInfo	= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
		$fundraiserId   = $fundraiserInfo[0]['id'];
	    $playerInfo		= $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug,'player_added_by'=>$fundraiserId));
		if(count($playerInfo>0)){
		$this->db->update('tbl_player', array('free_trial'=>0), array('player_added_by'=>$fundraiserId));	
		$this->db->update('tbl_player', array('free_trial'=>1), array('id'=>$playerInfo[0]['id']));
		}		
	}
	
	public function organization_tab($slug)
	{
		if(isset($_POST["orgz_submit"]))
		{
			//echo "<pre>"; print_r($_REQUEST); die;
			$this->form_validation->set_rules('fund_username', 'Organization Name', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('fund_slogan', 'Short Description', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_city', 'City', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('fund_state', 'State', 'trim|required|xss_clean');
			
			if ($this->form_validation->run() != FALSE)
			{
				$qslug			= $slug;
				$getId			= $this->admin_model->fundraiser_getID($slug);
				$fund_id 		= $getId[0]['id'];
				$fund_username  = $_POST['fund_username'];
				$fund_slogan    = $_POST['fund_slogan'];
				$fund_city      = $_POST['fund_city'];
				$fund_state     = $_POST['fund_state'];
				$col_r     		= $_POST['col_r'];
				$col_g     		= $_POST['col_g'];
				$col_b     		= $_POST['col_b'];
				$col_a     		= $_POST['col_a'];
				$fund_color_hex = $_POST['hex_color'];
				$fund_color_rgb	= 'rgba('.$col_r.', '.$col_g.','.$col_b.','.$col_a.')';
				$fund_font_color= isset($_POST['fund_font_color'])?$this->input->post('fund_font_color'):'';
				$fund_beneficiary_tax_id = $_POST['fund_beneficiary_tax_id'];
				$fundata = array();	
				//------------------------------------Fundraiser Slug Creation--------------------------------
				$slug_check		= $this->admin_model->view_check('tbl_fundraiser',array('fund_slug_name'=>$slug,'slug_change'=>'0'));
				if($slug_check > 0)
				{
					$lastInsId     = $fund_id;    
					$fname_slug    = strip_tags($fund_username);  
					
					$string 	   = str_replace(' ', '-', $fname_slug); // Replaces all spaces with hyphens.
					$st_slug	   = preg_replace('/[^A-Za-z0-9\-#$&^*()+=!~\']/', '', $string); // Removes special chars.
					$jSlug		   = strtolower(rtrim($st_slug,'-'));
					$slug11		   = url_title($jSlug);
					if($this->admin_model->isUrlExists('tbl_fundraiser',$slug11)==1){
					   $slug22 = $slug11.'-'.$lastInsId; 
					}else{
					 $slug22 = $qslug;
					}
					$fundata['fund_slug_name'] = $slug22;
					$fundata['slug_change'] = '1';
				}else{
					 $slug22 = $qslug;
				}
				
				//---------------------------------------------------------------------------------------------
				
				$fundata['fund_username']			= $fund_username;
				$fundata['fund_slogan']				= $fund_slogan;
				$fundata['fund_city']				= $fund_city;
				$fundata['fund_state']				= $fund_state;
				$fundata['fund_color_hex']			= $fund_color_hex;
				$fundata['fund_color_rgb']			= $fund_color_rgb;
				$fundata['fund_font_color']			= $fund_font_color;
				$fundata['fund_beneficiary_tax_id']	= $fund_beneficiary_tax_id;
				$return = $this->db->update('tbl_fundraiser',$fundata,array('fund_slug_name'=>$qslug));
				if($return){
					redirect('admin/fundraiser/'.$slug22.'/bankinfo');
				}else{
					$slug = $qslug;
				}
			}
		}
		$data['active_tab']			= 'organization'; 
		$data['active_page'] 		= 'fundraiser';
		$limit 						= 0;
		$offset 					= 10;
		$getId						= $this->admin_model->fundraiser_getID($slug);
		$fund_id 					= $getId['0']['id'];
        $data['fslug']				= $slug;
        $data['fundraiserInfo']		= $this->admin_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->admin_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->admin_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->admin_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes/footer');
		
	}
	public function bankinfo_tab($slug)
	{
		if(isset($_POST["bank_submit"]))
		{
			
			if($this->input->post('fund_pay_type')=='P'){
				$this->form_validation->set_rules('fund_pay_type', 'Payment Type', 'trim|required|xss_clean');
				$this->form_validation->set_rules('fund_paypal_acc', 'Paypal Account', 'trim|required|xss_clean');	
				$this->form_validation->set_rules('fund_donor_statement', 'Donor Statement', 'trim|required|xss_clean');
			}else{
				$this->form_validation->set_rules('make_check_payable_to', 'Make check payable to', 'trim|required|xss_clean');
				$this->form_validation->set_rules('city', 'CITY', 'trim|required|xss_clean');
				$this->form_validation->set_rules('state', 'STATE', 'trim|required|xss_clean');
				$this->form_validation->set_rules('zip', 'ZIP', 'trim|required|xss_clean');
				$this->form_validation->set_rules('street_address', 'Street Address', 'trim|required|xss_clean');
				
				/*
				$this->form_validation->set_rules('fund_bank_name', 'Bank Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('fund_donor_statement', 'Donor Statement', 'trim|required|xss_clean');
				$this->form_validation->set_rules('fund_routing_number', 'Bank Routing Number', 'trim|required|xss_clean');	
				$this->form_validation->set_rules('fund_account_number', 'Bank Account Number', 'trim|required|xss_clean');	*/	
			}
			
			if ($this->form_validation->run() != FALSE)
			{
				$fundata       = array();
				
				if($this->input->post('fund_pay_type')=='P'){
					/*$fundata["fund_pay_type"]		= $this->input->post('fund_pay_type');
					$fundata["fund_paypal_acc"]		= $this->input->post('fund_paypal_acc');
					$fundata["fund_donor_statement"]= $this->input->post('fund_donor_statement');*/
					$fundata["fund_pay_type"]		= $this->input->post('fund_pay_type');
					$fundata["fund_paypal_acc"]		= $this->input->post('fund_paypal_acc');
					$fundata["fund_donor_statement"]= $this->input->post('fund_donor_statement');
					$fundata["make_check_payable_to"]		= "";
					$fundata["city"]		= "";
					$fundata["state"]		= "";
					$fundata["zip"]		= "";
					$fundata["street_address"]		= "";
				}else{
					/*$fundata["fund_pay_type"]		= $this->input->post('fund_pay_type');
					$fundata["fund_bank_name"]		= $this->input->post('fund_bank_name');
					$fundata["fund_donor_statement"]= $this->input->post('fund_donor_statement');
					$fundata["fund_routing_number"]	= $this->input->post('fund_routing_number');
					$fundata["fund_account_number"]	= $this->input->post('fund_account_number');*/

					$fundata["fund_paypal_acc"]		= "";
					$fundata["fund_pay_type"]		= $this->input->post('fund_pay_type');
					$fundata["fund_donor_statement"]= $this->input->post('fund_donor_statement');
					$fundata["make_check_payable_to"]		= $this->input->post('make_check_payable_to');
					$fundata["city"]		= $this->input->post('city');
					$fundata["state"]		= $this->input->post('state');
					$fundata["zip"]		= $this->input->post('zip');
					$fundata["street_address"]		= $this->input->post('street_address');
				}
				
				$return = $this->db->update('tbl_fundraiser', $fundata, array('fund_slug_name'=>$slug));
				if($return){
					redirect('admin/fundraiser/'.$slug.'/fundraiser');
				}
			}	
		}
		$data['active_tab']			= 'bankinfo'; 
		$data['active_page'] 		= 'fundraiser';
		$limit 						= 0;
		$offset 					= 10;
		$getId						= $this->admin_model->fundraiser_getID($slug);
		$fund_id 					= $getId['0']['id'];
        $data['fslug']				= $slug;
        $data['fundraiserInfo']		= $this->admin_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->admin_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->admin_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->admin_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes/footer');  
	}
	public function fundraiser_tab($slug)
	{
		if(isset($_POST["fundraiser_submit"]))
		{
			$this->form_validation->set_rules('fund_org_goal', 'Overall Donations Goal', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_individual_goal', 'Donation Goal Per Participant', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_start_dt', 'Start Date', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('fund_end_dt', 'End Date', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('fund_des', 'Fundraiser Description', 'trim|required');	
			if ($this->form_validation->run() != FALSE)
			{
				$getId		   = $this->admin_model->fundraiser_getID($slug);
				$fund_id 	   = $getId['0']['id'];
				$fund_des 	   = $this->input->post('fund_des');
				$fund_des_static 	   = $this->input->post('fund_des_static');
				$fund_start_dt = $this->input->post('fund_start_dt');
				$ex_ful_st_dt  = explode('-',$fund_start_dt);
				$ex_ful_st_dt  = $ex_ful_st_dt[2].'-'.$ex_ful_st_dt[0].'-'.$ex_ful_st_dt[1];
				$fund_end_dt   =  $this->input->post('fund_end_dt');
				$ex_ful_en_dt  = explode('-',$fund_end_dt);
				$ex_ful_en_dt  = $ex_ful_en_dt[2].'-'.$ex_ful_en_dt[0].'-'.$ex_ful_en_dt[1];
				$fund_org_goal = str_replace(["$",","],"",$this->input->post('fund_org_goal'));
                $fund_individual_goal = str_replace(["$",","],"",$this->input->post('fund_individual_goal'));

				$media_type    = $this->input->post('media_type');

				$fundata    = array(
				'fund_start_dt'			=> $ex_ful_st_dt,
				'fund_end_dt'			=> $ex_ful_en_dt,
				'fund_org_goal'         => $fund_org_goal,
				'fund_des'        		=> $fund_des,
				'fund_des_static'        => @$fund_des_static,
				'fund_individual_goal'  => $fund_individual_goal);
				$return = $this->db->update('tbl_fundraiser', $fundata, array('fund_slug_name'=>$slug));
				if($return){
					redirect('admin/fundraiser/'.$slug.'/participants');
				}
			}
		}
		$data['active_tab']			= 'fundraiser'; 
		$data['active_page'] 		= 'fundraiser';
		$limit 						= 0;
		$offset 					= 50;
		$getId						= $this->admin_model->fundraiser_getID($slug);
		$fund_id 					= $getId['0']['id'];
        $data['fslug']				= $slug;
        $data['fundraiserInfo']		= $this->admin_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->admin_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->admin_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->admin_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes/footer');
		
	}
	
	
	public function participants_tab($slug)
	{
		
		if(isset($_POST["participant_submit"]))
		{
			//------------------------------------- 
				//***Player Image upload section***
				//-------------------------------------
				$player_image= isset($_FILES['player_image']['name'])?$_FILES['player_image']['name']:"";
				$fpath1 = "";
				$fpath1 = $this->admin_model->setPlayerImage($player_image);
				
				$player_email = $this->input->post('player_email');
				$player_fname = $this->input->post('player_fname');
				$player_lname = $this->input->post('player_lname');
				
			
			if($this->input->post('player_id')):
				$dt_array = array(
				'player_email' => $player_email,
				'player_fname' => $player_fname,
				'player_lname' => $player_lname
				);
				if($fpath1) $dt_array['player_image'] = $fpath1;
				$this->admin_model->edit('tbl_player',$dt_array,array('id'=>$this->input->post('player_id')));
			else:
				$this->form_validation->set_rules('player_email', 'Email', 'trim|required|valid_email');
				$this->form_validation->set_rules('player_fname', 'First Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('player_lname', 'Last Name', 'trim|required|xss_clean');		
			if ($this->form_validation->run() != FALSE){				
				$getId		   = $this->admin_model->fundraiser_getID($slug);
				$fund_id 	   = $getId['0']['id'];				
				
				$encrypt_key = $this->config->item('encryption_key');  
				$player_added_by = $fund_id;
				
				$player_pass  = time();
				$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
				
				$dt_array = array(
				'player_email' => $player_email,
				'player_fname' => $player_fname,
				'player_lname' => $player_lname,
				'player_pass'  => $player_passw,
				'player_image' => $fpath1,
				'player_added_by' => $player_added_by,
				'created' => date("Y-m-d H:i:s")
				);
				$this->admin_model->add('tbl_player',$dt_array);
				
				//-------------------------------------------  
				//Player Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($player_fname);
				$lname_slug    = strip_tags($player_lname);  
				$jSlug         = $fname_slug.'-'.$lname_slug;    
				$playerURL	   = strtolower(url_title($jSlug));
				if($this->admin_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
				   $playerURL = $playerURL.'-'.$lastInsId; 
				}
				$return = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
				if($return){
					//redirect('admin/fundraiser/'.$slug.'/startup');
					$this->session->set_userdata('up_msg','Added successfully');
				}
			}
			endif;
		}
		$data['current_palyer'] = '';
		if($this->uri->segment(5))
		{
			$data['action'] = $this->uri->segment(5);
			$id = $this->uri->segment(6);
			$current_palyer = $this->admin_model->view('tbl_player', array('id'=>$id));
			$data['current_palyer'] = $current_palyer[0];
		}
		
		$data['active_tab']			= 'participants'; 
		$data['active_page'] 		= 'fundraiser';
		$limit 						= 0;
		$offset 					= 10;
		$getId						= $this->admin_model->fundraiser_getID($slug);
		$fund_id 					= $getId['0']['id'];
        $data['fslug']				= $slug;
        $data['fundraiserInfo']		= $this->admin_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->admin_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->admin_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->admin_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes/footer');   
	}
	public function getEmailToAllAjax()
	{
        $data['fslug']		= $this->input->post('fslug');  
        echo $this->load->view('view_custom_mail_modal', $data, true);
	}
	public function email_all_participants()
	{
        $fslug  =$this->input->post('fslug');
		$getId	=$this->admin_model->fundraiser_getID($fslug);
		$fund_id=$getId['0']['id'];
		$players=$this->admin_model->view('tbl_player',array('player_added_by'=>$fund_id));
		$response=array(); 
		if(count($players)>0){
		$recepients = array();
		foreach($recepients as $recp){
		if(filter_var($recp['player_email'], FILTER_VALIDATE_EMAIL)) {
		$recepients[]=$recp['player_email'];
		}
		}
		
		if(!empty($recepients)){
		$header ='<!DOCTYPE html> 
				<html lang="en"> 
				<head> 
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
				<title>ThanksForSupporting</title> 
				<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
				</head> 
				<body> 
				<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;"> 
				<tr> 
				<td align="center">
				<table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0"> 
				<tr> 
				<td style="padding:0 30px;"><div style="text-align: center; border-bottom: 1px solid #ccc; padding:10px 0 20px">
				<img src="'.base_url('assets/images/logo_thanks-for-supporting.png').'"  /></div>
				</td> 
				</tr> 
				</table>
				</td> 
				</tr> 
				</table>';
		$subject=$this->input->post('mail_subject');
		$body   =$this->input->post('mail_content');
		$footer ='</body>
				</html>';
		
				
		$email_content = $header.$body.$footer;
		
		
		//echo $email_content;die;
		$config = array();
		$config['useragent']= "CodeIgniter";
		$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
		$config['protocol'] = "mail";
		$config['smtp_host']= "localhost";
		$config['smtp_port']= "25";
		$config['mailtype'] = 'html';
		$config['charset']	= 'iso-8859-1';
		$config['newline']  = "\r\n";
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);   
		$this->email->from('invite@thanksforsupporting.com');
		$this->email->to($recepients); 
		$this->email->subject('ThanksForSupporting - '.$subject); 
		$this->email->message($email_content);
		$bval=$this->email->send();	
		if($bval){
			$this->session->set_userdata('up_msg','Successfully send');
		}else{
			$this->session->set_userdata('up_msg','Oops! Something went wrong');
		}
		}else{
			$this->session->set_userdata('up_msg','Oops! Something went wrong');
		}
		}else{
			$this->session->set_userdata('up_msg','Oops! Something went wrong');
		}
		redirect('admin/fundraiser/'.$fslug.'/participants');
	}
	
	public function bulk_player_upload($fslug)
	{
		$ext 		= pathinfo($_FILES['bulk_player']['name'],PATHINFO_EXTENSION);
		$getId		= $this->admin_model->fundraiser_getID($fslug);
		$fund_id	= $getId['0']['id'];
		if($ext=='xls' || $ext=='xlsx')
		{
			$this->load->library('excel');
			//Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/)	 
			$configUpload['upload_path'] = FCPATH.'assets/uploads/excel/';
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '5000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('bulk_player');	
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension=$upload_data['file_ext'];    // uploded file extension
			
			$objReader		= PHPExcel_IOFactory::createReader('Excel2007');// For excel 2007 	  
			//Set to read only
			$objReader->setReadDataOnly(true);
			//Load excel file
			$objPHPExcel	= $objReader->load(FCPATH.'assets/uploads/excel/'.$file_name);		 
			$totalrows		= $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows available in excel      	 
			$objWorksheet	= $objPHPExcel->setActiveSheetIndex(0);
			$encrypt_key	= $this->config->item('encryption_key');
			$return			= false; 
			//loop from first data untill last data
			for($i=2;$i<=$totalrows;$i++)
			{	
				$player_fname = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$player_lname = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$player_email = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				$player_added_by = $fund_id;
				$player_pass  = time();
				$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
				if(filter_var($player_email, FILTER_VALIDATE_EMAIL)){
					$dt_array = array(
					'player_email' => $player_email,
					'player_fname' => $player_fname,
					'player_lname' => $player_lname,
					'player_pass'  => $player_passw,
					'player_added_by' => $player_added_by,
					'created' 		=> date("Y-m-d H:i:s")
					);
					$this->admin_model->add('tbl_player',$dt_array);
					//-------------------------------------------  
					//Player Slug Creation
					//-------------------------------------------
					$lastInsId     = $this->db->insert_id();    
					$fname_slug    = strip_tags($player_fname);
					$lname_slug    = strip_tags($player_lname);  
					$jSlug         = $fname_slug.'-'.$lname_slug;    
					$playerURL	   = strtolower(url_title($jSlug));
					if($this->admin_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
					   $playerURL = $playerURL.'-'.$lastInsId; 
					}
					$return = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
				}
			}
			if($return){
				unlink('./assets/uploads/excel/'.$file_name); //File Deleted After uploading in database .
				$this->session->set_userdata('up_msg','Added successfully');
			}
		}
		else
		{
			$dir_path = FCPATH.'assets/uploads/excel/';
			$configUpload['upload_path'] = $dir_path;
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '5000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('bulk_player');	
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension=$upload_data['file_ext'];    // uploded file extension
			
			$encrypt_key	= $this->config->item('encryption_key');
			
			$filepath=$dir_path.$file_name;
			$ntag='\r\n';
			$estag='\"';

			$sqlPl="LOAD DATA LOCAL INFILE '".addslashes($filepath)."' INTO TABLE `tbl_player_exception` 
			FIELDS TERMINATED BY ',' 
			OPTIONALLY ENCLOSED BY '".$estag."' 
			LINES TERMINATED BY '".$ntag."' 
			IGNORE 1 LINES (player_fname, player_lname, player_email)";
			$this->db->query($sqlPl);
			$qry = $this->db->query('SELECT * FROM tbl_player_exception');
			$qry_arr = $qry->result_array();
			$return = false; 
			foreach($qry_arr as $arr){
				$player_fname = $arr["player_fname"];
				$player_lname = $arr["player_lname"];
				$player_email = $arr["player_email"];
				$player_added_by = $fund_id;
				$player_pass  = time();
				$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
				if(filter_var($player_email, FILTER_VALIDATE_EMAIL)){
				$dt_array = array(
				'player_email' => $player_email,
				'player_fname' => $player_fname,
				'player_lname' => $player_lname,
				'player_pass'  => $player_passw,
				'player_added_by' => $player_added_by,
				'created' => date("Y-m-d H:i:s")
				);
				$this->admin_model->add('tbl_player',$dt_array);
				$this->admin_model->remove('tbl_player_exception',array('id'=>$arr["id"]));
				//-------------------------------------------  
				//Player Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($player_fname);
				$lname_slug    = strip_tags($player_lname);  
				$jSlug         = $fname_slug.'-'.$lname_slug;    
				$playerURL	   = strtolower(url_title($jSlug));
				if($this->admin_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
				   $playerURL = $playerURL.'-'.$lastInsId; 
				}
				$return = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
				}
			}
			if($return){
				unlink('./assets/uploads/excel/'.$file_name); //File Deleted After uploading in database .
				$this->session->set_userdata('up_msg','Added successfully');
			}
		}
		redirect('admin/fundraiser/'.$fslug.'/participants');
	}
	public function startup_tab($slug)
	{
		$data['active_tab']			= 'startup'; 
		$data['active_page'] 		= 'fundraiser';
		$limit 						= 0;
		$offset 					= 10;
		$getId						= $this->admin_model->fundraiser_getID($slug);
		$fund_id 					= $getId['0']['id'];
        $data['fslug']				= $slug;
        $data['fundraiserInfo']		= $this->admin_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->admin_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->admin_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->admin_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes/footer');  
	}
	public function checkfile()
	{
		$getswidth = $this->input->post("valid_width");
		$getsheight = $this->input->post("valid_height");
		$image_info = getimagesize($_FILES["file"]["tmp_name"]);
		$image_width = $image_info[0];
		$image_height = $image_info[1];
		if($image_width > $getswidth AND $image_height > $getsheight)
		{
			echo json_encode(array('valid'=>'true','msg'=>'Image Dimension Correct'));
		}
		else
		{
			echo json_encode(array('valid'=>'false','msg'=>'Image Dimension Exceeds'));
		}
	}
	public function upload_media()
	{
		$fslug	  = $this->input->post('fslug');
		$getId	  = $this->admin_model->fundraiser_getID($fslug);
		$fund_id  = $getId['0']['id'];
		
		$response = array('msg'=>'','html'=>'','mtype'=>'','valid'=>1);

		$media_type    = $this->input->post('media_type');
		
		if($media_type == 'I'){
			$feature_image     = isset($_FILES['feature_img']['name'])?$_FILES['feature_img']['name']:"";
			if($feature_image!="")
			{
				$config_img['upload_path']   = './assets/fundraiser_feature_image/';
				$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config_img['max_size']      = '100000';
				$this->load->library('upload', $config_img);
				$this->upload->initialize($config_img);			 
				$this->upload->do_upload('feature_img');			
				$data_img = $this->upload->data(); 
				if($data_img['is_image'] == 1)
				{ 
					$fpath1  = $data_img['file_name'];	
					$fpath1_ex = explode(".",$fpath1);
					$fpath1  = $fpath1_ex[0].'.'.strtolower($fpath1_ex[1]);
					$this->load->library('image_lib');
					$config_resize['image_library']   = 'gd2';	
					$config_resize['create_thumb']    = TRUE;
					$config_resize['maintain_ratio']  = TRUE;
					$config_resize['master_dim']      = 'height';
					$config_resize['quality']         = "100%";  
					$config_resize['source_image']    = './assets/fundraiser_feature_image/'.$fpath1;
					$config_resize['new_image']       = './assets/fundraiser_feature_image/thumb/'.$fpath1;
					$config_resize['height']          = 150;  //600
					$config_resize['width']           = 1;
					$this->image_lib->initialize($config_resize);
					$this->image_lib->resize(); 
					$fund_feature_image = $fpath1;
				}
				$fundata = array('fund_id'=>$fund_id,'media_type'=>$media_type,'fund_feature_image'=>$fund_feature_image);
				$img  = $this->db->insert('tbl_rel_fund_feature_images', $fundata);
				$last_id = $this->db->insert_id();
				$protimg = explode(".",$fund_feature_image);   
				$proimg  = base_url()."assets/fundraiser_feature_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
				$str = '<img src="'.$proimg.'" class="thumbnail" />';
				$html = '<div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px; overflow:hidden;height:150px;">
				<img src="'.$proimg.'" />
				<a href="javascript:void(0);" onclick="del_media('.$last_id.')" class="fa fa-times rem"></a>                 
				</div>';
				$response = array('msg'=>'uploaded','html'=>$html,'mtype'=>'I','valid'=>1);
			}
		}else{
			$fundata = array('fund_id'=>$fund_id,'media_type'=>$media_type,'fund_feature_image'=>$this->input->post('youtube_fil'));
			$vid  = $this->db->insert('tbl_rel_fund_feature_images', $fundata);
			$last_id = $this->db->insert_id();
			$html = '<div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px; overflow:hidden;height:150px;">
				<a class="youtube" href="http://youtube.com/watch?v='.$this->input->post('youtube_fil').'"><img src="https://img.youtube.com/vi/'.$this->input->post('youtube_fil').'/mqdefault.jpg" /></a>
				<a href="javascript:void(0);" onclick="del_media('.$last_id.')" class="fa fa-times rem"></a>                 
				</div>';
			$response = array('msg'=>'uploaded','html'=>$html,'mtype'=>'V','valid'=>1);
		}
		echo json_encode($response);
	}
	public function upload_fund_logo()
	{
		$fslug		= $_POST['fslug'];
		$getId	  = $this->admin_model->fundraiser_getID($fslug);
		$fund_id  = $getId['0']['id'];
		$fund_image	= isset($_FILES['fund_image']['name'])?$_FILES['fund_image']['name']:"";
		$arr		= $this->admin_model->view('tbl_fundraiser',array('id'=>$fund_id));
		$res		= array('msg'=>'','valid'=>0);
		if($fund_image!="")
		{
			if(!empty($arr)){
				if(isset($arr[0]['fund_image']) && $arr[0]['fund_image']!=""){
					if(file_exists("./assets/fundraiser_image/thumb/".$arr[0]['fund_image'])){
					  unlink("./assets/fundraiser_image/thumb/".$arr[0]['fund_image']);
					}
					if(file_exists("./assets/fundraiser_image/".$arr[0]['fund_image'])){
					  unlink("./assets/fundraiser_image/".$arr[0]['fund_image']);
					}
				}
			}

			$config_img['upload_path']   = './assets/fundraiser_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);		
			
			if($this->upload->do_upload('fund_image'))
			{
				$data_img = $this->upload->data(); 
				if($data_img['is_image'] == 1)
				{ 
					$fpath1  = $data_img['file_name'];			
					$this->load->library('image_lib');
					$config_resize['image_library']   = 'gd2';	
					$config_resize['create_thumb']    = TRUE;
					$config_resize['maintain_ratio']  = TRUE;
					$config_resize['master_dim']      = 'height';
					$config_resize['quality']         = "100%";  
					$config_resize['source_image']    = './assets/fundraiser_image/'.$fpath1;
					$config_resize['new_image']       = './assets/fundraiser_image/thumb/'.$fpath1;
					$config_resize['height']          = 170;
					$config_resize['width']           = 1;
					$this->image_lib->initialize($config_resize);
					$this->image_lib->resize(); 
					$fund_image = $fpath1;
				}
				$fundimg 	= explode(".",$fund_image);   
				$fundimage	=  base_url("assets/fundraiser_image/thumb/".$fundimg[0].'_thumb.'.$fundimg[1]);
				$fundata    = array('fund_image'=> $fund_image);
				$where		= array('id'=>$fund_id);
				$return = $this->admin_model->edit('tbl_fundraiser', $fundata, $where);
				if($return){
					$res = array('msg'=>'uploaded','valid'=>1,'src'=>$fundimage,'where'=>$where);
				}else{
					$res = array('msg'=>'Error occured','valid'=>0);
				}
			}else{
				$res = array('msg'=>$this->upload->display_errors(),'valid'=>0);
			}
		}
		echo json_encode($res);
	}
	public function invitationtoall($fslug)
	{
		if($fslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= date("F j, Y",strtotime($fund_details[0]['fund_start_dt']));
			$fundraiser_end		= date("F j, Y",strtotime($fund_details[0]['fund_end_dt']));
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_org_goal'];
			$player_goal		= $fund_details[0]['fund_individual_goal'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			if($fund_details[0]['fund_image']==""){
				$fundimg 		= base_url()."assets/images/noimage-150x150.jpg";   
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		= base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
			}
			
			$player_listing		= $this->admin_model->view('tbl_player',array('player_added_by'=>$fundraiser_id));
			foreach($player_listing as $player_li){
				$pslug				= $player_li['player_slug_name'];
				$player_first		= $player_li['player_fname'];
				$player_last		= $player_li['player_lname'];
				$player_email		= $player_li['player_email'];
				$player_password	= $player_li['player_pass'];
				
				$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
				$player_login_url	= base_url('admin/login');
				$start_fundraiser	= base_url("start-fundraiser");

				$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'asking-to-invite-donors-to-player-from-fundraiser'));
				$html_header = '<!DOCTYPE html>
				<html lang="en">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>ThanksForSupporting</title>
				<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
				</head>
				<body>
				<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato; font-size: 20px;">
				<tr>
				<td align="center"><table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0">
				<tr>
				<td style="padding:0 30px;"><table border="0" width="640" cellpadding="0" cellspacing="0">
				<tr>
				<td><img src="http://thanksforsupporting.com/apps/assets/images/logo_thanks-for-supporting.png" /></td>
				<td style="padding:0 10px;"><img src="'.$fundimg.'" style="width: 100px;height: 100px;border-radius: 50%;" /></td>
				<td><strong>'.$fundraiser_name.'</strong> <br>
				<div style="font-size: 14px;">'.$fundraiser_slog.'</div></td>
				</tr>
				</table></td>
				</tr>
				</table></td>
				</tr>
				</table>';
				$html_footer = '<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;">
				<tr>
				<td style="padding:5px 30px 30px; text-align: center; color:#9f9f9f; font-size: 16px"> This email was sent to you because we want to make the world a better place.<br/>
				You can choose to opt out of future opportunities to help others by clicking here. </td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				</body>
				</html>';

				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
				$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_bgcolor);
				$email_content		= $html_header.str_replace($search_key,$search_val,$template_body).$html_footer;
				//echo $email_content;die;
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				if($bval){
				$this->admin_model->edit('tbl_player',array("invitation"=>1,"invitation_dt"=>date("Y-m-d H:i:s")),array("id"=>$player_li["id"]));
				}
			}
			
			$response = "Invitation sent";
			$this->session->set_userdata('succ_msg', $response);
		}else{
			$response = "Sufficient data not provided";
		}
		redirect('admin/fundraiser/'.$fslug.'/startup');
	}
	
	public function delParticipant($fslug,$pslug)
	{
		$cnt=$this->admin_model->view_check('tbl_player',array('player_slug_name'=>$pslug));
		if($cnt > 0)
		{
			$pdetails = $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$pid = $pdetails[0]["id"];
			$this->admin_model->remove('tbl_player',array('id'=>$pid));
			$this->admin_model->remove('tbl_player_contacts',array('contact_added_by'=>$pslug));
			$this->admin_model->remove('tbl_donors_payment',array('player_id'=>$pid));
			$this->session->set_userdata('up_msg','Deleted successfully');
		}
		redirect('admin/fundraiser/'.$fslug.'/participants');
	}
	
	public function cron_mail_list()
	{
		$maillist = $this->admin_model->cron_template_list();
		$data['maillist'] = $maillist;        
		$this->load->view('../../includes/header');        
		$this->load->view('view_cron_mail_list',$data);
		$this->load->view('../../includes/footer'); 
	}

	public function test()
	{
		/*$config['mailtype'] = 'html';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset']	= 'iso-8859-1';
		$config['wordwrap'] = TRUE;*/
		
		/*$config = array();
        $config['useragent']= "CodeIgniter";
        $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol'] = "mail";
        $config['smtp_host']= "localhost";
        $config['smtp_port']= "25";
        $config['mailtype'] = 'html';
        $config['charset']  = 'utf-8';
        $config['newline']  = "\r\n";
        $config['wordwrap'] = TRUE;
		$str = '<h1>Thanks For Testing</h1>';
		$this->email->initialize($config);   
		$this->email->from('kaushik.ild@gmail.com');  
		$this->email->to('vikashk.ild@gmail.com'); 
		$this->email->subject("Thanks for Supporting"); 
		$this->email->message($str);
		$bval=$this->email->send();*/
		
		/*$db2=$this->load->database('wp_db',TRUE);
		$table = $db2->dbprefix.'posts';
		$qry = $db2->query("SELECT * FROM ".$table." WHERE ID = 11225" );
		$res = $qry->result_array();*/
	}

	public function fundraiserloadmore()
	{
		$offset = $this->config->item('pageoffset');
		$limit = $this->input->post('limit')*$offset;
		if($this->session->userdata('order_by')=="" && $this->session->userdata('title_by')==""){
			$list = $this->admin_model->fundraiser_list($limit,$offset);
		}else{
			$order_by = $this->session->userdata('order_by');
			$title_by = $this->session->userdata('title_by');
			$list = $this->admin_model->fundraiser_list1($limit,$offset,$order_by,$title_by);
		}
		$tbr='';
		if(count($list)>0)
		{
		foreach($list as $val)
		{
            //$contacts_sql	= "SELECT tbl_player_contacts.id FROM tbl_player INNER JOIN tbl_player_contacts ON tbl_player.player_slug_name = tbl_player_contacts.contact_added_by WHERE tbl_player.player_added_by='".$val['id']."'";
           // $contacts_qry	= $this->db->query($contacts_sql);

			if($val['fund_image']==""){
            $proimg =  base_url()."assets/images/noimage-150x150.jpg";   
            }else{
			$pro_timg = explode(".",$val['fund_image']); 
            $proimg =  base_url()."assets/fundraiser_image/thumb/".$pro_timg[0].'_thumb.'.$pro_timg[1];     
            }
			if($val['fund_status']==0){
			$ss='Inactive';	
			}else{
			$ss='Active';	
			}
			
			if($val['totalinvites']){
				$totalinvite=  $val['totalinvites']; 
			}else {
				$totalinvite= 0;
			}
		 $tbr.= "<tr>
				<td data-th=\"Name\">
					<span><img src=".$proimg." alt=\"\"></span>
					<div style=\"display:table-cell; padding-left:10px;\">
						<a href=".base_url()."admin/fundraiser/".$val['fund_slug_name']."/information><strong class=\"nopad_L\">".$val['fund_username']."</strong></a><br>".$val['fund_fname']." ".$val['fund_lname'].'<br>'.$val['fund_city'].",".$val['fund_state']."<br>
						
						<a class=\"grn2\"href=".base_url()."admin/fundraiser/".$val['fund_slug_name']."/information>Edit </a> | 
						<a class=\"grn2\" href=".base_url('admin/paymentdetails/'.$val['fund_slug_name']).">Donations </a> |
						 <a class=\"grn2\" href=".base_url('admin/fundraiser/'.$val['fund_slug_name'])."/sendtomailtofunraiser>Invoice </a> | 
						 <a class=\"grn2\" href=".base_url('admin/fundraiser/'.$val['fund_slug_name'].'/paymentinformation/add').">Payment</a> | 
						 <a class=\"grn2\" href=".base_url('admin/fundraiser/'.$val['fund_slug_name'].'/settings').">Login</a> | 
						 <a class=\"grn2\" target=\"_blank\" href=".base_url('donate/personal-info/'.$val['fund_slug_name'].'').">Webpage</a> | <a class=\"grn2\" href=\"javascript:void(0);\" onclick=\"Delete(".$val['id'].")\">Delete </a></div>
				</td>s
				<td data-th=\"Org Goal\" style=\"text-align:right;\">".date('M j\, Y',strtotime($val['creation_date']))."</td>
				<td data-th=\"Reached\" style=\"text-align:right;\">".date('m/d/Y',strtotime($val['fund_start_dt']))."</td>
				<td data-th=\"Remaining\" style=\"text-align:right;\">".date('m/d/Y',strtotime($val['fund_end_dt']))."</td>
				<td data-th=\"Status\" style=\"text-align:right;\">".$ss."</td>
				<td class=\"text-center\"><a class=\"grn2\" href=".base_url().'admin/fundraiser/'.$val['fund_slug_name'].'/report/list'.">".($val['totalparticiapte'] ? $val['totalparticiapte'] : "0")."</a></td>
				<td class=\"text-center\">$".($val['raised_amt'] ? $val['raised_amt'] : "0")."</td>
				<td class=\"text-right\"><a class=\"grn2\" href=".base_url().'admin/fundraiser/'.$val['fund_slug_name'].'/report/list/invited'.">".$totalinvite."</a></td>
			    

			    </tr>";
		}
		echo $tbr;
		}
		else
		{
		echo 0;	
		}
	}
    
	public function create_fundraiser()
	{
		if(isset($_POST["fund_email"])){
			$fname		= $this->input->post('fund_fname');
			$lname		= $this->input->post('fund_lname');
			$email		= $this->input->post('fund_email');
			$password1	= time();
			$username	= $fname.' '.$lname;
			
			$this->form_validation->set_rules('fund_fname', 'First Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_lname', 'Last Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_email', 'Email address', 'trim|required|valid_email');
			
			if ($this->form_validation->run() != FALSE)
			{
				$encrypt_key = $this->config->item('encryption_key');
				$password    = $this->encrypt->encode($password1, $encrypt_key);
				
				$fundata       = array();
				$fundata["fund_fname"]= $this->input->post('fund_fname');
				$fundata["fund_lname"]= $this->input->post('fund_lname');
				$fundata["fund_email"]= $this->input->post('fund_email');
				$fundata["fund_contact"]= $this->input->post('fund_contact');
				$fundata["fund_terms"]= $this->input->post('fund_terms');
				$fundata["fund_pass"]= $password;
				
				$this->admin_model->add('tbl_fundraiser',$fundata);
				
				//-------------------------------------------  
				//Fundraiser Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($username);  
				
				$string 	   = str_replace(' ', '-', $fname_slug); // Replaces all spaces with hyphens.
				$slug		   = preg_replace('/[^A-Za-z0-9\-#$&^*()+=!~\']/', '', $string); // Removes special chars.
				$jSlug		   = strtolower(rtrim($slug,'-'));
				$bgcolor	   = '#581b6c';
				$selected_color = '#581b6c';
				$fontcolor	   = '#fff';

				$fundURL = url_title($jSlug);
				if($this->admin_model->isUrlExists('tbl_fundraiser',$fundURL)==1){
				   $fundURL = $fundURL.'-'.$lastInsId; 
				}
				$slug1 = $fundURL;
				$x = $this->db->update('tbl_fundraiser', array('fund_slug_name'=>$slug1), array('id'=>$lastInsId));
				if($x)
				{
					$admin_details 		= $this->admin_model->view('tbl_admin',array('id'=>1));
					$admin_email 		= $admin_details[0]["contact_email"];
					$template 			= $this->admin_model->view('tbl_mail_template',array('template_slug'=>'create-fundraiser'));
					$login_url			= base_url("admin/login");
					$start_fundraiser	= base_url("start-fundraiser");
					$template_head		= $template[0]["template_header"];
					$template_body		= html_entity_decode($template[0]["template_content"]);
					$template_foot		= $template[0]["template_footer"];
					$template_subject	= $template[0]["template_subject"];
					
					$search_key 		= array('[FundraiserFirst]','[FundraiserEmail]','[Password]','[LoginUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
					$search_val			= array($fname,$email,$password1,$login_url,$start_fundraiser,$bgcolor,$fontcolor,$selected_color);
					$email_content		= $template_head.str_replace($search_key,$search_val,$template_body).$template_foot;
					
					$config = array();
					$config['useragent']= "CodeIgniter";
					$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config['protocol'] = "mail";
					$config['smtp_host']= "localhost";
					$config['smtp_port']= "25";
					$config['mailtype'] = 'html';
					$config['charset']	= 'iso-8859-1';
					$config['newline']  = "\r\n";
					$config['wordwrap'] = TRUE;
					
					$this->email->initialize($config);   
					$this->email->from($admin_email);  
					$this->email->to($email); 
					$this->email->subject($template_subject); 
					$this->email->message($email_content);
					$bval=$this->email->send();
					if($bval){
						redirect('admin/fundraiser/'.$slug.'/organization');
					}
				}
				//---------------------------------------------------------------------------------------------------
			}
		}
		
		$data['active_tab']			= 'information'; 
		$data['active_page'] 		= 'fundraiser';
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_creation',$data);
		$this->load->view('../../includes/footer');    
	}
    
	public function fundraiserdetails($slug)
	{
		$data['active_page']         = 'fundraiser'; 
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug); 
		$getId						 = $this->admin_model->fundraiser_getID($slug);
		$data['fundraiserImgInfo']   = $this->admin_model->fundraiserImgInfo($getId['0']['id']);
		$data['slug']				 = $slug;        
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_detail',$data);
		$this->load->view('../../includes/footer'); 
	}

	public function getFundraiserInfoOpenAjax()
	{
		$slug=$this->input->post('fslug');        
		$data['stateList']           = $this->admin_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_info_edit_modal', $data, true);
	}
    
	public function fundraiserInfoEditAjax()
	{
		//print_r($_FILES);
		$fund_id        = $_POST['hid_ffid'];
		$fund_image     = isset($_FILES['file']['name'])?$_FILES['file']['name']:"";
		$fund_username  = $_POST['fund_username'];
		$fund_slogan    = $_POST['fund_slogan'];
		$fund_city      = $_POST['fund_city'];
		$fund_state     = $_POST['fund_state'];
		$fund_org_goal  = $_POST['fund_individual_goal'];

		$arr=$this->admin_model->view('tbl_fundraiser',array('id'=>$fund_id));

		if($fund_image!="")
		{
			if(!empty($arr)){
				if(isset($arr[0]['fund_image']) && $arr[0]['fund_image']!=""){
					$ex_timg = explode(".",$arr[0]['fund_image']);	
					if(file_exists("./assets/fundraiser_image/thumb/".$ex_timg[0].'_thumb.'.$ex_timg[1])){
					  unlink("./assets/fundraiser_image/thumb/".$ex_timg[0].'_thumb.'.$ex_timg[1]);
					}
					if(file_exists("./assets/fundraiser_image/".$arr[0]['fund_image'])){
					  unlink("./assets/fundraiser_image/".$arr[0]['fund_image']);
					}
				}
			}
			/* Create the config for upload library */
			/*$config_img['upload_path']   = './assets/fundraiser_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);  
			$this->upload->initialize($config_img);
			$this->upload->do_upload('file');
			$data_img = $this->upload->data();            
			if($data_img['is_image'] == 1)
			{
				$fpath1     = $data_img['file_name'];
				$this->createThumb('./assets/fundraiser_image/thumb/',$fpath1,$data_img,120,120);
				$fund_image = $fpath1;
			} */		
			
			$config_img['upload_path']   = './assets/fundraiser_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
			{ 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/fundraiser_image/'.$fpath1;
				$config_resize['new_image']       = './assets/fundraiser_image/thumb/'.$fpath1;
				$config_resize['height']          = 120;
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$player_image = $fpath1;
			}
					   
		}
		else{
			$fund_image = $arr[0]['fund_image'];
		}
		
		/*$fname_slug    = strip_tags($this->input->post('fund_fname'));
		$lname_slug    = strip_tags($this->input->post('fund_lname'));  
		$jSlug         = $fname_slug.'-'.$lname_slug;    
		$fundraiserURL = strtolower(url_title($jSlug));
		if($this->admin_model->isUrlExistsEditMode('tbl_fundraiser',$fundraiserURL,$this->input->post('hid_fid'))==1){
		   $fundraiserURL = $fundraiserURL.'-'.$this->input->post('hid_fid'); 
		}*/

		$fundata    = array('fund_username'         => $this->input->post('fund_username'),
							//'fund_lname'            => $this->input->post('fund_lname'),
							'fund_city'             => $this->input->post('fund_city'),
							'fund_state'            => $this->input->post('fund_state'),
							'fund_org_goal'         => $this->input->post('fund_org_goal'),
							'fund_slogan'			=> $fund_slogan,
							'fund_image'            => $fund_image,
							'fund_individual_goal'  => $this->input->post('fund_individual_goal')
						   );
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_ffid')));
		//$data['fslug']=$fundraiserURL;
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}    

	public function getFundraiserDateOpenAjax()
	{
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_date_edit_modal', $data, true);
	}   

	public function fundraiserDateEditAjax()
	{
		//$fund_start_dt = isset($_POST['fund_start_dt'])?date('Y-m-d',strtotime($this->input->post('fund_start_dt'))):"0000-00-00";
		//$fund_end_dt   = isset($_POST['fund_end_dt'])?date('Y-m-d',strtotime($this->input->post('fund_end_dt'))):"0000-00-00";
		$fund_start_dt = $this->input->post('fund_start_dt');
		$fund_end_dt   =  $this->input->post('fund_end_dt');
		$ex_ful_st_dt  = explode('-',$fund_start_dt);
		$ex_ful_st_dt  = $ex_ful_st_dt[2].'-'.$ex_ful_st_dt[0].'-'.$ex_ful_st_dt[1];
		$ex_ful_en_dt  = explode('-',$fund_end_dt);
		$ex_ful_en_dt  = $ex_ful_en_dt[2].'-'.$ex_ful_en_dt[0].'-'.$ex_ful_en_dt[1];
		
		
		
		$fundata       = array('fund_start_dt'  => $ex_ful_st_dt,
							   'fund_end_dt'    => $ex_ful_en_dt);
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function getFundraiserContactOpenAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_contact_edit_modal', $data, true);
	}

	public function fundraiserContactEditAjax(){
		$fundata       = array('fund_fname'      => $this->input->post('fund_fname'),
							   'fund_lname'      => $this->input->post('fund_lname'),
							   'fund_email'      => $this->input->post('fund_email'),
							   'fund_contact'    => $this->input->post('fund_contact'));
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_ffid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function getFundraiserBankDetailOpenAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_bankdetails_edit_modal', $data, true);
	}

	public function fundraiserBankDetailsEditAjax(){
		$fundata       = array('fund_bank_name'  		=> $this->input->post('fund_bank_name'),
							   'fund_routing_number'    => $this->input->post('fund_routing_number'),
							   'fund_account_number'	=> $this->input->post('fund_account_number'));
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function getFundraiserDesOpenAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_desc_edit_modal', $data, true);
	}

	public function fundraiserDesEditAjax(){
		$fundata       = array('fund_des' => $this->input->post('fund_des'));
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function getFundraiserFetImgOpenAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_feature_img_edit_modal', $data, true);
	}

	public function getFundraiserFetImgOpenAddAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->admin_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_feature_img_add_modal', $data, true);
	}

	private function createThumb($path, $filename,$data,$w, $h=0, $crop=false){

		$config['image_library']    = "gd2";      
		$config['source_image']     = $data['full_path'];      
		$config['create_thumb']     = TRUE;      
		$config['maintain_ratio']   = FALSE;
		$config['thumb_marker']     = '';
		$config['new_image']    = $path.$filename;


		$width= $data['image_width'];
		$height= $data['image_height'];

		/*if($h!=0){
			$newwidth = $w;
			$newheight = $h;
		}else{
			$newwidth = $w;
			$fraction_amt = $w/$original_width;
			$newheight = intval($original_height*$fraction_amt);
		}*/
		$r = $width / $height;
		if ($crop) {
			if ($width > $height) {
				$width = ceil($width-($width*abs($r-$w/$h)));
			} else {
				$height = ceil($height-($height*abs($r-$w/$h)));
			}
			$newwidth = $w;
			$newheight = $h;
		} else {
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
		}

		$config['width']            = $newwidth; 
		$config['height']           = $newheight;
		$this->load->library('image_lib');
		$this->image_lib->initialize($config);
		if(!$this->image_lib->resize()){
		return $this->image_lib->display_errors();
		} 
		$this->image_lib->clear();
		return false; 
	}

	public function fundraiserEmailCheckerAjax()
	{
		$where = array('fund_email'=>$this->input->post('fund_email')); 
		$query=$this->admin_model->fundraiser_check('tbl_fundraiser',$where);
		if($query>0){  echo 'false'; }
		else{ echo 'true'; }
	}
	
	public function fundraiserUsernameCheckerAjax()
	{
		$where = array('fund_username'=>$this->input->post('fund_username')); 
		$query=$this->admin_model->fundraiser_check('tbl_fundraiser',$where);
		if($query>0){  echo 'false'; }
		else{ echo 'true'; }
	}

	public function fundraiserEmailCheckerEditAjax()
	{
		$ex = explode('##',$this->input->post('fund_email'));
		$fund_email = $ex[0];
		$fund_id    = $ex[1]; 
		$query=$this->admin_model->fundraiser_check_edit('tbl_fundraiser',$fund_email,$fund_id);
		if($query>0){  echo 'false'; }
		else{ echo 'true'; }
	}
	
	public function fundraiserEmailCheckerEditAjax1()
	{
		$ex = explode('##',$this->input->post('fund_email'),2);
		$fund_email = $ex[0];
		$fslug      = $ex[1]; 
		$query=$this->admin_model->fundraiser_check_edit_by_slug('tbl_fundraiser',$fund_email,$fslug);
		if($query>0){  echo 'false'; }
		else{ echo 'true'; }
	}
	
	public function fundraiserEmailCheckerEditAjax2()
	{
		$fund_email = $this->input->post('fund_email');
		$query=$this->admin_model->view_check('tbl_fundraiser',array('fund_email'=>$fund_email));
		if($query>0){  echo 'false'; }else{ echo 'true'; }
	}

	public function fundraiserFeatureAddAjax()
	{
		$fund_id        = $_POST['hid_fid'];
		$media_type     = $_POST['hid_mtype'];
		
		if($media_type == 'I'){
		$fund_image     = isset($_FILES['file']['name'])?$_FILES['file']['name']:"";    
		//$arr=$this->admin_model->view('tbl_fundraiser',array('id'=>$fund_id));
		if($fund_image!="")
		{
			/*if(!empty($arr)){
				if(isset($arr[0]['fund_feature_image']) && $arr[0]['fund_feature_image']!=""){
					$tfet_img = explode(".",$arr[0]['fund_feature_image']);
					if(file_exists("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1])){
					  unlink("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1]);
					}
					if(file_exists("./assets/fundraiser_feature_image/".$arr[0]['fund_feature_image'])){
					  unlink("./assets/fundraiser_feature_image/".$arr[0]['fund_feature_image']);
					}
				}
			}
	*/            /* Create the config for upload library */
			/*$config_img['upload_path']   = './assets/fundraiser_feature_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);  
			$this->upload->initialize($config_img);
			$this->upload->do_upload('file');
			$data_img = $this->upload->data();            
			if($data_img['is_image'] == 1)
			{
				$fpath1     = $data_img['file_name'];
				$this->createThumb('./assets/fundraiser_feature_image/thumb/',$fpath1,$data_img,120,120);
				$fund_feature_image = $fpath1;
			}*/ 
			$config_img['upload_path']   = './assets/fundraiser_feature_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
			{ 
				$fpath1  = $data_img['file_name'];	
				$fpath1_ex = explode(".",$fpath1);
				$fpath1  = $fpath1_ex[0].'.'.strtolower($fpath1_ex[1]);
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/fundraiser_feature_image/'.$fpath1;
				$config_resize['new_image']       = './assets/fundraiser_feature_image/thumb/'.$fpath1;
				$config_resize['height']          = 150;  //600
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$fund_feature_image = $fpath1;
			}            
		}
		$fundata    = array('fund_feature_image'=>$fund_feature_image,'media_type'=>$media_type,'fund_id'=>$this->input->post('hid_fid'));
		$return = $this->db->insert('tbl_rel_fund_feature_images', $fundata);
		//$data['fslug']=$fundraiserURL;
		}
		else
		{
		$return = $this->db->insert('tbl_rel_fund_feature_images', array('fund_id'=>$this->input->post('hid_fid'),'media_type'=>$media_type,' 	fund_feature_image'=>$this->input->post('youtube_fil')));	
		}
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function fundraiserFeatureEditAjax()
	{
		$fund_id        = $_POST['hid_fid'];
		$fund_image     = isset($_FILES['file']['name'])?$_FILES['file']['name']:"";    
		$arr=$this->admin_model->view('tbl_fundraiser',array('id'=>$fund_id));
		if($fund_image!="")
		{
			if(!empty($arr)){
				if(isset($arr[0]['fund_feature_image']) && $arr[0]['fund_feature_image']!=""){
					$tfet_img = explode(".",$arr[0]['fund_feature_image']);
					if(file_exists("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1])){
					  unlink("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1]);
					}
					if(file_exists("./assets/fundraiser_feature_image/".$arr[0]['fund_feature_image'])){
					  unlink("./assets/fundraiser_feature_image/".$arr[0]['fund_feature_image']);
					}
				}
			}
			/* Create the config for upload library */
			/*$config_img['upload_path']   = './assets/fundraiser_feature_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);  
			$this->upload->initialize($config_img);
			$this->upload->do_upload('file');
			$data_img = $this->upload->data();            
			if($data_img['is_image'] == 1)
			{
				$fpath1     = $data_img['file_name'];
				$this->createThumb('./assets/fundraiser_feature_image/thumb/',$fpath1,$data_img,120,120);
				$fund_feature_image = $fpath1;
			}*/ 
			$config_img['upload_path']   = './assets/fundraiser_feature_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
			{ 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/fundraiser_feature_image/'.$fpath1;
				$config_resize['new_image']       = './assets/fundraiser_feature_image/thumb/'.$fpath1;
				$config_resize['height']          = 150;  //600
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$fund_feature_image = $fpath1;
			}            
		}
		else{
			$fund_feature_image = $arr[0]['fund_image'];
		}
		$fundata    = array('fund_feature_image'            => $fund_feature_image);
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		//$data['fslug']=$fundraiserURL;
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function fundraiserplayerlist($slug)
	{
	//echo $slug;
	$getId	= $this->admin_model->fundraiser_getID($slug);
	$limit  = 0;
	$offset = 10;
	if(count($getId)>0){
		$playersInfo      = $this->admin_model->fundraiser_playersInfo($getId[0]['id'],$limit,$offset);	
	}
	else{
		$playersInfo      = array();
	}
	
	if($getId[0]['id']!=0){
		$fund_id = 	$getId[0]['id'];
	}else{
		$fund_id = 	0;
	}
	$data['active_page']      = 'fundraiser'; 
	$data['playersInfo']      = $playersInfo; 
	$data['fundraiserInfo']   = $this->admin_model->fundraiserinfo($slug); 
	$data['slug']			  = $slug;
	$data['fund_id']		  = $fund_id;
	$this->load->view('../../includes/header');        
	$this->load->view('view_fundraiser_player_list',$data);
	$this->load->view('../../includes/footer');
	}
	
	public function getSendMailModalAjax()
	{
		$pslug=$this->input->post('pslug');
		if($pslug!='')
		{
			$player_details=$this->admin_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		
		$data['playerInfo']	= $player_details;   
		echo $this->load->view('view_donation_mail_modal', $data, true);
	}
	
	public function donationemailsend()
	{
		$result 			= array();
		$email_csv  		= isset($_FILES['file']['name'])?$_FILES['file']['name']:"";
		$email_list 		= isset($_POST['email_list'])?$_POST['email_list']:"";
		$player_id			= isset($_POST['hid_pid'])?$_POST['hid_pid']:"";
		
		$player_details		= $this->admin_model->view('tbl_player',array('id'=>$player_id));
		$template			= $this->admin_model->view('tbl_mail_template',array('template_slug'=>'donations-email-send'));
		$template_body		= html_entity_decode($template[0]["template_content"]);
		$template_subject	= $template[0]["template_subject"];
		$player_slug		= $player_details[0]["player_slug_name"];
		$player_email		= $player_details[0]['player_email'];
		$fundraiser_id		= $player_details[0]['player_added_by'];
		$fundraiser_details	= $this->admin_model->view('tbl_fundraiser',array('id'=>$fundraiser_id));
		$fundraiser_slug	= $fundraiser_details[0]['fund_slug_name'];

		if($email_csv!="")
		{
			$filename=$_FILES["file"]["tmp_name"];
			$file = fopen($filename, "r");
			
			while (($importdata = fgetcsv($file, 1000, ",")) !== FALSE)
			{
				$indata = array(
					'from_id'		=> $player_id,
					'from_email'	=> $player_email,
					'to_email'		=> $importdata[0],
					'sent'			=> '1',
					'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert				= $this->admin_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->admin_model->last_insert_id();
				$player_url			= base_url().$fundraiser_slug.'/'.$player_slug.'/support/'.$report_id;
				$email_content		= str_replace('[player URL]',$player_url,$template_body);
				
				$updata 			= array('email_content'	=> $email_content);
				$where				= array('id' => $report_id);
				$update				= $this->admin_model->edit('tbl_email_share_reports',$updata,$where);
				
				$tracker_img		= '<img src="'.base_url().'getimage/'.$report_id.'" style="display:none;"/>';
				
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$str = $email_content.$tracker_img;
				$this->email->initialize($config);   
				$this->email->from($player_email);
				$this->email->to($importdata[0]); 
				$this->email->subject("Thanks for Supporting - ".$template_subject); 
				$this->email->message($str);
				$bval=$this->email->send();
			}
			
			fclose($file);
			$result = array('valid'=>1,'msg'=>'Email send for donations successfully');
		}
		else
		{
			$email_lists = explode(",", $email_list);
			foreach($email_lists as $email_li)
			{
				$indata = array(
					'from_id'		=> $player_id,
					'from_email'	=> $player_email,
					'to_email'		=> $email_li,
					'sent'			=> '1',
					'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert				= $this->admin_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->admin_model->last_insert_id();
				
				$player_url			= base_url().$fundraiser_slug.'/'.$player_slug.'/support/'.$report_id;
				$email_content		= str_replace('[player URL]',$player_url,$template_body);
				
				$updata 			= array('email_content'	=> $email_content);
				$where				= array('id' => $report_id);
				$update				= $this->admin_model->edit('tbl_email_share_reports',$updata,$where);
				
				$tracker_img		= '<img src="'.base_url().'getimage/'.$report_id.'" style="display:none;"/>';
				
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$str = $email_content.$tracker_img;
				$this->email->initialize($config);   
				$this->email->from($player_email);  
				$this->email->to($email_li); 
				$this->email->subject("Thanks for Supporting - ".$template_subject); 
				$this->email->message($str);
				$bval=$this->email->send();
			}
			$result = array('valid'=>1,'msg'=>'Email send for donations successfully');
		}
		echo json_encode($result);
	}

	public function getMailStructure()
	{
		$pslug=$this->input->post('pslug');
		$donor=$this->input->post('donor');  
		if($pslug!='')
		{
			$player_details=$this->admin_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		if($donor!='')
		{
			$donor_details=$this->admin_model->view('tbl_donors_payment',array("id"=>base64_decode($donor)));
		}
		$data['playerInfo']	= $player_details;  
		$data['donorInfo']	= $donor_details;  
		echo $this->load->view('view_player_mail_modal', $data, true); 
	}
	
	public function getMailfromReports()
	{
		$pslug=$this->input->post('pslug');
		$report=$this->input->post('report');  
		if($pslug!='')
		{
			$player_details=$this->admin_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		if($report!='')
		{
			$report_details=$this->admin_model->view('tbl_email_share_reports',array("id"=>base64_decode($report)));
		}
		$data['playerInfo']	= $player_details;  
		$data['reportInfo']	= $report_details;  
		echo $this->load->view('view_thanks_mail_modal', $data, true); 
	}

	public function sendingThanks()
	{
		$pid=$this->input->post('hid_pid'); 
		$did=$this->input->post('hid_did');
		if($pid!='' AND $did!='')
		{
			$player_details=$this->admin_model->view('tbl_player',array("id"=>base64_decode($pid)));
			$player_name = $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"];
			$player_email = $player_details[0]["player_email"];
			$donor_details=$this->admin_model->view('tbl_donors_payment',array("id"=>base64_decode($did)));
			$donor_name = $donor_details[0]["donor_fname"].' '.$donor_details[0]["donor_lname"];
			$donor_email = $donor_details[0]["donor_email"];
			
			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "mail";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;
			$str = "<p>Dear $donor_name,</p>
			<p>Thank you so much for your donation. I really appreciate you thinking of me and you couldn't have chosen a more perfect present for me. Again, thanks very much.</p>";
			$this->email->initialize($config);   
			$this->email->from($player_email);  
			$this->email->to($donor_email); 
			$this->email->subject("Thanks for Supporting"); 
			$this->email->message($str);
			$bval=$this->email->send();
			echo json_encode(array('msg'=>1));
		}
		else
		{
			echo json_encode(array('msg'=>0));
		}
	}
	
	public function sendThanksfromReports()
	{
		$rid=$this->input->post('hid_rid'); 
		if($rid!='')
		{
			$report_details	= $this->admin_model->view('tbl_email_share_reports',array("id"=>base64_decode($rid)));
			$from_email	= $report_details[0]["from_email"];
			$to_email	= $report_details[0]["to_email"];

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "mail";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;
			$str = "<p>Dear Friend,</p>
			<p>Thank you so much for your donation. I really appreciate you thinking of me and you couldn't have chosen a more perfect present for me. Again, thanks very much.</p>";
			$this->email->initialize($config);   
			$this->email->from($from_email);  
			$this->email->to($to_email); 
			$this->email->subject("Thanks for Supporting"); 
			$this->email->message($str);
			$bval=$this->email->send();
			echo json_encode(array('msg'=>1));
		}
		else
		{
			echo json_encode(array('msg'=>0));
		}
	}

	public function playerloadmore()
	{
		$offset = 10;
		$limit = $this->input->post('limit')*$offset;
		$fid   = $this->input->post('fid');
		$list = $this->admin_model->player_list($limit,$offset,$fid);		
		$tbr='';
		if(count($list)>0)
		{
		foreach($list as $val)
		{
			if($val['player_image']==""){
			  $proimg =  base_url()."assets/images/noimage-150x150.jpg";   
			  }else{
			  $proimg =  base_url()."assets/player_image/thumb/".$val['player_image'];   
			  }				
		$tbr.= "<tr>
			  <td data-th=\"Player Name\">\
			  <span><img src=".$proimg." alt=\"\"></span><strong><a href=\"#\">".$val['player_fname'].' '.$val['player_lname']."></a></strong></td>
			  <td data-th=\"Title\">".$val['player_title']."</td>
			  <td data-th=\"Goal\"><span class=\"dlr\">$</span>".$val['player_goal']."</td>
			  <td data-th=\"Reached\"><span class=\"dlr\">$</span>".$val['player_goal']."</td>
			  <td data-th=\"Remaining\"><span class=\"dlr\">$</span>".$val['player_goal']."</td>
			  <td data-th=\"\" class=\"text-right\"><small>".date('M j\, Y',strtotime($val['created']))."</small></td>
			</tr>";		
				
		}
		echo $tbr;
		}
		else
		{
		echo 0;	
		}
	}

	public function fundraiserManageRewards($slug)
	{
		if(isset($_REQUEST['hid_md']) && $_REQUEST['hid_md']=='r'){$this->db->delete('tbl_rewards', array('id'=>$this->input->post('hid_id')));}
		if(isset($_REQUEST['hid_md']) && $_REQUEST['hid_md']=='d'){
			$this->db->delete('tbl_rewards', array('id'=>$this->input->post('hid_id')));
			$this->db->delete('tbl_rel_donation_reward', array('donation_id'=>$this->input->post('hid_id')));
		}
			
		$getId	= $this->admin_model->fundraiser_getID($slug);
		if($getId[0]['id']!=0){
			$fund_id = 	$getId[0]['id'];
		}else{
			$fund_id = 	0;
		}
		/*if(count($getId)>0)
		{
			$playersInfo      = $this->admin_model->fundraiser_playersInfo($getId[0]['id'],$limit,$offset);	
		}
		else{
			$playersInfo      = array();
		}
		
		 
		$data['playersInfo']      = $playersInfo; */
		$data['active_page']      = 'fundraiser';
		$data['fundraiserInfo']   = $this->admin_model->fundraiserinfo($slug); 
		$donrecset 				  = $this->admin_model->donationinfo($fund_id);
		$rewardrecset 			  = $this->admin_model->func_common_listing_where('tbl_rewards',array('fund_id'=>$fund_id),'reward_name','ASC');
		$data['doninfo']		  =	$donrecset;
		$data['slug']			  = $slug;
		$data['fund_id']		  = $fund_id;
		$data['rewardinfo']		  = $rewardrecset;
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_rewards_list',$data);
		$this->load->view('../../includes/footer');
	}

	/*-----------------------------------Manage Donations Section Start---------------------------------*/
	public function managedonation()
	{
		if($this->input->post('hid_id')!=""){
			$this->admin_model->remove('tbl_donation', array('id'=>$this->input->post('hid_id')));
			$this->admin_model->remove('tbl_rel_donation_reward', array('donation_id'=>$this->input->post('hid_id')));
		}
		$recset = $this->admin_model->donationinfo($this->session->userdata('fundraiser_id'));
		$data['info'] = $recset;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_manage_donation',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function getDonationOpenAjax()
	{
		$did 		 = $this->input->post('did');
		$getId	= $this->admin_model->fundraiser_getID($this->input->post('fslug'));		
		
		if($did!=0){
		$data['info']= $this->admin_model->view('tbl_donation', array('id'=>$did,'fund_id'=>$getId[0]['id']));	
		}else{
		$data['info']= array();	
		}
		$data['did'] = $did;
		$data['fid'] = $getId[0]['id'];
		$data['rel_info']= $this->admin_model->rewadrs_array($did);
		//$data['rewards'] = $this->admin_model->view('tbl_rewards', array('status'=>1));
		$data['rewards'] = $this->admin_model->fundraiser_wise_rewardList($getId[0]['id']);
		echo $this->load->view('view_fundraiser_add_donation_modal', $data, true);
	}
	
	public function checkDonation()
	{
		$did			= $this->input->post('hid_did');
		$donate_start	= $this->input->post('donate_start');
		$donate_end		= $this->input->post('donate_end');
		$fid			= $this->session->userdata('fundraiser_id');
		$return = $this->admin_model->donationCheck($fid,$did,$donate_start,$donate_end);
		if($return == 0){
			$data['valid']=1;
			$data['msg']='';
		}
		else{
			$data['valid']=0;
			$data['msg']="This range is already in our system!!";
		}
		echo json_encode($data);
	}
	
	public function fundraiserDonationAddEditAjax()
	{
		if($this->input->post('hid_did')==0)
		{
			$fundata = array('donate_start'	=> $this->input->post('donate_start'),
							 'donate_end'	=> $this->input->post('donate_end'),
							 'fund_id'		=> $this->input->post('hid_fid'));
			$return  = $this->admin_model->add('tbl_donation', $fundata);
			$lastId  = $this->admin_model->last_insert_id();
			$rewards = $this->input->post('reward');
			foreach($rewards as $rewa)
			{
			$fundata1 = array('donation_id'	=> $lastId,'reward_id'	=> $rewa);
			$return1  = $this->admin_model->add('tbl_rel_donation_reward', $fundata1);	
			}
			$msg = "Successfully Added !!";
		}
		else
		{
			$fundata = array('donate_start'	=> $this->input->post('donate_start'),
							 'donate_end'	=> $this->input->post('donate_end'),
							 'fund_id'		=> $this->input->post('hid_fid'));
			$return  = $this->admin_model->edit('tbl_donation', $fundata, array('id'=>$this->input->post('hid_did')));
			$return  = $this->admin_model->remove('tbl_rel_donation_reward', array('donation_id'=>$this->input->post('hid_did')));
			$lastId  = $this->input->post('hid_did');
			$rewards = $this->input->post('reward');
			foreach($rewards as $rewa)
			{
			$fundata1 = array('donation_id'	=> $lastId,'reward_id'	=> $rewa);
			$return1  = $this->admin_model->add('tbl_rel_donation_reward', $fundata1);	
			}
			$msg = "Successfully Updated !!";
		}
		if($return){
			$data['valid']=1;
			$data['msg']=$msg;
		}
		else{
			$data['valid']=0;
			$data['msg']="Error occured while processing your request!";
		}
		echo json_encode($data);
	}
	/*-----------------------------------Manage Donations Section End---------------------------------*/	
	
	public function getRewardOpenAjax()
	{
		//$data = array();
		//echo $this->input->post('fslug');
		//echo '<br>';
		//echo $this->input->post('rid');
		$fid	= $this->admin_model->fundraiser_getID($this->input->post('fslug'));
		$rid    = $this->input->post('rid');
		$data['fid'] = $fid[0]['id'];
		$data['rid'] = $this->input->post('rid');
		if($rid!=0){
		$data['info']= $this->admin_model->view('tbl_rewards', array('id'=>$rid));	
		}else{
		$data['info']= array();	
		}
		echo $this->load->view('view_fundraiser_add_reward_modal', $data, true);
	}
	
	public function fundraiserRewardAddEditAjax()
	{
		if($this->input->post('hid_rid')==0)
		{
			$fundata = array('reward_name'  	=> $this->input->post('reward_name'),
							 'reward_info'      => $this->input->post('reward_info'),
							 'fund_id'		    => $this->input->post('hid_fid'));
			$return  = $this->db->insert('tbl_rewards', $fundata);
			$msg = "Successfully Added !!";
		}
		else
		{
			$fundata = array('reward_name'  	=> $this->input->post('reward_name'),
						     'reward_info'    => $this->input->post('reward_info'));
			$return  = $this->db->update('tbl_rewards', $fundata, array('id'=>$this->input->post('hid_rid'),'fund_id'=>$this->input->post('hid_fid')));
			$msg = "Successfully Updated !!";
		}
		if($return){
			$data['valid']=1;
			$data['msg']=$msg;
		}
		else{
			$data['valid']=0;
			$data['msg']="Error occured while processing your request!";
		}
		echo json_encode($data);
	}
	/*-----------------------------------Manage Rewards Section End---------------------------------*/
    
	public function fundraiserSettings($slug)
	{
		$getId	= $this->admin_model->fundraiser_getID($slug);
		if($getId[0]['id']!=0){
			$fund_id 			= 	$getId[0]['id'];
			$fund_pass 			= $this->admin_model->getColumn('tbl_fundraiser','fund_pass',array('id'=>$fund_id));
			$encrypt_key 		= $this->config->item('encryption_key');  
			$getPass 			= $this->encrypt->decode($fund_pass,$encrypt_key);
		}else{
			$fund_id = 	0;
			$getPass = '';
		}
		$data['active_page']    = 'fundraiser';
		$data['fundraiserInfo'] = $this->admin_model->fundraiserinfo($slug); 
		$data['slug']			= $slug;
		$data['dpass']			= $getPass;
		$data['fund_id']		= $fund_id;
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_setting',$data);
		$this->load->view('../../includes/footer');
		
	}
		
	public function fundPlayerStatus()
	{
		$fund_id  =$this->input->post("fid");		
		$status   =$this->input->post("sts");
		$response = array();
		if($fund_id!='' AND $status!='')
		{
            $cnt=$this->admin_model->view_check('tbl_fundraiser',array('id'=>$fund_id));
            if($cnt > 0)
            {
				/*
				if($status==1)
				{
				$encrypt_key = $this->config->item('encryption_key'); 
				
                $fund_dt=$this->admin_model->view('tbl_fundraiser',array('id'=>$fund_id));
				$fundraiser_name	= $fund_dt[0]['fund_username'];
				$fundraiser_org = $fund_dt[0]['fund_org_goal'];
				$fundraiser_ind = $fund_dt[0]['fund_individual_goal'];
				$fundraiser_start = $fund_dt[0]['fund_start_dt'];
				$fundraiser_end = $fund_dt[0]['fund_end_dt'];
				$fundraiser_email = $fund_dt[0]['fund_email'];
				$fundraiser_url = base_url('admin/login');
				$admin_cord = 'Thanksforsupporting';
				$admin_name = 'John Lewis';
				$admin_email = 'jlewis@mycustomvideo.com';
				$pass = $fund_dt[0]['fund_pass'];
				$password = $this->encrypt->decode($pass,$encrypt_key);
				
                $temp_dt=$this->admin_model->view('tbl_mail_template',array("template_slug"=>'create-fundraiser'));
				$temp_body = html_entity_decode(htmlspecialchars_decode($temp_dt[0]['template_content']));
				$search_key = array('Participant Email #1','[FundraiserCoordinator]','[FundraiserName]','[login@thanksforsupporting.com]','[ParticipantFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[ParticipantGoal]','[ParticipantEmail]','[Password]','https://thanksforsupporting.com/fundraiser/player');
				$search_val = array($fundraiser_email,$admin_cord,$admin_name,$admin_email,$fundraiser_name,$fundraiser_start,$fundraiser_end,$fundraiser_org,$fundraiser_ind,$fundraiser_email,$password,$fundraiser_url);
				$mail_content = str_replace($search_key,$search_val,$temp_body);
				//echo $mail_content;print_r($search_key);print_r($search_val);die;
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$str = $mail_content;
				$this->email->initialize($config);   
				$this->email->from($admin_email);  
				$this->email->to($fundraiser_email); 
				$this->email->subject("Thanks for Supporting - Approved by Admin"); 
				$this->email->message($str);
				$bval=$this->email->send();	
				}
				*/
				$updt=$this->admin_model->edit('tbl_fundraiser',array("fund_status"=>$status),array('id'=>$fund_id));
                $response=array("valid"=>1,"msg"=>"Status Updated Successfully");
            }
            else
            {
                $response=array("valid"=>0,"msg"=>"Invalid Data Provided");	
            }
		}
		else
		{
		  $response=array("valid"=>0,"msg"=>"Sufficient Data Not Provided");		
		}
		echo json_encode($response);
	}

	public function sendtomailtofunraiser($slug)
    {
       $data['response'] = '';
        $email_content="";
        if($slug!='')
        {
            $cnt = $this->admin_model->view_check('tbl_fundraiser',array('fund_slug_name'=>$slug));
            if($cnt > 0)
            {
                $this->load->model('cronmail/Cronmail_model');
                $encrypt_key 	= $this->config->item('encryption_key');
                $template = $this->Cronmail_model->view('tbl_mail_template',array('template_slug'=>'fundraiser-is-over-to-player-from-fundraiser'));
                $settings = $this->Cronmail_model->view('tbl_settings',array('id'=>1));
				$sql = "SELECT fr.*,tp.id player_id,tp.player_slug_name,tp.player_fname,tp.player_lname,tp.player_email,tp.player_pass,tp.player_goal FROM tbl_fundraiser fr 
				LEFT JOIN tbl_player tp ON fr.id = tp.player_added_by 
				WHERE fr.fund_slug_name='".$slug."'";
				//tp.invited = 1 AND 
                $query = $this->db->query($sql);

                $fundraiser_dt = $query->result_array();

                if($fundraiser_dt) {

                    $fund_email = $fundraiser_dt[0]["fund_email"];
                    $fundraiser_id = $fundraiser_dt[0]['id'];
                    $fundraiser_name = $fundraiser_dt[0]['fund_username'];
                    $fundraiser_start = date('F d, Y',strtotime($fundraiser_dt[0]['fund_start_dt']));
                    $fundraiser_end = date('F d, Y',strtotime($fundraiser_dt[0]['fund_end_dt']));
                    $fundraiser_email = $fundraiser_dt[0]['fund_email'];
                    $fundraiser_no = $fundraiser_dt[0]['fund_contact'];
                    $fundraiser_org_goal = $fundraiser_dt[0]['fund_org_goal'];
                    $fundraiser_ind_goal = $fundraiser_dt[0]['fund_individual_goal'];
					
                    $selected_color = $fundraiser_dt[0]['fund_color_rgb'];

                    $player_id = $fundraiser_dt[0]['player_id'];
                    $player_first = $fundraiser_dt[0]['player_fname'];
                    $player_last = $fundraiser_dt[0]['player_lname'];
                    $player_goal = $fundraiser_dt[0]['player_goal'];
                    $player_email = $fundraiser_dt[0]['player_email'];

                    $start_fundraiser = base_url("start-fundraiser");
                    $collects = $this->Cronmail_model->fundraiser_collects($fundraiser_id);

                    $fund_id = $collects[0]['fund_id'];
                    $deduct_amt = $settings[0]["deductable_amt"];
                    $less_per = $settings[0]["less_percent"];
                    $donations = $collects[0]['raised_amt'];
                    $collected = $collects[0]['raised_amt'];
                    $donors = $collects[0]['donors'];
                    $deductable = ($donors * $deduct_amt);
                    $lessamt = (($collects[0]['raised_amt'] * $less_per) / 100);
                    $deduct = ($deductable + $lessamt);
                    $deposits = ($collected - $deduct);

                    $goalpercent = (($donations / $fundraiser_ind_goal) * 100);

                    $participationrate = $this->Cronmail_model->view_check('tbl_player', array('player_added_by' => $fundraiser_id, 'invited' => 1));
                    $donations_part = $this->Cronmail_model->donations_part($fundraiser_id);
                    $donationdetailslist = '';

                    if (count($donations_part) > 0) {
                        $tot_donation = 0;
                        $tot_fees = 0;
                        $tot_transfer = 0;
                        foreach ($donations_part as $part) {

                            $lessamt = (($part['donation'] * $less_per) / 100);
                            $fees = ($deduct_amt + $lessamt);
                            $transfer = ($part['donation'] - $fees);
                            $donationdetailslist .= '<tr>
					<td valign="top" style="padding:15px;border-bottom:1px solid #eee;">' . $part["donor_name"] . '</td>
					<td valign="top" style="padding:15px 10px;border-bottom:1px solid #eee;">' . $part["participant_name"] . '</td>
					<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">' . date("m-d-Y", strtotime($part["donation_date"])) . '</td>
					<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$' . number_format($part["donation"], 2) . '</td>
					<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$' . number_format($fees, 2) . '</td>
					<td valign="top" style="text-align:right;padding:15px;border-bottom:1px solid #eee;">$' . number_format($transfer, 2) . '</td>
					</tr>
					<tr>';
                            $tot_donation += $part["donation"];
                            $tot_fees += $fees;
                            $tot_transfer += $transfer;
                        }
                        $donationdetailslist .= '<td valign="top" colspan="3" style="padding:15px;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #eee;">Grand Total</td>
				<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$' . number_format($tot_donation, 2) . '</td>
				<td valign="top" style="text-align:right;padding:15px 10px;border-bottom:1px solid #eee;">$' . number_format($tot_fees, 2) . '</td>
				<td valign="top" style="text-align:right;padding:15px;font-weight:bold;border-bottom:1px solid #eee;">$' . number_format($tot_transfer, 2) . '<span style="color:#ffb944;">**</span></td>
				</tr>';

                        $donationdetailslist .='<tr><td valign="top" colspan="3" style="padding:15px;"><p><span style="color:#ffb944;">*</span> Platform fees equal to $'.$settings[0]["deductable_amt"].' per donation<br>
<span style="color:#ffb944;">**</span> Amount of deposit</p></td></tr>';
                        $donationdetailslist .='<tr><td valign="top" colspan="3" style="padding:15px;"><p>Thank You,<br>ThanksForSupporting</p></td></tr>';
                    }

                    if(isset($_GET['emailtoclient']) && $_GET['emailtoclient']==1) {
                        $header		= $template[0]["template_header"];
                        $footer		= $template[0]["template_footer"];
                        $template_body		= $header.html_entity_decode($template[0]["template_content"]).$footer;
                    }
                    else
                        $template_body = $template[0]["template_content"];
                    $template_subject = $template[0]["template_subject"];
                    $search_key = array('[campaign]', '[startdate]', '[enddate]', '[firstname]', '[goal]', '[goalpercent]', '[participationrate]', '[donors]', '[donations]', '[deposits]', '[donationdetailslist]', '[selected_color]');
                    $search_val = array($fundraiser_name, $fundraiser_start, $fundraiser_end, $player_first, $fundraiser_org_goal, $goalpercent, $participationrate, $donors, $donations, $deposits, $donationdetailslist,$selected_color);
                    $email_content = str_replace($search_key, $search_val, $template_body);
                
					if(isset($_GET['emailtoclient']) && $_GET['emailtoclient']==1) {

						$config = array();
						$config['useragent'] = "CodeIgniter";
						$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
						$config['protocol'] = "mail";
						//$config['smtp_host'] = "localhost";
						//$config['smtp_port'] = "25";
						$config['mailtype'] = 'html';
						$config['charset'] = 'iso-8859-1';
						$config['newline'] = "\r\n";
						$config['wordwrap'] = TRUE;

						$this->email->initialize($config);
						$this->email->from('jlewis@thanksforsupporting.com', 'Admin');
						//$this->email->to('budhank.wms@gmail.com');

						$this->email->to($fund_email);
						$this->email->subject('ThanksForSupporting - ' . $template_subject);
						$this->email->message($email_content);
						$bval = $this->email->send();
						redirect(base_url('admin/managepayment'));

					}
				}else{
					$data['response'] = "There is no player invited.";
				}

            }
            else
            {
                $data['response'] = "Invalid Data Provided";
            }
        }
        else
        {
            $data['response'] = "Sufficient Data Not Provided";
        }

        $data['content']      = $email_content;
        $data['fundraiserInfo']   = $this->admin_model->fundraiserinfo($slug);
        $data['slug']			  = $slug;
        $this->load->view('../../includes/header');
        $this->load->view('view_fundraisersummary',$data);
        $this->load->view('../../includes/footer');



    }
	public function sendtoall($slug)
	{
		$response = array();
		if($slug!='')
		{
            $cnt = $this->admin_model->view_check('tbl_fundraiser',array('fund_slug_name'=>$slug));
            if($cnt > 0)
            {
                $fundraiser_dt		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$slug));
				$fundraiser_id		= $fundraiser_dt[0]['id'];
				//$fundraiser_name	= $fundraiser_dt[0]['fund_fname'].' '.$fundraiser_dt[0]['fund_lname'];
				$fundraiser_name	= $fundraiser_dt[0]['fund_username'];
				$fundraiser_end		= $fundraiser_dt[0]['fund_end_dt'];
				$fundraiser_email	= $fundraiser_dt[0]['fund_email'];
				$fundraiser_no		= $fundraiser_dt[0]['fund_contact'];

				$admin_cord 		= 'Thanksforsupporting';
				$admin_name 		= 'John Lewis';
				$admin_email 		= 'jlewis@thanksforsupporting.com';
				
                $temp_dt			= $this->admin_model->view('tbl_mail_template',array("template_slug"=>'fundraiser-is-over-to-player-from-fundraiser'));
				
				$temp_body			= html_entity_decode(htmlspecialchars_decode($temp_dt[0]['template_content']));
				$temp_subject		= $temp_dt[0]['template_subject'];
				$all_players		= $this->admin_model->view('tbl_player',array("player_added_by"=>$fundraiser_id));
				
				$search_key = array('[PlayerFirst]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]');
				
				foreach($all_players as $player)
				{
					$player_first = $player['player_fname'];
					$player_last = $player['player_lname'];
					$player_email = $player['player_email'];
					$search_val = array($player_first,$fundraiser_name,$fundraiser_email,$fundraiser_no);
					
					$mail_content = str_replace($search_key,$search_val,$temp_body);
					
					$config = array();
					$config['useragent']= "CodeIgniter";
					$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config['protocol'] = "mail";
					$config['smtp_host']= "localhost";
					$config['smtp_port']= "25";
					$config['mailtype'] = 'html';
					$config['charset']	= 'iso-8859-1';
					$config['newline']  = "\r\n";
					$config['wordwrap'] = TRUE;
					$str = $mail_content;
					$this->email->initialize($config);   
					$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
					$this->email->to($player_email); 
					$this->email->subject("ThanksforSupporting - ".$temp_subject); 
					$this->email->message($str);
					$bval=$this->email->send();	
				}

                $response = "Mail Send to All Successfully";
            }
            else
            {
                $response = "Invalid Data Provided";
            }
		}
		else
		{
		  $response = "Sufficient Data Not Provided";		
		}
		$this->session->set_flashdata('msgg',$response); 
		redirect(base_url('admin/fundraiser/'.$slug.'/players'));
	}

    public function paymentinformation($slug)
    {
        $getId	= $this->admin_model->fundraiser_getID($slug);
        if($getId[0]['id']!=0){
            $fund_id = 	$getId[0]['id'];
        }else{
            $fund_id = 	0;
        }



        if(isset($_POST["submit"]))
        {
            $this->form_validation->set_rules('transaction_no', 'Transaction No', 'trim|required|xss_clean');
            $this->form_validation->set_rules('transfer_date', 'Date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('transfer_amt', 'Payment Amout', 'trim|required|xss_clean');
            $this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
            if ($this->form_validation->run() != FALSE)
            {
                $transaction_no = $this->input->post('transaction_no');

                $ex_ful_st_dt  = explode('-',$this->input->post('transfer_date'));
                $ex_ful_st_dt  = $ex_ful_st_dt[2].'-'.$ex_ful_st_dt[0].'-'.$ex_ful_st_dt[1];

                $transfer_date = $ex_ful_st_dt;
                $transfer_amt = $this->input->post('transfer_amt');
                $payment_method = $this->input->post('payment_method');
                $fundraiser_id = $this->input->post('hid_fund_id');

                $dt_array = array(
                    'transaction_no' => $transaction_no,
                    'fundraiser_id' => $fundraiser_id ,
                    'transfer_date' => $transfer_date,
                    'transfer_amt' => $transfer_amt,
                    'payment_method'  => $payment_method
                );

                $fund_details_payment		= $this->admin_model->view('tbl_payment_transfer',array('fundraiser_id'=>$fund_id));
                if($fund_details_payment)
                     $this->db->update('tbl_payment_transfer', $dt_array, array('fundraiser_id'=>$fund_id));

                else
                    $this->admin_model->add('tbl_payment_transfer',$dt_array);

                $this->session->set_userdata('succ_msg', 'Payment Added Successfully');
                redirect('admin/managepayment');
            }
        }

        $data['active_page']      = 'fundraiser';
        $data['fundraiserInfo']   = $this->admin_model->fundraiserinfo($slug);
        $data['slug']			  = $slug;
        $data['fund_id']		  = $fund_id;
        $this->load->view('../../includes/header');
        $this->load->view('view_addeditpaymenttransfer',$data);
        $this->load->view('../../includes/footer');



    }
	public function fundraiserplayeradd($slug)
	{
		$getId	= $this->admin_model->fundraiser_getID($slug);
		if($getId[0]['id']!=0){
			$fund_id = 	$getId[0]['id'];
		}else{
			$fund_id = 	0;
		}
		
		if(isset($_POST["submit"]))
		{
			$this->form_validation->set_rules('player_email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('player_fname', 'First Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('player_lname', 'Last Name', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('player_pass', 'Password', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('player_goal', 'Goal', 'trim|required|xss_clean');	
			if ($this->form_validation->run() != FALSE)
			{
				$player_image= isset($_FILES['player_image']['name'])?$_FILES['player_image']['name']:"";
				$fpath1= "";
				//------------------------------------- 
				//***Player Image upload section***
				//-------------------------------------
				if($player_image!="") 
				{
					/* Create the config for upload library */				
					$config_img['upload_path']   = './assets/player_image/';
					$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
					$config_img['max_size']      = '100000';
					$this->load->library('upload', $config_img);
					$this->upload->initialize($config_img);			 
					$this->upload->do_upload('player_image');			
					$data_img = $this->upload->data(); 
					if($data_img['is_image'] == 1)
					{ 
						$fpath1  = $data_img['file_name'];			
						$this->load->library('image_lib');
						$config_resize['image_library']   = 'gd2';	
						$config_resize['create_thumb']    = TRUE;
						$config_resize['maintain_ratio']  = TRUE;
						$config_resize['master_dim']      = 'height';
						$config_resize['quality']         = "100%";  
						$config_resize['source_image']    = './assets/player_image/'.$fpath1;
						$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
						$config_resize['height']          = 120;
						$config_resize['width']           = 1;
						$this->image_lib->initialize($config_resize);
						$this->image_lib->resize(); 
					}
				}
				
				$encrypt_key = $this->config->item('encryption_key');  
				$player_added_by = $this->input->post('hid_fund_id');
				$player_email = $this->input->post('player_email');
				$player_fname = $this->input->post('player_fname');
				$player_lname = $this->input->post('player_lname');
				$player_goal  = $this->input->post('player_goal');
				$player_pass  = $this->input->post('player_pass');
				$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
				
				$dt_array = array(
				'player_email' => $player_email,
				'player_fname' => $player_fname,
				'player_lname' => $player_lname,
				'player_goal'  => $player_goal,
				'player_pass'  => $player_passw,
				'player_image' => $fpath1,
				'player_added_by' => $player_added_by,
				'created' => date("Y-m-d H:i:s")
				);
				$this->admin_model->add('tbl_player',$dt_array);
				
				//-------------------------------------------  
				//Player Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($player_fname);
				$lname_slug    = strip_tags($player_lname);  
				$jSlug         = $fname_slug.'-'.$lname_slug;    
				$playerURL	   = strtolower(url_title($jSlug));
				if($this->admin_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
				   $playerURL = $playerURL.'-'.$lastInsId; 
				}
				$x = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
				//----------------------------------------------------------------------------------------------------
				if($x)
				{
					$fund_details		= $this->admin_model->view('tbl_fundraiser',array('id'=>$player_added_by));
					$fund_email 		= $fund_details[0]["fund_email"];
					$fundraiser_id		= $fund_details[0]['id'];
					//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
					$fundraiser_name	= $fund_details[0]['fund_username'];
					$fundraiser_start	= $fund_details[0]['fund_start_dt'];
					$fundraiser_end		= $fund_details[0]['fund_end_dt'];
					$fundraiser_email	= $fund_details[0]['fund_email'];
					$fundraiser_no		= $fund_details[0]['fund_contact'];
					$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
					$selected_color		= $fund_details[0]['fund_color_rgb'];
					$player_login_url	= base_url('admin/login');
					
					$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'login-credentials-to-player-from-fundraiser'));
					
					$template_body		= html_entity_decode(htmlspecialchars_decode($template[0]["template_content"]));
					$template_subject	= $template[0]["template_subject"];
					$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNo]','https://thanksforsupporting.com/fundraiser/player/','[selected_color]');
					$search_val			= array($player_fname,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$selected_color);
					$email_content		= str_replace($search_key,$search_val,$template_body);
					
					$config = array();
					$config['useragent']= "CodeIgniter";
					$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config['protocol'] = "mail";
					$config['smtp_host']= "localhost";
					$config['smtp_port']= "25";
					$config['mailtype'] = 'html';
					$config['charset']	= 'iso-8859-1';
					$config['newline']  = "\r\n";
					$config['wordwrap'] = TRUE;
					
					$this->email->initialize($config);   
					$this->email->from('login@thanksforsupporting.com','ThanksforSupporting');  
					$this->email->to($player_email); 
					$this->email->subject('ThanksforSupporting - '.$template_subject); 
					$this->email->message($email_content);
					$bval=$this->email->send();
				}
				
				$this->session->set_userdata('succ_msg', 'Player Added Successfully');
				redirect('admin/fundraiser/'.$this->input->post('slug').'/players');
			}
		}
		
		$data['active_page']      = 'fundraiser';
		$data['fundraiserInfo']   = $this->admin_model->fundraiserinfo($slug); 
		$data['slug']			  = $slug;
		$data['fund_id']		  = $fund_id;
		$this->load->view('../../includes/header');        
		$this->load->view('view_addeditplayer',$data);
		$this->load->view('../../includes/footer');
	}
	
	public function asktoinvite($fslug,$pslug)
	{
		if($fslug!='' AND $pslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= date("F j, Y",strtotime($fund_details[0]['fund_start_dt']));
			$fundraiser_end		= date("F j, Y",strtotime($fund_details[0]['fund_end_dt']));
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_org_goal'];
			$player_goal		= $fund_details[0]['fund_individual_goal'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			if($fund_details[0]['fund_image']==""){
				$fundimg 		=  base_url()."assets/images/noimage-150x150.jpg";   
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		=  base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
			}
			
			$player_details		= $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_email		= $player_details[0]['player_email'];
			$player_password	= $player_details[0]['player_pass'];
			$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
			$player_login_url	= base_url('admin/login');
			$start_fundraiser	= base_url("start-fundraiser");

			$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'asking-to-invite-donors-to-player-from-fundraiser'));
			$html_header = '<!DOCTYPE html>
			<html lang="en">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>ThanksForSupporting</title>
			<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
			</head>
			<body>
			<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato; font-size: 20px;">
			<tr>
			<td align="center"><table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0">
			<tr>
			<td style="padding:0 30px;"><table border="0" width="640" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="http://thanksforsupporting.com/apps/assets/images/logo_thanks-for-supporting.png" /></td>
			<td style="padding:0 10px;"><img src="'.$fundimg.'" style="width: 100px;height: 100px;border-radius: 50%;" /></td>
			<td><strong>'.$fundraiser_name.'</strong> <br>
			<div style="font-size: 14px;">'.$fundraiser_slog.'</div></td>
			</tr>
			</table></td>
			</tr>
			</table></td>
			</tr>
			</table>';
			$html_footer = '<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;">
			<tr>
			<td style="padding:5px 30px 30px; text-align: center; color:#9f9f9f; font-size: 16px"> This email was sent to you because we want to make the world a better place.<br/>
			You can choose to opt out of future opportunities to help others by clicking here. </td>
			</tr>
			</table>
			</td>
			</tr>
			</table>
			</body>
			</html>';

			$template_body		= html_entity_decode($template[0]["template_content"]);
			$template_subject	= $template[0]["template_subject"];
			$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
			$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_bgcolor);
			$email_content		= $html_header.str_replace($search_key,$search_val,$template_body).$html_footer;
			//echo $email_content;die;
			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "mail";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
			$this->email->to($player_email); 
			$this->email->subject('ThanksForSupporting - '.$template_subject); 
			$this->email->message($email_content);
			$bval=$this->email->send();	
			if($bval){
			$this->admin_model->edit('tbl_player',array("invitation"=>1,"invitation_dt"=>date("Y-m-d H:i:s")),array('player_slug_name'=>$pslug));
			}
			$response = "Invitation sent";
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_flashdata('msgg',$response);
		redirect(base_url('admin/fundraiser/'.$fslug.'/participants'));
	}
	
	public function remindtoinvite($fslug,$pslug)
	{
		if($fslug!='' AND $pslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= $fund_details[0]['fund_start_dt'];
			$fundraiser_end		= $fund_details[0]['fund_end_dt'];
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
			$selected_color	= $fund_details[0]['fund_color_rgb'];
			
			$player_details		= $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_goal		= $player_details[0]['player_goal'];
			$player_email		= $player_details[0]['player_email'];
			$player_password	= $player_details[0]['player_pass'];
			$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
			$player_login_url	= base_url('admin/login');
			$start_fundraiser	= base_url("start-fundraiser");

			$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'reminder-to-invite-donors-to-player-from-fundraiser'));

			$template_body		= html_entity_decode($template[0]["template_content"]);
			$template_subject	= $template[0]["template_subject"];
			$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[selected_color]');
			$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$selected_color);
			$email_content		= str_replace($search_key,$search_val,$template_body);

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "mail";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
			$this->email->to($player_email); 
			$this->email->subject('ThanksForSupporting - '.$template_subject); 
			$this->email->message($email_content);
			$bval=$this->email->send();	
			if($bval){
			$this->admin_model->edit('tbl_player',array("reminder"=>1,"reminder_dt"=>date("Y-m-d H:i:s")),array('player_slug_name'=>$pslug));
			}
			$response = "Reminder sent";
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_flashdata('msgg',$response); 
		redirect(base_url('admin/fundraiser/'.$fslug.'/participants'));
	}
	
	public function secremindtoinvite($fslug,$pslug)
	{
		if($fslug!='' AND $pslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= $fund_details[0]['fund_start_dt'];
			$fundraiser_end		= $fund_details[0]['fund_end_dt'];
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
			$selected_color		= $fund_details[0]['fund_color_rgb'];
			
			$player_details		= $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_goal		= $player_details[0]['player_goal'];
			$player_email		= $player_details[0]['player_email'];
			$player_password	= $player_details[0]['player_pass'];
			$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
			$player_login_url	= base_url('admin/login');
			$start_fundraiser	= base_url("start-fundraiser");

			$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'2nd-reminder-to-invite-donors-to-player-from-fundraiser'));

			$template_body		= html_entity_decode($template[0]["template_content"]);
			$template_subject	= $template[0]["template_subject"];
			$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[selected_color]');
			$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$selected_color);
			$email_content		= str_replace($search_key,$search_val,$template_body);

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "mail";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
			$this->email->to($player_email); 
			$this->email->subject('ThanksForSupporting - '.$template_subject); 
			$this->email->message($email_content);
			$bval=$this->email->send();	
			if($bval){
			$this->admin_model->edit('tbl_player',array("sec_reminder"=>1,"sec_reminder_dt"=>date("Y-m-d H:i:s")),array('player_slug_name'=>$pslug));
			}
			$response = "2nd Reminder sent";
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_flashdata('msgg',$response); 
		redirect(base_url('admin/fundraiser/'.$fslug.'/participants'));
	}
	
	public function donationrequest($don_id)
	{
		if($don_id!='')
		{
			$don_details = $this->admin_model->view('tbl_donors_payment',array('id'=>$don_id));
			if(count($don_details)>0)
			{
				$fslug 				= $don_details[0]['fund_slug'];
				$pslug 				= $don_details[0]['player_slug'];
				
				$fund_details		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
				$fund_email 		= $fund_details[0]["fund_email"];
				$fundraiser_id		= $fund_details[0]['id'];
				//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
				$fundraiser_name	= $fund_details[0]['fund_username'];
				$fundraiser_start	= $fund_details[0]['fund_start_dt'];
				$fundraiser_end		= $fund_details[0]['fund_end_dt'];
				$fundraiser_email	= $fund_details[0]['fund_email'];
				$fundraiser_no		= $fund_details[0]['fund_contact'];
				$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
				$fundraiser_slog	= $fund_details[0]['fund_slogan'];
				$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
				$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
				if($fund_details[0]['fund_image']==""){
					$fundimg 		=  base_url()."assets/images/noimage-150x150.jpg";   
				}else{
					$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
					$fundimg  		=  base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
				}
				
				$player_details		= $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug));
				$player_first		= $player_details[0]['player_fname'];
				$player_last		= $player_details[0]['player_lname'];
				$player_name		= $player_first.' '.$player_last;
				$player_goal		= $player_details[0]['player_goal'];
				$player_email		= $player_details[0]['player_email'];
				if($player_details[0]['player_image']==""){
					$playerimg =  base_url()."assets/images/noimage-150x150.jpg";   
				}else{
					$playerTimg = explode(".",$player_details[0]['player_image']);	
					$playerimg  =  base_url()."assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1];   
				}
				$player_login_url	= base_url('admin/login');
				$player_url			= base_url($fslug.'/'.$pslug);
				$player_share_url	= base_url($fslug.'/'.$pslug);
				$start_fundraiser	= base_url("start-fundraiser");
				
				$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'request-for-donation-to-donor-from-player'));
				$html_header		= '<!DOCTYPE html>
				<html lang="en">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>ThanksForSupporting</title>
				<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
				</head>
				<body>
				<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato; font-size: 20px;">
				<tr>
				<td align="center">
				<table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0">
				<tr>
				<td style="padding:0 30px;">
				<table border="0" width="640" cellpadding="0" cellspacing="0">
				<tr><td><img src="http://thanksforsupporting.com/apps/assets/images/logo_thanks-for-supporting.png" /></td>
				<td style="padding:0 10px;"><img src="'.$fundimg.'" style="width: 100px;height: 100px;border-radius: 50%;" /></td>
				<td><strong>'.$fundraiser_name.'</strong> <br><div style="font-size: 14px;">'.$fundraiser_slog.'</div> </td></tr>
				</table>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>';
				$html_footer		= '<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;">
				<tr>
				<td align="center">
				<table bgcolor="#f0f0f0" border="0" width="700" cellpadding="0" cellspacing="0" style="font-size:20px;font-family:lato;">
				<tr>
				<td style="padding:5px 30px 30px; text-align: center; color:#9f9f9f; font-size: 16px">
				This email was sent to you because we want to make the world a better place.<br/>
				You can choose to opt out of future opportunities to help others by clicking here. 
				</td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				</body>
				</html>';

				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerImage]','[PlayerName]','[PlayerFirst]','[PlayerLast]','[FundraiserName]','[PlayerURL]','[PlayerShareLink]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
				$search_val			= array($playerimg,$player_name,$player_first,$player_last,$fundraiser_name,$player_url,$player_share_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_bgcolor);
				$email_content		= $html_header.str_replace($search_key,$search_val,$template_body).$html_footer;

				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('invite@thanksforsupporting.com',$player_first.' '.$player_last);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				$response = "Reminder sent";
			}else{
				$response = "Invalid id provided";
			}
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_flashdata('msgg',$response); 
		redirect(base_url('admin/fundraiser/'.$fslug.'/players/summary/'.$pslug));
	}
	
	public function reminddonationrequest($don_id)
	{
		if($don_id!='')
		{
			$don_details = $this->admin_model->view('tbl_donors_payment',array('id'=>$don_id));
			if(count($don_details)>0)
			{
				$fslug 				= $don_details[0]['fund_slug'];
				$pslug 				= $don_details[0]['player_slug'];
				
				$fund_details		= $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
				$fund_email 		= $fund_details[0]["fund_email"];
				$fundraiser_id		= $fund_details[0]['id'];
				//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
				$fundraiser_name	= $fund_details[0]['fund_username'];
				$fundraiser_start	= $fund_details[0]['fund_start_dt'];
				$fundraiser_end		= $fund_details[0]['fund_end_dt'];
				$fundraiser_email	= $fund_details[0]['fund_email'];
				$fundraiser_no		= $fund_details[0]['fund_contact'];
				$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
				$selected_color	= $fund_details[0]['fund_color_rgb'];
				
				$player_details		= $this->admin_model->view('tbl_player',array('player_slug_name'=>$pslug));
				$player_first		= $player_details[0]['player_fname'];
				$player_last		= $player_details[0]['player_lname'];
				$player_name		= $player_first.' '.$player_last;
				$player_goal		= $player_details[0]['player_goal'];
				$player_email		= $player_details[0]['player_email'];
				$player_login_url	= base_url('admin/login');
				$player_url			= base_url($fslug.'/'.$pslug);
				$player_share_url	= base_url($fslug.'/'.$pslug.'/share');
				$start_fundraiser	= base_url("start-fundraiser");
				
				$template = $this->admin_model->view('tbl_mail_template',array('template_slug'=>'reminder-request-for-donation-to-donor-from-player'));

				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerName]','[PlayerFirst]','[PlayerLast]','[FundraiserName]','[PlayerURL]','[PlayerShareLink]','[StartFundraiser]','[selected_color]');
				$search_val			= array($player_name,$player_first,$player_last,$fundraiser_name,$player_url,$player_share_url,$start_fundraiser,$selected_color);
				$email_content		= str_replace($search_key,$search_val,$template_body);

				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "mail";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('invite@thanksforsupporting.com',$player_first.' '.$player_last);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				$response = "Reminder sent";
			}else{
				$response = "Invalid id provided";
			}
		}else{
			$response = "Sufficient data not provided";
		}
		
		$this->session->set_flashdata('msgg',$response); 
		redirect(base_url('admin/fundraiser/'.$fslug.'/players/summary/'.$pslug));
	}

	public function playerInfoEditAjax()
	{
		$player_id			= $_POST['hid_pid'];
		$player_image		= isset($_FILES['proimage']['name'])?$_FILES['proimage']['name']:"";
		$player_email		= $_POST['player_email'];
		$player_fname		= $_POST['player_fname'];
		$player_lname		= $_POST['player_lname'];
		$player_goal		= $_POST['player_goal'];

		$arr=$this->admin_model->view('tbl_player',array('id'=>$player_id));
		if($player_image!="")
		{
			if(!empty($arr)){
				if(isset($arr[0]['player_image']) && $arr[0]['player_image']!=""){
					if(file_exists("./assets/player_image/thumb/".$arr[0]['player_image'])){
					  unlink("./assets/player_image/thumb/".$arr[0]['player_image']);
					}
					if(file_exists("./assets/player_image/".$arr[0]['player_image'])){
					  unlink("./assets/player_image/".$arr[0]['player_image']);
					}
				}
			}
			
			$config_img['upload_path']   = './assets/player_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('proimage');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
			{ 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/player_image/'.$fpath1;
				$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
				$config_resize['height']          = 120;
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$player_image = $fpath1;
			}
		}
		else
		{
			$player_image = $arr[0]['player_image'];
		}

		$playdata = array('player_email'	=> $this->input->post('player_email'),
						  'player_fname'	=> $this->input->post('player_fname'),
						  'player_lname'	=> $this->input->post('player_lname'),
						  'player_goal'		=> $this->input->post('player_goal'),
						  'player_image'	=> $player_image
						  );
		$return = $this->db->update('tbl_player', $playdata, array('id'=>$this->input->post('hid_pid')));
		
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}
	public function playerEmailCheckerAjax()
	{
		$where = array('player_email'=>$this->input->post('player_email')); 
		$query=$this->admin_model->view_check('tbl_player',$where);	   
		if($query>0)
		{
			echo 'false';
		}
		else
		{
			echo 'true';
		}
	}	
	public function playerSummary($fslug, $pslug)
	{
		$player_details = array();
		$donor_details = array();
		if($pslug!=''){
			$player_details=$this->admin_model->playersInfo($pslug);
			$donor_details=$this->admin_model->donors_list($pslug,10);
		}
		$data['fundraiserInfo']	= $this->admin_model->fundraiserinfo($fslug);
		$data['active_page']    = 'fundraiser'; 
		$data["player_details"] = $player_details;
		$data["donors_list"]	= $donor_details;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes/header');        
		$this->load->view('view_player_summary',$data);
		$this->load->view('../../includes/footer');    
	}
	public function getPlayerModalAjax()
	{
		$pslug=$this->input->post('pslug');  
		if($pslug!=''){
			$player_details=$this->admin_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		$data['playerInfo']      = $player_details;  
		echo $this->load->view('view_player_info_edit_modal', $data, true); 
	}
	public function playerDonors($fslug, $pslug)
	{
		$player_details = array();
		$donor_details = array();
		if($pslug!='')
		{
			$player_details=$this->admin_model->playersInfo($pslug);
			$donor_details=$this->admin_model->donors_list($pslug,10);
		}
		$data['fundraiserInfo'] = $this->admin_model->fundraiserinfo($fslug);
		$data['active_page']    = 'fundraiser';
		$data["player_details"] = $player_details;
		$data["donors_list"]	= $donor_details;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes/header');        
		$this->load->view('view_player_donors',$data);
		$this->load->view('../../includes/footer');    
	}
	public function playerEmailReportsGraph($fslug, $pslug)
	{
		$player_details = array();
		if($pslug!='')
		{
			$player_details=$this->admin_model->playersInfo($pslug);
		}
		if(count($player_details)>0)
		{
			$player_id = $player_details[0]['id'];
		}
		$report_chart 			= $this->admin_model->email_reports($player_id);
		$data['fundraiserInfo'] = $this->admin_model->fundraiserinfo($fslug);
		$data['active_page']    = 'fundraiser';
		$data["player_details"] = $player_details;
		$data['report_chart'] 	= $report_chart;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes/header');        
		$this->load->view('view_player_emailreports_graph',$data);
		$this->load->view('../../includes/footer');    
	}

	public function playerEmailReportsList($fslug, $pslug)
	{
		$player_details = array();
		$report_list = array();
		$player_id = 0;
		if($pslug!='')
		{
			$player_details = $this->admin_model->playersInfo($pslug);
		}
		if(count($player_details)>0)
		{
			$report_list = $this->admin_model->view('tbl_email_share_reports',array('from_id'=>$player_details[0]['id']));
			$player_id = $player_details[0]['id'];
		}
		$report_chart = $this->admin_model->email_reports($player_id);
		$data['fundraiserInfo'] = $this->admin_model->fundraiserinfo($fslug);
		
		$data['active_page']    = 'fundraiser';
		$data["player_details"] = $player_details;
		$data['report_list'] 	= $report_list;
		$data['report_chart'] 	= $report_chart;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes/header');        
		$this->load->view('view_player_emailreports_list',$data);
		$this->load->view('../../includes/footer');    
	}
	public function playerSetting($fslug, $pslug)
	{
		$player_details = array();
		$getPass 		= '';
		if($pslug!='')
		{
			$player_details		= $this->admin_model->playersInfo($pslug);
			$player_id 			= $player_details[0]['id'];
			$player_pass		= $this->admin_model->getColumn('tbl_player','player_pass',array('id'=>$player_id));
			$encrypt_key 		= $this->config->item('encryption_key');  
			$getPass 			= $this->encrypt->decode($player_pass,$encrypt_key);
		}
		$data['fundraiserInfo'] = $this->admin_model->fundraiserinfo($fslug);
		$data['active_page']    = 'fundraiser';
		$data["player_details"] = $player_details;
		$data['dpass']			= $getPass;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes/header');        
		$this->load->view('view_player_setting',$data);
		$this->load->view('../../includes/footer');    
	}
	public function playerStatus()
	{
		$fund_id=$this->input->post("fid");
		$play_id=$this->input->post("pid");
		$status=$this->input->post("sts");
		$response = array();
		if($fund_id!='' AND $play_id!='' AND $status!='')
		{
		$cnt=$this->admin_model->view_check('tbl_player',array("player_added_by"=>$fund_id,'id'=>$play_id));
		if($cnt > 0)
		{
		$updt=$this->admin_model->edit('tbl_player',array("player_status"=>$status),array('id'=>$play_id));	
		$response=array("valid"=>1,"msg"=>"Status Updated Successfully");
		}
		else
		{
		$response=array("valid"=>0,"msg"=>"Invalid Data Provided");	
		}
		}
		else
		{
		$response=array("valid"=>0,"msg"=>"Sufficient Data Not Provided");		
		}
		echo json_encode($response);
	}
		
	//*******************End of Fundraisers Menu**********************//

	//*******************Start of Email Template Menu**********************//
	public function emailtemplate(){
		$data['active_page']	= 'emailtemp'; 
		$data['template_list']	= $this->admin_model->email_template_list();        
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_email_template',$data);
		$this->load->view('../../includes/footer'); 
	}
	public function opentemplateModal(){
		$template_id		= $this->input->post('template_id');
		$data["template"]	= $this->admin_model->view('tbl_mail_template',array('id'=>$template_id));        
		$this->load->view('view_email_template_modal',$data); 
	}
	public function viewtemplateModal(){
		$template_id	= $this->input->post('template_id');
		$template		= $this->admin_model->view('tbl_mail_template',array('id'=>$template_id));
		$subject		= $template[0]['template_subject'];
		
		$fundraiser_name= 'Fundraiser Name';
		$fundraiser_img = 'http://thanksforsupporting.com/apps/assets/images/sub_img.jpg';
		$fundraiser_slog= 'Fundraiser Slogun';
		
		$header_key 	= array('[FundraiserName]','[FundraiserImage]','[FundraiserSlogan]','[bgcolor]','[selected_color]');
		$header_val		= array($fundraiser_name,$fundraiser_img,$fundraiser_slog,'#000','#000');
		$header			= str_replace($header_key,$header_val,$template[0]['template_header']);
		
		$body_key 		= array('[FundraiserName]','[FundraiserImage]','[FundraiserSlogan]','[PlayerImage]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
		$body_val		= array($fundraiser_name,$fundraiser_img,$fundraiser_slog,'http://thanksforsupporting.com/apps/assets/images/user_img.png','#','#000','#000','#000');
		$body			= str_replace($body_key,$body_val,html_entity_decode($template[0]['template_content']));
		
		$footer_key 	= array('[Facebook]','[Twitter]','[Google]','[Instagram]');
		$footer_val		= array('#','#','#','#');
		$footer			= str_replace($footer_key,$footer_val,$template[0]['template_footer']);
		
		$full_content	= $header.$body.$footer;
		$data['subject']= $subject;
		$data['content']= $full_content;
		$this->load->view('view_email_template_view',$data); 
	}
	public function emailtemplateAddEditAjax(){
		$template_id		= $this->input->post('template_id');
		$template_subject	= $this->input->post('template_subject');
		$template_content	= $this->input->post('template_content');
		$data	=	array(
		'template_subject'	=>	addslashes($template_subject),
		'template_content'	=>	addslashes($template_content),
		);
		$template	= $this->admin_model->edit('tbl_mail_template',$data,array('id'=>$template_id));        
		if($template)
		{
		$response=array("valid"=>1,"msg"=>"Updated Successfully");
		}
		else
		{
		$response=array("valid"=>0,"msg"=>"Error occured while processing.");		
		}
		echo json_encode($response);
	}
	//*******************End of Email Tempalte Menu************************//

	//*******************Start of Manage Payments**********************//
	public function managepayment(){
		$data['fundraiser_collects'] = $this->admin_model->fundraiser_collects();
		$data['settings'] 			 = $this->admin_model->view('tbl_settings',array('id'=>1));
		$data['active_page']         = 'payments';      
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_payment_management',$data);
		$this->load->view('../../includes/footer'); 
	}
	public function fundtransfer(){
		$fundraiser_id 		= $this->input->post('fundraiser_id');
		$total_collection 	= $this->input->post('total_collection');
		$less_percent 		= $this->input->post('less_percent');
		$less_per_amt 		= $this->input->post('less_per_amt');
		$total_deduction 	= $this->input->post('total_deduction');
		$transfer_amt 		= $this->input->post('transfer_amt');
		$transfer_date 		= date("Y-m-d H:i:s");
		$dt_array=array(
		'fundraiser_id'=>$fundraiser_id,
		'total_collection'=>$total_collection,
		'less_percent'=>$less_percent,
		'less_per_amt'=>$less_per_amt,
		'total_deduction'=>$total_deduction,
		'transfer_amt'=>$transfer_amt,
		'transfer_date'=>$transfer_date
		);
		$sts = $this->admin_model->add('tbl_payment_transfer',$dt_array);
		if($sts)
		{
			$msg = "Successfully Transfer";
		}
		else
		{
			$msg = "Transfer Fail";
		}
		$this->session->set_userdata('succ_msg', $msg);
		redirect("admin/managepayment");
	}
	public function paymentdetails($fund_slug){
		$data['active_page']  = 'payments'; 
		$data['fund_details'] = $this->admin_model->view('tbl_fundraiser',array('fund_slug_name'=>$fund_slug));
		$data['fund_collects']= $this->admin_model->fund_collection_list($fund_slug);
		//print_r($data);die;
		$this->load->view('../../includes/header');        
		$this->load->view('view_fundraiser_full_payment_list',$data);
		$this->load->view('../../includes/footer'); 
	}
	//*******************End of Manage Payments************************//

	//*******************Start of Settings**********************//
	public function settings()
	{
		$data['active_page']= 'settings';
		$fund_pass			= $this->session->userdata('password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$fund_passw 		= $this->encrypt->decode($fund_pass,$encrypt_key);
		$data["fund_dpass"]	= $fund_passw;
		$data["fund_epass"]	= $fund_pass;
		$this->load->view('../../includes/header');        
		$this->load->view('view_settings',$data);
		$this->load->view('../../includes/footer');    
	}
	public function ajaxSettings()
	{
        $fund_id			= $this->session->userdata('id');
        //$fund_email			= $_POST['username'];
        $fund_pass			= $_POST['fund_pass'];
        $fund_pass_confirm	= $_POST['fund_pass_confirm'];
		$data = array();
		if($fund_pass==$fund_pass_confirm)
		{
			$check = $this->admin_model->view_check('tbl_admin',array('id'=>$fund_id));
			if($check==1)
			{
				$encrypt_key = $this->config->item('encryption_key');  
				$fund_passw = $this->encrypt->encode($fund_pass,$encrypt_key);
			
				$fund_data = array('password' => $fund_passw);
				$return = $this->admin_model->edit('tbl_admin', $fund_data, array('id'=>$fund_id));
				
				if($return)
				{
					$this->session->set_userdata('password',$fund_passw);
					$data['valid']=1;
					$data['msg']="Password Changed Successfully";
				}
				else
				{
					$data['valid']=0;
					$data['msg']="Error Occured while processing your request.";
				}
			}
			else
			{
				$data['valid']=0;
				$data['msg']="Email address not found in our system.";
			}
		}
		else
		{
			$data['valid']=0;
            $data['msg']="Confirm Password is not matching.";
		}
        echo json_encode($data);
	}
	
	public function ShowHidePassword()
	{
		$stat = $this->input->post("term");
		if(strtolower($stat)=="show")
		{
		$fund_pass			= $this->session->userdata('password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$fund_passw 		= $this->encrypt->decode($fund_pass,$encrypt_key);
		$result				= array("spassword"=>$fund_passw,"btext"=>"Hide");
		}
		else
		{
		$fund_pass			= $this->session->userdata('password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$fund_passw 		= $this->encrypt->decode($fund_pass,$encrypt_key);
		$fund_passw			= str_repeat("*",strlen($fund_passw));
		$result				= array("spassword"=>$fund_passw,"btext"=>"Show");
		}
		echo json_encode($result);
	}	
	
	public function delMedia()
	{
		if(isset($_REQUEST['hid_fid']) && $_REQUEST['hid_fid']!="") 
		{
			$fslug = $_REQUEST['fslug'];
			$img = $this->admin_model->view('tbl_rel_fund_feature_images', array('id'=>$_REQUEST['hid_fid']));
			if(isset($img[0]['media_type']) && $img[0]['media_type']=="I"){
				if(isset($img[0]['fund_feature_image']) && $img[0]['fund_feature_image']!=""){
					$tfet_img = explode(".",$img[0]['fund_feature_image']);
					if(file_exists("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1])){
					  unlink("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1]);
					}
					if(file_exists("./assets/fundraiser_feature_image/".$img[0]['fund_feature_image'])){
					  unlink("./assets/fundraiser_feature_image/".$img[0]['fund_feature_image']);
					}
				}
			}
			$this->db->delete('tbl_rel_fund_feature_images', array('id'=>$_REQUEST['hid_fid']));
			$this->session->set_userdata('base_msg', "Successfully deleted");
			redirect("admin/fundraiser/".$fslug.'/fundraiser');
		}
	}
	//*******************End of Settings************************//

	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('password');
		$this->session->unset_userdata('contact_email');
		$this->session->set_userdata('err_msg', 'Successfully Logout !');
		redirect('admin/login');
	}

	public function FundShowHidePassword()
	{
		$stat 				= $this->input->post("term");
		$getId 				= $this->input->post("fund_id");
		$fund_pass			= $this->admin_model->getColumn('tbl_fundraiser','fund_pass',array('id'=>$getId));
		$encrypt_key 		= $this->config->item('encryption_key');  
		$fund_passw 		= $this->encrypt->decode($fund_pass,$encrypt_key);
		if(strtolower($stat)=="show password")
		{
		$result				= array("spassword"=>$fund_passw,"btext"=>"Hide Password");
		}
		else
		{
		$fund_passw			= str_repeat("*",strlen($fund_passw));
		$result				= array("spassword"=>$fund_passw,"btext"=>"Show Password");
		}
		echo json_encode($result);
	}

    public function FundShowHideEmail()
    {
        $stat 				= $this->input->post("term");
        $getId 				= $this->input->post("fund_id");
        $fund_pass			= $this->admin_model->getColumn('tbl_fundraiser','fund_email',array('id'=>$getId));


        if(strtolower($stat)=="show login name/email")
        {
            $result				= array("spassword"=>$fund_pass,"btext"=>"Hide Login name/email");
        }
        else
        {
            $fund_passw			= str_repeat("*",strlen($fund_pass));
            $result				= array("spassword"=>$fund_passw,"btext"=>"Show Login name/email");
        }
        echo json_encode($result);
    }

	
	public function PlayShowHidePassword()
	{
		$stat 				= $this->input->post("term");
		$getId 				= $this->input->post("player_id");
		$player_pass		= $this->admin_model->getColumn('tbl_player','player_pass',array('id'=>$getId));
		$encrypt_key 		= $this->config->item('encryption_key');  
		$player_passw 		= $this->encrypt->decode($player_pass,$encrypt_key);
		if(strtolower($stat)=="show password")
		{
		$result				= array("spassword"=>$player_passw,"btext"=>"Hide Password");
		}
		else
		{
		$player_passw		= str_repeat("*",strlen($player_passw));
		$result				= array("spassword"=>$player_passw,"btext"=>"Show Password");
		}
		echo json_encode($result);
	}	
}
