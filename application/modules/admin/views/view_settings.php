<div id="page-wrapper">
  <div class="full_top_wrp bg_wht">
    <ul class="breadcrumb">
      <li> <a href="settings.html">Settings</a> </li>
    </ul>
  </div>
  <div class="container-fluid">
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="tit1_sec clearfix">
        <div class="left">
          <h2>Settings</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
          <div class="acc_cre">
            <div class="bg_wht mar_t_15 tot_pad">
              <div class="tit2_sec clearfix">
                <div class="left">
                  <h3>Account credentials</h3>
                </div>
                <div class="right"><a href="#" class="grn2" data-toggle="modal" data-target="#edit_login_credential">Edit</a></div>
              </div>
              <div class="fund_con">
                <p>User Name <span><?php echo $this->session->userdata('username');?></span></p>
                <p><span style="display:block;font-weight:normal !important;">Password</span> <span class="left" id="pass_str"><?php echo str_repeat("*",strlen($fund_dpass));?></span> <a href="#" class="grn2 pull-right" id="btshow">Show</a> </p>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Edit Login Credential -->
<div id="edit_login_credential" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tit1_sec clearfix">
          <div class="left">
            <h2>Edit Login Credential</h2>
            <div class="ms" style="display:none;"></div>
          </div>
        </div>
        <div class="add_plr_wrp">
          <form method="post" name="changesettings" id="changesettings">
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="frm">
                  <div class="form-group">
                    <label>Change password for</label>
                    <input type="text" class="form-control" id="fund_email" name="fund_email" value="<?php echo $this->session->userdata('username');?>" readonly/>
                  </div>
                  <div class="form-group">
                    <label for="new-pass">New password</label>
                    <input type="password" class="form-control" id="fund_pass" name="fund_pass" required/>
                  </div>
                  <div class="form-group">
                    <label for="re-pass">Retype new password</label>
                    <input type="password" class="form-control" id="fund_pass_confirm" name="fund_pass_confirm" required/>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="frm_btn_grp">
              <input type="submit" class="btn_round" name="submit" value="Save"/>
              <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a> </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Edit Login Credential ends --> 
<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script> 
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script> 
<script type="text/javascript">
$(document).ready(function(){ 
	$( "#changesettings" ).validate({
		rules: {
			fund_pass: {
				required: true
			},
			fund_pass_confirm: {
				required: true,
				equalTo: "#fund_pass"
			}
		},
		submitHandler: function ( form ) {
			$.ajax( {
				url: "<?php echo base_url();?>admin/ajaxSettings",
				type: "POST",
				data: $( "#changesettings" ).serialize(),
				dataType: "json",
				success: function ( results ) {
					if(results.valid==1)
					{
						$('.ms').html(results.msg);
						$('.ms').show();
						setTimeout(function() {
							//$('.ms').html('');                
							location.reload();
						}, 3000);
					}
					else
					{
						$('.ms').html(results.msg);
						$('.ms').show();
					}
				}
			});
			return false;
		}
	});
	
	$( "#btshow" ).click(function(){
		$.ajax({
			url: "<?php echo base_url();?>admin/ShowHidePassword",
			type: "POST",
			data: {term:$( "#btshow" ).text()},
			dataType: "json",
			success: function ( results ) {
			$('#pass_str').html(results.spassword);
			$('#btshow').text(results.btext);
			}
		});
		return false;
	});
});
</script> 