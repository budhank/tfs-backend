<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
			<li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug; ?>/players">All fundraisers</a></li>
            <li><?php echo $fundraiserInfo[0]['fund_fname'].' '.$fundraiserInfo[0]['fund_lname'];?></li>
			<li class="active">Add New Player</li>
		</ul>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Add New Player</h2></div>
			</div>
			<div class="add_plr_wrp mar_t_15">
			<form action="<?php echo base_url()?>admin/fundraiser/<?php echo $slug; ?>/players/add" method="POST" id="playeraddedit" name="playeraddedit" enctype="multipart/form-data">
            <input type="hidden" name="hid_fund_id" id="hid_fund_id" value="<?php echo $fund_id;?>" />
            <input type="hidden" name="slug" id="slug" value="<?php echo $slug;?>" />
				<div class="row">
					<div class="col-md-12"><h4>Personal Details</h4></div>
					<div class="col-md-6 col-sm-8 col-xs-12">
						<div class="frm">
							<div class="form-group">
								<label>Email address(Player Login id)</label>
								<input type="email" class="form-control" id="player_email" name="player_email" required/>
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control" id="player_pass" name="player_pass" required/>
							</div>
							<div class="form-group">
								<label>First Name</label>
								<input type="text" class="form-control" id="player_fname" name="player_fname" required/>
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" class="form-control" id="player_lname" name="player_lname" required/>
							</div>
							<div class="form-group">
								<label>Goal</label>
								<input type="text" class="form-control" id="player_goal" name="player_goal" required/>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-4 col-xs-12 text-center">
						<label>Picture</label>
						<div class="upld_img" style="margin:auto;">
							<img id="uploadPreview1" src="<?php echo base_url()?>assets/admin/images/no_image.jpg" />
							<input class="up_file" id="uploadImage1" type="file" name="player_image" onchange="PreviewImage(1);" />
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
					<input type="submit" name="submit" value="Add" class="btn_round"/>
					<a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug; ?>/players" class="undr_lin">Cancel</a>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
   $("#playeraddedit").validate({
            rules: {
                fund_end_dt: {
                    required: true,
                    endDate: true
                },
                player_email: {
                    required  : true,
                    email: true,
                    remote: {
                        url : '<?php echo base_url();?>admin/playerEmailCheckerAjax',
                        type: "post",
                        data: {
                        player_email: function() {
                        return $( "#player_email" ).val();
						}
						}
					}
				}
		},
		messages:
		{
            player_email:{
                remote:"This email id is already in use!"
            }
		}
		});
});
</script>