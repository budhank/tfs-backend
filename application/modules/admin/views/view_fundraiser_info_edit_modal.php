<form name="frm_popup_edit" id="frm_popup_edit" method="post" enctype="multipart/form-data" action=""> 
    <input type="hidden" name="hid_ffid" id="hid_ffid" value="<?php echo $fundraiserInfo[0]['id'];?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="tit1_sec clearfix">
                        <div class="left">
                            <h2>Edit Profile info</h2>                            
                        </div>
                        <div class="ms" style="display:none;"></div>
                        <button type="button" class="close right" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i></button>
                    </div>
                    <div class="add_plr_wrp">
                        <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="frm">
                                    <div class="form-group">
                                        <label>Fundraiser Name</label>
                                        <input type="text" class="form-control required" id="fund_username" name="fund_username" value="<?php echo $fundraiserInfo[0]['fund_username'];?>">
                                    </div>
                                    <!--<div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control required" id="fund_lname" name="fund_lname" value="<?php //echo $fundraiserInfo[0]['fund_lname'];?>">
                                    </div>-->
                                    <!--<div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control" id="" placeholder="celtic2017@ctt.org">
                                </div>-->
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" class="form-control required" id="fund_city" name="fund_city" value="<?php echo $fundraiserInfo[0]['fund_city'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <select name="fund_state" id="fund_state" class="form-control required">
                                    <option value="">Select</option>
                                    <?php                                            
                                    foreach($stateList as $sval) 
                                    {
                                    ?>
                                    <option value="<?php echo $sval['state_initial'];?>" <?php if($fundraiserInfo[0]['fund_state']==$sval['state_initial']){ echo 'selected="selected"';} ?> ><?php echo $sval['name'];?></option>
                                    <?php
                                    }  
                                    ?>
                                    </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Slogan</label>
                                        <input type="text" class="form-control required" id="fund_slogan" name="fund_slogan" value="<?php echo $fundraiserInfo[0]['fund_slogan'];?>">
                                    </div>
                                </div>
                            </div>
                            <?php
                            if($fundraiserInfo[0]['fund_image']==""){
                            $proimg =  base_url()."assets/images/noimage-150x150.jpg";   
                            }else{
                            $protimg = explode(".",$fundraiserInfo[0]['fund_image']);	
                            $proimg  =  base_url()."assets/fundraiser_image/thumb/".$protimg[0].'_thumb.'.$protimg[1];   
                            } 
                            ?>
                            <div class="col-sm-5 col-xs-12 text-center">
                                <label>Picture</label>
                                <div class="upld_img" style="margin:auto;">
                                    <img id="uploadPreview1" src="<?php echo $proimg;?>" />
                                </div>
                                <div class="chng_up_file_wrp">
                                    <input class="chng_up_file" id="uploadImage1" placeholder="Cnange" type="file" name="proimage" onchange="PreviewImage(1);" />
                                    <a href="JavaScript:Void(0);">Change or remove</a> 
                                    <!--<input type="file" name="proimg" id="proimg" />-->
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Goal</h4>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="frm">
                                    <div class="form-group ico_fld">
                                        <label>Goal amount</label>
                                        <span class="fa dlr">$</span>
                                        <input id="fund_org_goal" name="fund_org_goal" type="text" class="form-control" value="<?php echo $fundraiserInfo[0]['fund_org_goal'];?>">
                                    </div>
                                    <div class="form-group ico_fld">
                                        <label>Individual Goal amount</label>
                                        <span class="fa dlr">$</span>
                                        <input id="fund_individual_goal" name="fund_individual_goal" type="text" class="form-control" value="<?php echo $fundraiserInfo[0]['fund_individual_goal'];?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12"></div>
                        </div>
                        <hr>
                        <div class="frm_btn_grp">
                            <!--<a href="#" class="btn_round">Save</a> -->
                            <button type="submit" name="Save" class="btn_round">Save</button>
                            <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</form>




<script type="text/javascript">
    $(document).ready(function() {  
      //var formData = new FormData();
      //formData.append('file', $('input[type=file]')[0].files[0]);    
        
      $("#frm_popup_edit").validate({
        submitHandler: function(form) {
            
        //var $form    = form,
        var formData = new FormData();
        //params   = $form.serializeArray(),
            
        formData.append('file', $('input[type=file]')[0].files[0]); 
        formData.append('fund_username', $('#fund_username').val());  
        formData.append('fund_slogan', $('#fund_slogan').val()); 
        formData.append('fund_city', $('#fund_city').val()); 
        formData.append('fund_state', $('#fund_state').val()); 
        formData.append('fund_org_goal', $('#fund_org_goal').val());   
        formData.append('fund_individual_goal', $('#fund_individual_goal').val()); 
        formData.append('hid_ffid', $('#hid_ffid').val()); 
            
        $.ajax({
        url:'<?php echo base_url();?>admin/fundraiserInfoEditAjax',
            
        type:'POST',
        //data: $('#frm_popup_edit').serializeArray(),
        data: formData,    
        contentType: false,       
        //cache: false,             
        processData:false,     
        dataType:"json",
        //mimeType: "multipart/form-data",  
            
        beforeSend: function(){
           // $('#div_loading').html('<div class="loading_inn">loading...</div>');
        },
        success:function(results){
            //jQuery.noConflict();
            //alert(results);
			$('.ms').show();	
            if(results.msg==1){			
            $('.ms').html('Successfully updated!!');
            setTimeout(function() {
                //$('.ms').html('');                
                location.reload();
				//window.location.href=results.fslug;
                }, 3000);
            }
            else{
                $('.ms').html('Updation is failured');
            }            
        }
       });
      }
      });   
    });
</script>    
