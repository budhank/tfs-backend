<form name="frm_popup_fund_des" id="frm_popup_fund_des" method="post" enctype="multipart/form-data" action="">
  <input type="hidden" name="hid_fid" id="hid_fid" value="<?php echo $fundraiserInfo[0]['id'];?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tit1_sec clearfix">
          <div class="left">
            <h2>Edit Fundraiser Description</h2>            
          </div>
          <div class="ms" style="display:none;"></div>
        </div>
        <div class="add_plr_wrp">
              <div class="frm">
                <div class="form-group">
                  <label for="fund_des">Description</label>                  
                  <textarea name="fund_des"  id="fund_des" class="form-control editme required" rows="5"><?php echo $fundraiserInfo[0]['fund_des'];?></textarea>
                </div>                
              </div>
          <hr>
          <div class="frm_btn_grp"> 
            <!--<a href="#" class="btn_round">Save</a>-->
            <input type="submit" name="btn_save" id="btn_save" class="btn_round" value="Save" />
            <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a> </div>
        </div>
      </div>
    </div>
  </div>
</form>
<script src="<?php echo base_url(); ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({ 
	selector:'textarea.editme' ,
	height: 250,
	menubar: false,
	plugins: [
	'advlist autolink lists charmap preview anchor',
	'searchreplace visualblocks code fullscreen',
	'insertdatetime media contextmenu paste code'
	],
	toolbar: 'bold italic underline |  alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect fontsizeselect',
	content_css: [
	'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
	'//www.tinymce.com/css/codepen.min.css'
	],
	onchange_callback: function(editor) {
		tinyMCE.triggerSave();
		$("#" + editor.id).valid();
	},
	encoding: "xml"
});
</script>
<script type="text/javascript">
$(function() {
		var validator = $("#frm_popup_fund_des").submit(function() {
			// update underlying textarea before submit validation
			tinyMCE.triggerSave();
		}).validate({
			ignore: "",
			rules: {
				fund_des: "required"
			},
			errorPlacement: function(error, element) {
				// position error label after generated textarea
				if (element.is("textarea")) {
					error.insertAfter(element);
				} else {
					error.insertAfter(element)
				}
			},
			submitHandler: function(form) {
				$.ajax({
				url:'<?php echo base_url();?>admin/fundraiserDesEditAjax',            
				type:'POST',
				data: $('#frm_popup_fund_des').serializeArray(),     
				dataType:"json",  
				beforeSend: function(){
				   //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
				},
				success:function(results){
					if(results.msg==1){
					$('.ms').html('Successfully updated!!');
					$('.ms').show();
					setTimeout(function() {               
						location.reload();
						}, 3000);
					}
					else{
						$('.ms').html('Updation is failured');
					}            
				}
			   });
			 }
		});
		validator.focusInvalid = function() {
			// put focus on tinymce on submit validation
			if (this.settings.focusInvalid) {
				try {
					var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
					if (toFocus.is("textarea")) {
						tinyMCE.get(toFocus.attr("id")).focus();
					} else {
						toFocus.filter(":visible").focus();
					}
				} catch (e) {
					// ignore IE throwing errors when focusing hidden elements
				}
			}
		}
	});

</script> 
