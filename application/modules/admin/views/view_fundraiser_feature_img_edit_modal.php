<form name="frm_popup_fund_fimg" id="frm_popup_fund_fimg" method="post" enctype="multipart/form-data" action="">
<input type="hidden" name="hid_fid" id="hid_fid" value="<?php echo $fundraiserInfo[0]['id'];?>">
  <?php
  if($fundraiserInfo[0]['fund_feature_image']==""){
  $featureimg =  base_url()."assets/images/no-image570x200.jpg";   	  
  }else{
  $featureimg =  base_url()."assets/fundraiser_feature_image/".$fundraiserInfo[0]['fund_feature_image'];   
  }
  ?>				   
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Edit Fundraiser image / video</h2></div>
                <div class="ms" style="display:none; padding-bottom:3px;"></div>
			</div>
			<div class="add_plr_wrp">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="edit_found_img">
							<img src="<?php echo $featureimg;?>" class="img-responsive" alt="">
						</div>
						<div class="clearfix mar_t_15">							
                            <input type="file" name="proimage" id="proimage" required/>                            
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
                    <button type="submit" name="Save" class="btn_round">Save</button>
                    <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
			</div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function ($) {
$("#frm_popup_fund_fimg").validate({
	rules: {
		proimage:{
		extension: "jpg|jpeg|png|gif",
		//filesize : 7340032,  //1048576
	   }
	},
	messages:
		 {
			 proimage:
			 {
				proimage:"Please upload valid file type.",
			 }
		 },
	submitHandler: function(form) {
            
        //var $form    = form,
        var formData = new FormData();
        //params   = $form.serializeArray(),
            
        formData.append('file', $('input[type=file]')[0].files[0]);         
        formData.append('hid_fid', $('#hid_fid').val()); 
            
        $.ajax({
        url:'<?php echo base_url();?>admin/fundraiserFeatureEditAjax',
            
        type:'POST',
        //data: $('#frm_popup_edit').serializeArray(),
        data: formData,    
        contentType: false,       
        //cache: false,             
        processData:false,     
        dataType:"json",
        //mimeType: "multipart/form-data",  
            
        beforeSend: function(){
           // $('#div_loading').html('<div class="loading_inn">loading...</div>');
        },
        success:function(results){
            //jQuery.noConflict();
            //alert(results);
			$('.ms').show();	
            if(results.msg==1){			
            $('.ms').html('Successfully updated!!');
            setTimeout(function() {
                //$('.ms').html('');                
                location.reload();
				//window.location.href=results.fslug;
                }, 3000);
            }
            else{
                $('.ms').html('Updation is failured');
            }            
        }
       });
      }
	});   
    
});

</script> 
