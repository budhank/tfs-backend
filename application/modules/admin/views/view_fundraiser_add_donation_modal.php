<form name="frm_donation_add" id="frm_donation_add" class="form-inline" method="post" enctype="multipart/form-data" onsubmit="">
   <input type="hidden" name="hid_did" id="hid_did" value="<?php echo $did;?>"/>
   <input type="hidden" name="hid_fid" id="hid_fid" value="<?php echo $fid;?>">
	<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Add/Edit Donation</h2></div>
				<div class="ms" style="display:none;"></div>
			</div>
			<div class="add_plr_wrp">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
                        <?php
						if(count($rewards)==0)
						{
						?>
                        <script type="text/javascript">
						$('.ms').show();
						$('.ms').html('There is no rewards found for donation record creation!!');
						</script>
						<?php
						}
						else
						{
						?>
						<div class="edit_don_am" style="position:relative;">
							<div class="form-group">
							<span style="display:block;">Use below text box to give donation range or specific value</span>
							<label for="email">Donation Range Amount:</label>&nbsp;&nbsp;
							<input type="text" class="form-control don" id="donate_start" name="donate_start" placeholder="Start" style="width:80px" value="<?php echo (!empty($info) && $info[0]['donate_start']!="")?$info[0]['donate_start']:'';?>"/>&nbsp;&nbsp;
							<span>=, <, ></span>&nbsp;&nbsp;
							<input type="text" class="form-control don" id="donate_end" name="donate_end" placeholder="End" style="width:80px" value="<?php echo (!empty($info) && $info[0]['donate_end']!="")?$info[0]['donate_end']:'';?>"/>
							&nbsp;&nbsp;
							</div>
						</div>
						<hr>
						<div class="rew_don">
							<h4>Select Rewards for donation:</h4>
							<p>You can select more then one reward</p>
							<div class="row reward-wrap">
								<div class="" id="rewards_row">
								<?php
								/*echo "<pre>";
								print_r($rewards);
								echo "</pre>";*/
								foreach($rewards as $rew)
								{
								?>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="rew_don_inr">
										<label>
											<div class="rew_don_rdo">
											<input type="checkbox" class="rew" name="reward[]" value="<?php echo $rew["id"];?>" <?php if(in_array($rew["id"],$rel_info)){ echo 'checked'; }?>/>
											</div>
											<div class="rew_don_con">
												<strong><?php echo $rew["reward_name"];?></strong>
											</div>
										</label>
									</div>
								</div>
								<?php
								}
								?>
								<div class="clearfix"></div>
								</div>
							</div>
						</div>
                        <?php
						}
						?>
					</div>
				</div>
                
				<div class="frm_btn_grp">
                <?php if(count($rewards)!=0){?>
					<input type="submit" name="submit" class="btn_round" value="Save" />
                <?php } ?>    
					<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
                
			</div>
      </div>
    </div>
	</div>
</form>
<script type="text/javascript">
	$( document ).ready( function ( $ ) {
		$( "#frm_donation_add" ).submit(function(e) {			
			var error=0;
			var don_min = $("#donate_start").val().trim();
			var don_max = $("#donate_end").val().trim();
			var rew_len = $('.rew:checked').length;
			
			$(".error").remove();
			$("#rewards_row").removeClass('rewards_row');
			
			if(don_min=='' || don_max=='')
			{
				$("#donate_end").after('<label for="donate_end" class="error" style="left:168px;">Both fields are required.</label>');
				error=1;
			}
			else if(don_min!='' && don_max!='')
			{
				if(don_max < don_min)
				{
					$(".error").remove();
					$("#donate_end").after('<label for="donate_end" class="error" style="left:168px;">Start Amount must be greater than End Amount.</label>');
					error=1;
				}
				else
				{
					$.ajax({
						url: '<?php echo base_url();?>admin/checkDonation',
						type: 'POST',
						data: $( '#frm_donation_add' ).serializeArray(),  
						async: false,
						dataType: "json",
						success: function ( results ){
							if (results.valid != 1){
							$("#donate_end").after('<label for="donate_end" class="error" style="left:168px;">'+results.msg+'</label>');
							error=1;
							}
						}
					});
				}
			}
			
			if(rew_len=='' || rew_len==0)
			{
				$("#rewards_row").addClass('rewards_row');
				$("#rewards_row").after('<label class="error">Please select atleast one reward</label>');
				error=1;
			}
			
			if(error!=1)
			{
				$.ajax({
					url: '<?php echo base_url();?>admin/fundraiserDonationAddEditAjax',
					type: 'POST',
					data: $( '#frm_donation_add' ).serializeArray(),    
					dataType: "json",
					beforeSend: function () {
						$('.error').remove();
					},
					success: function ( results ){
						if (results.valid == 1){
							$( '.ms' ).html( results.msg );
							$( '.ms' ).show();
							setTimeout(function(){             
								location.reload();
							},3000);
						}else{
							$('.ms').html('Updation is failured');
						}
					}
				});
			}
			e.preventDefault();
		});
	});
</script>