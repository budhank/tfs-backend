<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
// General options

        mode: "exact",

        elements : "agreement",

        theme : "advanced",

		width : "300",

         plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",



  // Theme options

  theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",

  //theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,preview,|,forecolor,backcolor",

  theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,advhr,|,print,|,fullscreen,|,styleprops,|,nonbreaking",

  //theme_advanced_buttons4 : "",

  theme_advanced_toolbar_location : "top",

  theme_advanced_toolbar_align : "left",

  theme_advanced_statusbar_location : "bottom",

  theme_advanced_resizing : true,

  // Example content CSS (should be your site CSS)

  content_css : "css/content.css",

  // Drop lists for link/image/media/template dialogs

  template_external_list_url : "lists/template_list.js",

  external_link_list_url : "lists/link_list.js",

  external_image_list_url : "images.php",

  media_external_list_url : "lists/media_list.js",

  // Style formats

  style_formats : [

   {title : 'Bold text', inline : 'b'},

   {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},

   {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},

   {title : 'Example 1', inline : 'span', classes : 'example1'},

   {title : 'Example 2', inline : 'span', classes : 'example2'},

   {title : 'Table styles'},

   {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}

  ],

  // Replace values for the template plugin

  template_replace_values : {

   username : "Some User",

   staffid : "991234"

  }
});
</script>

<div class="main-body container">
  <div class="page-heading">
     <h1>Agreement</h1>
     <a href="<?php echo base_url()?>admin/home" class="btn-right org-btn btn-back">Back</a>    
  </div>
  <div class="tab-main-wrapper tab_4">
  
  </div>
   <?php 
		  $attributes = array('name' => 'updateagreement', 'id' => 'updateagreement', 'class'=>'form-coor-add');
		  echo form_open_multipart('admin/agreement/',$attributes);
	?>
  	<div class="page-102">
      <div class="form-wrap-tab">
        <div class="tab-step">Agreement</div>
        <div class="form-title">
        	  <?php
			  		$update_msg = $this->session->userdata('update_msg');
					if(!empty($update_msg)){
						echo '<span style="color:#F00">Successfully Updated</span>';
						$this->session->unset_userdata('update_msg');
					}
			  ?>		
        </div>
        <div class="form-fild">
          <?php echo form_textarea(array('name'=>'agreement','id'=>'agreement','class'=>'form-coor-add-field','value'=>set_value('agreement',$agreement_row[0]['agreement_text'])));?> 
	      <?php echo form_error('agreement', '<div class="error">', '</div>'); ?>
        </div>
      </div>
      
    
      <div class="fc-list-wrapper bottom-btn">
       <a href="<?php echo base_url()?>admin/home" class="btn-right org-btn btn-back">Back</a>
      <?php
        echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'add blue-btn','value'=>'Update'));
		?>
      </div>
    </div>
  <?php
      echo form_close();
  ?>
</div>
