<div class="modal-dialog email-view-popup-wrapper">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tit1_sec clearfix">
          <div class="left">
            <h2><?php echo $subject;?></h2>
          </div>
        </div>
        <div class="add_plr_wrp">
			<div class="frm">
			<?php echo $content;?>
			</div>
			<hr>
			<div class="frm_btn_grp"> 
			<a href="#" class="undr_lin" data-dismiss="modal">Close</a> 
			</div>
        </div>
      </div>
    </div>
</div>
