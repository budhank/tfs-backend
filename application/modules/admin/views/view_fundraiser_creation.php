<script src="<?php echo base_url('assets/admin/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/admin/js/additional-methods.js')?>"></script>
<link href="<?php echo base_url('assets/css/jquery.colorpicker.bygiro.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/jquery.colorpicker.bygiro.js'); ?>"></script>
<style>
.youtube:before {
	content: "\f04b";
	font-family: "FontAwesome";
	position: absolute;
	width: 30px;
	height: 30px;
	z-index: 10;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	margin: auto;
	color: #F00;
	font-size: 30px;
}
.rem, .fa.rem {
	position: absolute;
	right: 0;
	top: 0;
	heidght: 24px;
	width: 24px;
	line-height: 22px;
	background: #fff;
	color: #333;
	text-align: center;
	cursor: pointer;
}
.congratulations-icn {
	background:#3aca60;
	color: #fff;
	font-size: 200px;
	padding: 50px;
	border-radius: 100%;
	width: 310px; 
	margin: auto auto 30px auto;
}
.col-sm-1 .chng_up_file_wrp {
    margin-top: 0px;
    overflow: inherit;
}
.color-col {
	cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    height: 30px;
    width: 30px;
}
.color-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}
.bg-black{background:#000; border:solid 1px #000; box-shadow: 1px 2px 2px 0px;}
.bg-white{background:#fff; border:solid 1px #000; box-shadow: 1px 2px 2px 0px;}

.color-selector input:active +.color-col{opacity: .9;}
.color-selector input:checked +.color-col{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
			border: solid 2px #12A5F4;
			box-shadow: 0px 0px 10px 0px #12A5F4;
			
}

</style>
<div id="page-wrapper" class="nw-admin">
  <div class="container-fluid p-gap2-0">
    <div class="sec-title">Setup Guide</div>
  </div>
  <div class="container-fluid">
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p class="p-gap2-20">Complete the Setup Steps below to get your fundraiser up and running in as little as 15 minutes.</p>
        </div>
      </div>
    </div>
    <div class="tab-section mar_t_15 ">
      <ul class="nav tab-title">
        <li class="active"> <a href="#"><b>Step 1</b><span>Your Info</span></a> </li>
        <li> <a href="#"><b>Step 2</b><span>Organization</span></a> </li>
        <li> <a href="#"><b>Step 3</b><span>Bank Info</span></a> </li>
        <li> <a href="#"><b>Step 4</b><span>Fundraiser</span></a> </li>
        <li> <a href="#"><b>Step 5</b><span>Participants</span></a> </li>
        <li> <a href="#"><b>Step 6</b><span>Start</span></a> </li>
      </ul>
      <div class="bg_wht tot_pad">
        <div class="tab-content">
          <div id="step1" class="tab-pane fade in active">
            <div class="row">
              <div class="col-sm-8 lt-form-sec">
                <h2>Step 1 - Enter Your Contact Information</h2>
                <div class="lt-form-wrap m-gap2-15">
                  <form action="<?php echo base_url('admin/create_fundraiser');?>" id="frm_contact_info" name="frm_contact_info" method="POST">
                    <div class="form-group">
                      <label>First Name</label>
                      <input type="text" class="form-control required" id="fund_fname" name="fund_fname" value="<?php echo set_value("fund_fname"); ?>">
                    </div>
                    <div class="form-group">
                      <label>Last Name</label>
                      <input type="text" class="form-control required" id="fund_lname" name="fund_lname" value="<?php echo set_value("fund_lname"); ?>">
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control required" id="fund_email" name="fund_email" value="<?php echo set_value("fund_email"); ?>">
                    </div>
                    <div class="form-group">
                      <label>Phone Number</label>
                      <input type="text" class="form-control required" id="fund_contact" name="fund_contact" value="<?php echo set_value("fund_contact"); ?>">
                    </div>
                    <div class="form-group">
                      <label>Responsible Party</label>
                    </div>
                    <div class="form-group">
                      <label class="container">I agree to act as the Coordinator for the Fundraiser. As such, I am 
                        the party responsible for the Fundraiser and the only authorized 
                        person who can communicate with ThanksForSupporting <sub>TM</sub> on 
                        behalf of the Organization. By clicking below, I acknowledge that I 
                        have read and agree to the <a href="javascript:void(0);" onclick="displayPage('<?php echo base64_encode("11230");?>')">Terms &amp; Conditions</a>
                        <input type="checkbox" class="checkbox" id="fund_terms" name="fund_terms" value="y" />
                        <span class="checkmark"></span> </label>
                    </div>
                    <div class="sec-line"></div>
                    <div class="btn-gp text-center m-gap2-25">
                      <input type="submit" name="add_btn" class="btn33" value="Next">
                    </div>
                  </form>
                </div>
              </div>
              <div  class="col-sm-4">
                <h2 class="text-center">Description</h2>
                <p>Enter your personal contact information in this form. This is the information we will use to contact you about your Fundraiser, if necessary. </p>
                <p> When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
              </div>
            </div>
          </div>
		</div>
      </div>
    </div>
  </div>
</div>
<?php
if($active_tab=='information'){
?>
<div id="terms" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content terms">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function ($) { 

	$("#frm_contact_info").validate({
	    rules: {
	    fund_email: {
			required  : true,
			email: true,
			remote: {
				url:"<?php echo base_url('admin/fundraiserEmailCheckerEditAjax2');?>",
				type: "post",
				data: {
					fund_email: function() {
						return $( "#fund_email" ).val();
					}
				}
			}
		},
		fund_terms: {
			required  : true
		}
		},
		messages:
		{
			fund_email:
			{
			remote:"The email id is already in use!",
			},
			fund_terms: {
			required:"Please accept Terms & Conditions",
			}
		}
   });
});

function displayPage(page_id){
	var url = "<?php echo base_url('wppages/get_page');?>";
	$.ajax({
		type: 'POST',
		url: url,
		data: {page_id: page_id},
		success: function (msg) {
			$(".modal-body").html(msg);
			$("#terms").modal('show');
		}
	});
}
</script>
<?php	
}
?>