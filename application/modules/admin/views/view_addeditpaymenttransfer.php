<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
			<li>Payment Information</li>
		</ul>
	</div>
    <?php
    $payment_method="";
    $transaction_no="";
    $transfer_amt="";
    $transfer_date="";
    $fund_details_payment		= $this->admin_model->view('tbl_payment_transfer',array('fundraiser_id'=>$fund_id));
    if($fund_details_payment)
    {
        $payment_method=$fund_details_payment[0]['payment_method'];
        $transaction_no=$fund_details_payment[0]['transaction_no'];
        $transfer_amt=$fund_details_payment[0]['transfer_amt'];
        $transfer_date= date('m-d-Y',strtotime($fund_details_payment[0]['transfer_date']));
    }
    ?>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2><?php echo $fundraiserInfo[0]['fund_username'] ?></h2></div>
			</div>
			<div class="add_plr_wrp mar_t_15">
			<form action="<?php echo base_url()?>admin/fundraiser/<?php echo $slug; ?>/paymentinformation/add" method="POST" id="playeraddedit" name="playeraddedit" enctype="multipart/form-data">
            <input type="hidden" name="hid_fund_id" id="hid_fund_id" value="<?php echo $fund_id;?>" />
            <input type="hidden" name="slug" id="slug" value="<?php echo $slug;?>" />
				<div class="row">

					<div class="col-md-6 col-sm-8 col-xs-12">
						<div class="frm">
							<div class="form-group">
								<label>Payment Method</label>
								<select class="form-control required valid" name="payment_method" id="payment_method">
                                    <option value="">Select Method</option>
                                    <option <?php if($payment_method=="P") echo "selected";?> value="P">Paypal</option>
                                    <option <?php if($payment_method=="C") echo "selected";?> value="C">Check</option>

                                </select>
							</div>
							<div class="form-group">
								<label>Check# / Transaction # </label>
								<input type="text" class="form-control" value="<?php echo $transaction_no; ?>" id="transaction_no" name="transaction_no" required/>
							</div>
							<div class="form-group">
								<label>Payment Date</label>
								<input type="text" class="form-control datepicker" value="<?php echo $transfer_date;?>"  id="transfer_date" placeholder="MM-DD-YYYY" name="transfer_date" required/>
							</div>
							<div class="form-group">
								<label>Payment Amount</label>
								<input type="text" class="form-control" value="<?php echo $transfer_amt; ?>" id="transfer_amt" name="transfer_amt" required/>
							</div>
						</div>
					</div>

				</div>
				<hr>
				<div class="frm_btn_grp">
					<input type="submit" name="submit" value="Save" class="btn_round"/>
					<a href="<?php echo base_url()?>admin/managepayment" class="undr_lin">Cancel</a>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#transfer_date").datepicker({
        dateFormat: 'mm-dd-yy',
    });

   $("#playeraddedit").validate({});
});
</script>