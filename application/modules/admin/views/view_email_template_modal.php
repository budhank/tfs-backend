<form name="frm_popup_email_template" id="frm_popup_email_template" method="post" enctype="multipart/form-data" action="">
  <input type="hidden" name="template_id" id="template_id" value="<?php echo $template[0]['id'];?>">
  <div class="modal-dialog email-edit-temp-wrapper">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tit1_sec clearfix">
          <div class="left">
            <h2>Edit Email Template</h2>            
          </div>
          <div class="ms" style="display:none;"></div>
        </div>
        <div class="add_plr_wrp">
              <div class="frm">
				<div class="form-group">
					<label>Email Template Name</label>
					<input type="text" class="form-control required" id="template_name" name="template_name" value="<?php echo $template[0]['template_name'];?>" readonly/>
				</div>
				<div class="form-group">
					<label>Email Subject</label>
					<input type="text" class="form-control required" id="template_subject" name="template_subject" value="<?php echo $template[0]['template_subject'];?>">
				</div>
                <div class="form-group">
                  <label for="template_content">Email Content</label>                  
                  <textarea name="template_content"  id="template_content" class="form-control editme required" rows="5">
				  <?php echo html_entity_decode($template[0]['template_content']);  ?>
				  </textarea>
                </div>                
              </div>
          <hr>
          <div class="frm_btn_grp"> 
            <!--<a href="#" class="btn_round">Save</a>-->
            <input type="submit" name="btn_save" id="btn_save" class="btn_round" value="Save" />
            <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a> </div>
        </div>
      </div>
    </div>
  </div>
</form>
<script src="<?php echo base_url(); ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({ 
	selector:'textarea.editme' ,
	height: 250,
	menubar: false,
	relative_urls : false,
	remove_script_host : false,
	convert_urls : false,
	plugins: [
	'advlist autolink lists charmap preview anchor',
	'searchreplace visualblocks code fullscreen',
	'insertdatetime media contextmenu paste code'
	],
	toolbar: 'bold italic underline |  alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect fontsizeselect | code',
	content_css: [
	'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
	'//www.tinymce.com/css/codepen.min.css'
	],
	onchange_callback: function(editor) {
		tinyMCE.triggerSave();
		$("#" + editor.id).valid();
	},
	encoding: "xml"
});
</script>
<script type="text/javascript">
$(function() {
	var validator = $("#frm_popup_email_template").submit(function() {
		// update underlying textarea before submit validation
		tinyMCE.triggerSave();
	}).validate({
		ignore: "",
		rules: {
			fund_des: "required"
		},
		errorPlacement: function(error, element) {
			// position error label after generated textarea
			if (element.is("textarea")) {
				error.insertAfter(element);
			} else {
				error.insertAfter(element)
			}
		},
		submitHandler: function(form) {
			$.ajax({
			url:'<?php echo base_url();?>admin/emailtemplateAddEditAjax',            
			type:'POST',
			data: $('#frm_popup_email_template').serializeArray(),     
			dataType:"json",  
			beforeSend: function(){
			   //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
			},
			success:function(results){
				if(results.valid==1){
				$('.ms').html(results.msg);
				$('.ms').show();
				setTimeout(function() {               
					location.reload();
					}, 3000);
				}
				else{
					$('.ms').html(results.msg);
				}            
			}
		   });
		 }
	});
	validator.focusInvalid = function() {
		// put focus on tinymce on submit validation
		if (this.settings.focusInvalid) {
			try {
				var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
				if (toFocus.is("textarea")) {
					tinyMCE.get(toFocus.attr("id")).focus();
				} else {
					toFocus.filter(":visible").focus();
				}
			} catch (e) {
				// ignore IE throwing errors when focusing hidden elements
			}
		}
	}
});
</script> 
