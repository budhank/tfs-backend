<form name="sendfordonations" id="sendfordonations" method="post" enctype="multipart/form-data" action=""> 
<input type="hidden" name="hid_pid" id="hid_pid" value="<?php echo $playerInfo[0]['id'];?>"/>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left">
				<h2>Send Emails for Donation</h2>
				<div class="ms" style="display:none;"></div>
				</div>
			</div>
			<div class="add_plr_wrp">
				<form id="sendfordonations" name="sendfordonations" method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-8 col-sm-9 col-xs-12">
						<div class="frm">
							<div class="form-group">
								<label>Enter Email Address </label> <span> (Separate them by comma " , " )</span>
								<textarea id="email_list" name="email_list" rows="4" class="form-control myclass"></textarea>
							</div>
							<div class="form-group">
								<label>Upload email list(CSV Only)</label>
								<input type="file" id="email_csv" name="email_csv" class="myclass"/>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
					<button type="submit" id="esubmit" name="esubmit" class="btn_round">Send Emails</button>
					<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	$.validator.addMethod('oneMust', function (value, element, params) {
	return (($('#' + params[0]).val()!='' && $('#' + params[1]).val()=='') || ($('#' + params[0]).val()=='' && $('#' + params[1]).val()!=''));
	}, "you must choose one of these option");
	
	$.validator.addMethod('notBoth', function (value, element, param){
	return this.optional(element) || ($(element).is(':filled') && $('[name="' + param + '"]').is(':blank'));
	}, "you must leave one of these blank");
	
	$("#sendfordonations").validate({

		rules:{
			email_list: {
				oneMust: ['email_list','email_csv'],
				notBoth: 'email_csv'
			},
			email_csv: {
				oneMust: ['email_list','email_csv'],
				notBoth: 'email_list'
			}
		},
		groups: {
			mygroup: 'email_list email_csv'
		},
		errorPlacement: function (error, element) {
			if ($(element).hasClass('myclass')) {
				error.insertAfter('#email_csv');
			} else {
				error.insertAfter(element);
			}
		},
        submitHandler: function(form) {
			
			var formData = new FormData();
			formData.append('file', $('input[type=file]')[0].files[0]); 
			formData.append('email_list', $('#email_list').val());
			formData.append('hid_pid', $('#hid_pid').val());
			$.ajax({
				url:'<?php echo base_url();?>admin/donationemailsend',
				type:'POST',
				data: formData,    
				contentType: false,          
				processData:false,     
				dataType:"json",
				beforeSend: function(){
					//$('#infocontent').html('<div style="padding:5px;">loading...</div>');
				},
				success:function(results)
				{
					if(results.valid==1)
					{
						$('.ms').html(results.msg);
						$('.ms').show();
						setTimeout(function() {            
							location.reload();
						}, 3000);
					}
					else
					{
						$('.ms').html('Processing failed');
						$('.ms').show();
					}            
				}
			});
		}
	});
});
</script>