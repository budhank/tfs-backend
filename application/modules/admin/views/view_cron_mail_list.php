<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<div id="page-wrapper">
  <div class="full_top_wrp bg_wht">
    <ul class="breadcrumb">
      <li class="active">Send CRON Emails</li>
    </ul>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="bg_wht mar_t_15 tot_pad">
          <div class="email-wrap">
			<table class="table">
			<thead>
			</thead>
			<tbody>
			<?php
			if(count($maillist)>0)
			{
			$i=1;
			$template_for = '';
			foreach($maillist as $mail_li)
			{
			?>
			  <tr>
				<td style="border-top:0;"><?php echo $mail_li["template_name"]; ?> </td>
				<td style="border-top:0;"><?php echo $mail_li["template_schedule"]; ?></td>
				<td style="border-top:0;">
                    <a href="<?php echo ($mail_li["cron_url"]!='')? base_url($mail_li["cron_url"]):'#'?>" class="grn2"> Send Email </a>
				</td>
			  </tr>
			<?php
			$i++;
			}
			}
			?>
			</tbody>
			</table>
          </div>
		</div>
      </div>
      
    </div>
  </div>
</div>