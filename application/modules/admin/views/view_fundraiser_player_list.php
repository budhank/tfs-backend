<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<div id="page-wrapper">
  <div class="full_top_wrp bg_wht">
    <ul class="breadcrumb">
      <li>All fundraisers</li>
      <li class="active"><?php echo $fundraiserInfo[0]['fund_username'];?></li>
    </ul>
    <input type="hidden" name="fund_id" id="fund_id" value="<?php echo $fund_id;?>" />    
    <?php include(APPPATH.'modules/admin/views/view_top_section.php');?>
    <div class="tab_mnu">
      <ul>
        <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>">PROFILE</a></li>
        <li class="active"><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/players">PLAYERS</a></li>
        <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/maganage-rewards">MANAGE REWARDS</a></li>
        <!--<li><a href="#">EMAIL TEMPLATE</a></li>-->
        <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/settings">SETTINGS</a></li>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="bg_wht mar_t_15 tot_pad">
	<div class="tit1_sec clearfix">
		<div class="left">
		<h2>All Players</h2>
		</div>
		<div style="margin: 7px 0 0 10px;display:inline-block;">
		<?php echo $this->session->flashdata('msgg'); ?>
		</div>
		<a href="<?php echo base_url('admin/fundraiser/'.$slug.'/players/add');?>" class="btn_round pull-right">Add new player</a> 
		<a href="<?php echo base_url('admin/fundraiser/'.$slug.'/sendtoall');?>" class="btn_round pull-right">Send Thank You Mail To All</a>
	</div>
      <div class="donations_tbl">
        <table class="table table-hover table-condensed">
          <thead>
            <tr>
              <th style="width:22%">Player Name</th>
              <th style="width:10%; text-align:right;">Goal</th>
              <th style="width:10%; text-align:right;">Reached</th>
              <th style="width:10%; text-align:right;">Remaining</th>
              <th style="width:14%;"></th>
              <th style="width:10%" class="text-right"><a href="#" style="color: #333"><i class="fa fa-list-ul">&nbsp;&nbsp; Sort</i></a></th>
            </tr>
          </thead>
          <tbody id="b_content">
          <?php
		  if(count($playersInfo)>0)
		  {
		  foreach($playersInfo as $val)
		  {
			  if($val['player_image']==""){
              $proimg =  base_url()."assets/images/noimage-150x150.jpg";   
              }else{
			  $protimg = explode(".",$val['player_image']);	  
              $proimg =  base_url()."assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
              } 
			$org_goal = $val['player_goal'];
			$raised_amt = $val['raised_amt'];
			$remaining_goal = ($org_goal - $raised_amt);
			$pslugs = $val['player_slug_name'];
		  ?>
            <tr>
              <td data-th="Player Name"> <!--players-03.html-->
              <span><img src="<?php echo $proimg;?>" alt=""></span>
              <strong><a href="<?php echo base_url('admin/fundraiser/'.$slug.'/players/summary/'.$pslugs);?>"><?php echo $val['player_fname'].' '.$val['player_lname'];?></a></strong>
              </td>
              <td data-th="Goal" style="text-align:right;"><span class="dlr">$</span><?php echo number_format($org_goal,2,'.',',');?></td>
              <td data-th="Raised" style="text-align:right;"><span class="dlr">$</span><?php echo number_format($raised_amt,2,'.',',');?></td>
              <td data-th="Remaining" style="text-align:right;"><span class="dlr">$</span><?php echo number_format($remaining_goal,2,'.',',');?></td>
			  <td data-th="Mail">
			  <a href="<?php echo base_url('admin/asktoinvite/'.$slug.'/'.$pslugs);?>" style="text-decoration:underline;">Ask player to invite donor</a><br/><br/>
			  <a href="<?php echo base_url('admin/remindtoinvite/'.$slug.'/'.$pslugs);?>" style="text-decoration:underline;">Reminder to invite donor</a><br/><br/>
			  <a href="<?php echo base_url('admin/secremindtoinvite/'.$slug.'/'.$pslugs);?>" style="text-decoration:underline;">2nd Reminder to invite donor</a>
			  </td>
              <td data-th="" class="text-right"><small><?php echo date('M j\, Y',strtotime($val['created'])) ?></small></td>
            </tr>
          <?php
		    } 
		  }
		  else 
		  { 
		  ?>
            <tr>
              <td colspan="6" style="text-align:center;">No records found!!</td>
            </tr>
          <?php 
		  }
		  ?>
          </tbody>
            <tfoot id="f_content">
            <?php
			if(count($playersInfo)!=0)
			{
				if(count($playersInfo)>=10)
				{
				?>
                    <tr>
                    <td colspan="6" class="text-center"><a class="grn2" id="ldmore" href="javascript:void(0);" onclick="loadmore();">Load more</a></td>
                    </tr>
				<?php
				}
				else
				{
				?>
                    <tr>
                    <td colspan="6" class="text-center"><a class="grn2" href="#">That's All</a></td>
                    </tr>
				<?php
				}
			}
			?>
            </tfoot>
            		  
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function openProfile()
{
$.ajax({
            type      :"POST",
            url       :"<?php echo base_url(); ?>admin/getFundraiserInfoOpenAjax",
            data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
            dataType  :"html",
            beforeSend: function(){
                $("#fundInfoModal").modal('show');
				$('#div_loading').show();
            },
            success: function(response){
				$('#div_loading').hide();               
                $("#fundInfoModal").html(response);
                $("#fundInfoModal").modal('show');               
            }
    });
}

var cnt = 0;
function loadmore()
{
var fid = $("#fund_id").val();	
cnt++;
$.ajax({
	url:'<?php echo base_url();?>admin/playerloadmore',
	type:'POST',
	data:'limit='+cnt+'&fid='+fid,            
	dataType:"text",
	beforeSend: function(){
	   $('#ldmore').text('loading...');
	},
	success:function(results){
		if(results!=0){
		$('#ldmore').text('Load more');
		$('#b_content').append(results);
		}
		else
		{
		$('#f_content').html('<tr><td colspan="6" class="text-center"><a class="grn2" href="#">That\'s All</a></td></tr>');
		}            
	  }
   });
}

</script>
<div id="fundInfoModal" class="modal fade" role="dialog"></div>
