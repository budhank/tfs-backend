<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		  <li>Manage Payments</li>
		  <li class="active"><?php echo $fund_details[0]["fund_username"];?> Payments</li>
		</ul>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2><?php echo $fund_details[0]["fund_username"];?> Payments</h2></div>
			</div>
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:25%">Player</th>
							<th style="width:25%">Donor</th>
							<th style="width:18%" class="text-right"> Amount</th>
							<th class="text-right">
								<a href="#" style="color: #333"><i class="fa fa-flask">&nbsp;Filter</i></a>&nbsp;&nbsp;
								<a href="#" style="color: #333"><i class="fa fa-list-ul">&nbsp;Sort</i></a>
							</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if(count($fund_collects)>0)
					{
						foreach($fund_collects as $fund_col)
						{
					?>
						<tr>
							<td>
							<div style="display:table-cell;">
							<a href="<?php echo base_url('admin/fundraiser/'.$fund_col["fund_slug_name"].'/players/summary/'.$fund_col["player_slug_name"]);?>">
							<strong class="nopad_L">
							<?php echo $fund_col["player_fname"].' '.$fund_col["player_lname"];?>
							</strong>
							</a>
							</div>
							</td>	
							<td>
							<strong class="nopad_L">
							<?php echo $fund_col["donor_fname"].' '.$fund_col["donor_lname"];?>
							</strong>
							</td>
							<td class="text-right">
							<span class="dlr">$</span><?php echo number_format($fund_col["donation"]); ?>
							</td>
							<td class="text-right">
							<small><?php echo date("F d, Y",strtotime($fund_col["donation_date"])); ?></small>
							</td>
						</tr>
					<?php
						}
					}
					?>
					</tbody>
					<!--<tfoot>
						<tr>
							<td colspan="5" class="text-center"><a class="grn2" href="#">Load more</a></td>
						</tr>
					</tfoot>-->
				</table>
			</div>
		</div>
	</div>
</div>