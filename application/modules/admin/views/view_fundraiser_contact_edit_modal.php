<form name="frm_popup_fund_contact" id="frm_popup_fund_contact" method="post" enctype="multipart/form-data" action="">
 <input type="hidden" name="hid_ffid" id="hid_ffid" value="<?php echo $fundraiserInfo[0]['id'];?>">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Edit Fundraiser Contact info</h2></div>
                <div class="ms" style="display:none;"></div>
			</div>
			<div class="add_plr_wrp">
				<div class="row">
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="frm">
							<!--<div class="form-group">
								<label for="name">Name</label>
								<input type="text" class="form-control" id="name" placeholder="John Lewis">
							</div>-->
                            <div class="form-group">
								<label for="email">First Name </label>
								<input type="text" class="form-control" id="fund_fname" name="fund_fname" value="<?php echo $fundraiserInfo[0]['fund_fname'];?>">
							</div>
                            <div class="form-group">
								<label for="email">Last Name </label>
								<input type="text" class="form-control" id="fund_lname" name="fund_lname" value="<?php echo $fundraiserInfo[0]['fund_lname'];?>">
							</div>
							<div class="form-group">
								<label for="email">Email </label>
								<input type="email" class="form-control" id="fund_email" name="fund_email" value="<?php echo $fundraiserInfo[0]['fund_email'];?>">
							</div>
							<div class="form-group">
								<label for="phone">Phone Number</label>
								<input type="text" class="form-control" id="fund_contact" name="fund_contact" value="<?php echo $fundraiserInfo[0]['fund_contact'];?>">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
					<button type="submit" name="Save" class="btn_round">Save</button>
					<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
			</div>
      </div>
    </div>
  </div>
 </form>
<script type="text/javascript">
$(document).ready(function ($) { 
$("#frm_popup_fund_contact").validate({
	    rules: {
	    fund_email: {
                    required  : true,
                    email: true,
                    remote: {
                        url : '<?php echo base_url();?>admin/fundraiserEmailCheckerEditAjax',
                        type: "post",
                        data: {
                        fund_email: function() {
                        return $( "#fund_email" ).val() +'##'+$( "#hid_ffid" ).val();
                    //return $('#myformregistration :input[name="email"]').val();
                  }
                }
              }
            },
		fund_contact: {
			required  : true,
		}
		},
		
		messages:
         {
             fund_email:
             {
                remote:"The email id is already in use!",
                //password: "Please enter your old password.",
                //repass: "Please match confirm password.",
                //age:"Please enter age should be two Digit.",
                //cvORaspirations:"Please enter valid file type.",
                //profile:"Please upload valid file type.",
             }
         },
		 
        submitHandler: function(form) {    
            
        $.ajax({
        url:'<?php echo base_url();?>admin/fundraiserContactEditAjax',            
        type:'POST',
        data: $('#frm_popup_fund_contact').serializeArray(),
        //data: formData,    
        //contentType: false,       
        //cache: false,             
        //processData:false,     
        dataType:"json",
        //mimeType: "multipart/form-data",  
            
        beforeSend: function(){
           //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
        },
        success:function(results){
            //jQuery.noConflict();
            //alert(results);
			$('.ms').show();
            if(results.msg==1){
            $('.ms').html('Successfully updated!!');
            setTimeout(function() {
                //$('.ms').html('');                
                location.reload();
                }, 3000);
            }
            else{
                $('.ms').html('Updation is failured');
            }            
         }
       });
      }
   });
});