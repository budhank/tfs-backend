<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/raphael-min.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/morris-0.4.1.min.js"></script>

<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		  <li><a href="<?php echo base_url(); ?>admin/fundraiser/<?php echo $fslug;?>">All fundraisers</a></li>
          <li><a href="<?php echo base_url(); ?>admin/fundraiser/<?php echo $fslug;?>/players"><?php echo $fundraiserInfo[0]['fund_username'];?></a></li>
		  <li class="active"><?php echo $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]; ?></li>
		</ul>
		<?php include(APPPATH.'modules/admin/views/view_top_section_player.php');?>
		<div class="tab_mnu">
			<ul>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/summary/<?php echo $pslug;?>">SUMMARY</a></li>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/donors/<?php echo $pslug;?>">DONATION/DONORS</a></li>
				<li class="active"><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/emailreportgraph//<?php echo $pslug;?>">EMAIL SHARE REPORTS</a></li>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/setting/<?php echo $pslug;?>">SETTINGS</a></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Email Share Reports</h2></div>
				<a href="#" class="grn2 right nobor" style="margin-top: 5px; margin-left: 10px;"><strong>Filters</strong></a>
				<div class="right">
					<a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/emailreportgraph/<?php echo $pslug;?>">Graph View</a>
					<a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/emailreportlist/<?php echo $pslug;?>" class="active">list View</a>
				</div>
			</div>
			<div class="total_info">
				<div class="row">
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Total Emails sent</p>
							<span><?php echo $report_chart["sent"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Unopened Emails</p>
							<span><?php echo $report_chart["unopen"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Emailed Opened</p>
							<span><?php echo $report_chart["open"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Donated</p>
							<span><?php echo $report_chart["donate"];?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:20%">Recipient Name</th>
							<th style="width:25%">Email address</th>
							<th style="width:15%">Status</th>
							<th style="width:25%">Thank You Email</th>
							<th class="text-right">
								<a href="#" style="color: #333"><i class="fa fa-list-ul">&nbsp;&nbsp; Sort</i></a>
							</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if(count($report_list)>0){
						foreach($report_list as $report_li){
					?>
						<tr>
							<td data-th="Recipient Name"></td>
							<td data-th="Email address"><?php echo $report_li["to_email"];?></td>
							<td data-th="Status">
							<strong>
							<?php 
							if($report_li["sent"] == 1 AND $report_li["opened"] == 0 AND $report_li["donated"] == 0)
							{
								echo 'Sent';
								$date = date('d-M-y H:i A',strtotime($report_li["sent_time"]));
							}
							elseif($report_li["sent"] == 1 AND $report_li["opened"] == 1 AND $report_li["donated"] == 0)
							{
								echo 'Opened';
								$date = date('d-M-y H:i A',strtotime($report_li["opened_time"]));
							}
							elseif($report_li["sent"] == 1 AND $report_li["opened"] == 1 AND $report_li["donated"] == 1)
							{
								echo 'Donated';
								$date = date('d-M-y H:i A',strtotime($report_li["donated_time"]));
							}
							else
							{
								echo '';
								$date = '';
							}
							?>
							</strong>
							</td>
							<td data-th="Thank You Email">
							<a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($report_li["id"]);?>" style="text-decoration:underline;">Send a Thank you Email</a>
							</td>
							<td data-th="Date" class="text-right"><small><?php echo $date;?></small></td>
						</tr>
					<?php
						}
					}
					else
					{
					?>
						<tr>
							<td colspan="5" class="text-center">No Donation Found</td>
						</tr>
					<?php	
					}
					?>
					</tbody>
					<tfoot>
					<?php 
					if(count($report_list)>10)
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<a class="grn2" href="#">+Load more</a>
							</td>
						</tr>
					<?php	
					}
					else
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<span class="grn2">Thats all</span>
							</td>
						</tr>
					<?php
					}
					?>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
		
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<div id="mail_pro" class="modal fade" role="dialog"></div>
<div id="send_email_donations" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function openProfile()
{
$.ajax({
            type      :"POST",
            url       :"<?php echo base_url(); ?>admin/getPlayerModalAjax",
            data      :"pslug=<?php echo $pslug;?>",
            dataType  :"html",
            beforeSend: function(){
				$('#div_loading').show();
                $("#edit_pro").modal('show');
            },
            success: function(response){
				$('#div_loading').hide();
                $("#edit_pro").html(response);
                $("#edit_pro").modal('show');               
            }
    });
}
function openSendmail()
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/getSendMailModalAjax",
		data      :"pslug=<?php echo $pslug;?>",
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#send_email_donations").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#send_email_donations").html(response);
			$("#send_email_donations").modal('show');               
		}
    });
}
function thanksent(ref)
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/getMailfromReports",
		data      :"pslug=<?php echo $pslug;?>&report="+ref.rel,
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#mail_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#mail_pro").html(response);
			$("#mail_pro").modal('show');               
		}
    });
}
</script>