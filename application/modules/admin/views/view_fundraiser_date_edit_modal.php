<form name="frm_popup_fund_date" id="frm_popup_fund_date" method="post" enctype="multipart/form-data" action="">
  <input type="hidden" name="hid_fid" id="hid_fid" value="<?php echo $fundraiserInfo[0]['id'];?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="tit1_sec clearfix">
          <div class="left">
            <h2>Edit Fundraiser Dates</h2>            
          </div>
          <div class="ms" style="display:none;"></div>
        </div>
        <div class="add_plr_wrp">
          <div class="row">
            <div class="col-sm-7 col-xs-12">
              <div class="frm">
                <div class="form-group ico_fld">
                  <label for="str-date">Start Date</label>
                  <i class="fa fa-calendar-o"></i>
                  <input type="text" class="form-control datepicker" id="fund_start_dt" name="fund_start_dt" placeholder="" readonly="readonly" class="required" value="<?php echo date('m-d-Y',strtotime($fundraiserInfo[0]['fund_start_dt']));?>">
                </div>
                <div class="form-group ico_fld">
                  <label for="end-date">End Date</label>
                  <i class="fa fa-calendar-o"></i>
                  <input type="text" class="form-control datepicker" id="fund_end_dt" name="fund_end_dt" placeholder="" readonly="readonly" class="required" value="<?php echo date('m-d-Y',strtotime($fundraiserInfo[0]['fund_end_dt']));?>">
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="frm_btn_grp"> 
            <!--<a href="#" class="btn_round">Save</a>-->
            <input type="submit" name="btn_save" id="btn_save" class="btn_round" value="Save" />
            <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a> </div>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function ($) {
$("#fund_start_dt").datepicker({ 
    dateFormat: 'mm-dd-yy', 
    onSelect: function(selected) 
    {
        var sdate = $(this).datepicker('getDate');
        if(sdate){sdate.setDate(sdate.getDate() + 1);}
        $("#fund_end_dt").datepicker("option","minDate", sdate)
    }     
});
    
$("#fund_end_dt").datepicker({ 
    dateFormat: 'mm-dd-yy', 
});

$("#frm_popup_fund_date").validate({
        submitHandler: function(form) {    
            
        $.ajax({
        url:'<?php echo base_url();?>admin/fundraiserDateEditAjax',            
        type:'POST',
        data: $('#frm_popup_fund_date').serializeArray(),
        //data: formData,    
        //contentType: false,       
        //cache: false,             
        //processData:false,     
        dataType:"json",
        //mimeType: "multipart/form-data",  
            
        beforeSend: function(){
           //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
        },
        success:function(results){
            //jQuery.noConflict();
            //alert(results);
			$('.ms').show();
            if(results.msg==1){
            $('.ms').html('Successfully updated!!');
            setTimeout(function() {
                //$('.ms').html('');                
                location.reload();
                }, 3000);
            }
            else{
                $('.ms').html('Updation is failured');
            }            
        }
       });
      }
      });   
    
});
</script> 
