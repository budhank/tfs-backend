<div class="top_pro_wrp">
	<div class="clearfix">
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
			<div class="top_pro">
			<?php
			if($player_details[0]['player_image']==""){
			$proimg =  base_url()."assets/images/noimage-150x150.jpg";   
			}else{
			$protimg = explode(".",$player_details[0]['player_image']);   
			$proimg =  base_url()."assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
			} 
			$org_goal = $player_details[0]['player_goal'];
			$raised_amt = $player_details[0]['raised_amt'];
			$remaining_goal = ($org_goal - $raised_amt);
			?>
				<div class="top_pro_img">
				<img src="<?php echo $proimg; ?>" alt="">
				</div>
				<div class="top_pro_con">
					<h4><?php echo ucwords($player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]); ?></h4>
					<!--<p><?php //echo ucfirst($player_details[0]["player_title"]); ?></p>-->
				</div>
			</div>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<div class="top_pro_prc">
				<h4><span class="glyphicon glyphicon-usd"></span> 
				<?php echo number_format($org_goal); ?>
				</h4>
				<p>Goal</p>
			</div>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<div class="top_pro_prc">
				<h4 class="grn1"><span class="glyphicon glyphicon-usd"></span> 
				<?php echo number_format($raised_amt); ?>
				</h4>
				<p>Raised</p>
			</div>
		</div>
		<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
			<div class="top_pro_prc">
				<h4 class="txt-red"><span class="glyphicon glyphicon-usd"></span> 
				<?php echo number_format($remaining_goal); ?>
				</h4>
				<p>Remaining</p>
			</div>
		</div>
		<div class="col-lg-3 col-xs-12 text-right">
			<div class="top_pro_donation">
			<a href="javascript:void(0)" class="btn_round" onclick="openSendmail();">Send Emails for Donation</a>
			<br>
			<a href="javascript:void(0)" class="grn2" onclick="openProfile();">Edit profile</a>
			</div>
		</div>
	</div>
</div>