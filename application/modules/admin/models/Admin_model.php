<?php
class Admin_model extends CI_Model {
	public function view_all($table_name)
	{
		$query = $this->db->get($table_name);
		return $query->result_array(); 	
	}
	public function view($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return $query->result_array(); 
	}
	public function view_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function func_common_listing_where($table,$where,$field='',$ord=''){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		if($field!='' AND $ord!='')
		{
		$this->db->order_by($field, $ord);	
		}
		$query = $this->db->get(); 
		//return $query->result_array();
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function view_limit($table,$where='',$limit,$offset,$field,$ord)
	{
		$this->db->select('*');
		$this->db->from($table);
		if($where!='')
		{
		$this->db->where($where);	
		}
		if($field!='' && $ord!=''){
		$this->db->order_by($field, $ord);
		}
		$this->db->limit($offset,$limit);	

		$query = $this->db->get();
		return $query->result_array(); 	
	}
	public function add($table_name, $data)
	{
		return $this->db->insert($table_name, $data); 
	}  
	public function edit($table_name, $data, $where)
	{
		return $this->db->update($table_name, $data, $where);
	}
	public function remove($table_name, $where)
	{
		return $this->db->delete($table_name, $where); 
	}
	public function truncate($table_name)
    {
		return $this->db->truncate($table_name);
	}
	public function last_insert_id()
	{
		return $this->db->insert_id();
	}
	public function last_qry()
	{
		echo $this->db->last_query();
	}	
	public function email_template_list()
	{
		$sql   = "SELECT * FROM tbl_mail_template WHERE show_in_list = 1 ORDER BY template_for_order,id ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function cron_template_list()
	{
		$sql   = "SELECT * FROM tbl_mail_template WHERE template_for IN('Participant Emails','Contact Emails') AND show_in_list = 1 ORDER BY template_for_order,id ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function login_check($table,$where){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query;
	}
	public function encry_login_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function all_user_check($username)
	{
		$sql = "SELECT * FROM
		(
		SELECT id,UCASE(name) name,username,password,CONCAT(username,'-admin') slug,'admin' role  FROM tbl_admin
		UNION ALL
		SELECT id,UCASE(fund_username) name,fund_email username,fund_pass password,CONCAT(fund_slug_name,'-fundraiser') slug,'fundraiser' role FROM tbl_fundraiser
		UNION ALL
		SELECT p.id,UCASE(CONCAT(p.player_fname,' ',p.player_lname,' - ',f.fund_username)) name,player_email username,p.player_pass password,CONCAT(player_slug_name,'-player') slug,'player' role FROM tbl_player p INNER JOIN tbl_fundraiser f ON p.player_added_by = f.id
		) tbl_all_users
		WHERE username = '".$username."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function all_user_check_email($email)
	{
		$sql = "SELECT * FROM
		(
		SELECT id,UCASE(fund_username) name,fund_email username,fund_pass password,CONCAT(fund_slug_name,'-fundraiser') slug,'fundraiser' role FROM tbl_fundraiser
		UNION ALL
		SELECT p.id,UCASE(CONCAT(p.player_fname,' ',p.player_lname,' - ',f.fund_username)) name,player_email username,p.player_pass password,CONCAT(player_slug_name,'-player') slug,'player' role FROM tbl_player p INNER JOIN tbl_fundraiser f ON p.player_added_by = f.id
		) tbl_all_users
		WHERE username = '".$email."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function all_user_login_check($username,$slug)
	{
		$sql = "SELECT * FROM
		(
		SELECT id,UCASE(name) name,username,password,CONCAT(username,'-admin') slug,'admin' role  FROM tbl_admin
		UNION ALL
		SELECT id,UCASE(fund_username) name,fund_email username,fund_pass password,CONCAT(fund_slug_name,'-fundraiser') slug,'fundraiser' role FROM tbl_fundraiser
		UNION ALL
		SELECT p.id,UCASE(CONCAT(p.player_fname,' ',p.player_lname,' - ',f.fund_username)) name,player_email username,p.player_pass password,CONCAT(player_slug_name,'-player') slug,'player' role FROM tbl_player p INNER JOIN tbl_fundraiser f ON p.player_added_by = f.id
		) tbl_all_users
		WHERE username = '".$username."' AND slug = '".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function func_common_listing($table,$field,$ord)
	{
		$this->db->from($table);
		if($field!='' && $ord!=''){
		$this->db->order_by($field, $ord);
		}
		$query = $this->db->get(); 
		return $query->result_array();
	}
	public function fundraiser_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function fundraiser_username_check($username)
	{
		$query = $this->db->query("SELECT * FROM tbl_fundraiser WHERE fund_username='".$username."'");
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function fundraiser_email_check($email='',$table='tbl_fundraiser')
	{
		$date = date("Y-m-d");
		$sql = "SELECT * FROM ".$table." WHERE 1 ";
		if($email)
		{
		$sql .= " AND fund_email='".$email."'";
		$sql .= " AND fund_start_dt < NOW() AND fund_end_dt >= NOW() ";
		}
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	
	
	
	public function fundraiser_wise_rewardList($fid)
	{
		$sql = "SELECT * FROM tbl_rewards WHERE fund_id=".$fid." AND status=1";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}  
	public function has_rewords_rel_donation($rid)
	{
		$sql = "SELECT * FROM tbl_rel_donation_reward WHERE reward_id=".$rid."";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function fundraiser_check_edit($table_name, $femail, $fid)
	{
		///$query = $this->db->get_where($table_name, $where);
		///return ($query->num_rows() > 0)?$query->num_rows():0;
		$sql   = "SELECT * FROM $table_name WHERE fund_email='".$femail."' AND id!=".$fid;
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}
	
	public function fundraiser_check_edit_by_slug($table_name, $femail, $fslug)
	{
		$sql   = "SELECT * FROM $table_name WHERE fund_email='".$femail."' AND fund_slug_name!='".$fslug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}

	public function fundraiser_playersInfo($fund_id,$limit='',$offset='')
	{
		$sql   = "SELECT tp.*, tdnp.pay_id, tdnp.player_id, tdnp.amount raised_amt
		FROM tbl_player tp
		LEFT JOIN (
		SELECT id as pay_id,id as don_id,player_id, IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY player_id
		) tdnp ON tdnp.player_id=tp.id 
		WHERE player_added_by='".$fund_id."' 
		ORDER BY player_fname ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function playersInfo($slug)
	{
		$sql   = "SELECT tp.*, tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt
		FROM tbl_player tp
		LEFT JOIN (
		SELECT id as pay_id,fund_id,player_id ,IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY player_id
		) tdnp ON tdnp.player_id=tp.id 
		WHERE tp.player_slug_name='".$slug."' 
		GROUP BY tp.id
		ORDER BY player_fname ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function fundraiser_getID($slug)
	{
		$sql   = "SELECT * FROM tbl_fundraiser WHERE fund_slug_name='".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
/*
	public function fundraiser_list($limit,$offset)
	{
		$sql = "SELECT fundr.*, tst.name as state_ini, tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt, plyr.totalparticiapte ,plyrs.totalinvites
				FROM tbl_fundraiser fundr
				LEFT JOIN tbl_mst_state tst ON tst.state_initial=fundr.fund_state 
				LEFT JOIN (
				SELECT id as pay_id,fund_id, IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY fund_id
				) tdnp ON tdnp.fund_id=fundr.id 
				LEFT JOIN (
				SELECT count(*) as totalparticiapte ,player_added_by,player_slug_name FROM tbl_player where player_status=1 GROUP BY player_added_by
				) plyr ON plyr.player_added_by=fundr.id
				LEFT JOIN (
				SELECT count(*) as totalinvites,player_added_by  FROM tbl_player 
				INNER JOIN tbl_player_contacts ON tbl_player.player_slug_name = tbl_player_contacts.contact_added_by
				GROUP BY player_added_by
				) plyrs ON plyrs.player_added_by=fundr.id
				GROUP BY fundr.id 
				ORDER BY creation_date DESC 
				limit $limit,$offset";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	*/
	public function fundraiser_list($limit='',$offset='')
	{
		$sql = "SELECT fundr.*, tst.name as state_ini, tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt, plyr.totalparticiapte,plyrs.totalinvites
				FROM tbl_fundraiser fundr
				LEFT JOIN tbl_mst_state tst ON tst.state_initial=fundr.fund_state 
				LEFT JOIN (
				SELECT id as pay_id,fund_id, IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY fund_id
				) tdnp ON tdnp.fund_id=fundr.id 
				LEFT JOIN (
				SELECT count(*) as totalparticiapte ,player_added_by,player_slug_name FROM tbl_player where player_status=1 GROUP BY player_added_by
				) plyr ON plyr.player_added_by=fundr.id				
				LEFT JOIN (
				SELECT count(*) as totalinvites,player_added_by  FROM tbl_player 
				INNER JOIN tbl_player_contacts ON tbl_player.player_slug_name = tbl_player_contacts.contact_added_by
				GROUP BY player_added_by
				) plyrs ON plyrs.player_added_by=fundr.id";
				
				
			$order_by = '';
			$title_by = '';
			$keyword = '';
			//fund_username fund_fname
			$inputs = $this->input->post();
			
		if(isset($inputs['keyword']) && $inputs['keyword']!=''){
			$keyword = $inputs['keyword'];
			$order_by = $inputs['order_by'] = $this->session->userdata('order_by');
			$title_by = $inputs['title'] = $this->session->userdata('title_by');
			$this->session->set_userdata('keyword',$keyword);
		}
			
		if(isset($inputs['mode']) && $inputs['mode']=='search'){
			$order_by = $inputs['order_by'];
			$title_by = $inputs['title'];
			$keyword = $inputs['keyword'] = $this->session->userdata('keyword');
			$this->session->set_userdata('order_by',$order_by);
			$this->session->set_userdata('title_by',$title_by);
		}
		if($keyword) $sql .= " WHERE (fundr.fund_username LIKE '%".$keyword."%' OR fundr.fund_fname LIKE '%".$keyword."%')"; 
				
				$sql .= " GROUP BY fundr.id"; 
				if($order_by && $title_by) $sql .= " ORDER BY ".$title_by." ". $order_by; 
				if($limit && $offset) $sql .= " limit ".$limit.",".$offset;
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}

	public function donors_list($pslug,$limit)
	{
		$sql = "SELECT * FROM tbl_donors_payment WHERE player_slug = '".$pslug."' ORDER BY donation_date DESC LIMIT ".$limit." ";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}

	public function player_list($limit,$offset,$id)
	{
		$sql   = "SELECT * FROM tbl_player WHERE player_added_by='".$id."' ORDER BY player_fname ASC limit $limit,$offset";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}

	public function isUrlExists($tname,$slug)
	{
		$sql   = "SELECT * FROM $tname WHERE fund_slug_name LIKE '".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}

	public function isUrlExistsEditMode($tname,$slug,$fid)
	{
		$sql   = "SELECT * FROM $tname WHERE fund_slug_name LIKE '".$slug."' AND id!=".$fid;
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}

	public function fundraiserinfo($slug)
	{
		$sql = "SELECT fundr.*, tst.name as state_ini, tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt
		FROM tbl_fundraiser fundr
		LEFT JOIN tbl_mst_state tst ON tst.state_initial=fundr.fund_state 
		LEFT JOIN (
		SELECT id as pay_id,fund_id, IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY fund_id
		) tdnp ON tdnp.fund_id=fundr.id 
		WHERE fundr.fund_slug_name='".$slug."'
		GROUP BY fundr.id";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fundraiser_collects()
	{
		$sql = "SELECT fundr.*, tst.name as state_ini, tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt, tdnpc.donors, IFNULL(tpt.fundraiser_id,0) transferStatus, tpt.transfer_date
		FROM tbl_fundraiser fundr
		LEFT JOIN tbl_mst_state tst ON tst.state_initial=fundr.fund_state 
		LEFT JOIN (
		SELECT id as pay_id,fund_id, IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY fund_id
		) tdnp ON tdnp.fund_id=fundr.id 
		LEFT JOIN (
		SELECT fund_id fundr_id, COUNT(fund_id) donors FROM tbl_donors_payment GROUP BY fund_id
		) tdnpc ON tdnpc.fundr_id=fundr.id 
		LEFT JOIN tbl_payment_transfer tpt ON tpt.fundraiser_id=fundr.id 
		GROUP BY fundr.id";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function fund_collection_list($fund_slug)
	{
		$sql = "SELECT *
		FROM tbl_donors_payment tdp
		LEFT JOIN tbl_fundraiser tf ON tf.id=tdp.fund_id 
		LEFT JOIN tbl_player tp ON tp.id=tdp.player_id   
		WHERE fund_slug='".$fund_slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}

	public function fundraiserImgInfo($fid)
	{
		$sql = "SELECT * FROM tbl_rel_fund_feature_images WHERE fund_id=".$fid;
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}

	public function donationinfo($fund_id)
	{
		$sql = "SELECT don.*, rew.reward_name, rew.reward_info 
				FROM tbl_donation don
				INNER JOIN tbl_rel_donation_reward rel ON don.id=rel.donation_id 
				INNER JOIN tbl_rewards rew ON rew.id=rel.reward_id 
				WHERE don.fund_id='".$fund_id."' AND don.status=1";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}

	public function rewadrs_array($donation_id)
	{
		$sql = "SELECT reward_id FROM tbl_rel_donation_reward WHERE donation_id='".$donation_id."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$result = array();
			foreach($query->result_array() as $rid)
			{
			$result[] =$rid["reward_id"];
			}
		}
		else{
			$result = array();
		}
		return $result;
	}
	public function donationCheck($fid,$did,$donate_start,$donate_end)
	{
		$sql = "SELECT id FROM tbl_donation WHERE id!='".$did."' AND fund_id='".$fid."' AND ((".$donate_start." between donate_start and donate_end) OR (".$donate_end." between donate_start and donate_end) OR (donate_start between ".$donate_start." and ".$donate_end.") OR (donate_end between ".$donate_start." and ".$donate_end."))";

		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	
	public function email_reports($pid)
	{
		$result		= array();
		/*-------------------------------------------SENT SQL-----------------------------------------------------------*/
		$sent_sql		= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1";
		$sent_qry		= $this->db->query($sent_sql);
		$sent_cnt		= ($sent_qry->num_rows() > 0)?$sent_qry->num_rows():0;
		$result['sent']	=$sent_cnt;
		/*-------------------------------------------UNOPEN SQL---------------------------------------------------------*/
		$unopen_sql		= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1 AND opened=0";
		$unopen_qry		= $this->db->query($unopen_sql);
		$unopen_cnt		= ($unopen_qry->num_rows() > 0)?$unopen_qry->num_rows():0;
		$result['unopen']=$unopen_cnt;
		/*-------------------------------------------OPEN SQL-----------------------------------------------------------*/
		$open_sql		= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1 AND opened=1";
		$open_qry		= $this->db->query($open_sql);
		$open_cnt		= ($open_qry->num_rows() > 0)?$open_qry->num_rows():0;
		$result['open']	=$open_cnt;
		/*-------------------------------------------DONATE SQL---------------------------------------------------------*/
		$donate_sql		= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1 AND opened=1 AND donated=1";
		$donate_qry		= $this->db->query($donate_sql);
		$donate_cnt		= ($donate_qry->num_rows() > 0)?$donate_qry->num_rows():0;
		$result['donate']=$donate_cnt;

		return $result;
	}

	public function isPlayerUrlExists($tname,$slug)
	{
		$sql   = "SELECT * FROM $tname WHERE player_slug_name LIKE '".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}  

	public function getColumn($table,$column,$where)
	{
		$this->db->select($column);
		$query = $this->db->get_where($table,$where);
		return ($query->num_rows() > 0)?$query->row()->$column:'';
	}
	
	public function setPlayerImage($player_image=''){
				if($player_image!="") 
				{
					/* Create the config for upload library */				
					$config_img['upload_path']   = './assets/player_image/';
					$config_img['allowed_types'] = 'gif|jpg|png|jpeg';
					$config_img['max_size']      = '100000';
					$this->load->library('upload', $config_img);
					$this->upload->initialize($config_img);			 
					$this->upload->do_upload('player_image');			
					$data_img = $this->upload->data(); 
					if($data_img['is_image'] == 1)
					{ 
						$fpath1  = $data_img['file_name'];			
						$this->load->library('image_lib');
						$config_resize['image_library']   = 'gd2';	
						$config_resize['create_thumb']    = TRUE;
						$config_resize['maintain_ratio']  = TRUE;
						$config_resize['master_dim']      = 'height';
						$config_resize['quality']         = "100%";  
						$config_resize['source_image']    = './assets/player_image/'.$fpath1;
						$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
						$config_resize['height']          = 120;
						$config_resize['width']           = 1;
						$this->image_lib->initialize($config_resize);
						$this->image_lib->resize(); 
						return $fpath1;
					}
					 
				}
	}
	public function get_donors_payments($order_by='day'){
		$sql = "SELECT IFNULL(SUM(donation),0.00) amount, donation_date FROM tbl_donors_payment GROUP BY ";

		if($order_by=='day') $sql .= " donation_date";
		if($order_by=='week') $sql .= " CONCAT(YEAR(donation_date), '/', WEEK(donation_date))";
		if($order_by=='month') $sql .= " CONCAT(YEAR(donation_date), '/', MONTH(donation_date))";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$donation_array = $query->result_array();
			$donation_day = array();
			foreach($donation_array as $donations)
			{
				$donation_day[] = array('x'=>'new Date('.date('Y, m, d',strtotime($donations['donation_date'])).')','y'=>$donations['amount']);
			}


			return $donation_day;
		}
	}
	public function get_amount_raised(){		
		$sql = "SELECT SUM(donation) amount_raised
		FROM tbl_donors_payment tdp
		LEFT JOIN tbl_fundraiser tf ON tf.id=tdp.fund_id 
		LEFT JOIN tbl_player tp ON tp.id=tdp.player_id ";
		$query = $this->db->query($sql);
		return $query->result_array();
		}
}
?>
