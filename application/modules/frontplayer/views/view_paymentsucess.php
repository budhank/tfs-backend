<!-- Green Bar start-->
<?php 
if($player_details[0]['player_image']=="")
{
	$playerimg =  base_url("assets/images/noimage-150x150.jpg");   
}
else
{
	$playerTimg = explode(".",$player_details[0]['player_image']);	
	$playerimg  =  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
} 

if($fund_details[0]['fund_image']=="")
{
	$fundimg =  base_url("assets/images/noimage-150x150.jpg");
}
else
{
	$fundTimg = explode(".",$fund_details[0]['fund_image']);	
	$fundimg  =  base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
} 

$player_name 	 = $player_details[0]['player_fname'].' '.$player_details[0]['player_lname'];
$fund_enddate	 = $fund_details[0]["fund_end_dt"];
$today_date		 = date("Y-m-d");
$fundraiser_name = $fund_details[0]['fund_username'];
$fundraiser_slog = $fund_details[0]['fund_slogan'];
$ended_days_in 	 = (strtotime($fund_enddate)-strtotime($today_date));

if($ended_days_in < 0)
{
	echo '<script>window.location.href="'.base_url($fslug.'/'.$pslug).'"</script>';
}
$_SESSION['donation_id']=$this->session->userdata('donation_id');
?>
<script src="<?php echo base_url('assets/js/jquery.validate.js');?>"></script>
<script src="<?php echo base_url('assets/js/additional-methods.js');?>"></script>
<script>
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '1455592167892437', // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });
    
    // Check whether the user already logged in
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			//getFbUserData();//display user data
		}
	});
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            getFbUserData();// Get and display the user profile data
        } else {
            document.getElementById('status').innerHTML = 'User did not fully authorize.';
        }
    },{scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
		var donation_id = <?php echo $donation_id; ?>;
        var fb_id = response.id;
		var fb_profile = response.picture.data.url;
		$.ajax({
			url : '<?php echo base_url('Frontplayer/ajax_donation_post'); ?>',
			type: 'POST',
			data: 'fb_id='+fb_id+'&fb_image='+fb_profile+'&don_id='+donation_id,     
			dataType: "json",
			success: function ( results ) {
				if ( results.valid == 1 ) {
					fbLogout();
				} else {
					
				}
			}
		});
    });
}

// Logout from facebook
function fbLogout() {
    FB.logout(function() {
		nextRef();
    });
}
</script>
<script type="text/javascript">
console.log('<?php echo 'aa'.$this->session->userdata('donation_id');?>');
function nextRef(){
	window.location.href="<?php echo base_url($fslug.'/'.$pslug.'/share'); ?>";
}
</script>
<?php //echo '<pre>';print_r($this->session->all_userdata());echo '</pre>';?>
<div id="myModal" class="modal fade video-play-wrapper" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<p class="bold text-center">Would you like to share your facebook profile image with Thanksforsupporting?</p>
				<div class="text-center">
				<button class="btn btn-success" onClick="fbLogin();">Yes</button>
				<button class="btn btn-success" onClick="nextRef();">No</button>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="subject-wrapper" style="<?php echo 'background-color:'.$fund_details[0]['fund_color_rgb'] ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-3 col-md-2">
	  <img src="<?php echo $fundimg;?>" alt="Subject Image" class="img-circle"/>
	  </div>
      <div class="col-xs-9  col-md-10 subject-title"> 
	  <span class="item-title"><?php echo $fundraiser_name; ?></span>
	  <p class="item-info"><?php echo $fundraiser_slog; ?></p>
      </div>
    </div>
  </div>
</section>
<!-- Green Bar ends-->

<!-- Inner Content start-->
<section class="bg-white inner-content-wrapper">
  <div class="container">
	  <div class="row">
		  <div class="col-xs-12 text-center page-title add-img">
			<img src="<?php echo $playerimg;?>" class="img-circle" alt="Team member"/> 
			<span class="mem-name"><?php echo $player_name;?></span>
		  </div>
		  <div class="progress-meter">
			  <span class="meter-block first" data-title="Donate">1</span>
			  <span class="meter-block middle active" data-title="Thank you">2</span>
			  <span class="meter-block last" data-title="Share">3</span>
		  </div>
		  <div class="clearfix"></div>
		  <div class="col-xs-12 col-sm-10 col-md-8 middle-wrapper">
			<?php
			$succ = $this->session->userdata('suc_msg');
			$succ1 = $this->session->userdata('suc_msg1'); 
			if(isset($succ))
			{
			echo '<div class="msg-thankyou txt-green text-center">'.$succ.'</div>';  
			$this->session->unset_userdata('suc_msg');
			}
			if(isset($succ1))
			{
			echo '<div class="msg-email-confirm text-center">'.$succ1.'</div>';  
			$this->session->unset_userdata('suc_msg1');
			}
			?>
			<div class="post-title text-center">Post a message for <?php echo $player_name;?></div>
			<form action="<?php echo base_url($fslug.'/'.$pslug.'/post-message'); ?>" method="POST" id="shareform" name="shareform" class="message-form text-center">
			<input type="hidden" name="donation_id" id="donation_id" value="<?php echo $donation_id;?>"/>
			<textarea class="form-control" id="share_message" name="share_message"></textarea>
			<button class="btn btn-default btn-blue btn-message" type="submit" name="share">
			<i class="fa fa-facebook"></i> Post Message
			</button>
			</form>
		  </div>
		  <div class="col-xs-12 col-sm-10 col-md-8 middle-wrapper reward-note">
			<div class="wrapper">
			  <span class="ribon">
			  <img src="<?php echo base_url('assets/images/reward-ribbon.png');?>" alt="Reward Ribon"/>
			  </span>
			  <img src="<?php echo base_url('assets/images/device-sample.jpg');?>" alt="Device img" class="pull-left device-sample"/>
			  <div class="reward-detail-info">
				  <p class="bold">Your Gift has been emailed to you</p>
				  <p>The activation code to access coupon app has been sent to your email</p>
				  <p class="bold step-note">Activate & start using your coupon App in simple ways</p>
				  <ul class="list-steps">
					  <li>Download GeoCouponAlerts the App</li>
					  <li>Enter activation code</li>
					  <li>Access coupon from merchants & save 1000's</li>
				  </ul>
			  </div>
			  <div class="clearfix"></div>
		  </div>
			  <div class="text-center"><button type="button" class="btn btn-default btn-green" onClick="nextRef();">Next</button></div>
		  </div> 
	  </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function ($) {
	$("#shareform").validate({
		rules:{
			share_message:{
				required:true
			}
		},
		submitHandler: function ( form ) {
			$.ajax({
				url: '<?php echo base_url($fslug.'/'.$pslug.'/post-message'); ?>',
				type: 'POST',
				data: $( '#shareform' ).serializeArray(),     
				dataType: "json",
				success: function ( results ) {
					if ( results.valid == 1 ) {
						$("#myModal").modal('show').css({'margin-top':'10%'});
					} else {
						//$( '.ms' ).html( results.msg );
					}
				}
			});
		}
	});
});
</script>