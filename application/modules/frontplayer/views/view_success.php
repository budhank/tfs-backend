<style>
body {
	background-color: #fff;
}
</style>
<!-- Payment option start-->
<section class="inner-content-wrapper">
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 donation-wrapper">        	 
            	<div class="alert alert-success">
  					Your payment is <strong>successfull!</strong>. Click <a href="<?php echo base_url('donate')?>" class="alert-link">here</a> to go back to home.
               </div>
      </div>      
      <div class="col-xs-12 col-sm-4 col-md-6 d-thumbnail">
        <table class="table">
          <tr>
            <td><img src="<?php echo base_url()?>assets/images/all_merchants.png" alt="Thumbnail1"></td>
            <td><img src="<?php echo base_url()?>assets/images/pizza_guys.png" alt="Thumbnail2"></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</section>