<div class="modal-dialog login-reg-wrapper">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-body">
        <div class="row"> 
          
          <!-- Login -->
          <form name="frm_login_popup" id="frm_login_popup" method="post" enctype="multipart/form-data" action="">
          <div class="col-xs-12 col-sm-6 login-wrapper">
            <div class="page-title">
              <h4>Login</h4>
            </div>
            <div class="login-form-wrapper"> 
              <div class="form-group">
                <label for="login-email" class="sr-only">Email</label>
                <input type="email" class="form-control required" id="log_donor_email" name="log_donor_email" placeholder="Email" value="">
              </div>
              <div class="form-group">
                <label for="login-password" class="sr-only">Password</label>
                <input type="Password" class="form-control required" id="log_donor_pass" name="log_donor_pass" placeholder="Password" value="">
              </div>
              <input type="submit" name="login" class="btn btn-green" value="Login">
              <div class="modal-footer">
                <p>Forgot <a href="#">Password?</a></p>
                <div class="ms" style="display:none;"></div>
              </div>
            </div>
          </div>
          </form>
          
          <!-- Registration -->
          <form name="frm_reg_popup" id="frm_reg_popup" method="post" enctype="multipart/form-data" action="">
          <div class="col-xs-12 col-sm-6 login-wrapper" style="border-left: 1px solid #eee;">
          <input type="hidden" name="pslug" id="" value="<?php echo $pslug;?>" />
            <div class="page-title">
              <h4>Registration</h4>              
            </div>
            <div class="login-form-wrapper"> 
              <div class="form-group">
                <label for="reg-email" class="sr-only">Email</label>
                <input type="email" class="form-control required" id="donor_email" name="donor_email" placeholder="Email" value="">
              </div>
              <div class="form-group">
                <label for="firstname" class="sr-only">Firstname</label>
                <input type="text" class="form-control required" id="donor_fname" name="donor_fname" placeholder="Firstname" value="">
              </div>
              <div class="form-group">
                <label for="lastname" class="sr-only">Lastname</label>
                <input type="text" class="form-control required" id="donor_lname" name="donor_lname" placeholder="Lastname" value="">
              </div>
              <div class="form-group">
                <label for="reg-password" class="sr-only">Password</label>
                <input type="password" class="form-control required" id="donor_pass" name="donor_pass" placeholder="Password" value="">
              </div>
              <div class="form-group">
                <label for="re-password" class="sr-only">Retype Password</label>
                <input type="password" class="form-control required" id="re_password" name="re_password" placeholder="Retype Password" value="">
              </div>
              <input type="submit" name="registration" class="btn btn-green" value="Register">
            </div>
          </div>
          </form>
          
          
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function ($) { 
$("#frm_login_popup").validate({    
        submitHandler: function(form) {    
        $.ajax({
        url:'<?php echo base_url();?>frontplayer/loginAjax',            
        type:'POST',
        data: $('#frm_login_popup').serializeArray(),
        //data: formData,    
        //contentType: false,       
        //cache: false,             
        //processData:false,     
        dataType:"json",
        //mimeType: "multipart/form-data",  
            
        beforeSend: function(){
           //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
        },
        success:function(results){
            //jQuery.noConflict();
            //alert(results.msg);
            if(results.msg==1){
            $('.ms').html('Thankyou!! Please wait .. ');
            setTimeout(function() {
                //$('.ms').html('');                
                location.reload();
                }, 3000);
            }
            else{
				$('.ms').show();
                $('.ms').html('Login is failured!! Username/Password is mismatched.');
            }            
         }
       });
      }
   });
   
$("#frm_reg_popup").validate({
	    rules: {	
	    	donor_email: {
                    required  : true,
                    email: true,
                    remote: {
                        url : '<?php echo base_url();?>frontplayer/userCheckAjax',
                        type: "post",
                        data: {
                        donor_email: function() {
                        return $( "#donor_email" ).val();
                    //return $('#myformregistration :input[name="email"]').val();
                  }
                }
              }
            },
			re_password: {
		  	equalTo: "#donor_pass"
			}
		},
		
		messages:
         {
             donor_email:
             {
                remote:"The email id is already in use!"
             }
         },
		 
        submitHandler: function(form) {    
            
        $.ajax({
        url:'<?php echo base_url();?>frontplayer/registrationAjax',            
        type:'POST',
        data: $('#frm_reg_popup').serializeArray(),
        //data: formData,    
        //contentType: false,       
        //cache: false,             
        //processData:false,     
        dataType:"json",
        //mimeType: "multipart/form-data",  
            
        beforeSend: function(){
           //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
        },
        success:function(results){
            //jQuery.noConflict();
            //alert(results);
            if(results.msg==1){
            $('.ms').html('Registration has been completed successfully!! <br> Please login to proceed!!');
			$('.ms').show();
			$("#frm_reg_popup")[0].reset();
            /*setTimeout(function() {
                //$('.ms').html('');                
                location.reload();
                }, 3000);*/
            }
            else{
                $('.ms').html('Registraion failured');
            }            
         }
       });
      }
   });
});
</script>
