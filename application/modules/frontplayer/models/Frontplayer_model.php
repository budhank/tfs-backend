<?php
class Frontplayer_model extends CI_Model {
	public function view_all($table_name)
    {
		$query = $this->db->get($table_name);
		return $query->result_array(); 	
    }
	public function view_limit($table,$where='',$limit,$offset)
    {
		$this->db->select('*');
		$this->db->from($table);
		if($where!='')
		{
		$this->db->where($where);	
		}
		$this->db->limit($offset,$limit);	
		
		$query = $this->db->get();
		return $query->result_array(); 	
    }
	public function view($table_name, $where)
    {
		$query = $this->db->get_where($table_name, $where);
		return $query->result_array(); 
    }
	public function view_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function add($table_name, $data)
    {
		return $this->db->insert($table_name, $data); 
	}  
	public function edit($table_name, $data, $where)
    {
		return $this->db->update($table_name, $data, $where);
	}
	public function remove($table_name, $where)
    {
		return $this->db->delete($table_name, $where); 
	}
	public function last_insert_id()
	{
		return $this->db->insert_id();
	}
	public function last_qry()
	{
		echo $this->db->last_query();
	}	
	public function login_check($table,$where){
		   $this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query;
	}	
	public function encry_login_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function func_common_listing($table,$field,$ord)
	{
        $this->db->from($table);
        if($field!='' && $ord!=''){
        $this->db->order_by($field, $ord);
        }
        $query = $this->db->get(); 
        return $query->result_array();
	}
	public function func_common_listing_where($table,$where,$field,$ord){
		    $this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$this->db->order_by($field, $ord);
			$query = $this->db->get(); 
			return $query->result_array();
	}
	public function donor_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function fundraiser_donors($fund_id)
	{
		$sql = "SELECT count(*) as total_donor FROM tbl_donors_payment WHERE fund_id='".$fund_id."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function fundraiser_funds($fund_id)
	{
		$sql = "SELECT IFNULL(SUM(donation),0.00) raised_amt FROM tbl_donors_payment WHERE fund_id='".$fund_id."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function fundraiser_messages($fund_id)
	{
		$sql="SELECT tm.*,CONCAT(tp.player_fname,' ',tp.player_lname) player_name ,CONCAT(tdp.donor_fname,' ',tdp.donor_lname) donor_name 
		FROM tbl_messages tm
		LEFT JOIN tbl_player tp ON tm.player_slug=tp.player_slug_name
		LEFT JOIN tbl_donors_payment tdp ON tm.donation_id=tdp.id
		WHERE tdp.fund_id='".$fund_id."' AND CONCAT(tdp.donor_fname,' ',tdp.donor_lname) != '' ORDER BY tm.id desc LIMIT 6";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function player_donors($player_slug)
	{
		$sql = "SELECT count(*) as total_donor FROM tbl_donors_payment WHERE player_slug='".$player_slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function total_player_fund($slug)
	{
		$sql="SELECT *, IFNULL(SUM(donation),0.00) raised_amt FROM tbl_donors_payment WHERE player_slug='".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function player_messages($slug)
	{
		$sql="SELECT tm.*,CONCAT(tp.player_fname,' ',tp.player_lname) player_name ,CONCAT(tdp.donor_fname,' ',tdp.donor_lname) donor_name 
		FROM tbl_messages tm
		LEFT JOIN tbl_player tp ON tm.player_slug=tp.player_slug_name
		LEFT JOIN tbl_donors_payment tdp ON tm.donation_id=tdp.id
		WHERE tm.player_slug='".$slug."' AND CONCAT(tdp.donor_fname,' ',tdp.donor_lname) != '' ORDER BY tm.id desc LIMIT 6";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function playersInfo($slug)
	{
		$sql   = "SELECT tp.*, tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt
		FROM tbl_player tp
		LEFT JOIN (
		SELECT id as pay_id,fund_id,player_id ,IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY player_id
		) tdnp ON tdnp.player_id=tp.id 
		WHERE tp.player_slug_name='".$slug."' 
		GROUP BY tp.id
		ORDER BY player_fname ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function get_activation_code()
	{
		$sql   = "SELECT * FROM tbl_activation_codes WHERE consumed='0' ORDER BY id ASC LIMIT 1";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
}
?>