<style>
#wrapper{padding-left:0 !important;}
.login_frm{left:0 !important;margin: 0px auto 0 !important; width: auto !important;}
#page-wrapper,.mainbox ,.container,.blue-bg, .white-bg{height:92vh}	
.navbar{margin-bottom: 0 !important;}
.blue-bg{ background:#147dbe;}
.white-bg{ background:#fff;}
/*-------------------------------*/
</style>

<script src="<?php echo base_url('assets/admin/js/jquery.validate.js')?>"></script>

<script src="<?php echo base_url('assets/admin/js/additional-methods.js')?>"></script>

	<div class="container">

		<div class="col-sm-5 blue-bg">

			<div class="log-txt log-gap">

				<div class="logo-log"><img src="<?php echo base_url('assets/admin/images/Logo_Clear_white.png');?>"></div>

				<h4>Raising Money Has<br> Never Been So Easy </h4>

				<ul class="log-ul-info">

					<li>1) &nbsp; &nbsp; &nbsp;  Create an Account</li>

					<li>2) &nbsp; &nbsp; &nbsp; Complete the Fundraiser Form</li>

					<li>3) &nbsp; &nbsp; &nbsp; Invite Participants</li>

					<li>4) &nbsp; &nbsp; &nbsp; Receive Donations </li>

					<li>5) &nbsp; &nbsp; &nbsp; GIFT IS GIVEN TO DONORS </li>

					

				</ul>

			</div>
		</div>

		<div class="col-sm-7 white-bg">

			   <div class="log-sec log-gap">

				<h3 class="heading-log">

					<div class="panel-title" style="margin-bottom: 0;">Create an Account<!--Start A Fundraiser--></div>

				</h3>

				<?php

				$err = '';

				if(isset($err_msg) && $err_msg==1){

				$err = 'Username or Password is wrong !';

				unset($err_msg);

				}

				?>  

				<div class="err_msg_div" style="background-color:transparent; color:#f00;"><?php echo $err ?></div>

				<div class="panel-body">

					<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

					<form id="signupform" name="signupform" class="form-horizontal" role="form" method="post" action="<?php echo base_url('start-fundraiser');?>">

						<div class="row"><div class="col-sm-6">

						<div class="input-group">

							<label>First name</label>

							<input id="fname" type="text" class="form-control" name="fname" required="" />

						</div></div>

						<div class="col-sm-6"><div class="input-group">

							<label>Last name</label>

							<input id="lname" type="text" class="form-control" name="lname" required=""/>

						</div></div></div>

						<div class="input-group">

							<label>Organization name</label>

							<input id="username" type="text" class="form-control" name="username" required=""/>

						</div>

						<div class="input-group">

							<label>Email address</label>

							<input id="email" type="email" class="form-control" name="email" required=""/>

						</div>

						<div  class="input-group"  style="margin-bottom: 15px;">

							<label>Password <span>(min.6 char)</span></label>

							<input id="password" type="password" class="form-control" name="password" required=""/>

						</div>

						<div class="form-group">

							<div class="col-sm-12 controls">

							<input type="submit" name="signup_btn" id="signup_btn" class="btn3" value="CREATE ACCOUNT"/>

							</div>

						</div>

					</form>

					<div class="log-foot blue-txt">By creating an account, you agree to the 
					<a href="javascript:void(0);" onclick="displayPage('<?php echo base64_encode("11230");?>')">Terms</a> 
					and 
					<a href="javascript:void(0);" onclick="displayPage('<?php echo base64_encode("11225");?>')">Privacy Policy</a></div>
				</div>
			</div>
		</div>
	</div>

<div id="terms" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content terms">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>


<script type="text/javascript">

$(document).ready(function() {     

	$("#signupform").validate({
		rules: {
			fname: {
				required  : true,
			},
			lname: {
				required  : true,
			},

			username: {
				required  : true,
				remote: {
					url : "<?php echo base_url('check/fund_username_check');?>",
					type: "post",
					data: {
					username: function() {
						return $( "#username" ).val();
					}
					}
				}
			},

			email: {
				required  : true,
				email: true,
				remote: {
					url : "<?php echo base_url('check/fund_email_check');?>",
					type: "post",
					data: {
					email: function() {
						return $( "#email" ).val();

					}
					}
				}
			},
			password: {
				required  : true,
				minlength : 6
			}
		},

		messages: {
			username:
			{
				remote:"The Organization is already in use!!"
			},
			email:
			{
				remote:"This email is already in use against a campaign!!"
			}
		}
	});
});
</script>
<script type="text/javascript">
function displayPage(page_id){
	var url = "<?php echo base_url('wppages/get_page');?>";
	$.ajax({
		type: 'POST',
		url: url,
		data: {page_id: page_id},
		success: function (msg) {
				$(".modal-body").html(msg);
				$("#terms").modal('show');
		}
	});
}
</script>   