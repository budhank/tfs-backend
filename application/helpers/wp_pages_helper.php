<?php
ini_set('memory_limit', '256M');

function get_page_content($page_id = 0) {
require_once(str_replace("apps","wp-load.php",realpath(FCPATH)));
$post_12 = get_post($page_id); 
$page_content = apply_filters('the_content',$post_12->post_content);
return $page_content;
}
?>