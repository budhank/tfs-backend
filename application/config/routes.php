<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] 									= 'front';
$route['home']               									= 'front/home';
$route['unsubscribe/(:any)']                                            = 'front/unsubscribe/$1';
$route['getimage/(:any)']               						= 'front/getTrackimage/$1';
$route['getimagefund/(:any)']               					= 'front/getTrackimagefund/$1';

$route['404_override']= '';

/*-------------------------------------------ADMIN ROUTING START-----------------------------------------------*/

$route['start-fundraiser']     									= 'check/start_fundraiser';
$route['forget-password']     									= 'check/forget_password';
$route['check/validUser']     									= 'check/validUser';
$route['check/verifyUser']     									= 'check/verifyUser';
$route['check/fund_email_check']     							= 'check/fund_email_check';
$route['check/fund_username_check']     						= 'check/fund_username_check';
$route['admin']     											= 'check/login';
$route['admin/login'] 											= 'check/login';
$route['login/adminlogin'] 										= 'check/adminlogin';
$route['admin/checkfile'] 										= 'admin/checkfile';

$route['admin/fundraiser/(:any)/invitationtoall'] 				= 'admin/invitationtoall/$1';
$route['admin/fundraiser/(:any)/information'] 					= 'admin/information_tab/$1';
$route['admin/fundraiser/(:any)/report/list'] 					= 'admin/reportlist/$1';
$route['admin/fundraiser/(:any)/report/graph']			        = 'admin/reportgraph/$1';
$route['admin/fundraiser/(:any)/report/list/invited'] 			= 'admin/reportlistinvited/$1';
$route['admin/fundraiser/(:any)/report/graph/invited']			= 'admin/reportgraphinvited/$1';

$route['admin/fundraiser/(:any)/paymentinformation/add'] 		= 'admin/paymentinformation/$1';

$route['admin/fundraiser/(:any)/organization'] 					= 'admin/organization_tab/$1';
$route['admin/fundraiser/(:any)/bankinfo'] 						= 'admin/bankinfo_tab/$1';
$route['admin/fundraiser/(:any)/fundraiser'] 					= 'admin/fundraiser_tab/$1';
$route['admin/fundraiser/(:any)/participants'] 					= 'admin/participants_tab/$1';
$route['admin/fundraiser/(:any)/startup'] 						= 'admin/startup_tab/$1';
$route['admin/fundraiser/(:any)/bulk_player_upload'] 			= 'admin/bulk_player_upload/$1';
$route['admin/fundraiser/(:any)/delete-participant/(:any)'] 	= 'admin/delParticipant/$1/$2';

$route['admin/fundraiser/(:any)'] 								= 'admin/fundraiserdetails/$1';
$route['admin/fundraiser/(:any)/players'] 						= 'admin/fundraiserplayerlist/$1';
$route['admin/fundraiser/(:any)/players/add'] 					= 'admin/fundraiserplayeradd/$1';
$route['admin/fundraiser/(:any)/sendtoall'] 					= 'admin/sendtoall/$1';
$route['admin/fundraiser/(:any)/sendtomailtofunraiser'] 		= 'admin/sendtomailtofunraiser/$1';
$route['admin/fundraiser/(:any)/players/summary/(:any)'] 		= 'admin/playerSummary/$1/$2';
$route['admin/fundraiser/(:any)/players/donors/(:any)'] 		= 'admin/playerDonors/$1/$2';
$route['admin/fundraiser/(:any)/players/emailreportgraph/(:any)']= 'admin/playerEmailReportsGraph/$1/$2';
$route['admin/fundraiser/(:any)/players/emailreportlist/(:any)']= 'admin/playerEmailReportsList/$1/$2';
$route['admin/fundraiser/(:any)/players/setting/(:any)']		= 'admin/playerSetting/$1/$2';
$route['admin/fundraiser/(:any)/maganage-rewards']  			= 'admin/fundraiserManageRewards/$1';
$route['admin/fundraiser/(:any)/settings'] 						= 'admin/fundraiserSettings/$1';
$route['admin/asktoinvite/(:any)/(:any)'] 						= 'admin/asktoinvite/$1/$2';
$route['admin/remindtoinvite/(:any)/(:any)'] 					= 'admin/remindtoinvite/$1/$2';
$route['admin/secremindtoinvite/(:any)/(:any)'] 				= 'admin/secremindtoinvite/$1/$2';
$route['admin/donationrequest/(:any)'] 							= 'admin/donationrequest/$1';
$route['admin/reminddonationrequest/(:any)'] 					= 'admin/reminddonationrequest/$1';
$route['admin/cron-mail-list'] 									= 'admin/cron_mail_list';


$route['wppages/get_page'] 										= 'wppages/get_page';

/*-------------------------------------------ADMIN ROUTING END--------------------------------------------------*/

/*-------------------------------------------FUNDRAISER ROUTING START-------------------------------------------*/

$route['(:any)/admin'] 											= 'checkfundraiser/login/$1';
$route['(:any)/admin/login'] 									= 'checkfundraiser/login/$1';
$route['(:any)/admin/fundraiserlogin'] 							= 'checkfundraiser/fundraiserlogin/$1';
$route['(:any)/admin/logout']  									= 'fundraiser/logout/$1';
//$route['(:any)/admin/test'] 									= 'fundraiser/test/$1';
$route['(:any)/admin/(:any)'] 									= 'fundraiser/test/$1';
$route['(:any)/admin/contacts/(:any)'] 				= 'fundraiser/test/$1/$2';
$route['(:any)/admin/donations/(:any)'] 				= 'fundraiser/test/$1/$2';


$route['(:any)/admin/home'] 									= 'fundraiser/home/$1';

$route['(:any)/admin/information'] 								= 'fundraiser/information_tab/$1';
$route['(:any)/admin/organization'] 							= 'fundraiser/organization_tab/$1';
$route['(:any)/admin/bankinfo'] 								= 'fundraiser/bankinfo_tab/$1';
$route['(:any)/admin/fundraiser'] 								= 'fundraiser/fundraiser_tab/$1';
$route['(:any)/admin/participants'] 							= 'fundraiser/participants_tab/$1';
$route['admin/fundraiser/(:any)/participants/edit/(:num)']      = 'admin/participants_tab/$1';
$route['(:any)/admin/getEmailToAllAjax'] 						= 'fundraiser/getEmailToAllAjax/$1';
$route['(:any)/admin/email_all_participants'] 					= 'fundraiser/email_all_participants/$1';
$route['(:any)/admin/bulk_player_upload'] 						= 'fundraiser/bulk_player_upload/$1';
$route['(:any)/admin/startup'] 									= 'fundraiser/startup_tab/$1';


$route['(:any)/admin/invitationtoall'] 							= 'fundraiser/invitationtoall/$1';
$route['(:any)/admin/delete/(:any)'] 				= 'fundraiser/delParticipant/$1/$2';
$route['(:any)/admin/edit/(:any)'] 				= 'fundraiser/editParticipant/$1/$2';


$route['(:any)/admin/pay-again'] 								= 'fundraiser/payagain/$1';
$route['(:any)/admin/report/graph']								= 'fundraiser/fundraiserReportsGraph/$1';
$route['(:any)/admin/report/list']								= 'fundraiser/fundraiserReportsList/$1';

$route['(:any)/admin/settings'] 								= 'fundraiser/settings/$1';
$route['(:any)/admin/manage-donations'] 						= 'fundraiser/managedonation/$1';
$route['(:any)/admin/manage-rewards'] 							= 'fundraiser/managereward/$1';
$route['(:any)/admin/org-profile'] 								= 'fundraiser/orgprofile/$1';
$route['(:any)/admin/org-profile1'] 							= 'fundraiser/orgprofile1/$1';
$route['(:any)/admin/players'] 									= 'fundraiser/players/$1';
$route['(:any)/admin/players/add'] 								= 'fundraiser/addeditplayer';
$route['(:any)/admin/players/summary/(:any)'] 					= 'fundraiser/playerSummary/$1/$2';
$route['(:any)/admin/players/donors/(:any)'] 					= 'fundraiser/playerDonors/$1/$2';
$route['(:any)/admin/players/emailreportgraph/(:any)']			= 'fundraiser/playerEmailReportsGraph/$1/$2';
$route['(:any)/admin/players/emailreportlist/(:any)']			= 'fundraiser/playerEmailReportsList/$1/$2';
$route['(:any)/admin/players/setting/(:any)'] 					= 'fundraiser/playerSetting/$1/$2';
$route['(:any)/admin/players/asktoinvite/(:any)'] 				= 'fundraiser/asktoinvite/$1/$2';
$route['(:any)/admin/players/remindtoinvite/(:any)'] 			= 'fundraiser/remindtoinvite/$1/$2';
$route['(:any)/admin/players/secremindtoinvite/(:any)'] 		= 'fundraiser/secremindtoinvite/$1/$2';


/*------------------------------------------------AJAX----------------------------------------------------------*/

$route['(:any)/admin/playerloadmore'] 							= 'fundraiser/playerloadmore';
$route['(:any)/admin/getPlayerModalAjax'] 						= 'fundraiser/getPlayerModalAjax';
$route['(:any)/admin/playerInfoEditAjax'] 						= 'fundraiser/playerInfoEditAjax';

$route['(:any)/admin/getFundraiserInfoOpenAjax'] 				= 'fundraiser/getFundraiserInfoOpenAjax';
$route['(:any)/admin/fundraiserInfoEditAjax'] 					= 'fundraiser/fundraiserInfoEditAjax';

$route['(:any)/admin/getFundraiserDateOpenAjax'] 				= 'fundraiser/getFundraiserDateOpenAjax';
$route['(:any)/admin/fundraiserDateEditAjax'] 					= 'fundraiser/fundraiserDateEditAjax';

$route['(:any)/admin/getFundraiserContactOpenAjax'] 			= 'fundraiser/getFundraiserContactOpenAjax';
$route['(:any)/admin/fundraiserContactEditAjax'] 				= 'fundraiser/fundraiserContactEditAjax';
$route['fundraiserEmailCheckerEditAjax']			= 'fundraiser/fundraiserEmailCheckerEditAjax';

$route['(:any)/admin/getFundraiserBankDetailOpenAjax']			= 'fundraiser/getFundraiserBankDetailOpenAjax';
$route['(:any)/admin/fundraiserBankDetailsEditAjax']			= 'fundraiser/fundraiserBankDetailsEditAjax';

$route['(:any)/admin/getFundraiserDesOpenAjax'] 				= 'fundraiser/getFundraiserDesOpenAjax';
$route['(:any)/admin/fundraiserDesEditAjax'] 					= 'fundraiser/fundraiserDesEditAjax';

$route['(:any)/admin/getFundraiserFetImgOpenAddAjax']			= 'fundraiser/getFundraiserFetImgOpenAddAjax';
$route['(:any)/admin/fundraiserFeatureAddAjax'] 				= 'fundraiser/fundraiserFeatureAddAjax';
$route['(:any)/admin/upload_fund_logo'] 						= 'fundraiser/upload_fund_logo/$1';
$route['(:any)/admin/upload_media'] 							= 'fundraiser/upload_media';
$route['(:any)/admin/delMedia'] 								= 'fundraiser/delMedia';

$route['(:any)/admin/getDonationOpenAjax'] 						= 'fundraiser/getDonationOpenAjax';
$route['(:any)/admin/fundraiserDonationAddEditAjax']			= 'fundraiser/fundraiserDonationAddEditAjax';
$route['(:any)/admin/checkDonation'] 							= 'fundraiser/checkDonation';

$route['(:any)/admin/getRewardOpenAjax'] 						= 'fundraiser/getRewardOpenAjax';
$route['(:any)/admin/fundraiserRewardAddEditAjax'] 				= 'fundraiser/fundraiserRewardAddEditAjax';

/*-------------------------------------------------AJAX-------------------------------------------------------------*/

$route['(:any)/admin/getMailStructure']        	 				= 'fundraiser/getMailStructure';
$route['(:any)/admin/sendingThanks']        	 				= 'fundraiser/sendingThanks';
$route['(:any)/admin/getMailfromReports']        	 			= 'fundraiser/getMailfromReports';
$route['(:any)/admin/sendThanksfromReports']        	 		= 'fundraiser/sendThanksfromReports';
$route['(:any)/admin/playerStatus'] 							= 'fundraiser/playerStatus';
$route['ajaxSettings'] 							= 'fundraiser/ajaxSettings';
$route['(:any)/admin/ShowHidePassword'] 						= 'fundraiser/ShowHidePassword';
$route['(:any)/admin/checkfile'] 								= 'fundraiser/checkfile';
$route['(:any)/admin/donationemailsend'] 			 			= 'fundraiser/donationemailsend';
$route['(:any)/admin/playerEmailCheckerAjax'] 			 		= 'fundraiser/playerEmailCheckerAjax';
$route['(:any)/admin/graph_json'] 			 					= 'fundraiser/graph_json/$1';


/*-------------------------------------------FUNDRAISER ROUTING END-----------------------------------------------*/

/*-------------------------------------------PLAYER ROUTING START-------------------------------------------------*/
$route['(:any)/(:any)/admin']                     		 		= 'checkplayer/login/$1/$2';
$route['(:any)/(:any)/admin/login']                     		= 'checkplayer/login/$1/$2';
$route['(:any)/(:any)/login/playerlogin']                      	= 'checkplayer/playerlogin/$1/$2';
$route['(:any)/(:any)/admin/logout']                    		= 'player/logout/$1/$2';
$route['(:any)/(:any)/admin/home']               				= 'player/home/$1/$2';
$route['(:any)/(:any)/admin/test']               				= 'player/testing/$1/$2';
$route['(:any)/(:any)/admin/donors']             				= 'player/playerDonors/$1/$2';
$route['(:any)/(:any)/admin/profile']            				= 'player/profile/$1/$2';

$route['(:any)/(:any)/admin/information']            			= 'player/information_tab/$1/$2';
$route['(:any)/(:any)/admin/contacts']            				= 'player/contact_tab/$1/$2';
$route['(:any)/(:any)/admin/invite']            				= 'player/invite_tab/$1/$2';
$route['(:any)/(:any)/admin/checkoutweb']            			= 'player/checkoutweb_tab/$1/$2';
$route['(:any)/(:any)/admin/bulk_contact_upload']            	= 'player/bulk_contact_upload/$1/$2';
$route['(:any)/(:any)/admin/invitetoall']            			= 'player/invitetoall/$1/$2';
$route['(:any)/(:any)/admin/invitetoall/unsubscribe/(:any)']    = 'player/invitetoall/$1/$2';

$route['(:any)/(:any)/admin/del_contact']            			= 'player/del_contact/$1/$2';
$route['(:any)/(:any)/admin/upload_player_logo'] 				= 'player/upload_player_logo/$1/$2';
$route['(:any)/(:any)/admin/getCustomMailAjax']            		= 'player/getCustomMailAjax/$1/$2';
$route['(:any)/(:any)/admin/pay-again']            				= 'player/payagain/$1/$2';

$route['(:any)/(:any)/admin/emailreport/graph']  				= 'player/playerEmailReportsGraph/$1/$2';
$route['(:any)/(:any)/admin/emailreport/list']   				= 'player/playerEmailReportsList/$1/$2';
$route['(:any)/(:any)/admin/settings']           				= 'player/settings/$1/$2';

$route['player/admin/ajaxSettingsp']					 		= 'player/ajaxSettings';
$route['player/admin/ShowHidePasswordp'] 		 				= 'player/ShowHidePassword';
$route['player/admin/getMailStructurep']        	 			= 'player/getMailStructure';
$route['player/admin/sendingThanksp']        	 				= 'player/sendingThanks';
$route['player/admin/getMailfromReportsp']        	 			= 'player/getMailfromReports';
$route['player/admin/sendThanksfromReportsp']        	 		= 'player/sendThanksfromReports';
$route['player/admin/getPlayerModalAjaxp']        				= 'player/getPlayerModalAjax';
$route['player/admin/playerInfoEditAjaxp']        				= 'player/playerInfoEditAjax';
$route['player/admin/donationemailsendp'] 			 			= 'player/donationemailsend';

/*-------------------------------------------PLAYER ROUTING END---------------------------------------------------*/

/*-------------------------------------------CRON ROUTING START---------------------------------------------*/
$route['send-mail-before-start'] 			 		 			= 'cronmail/cron_mail_log_credential';
$route['send-reminder-before-start'] 			 		 		= 'cronmail/cron_mail_reminder1';
$route['send-reminder-before-end'] 			 		 			= 'cronmail/cron_mail_reminder2';
$route['send-donation-details'] 			 		 			= 'cronmail/cron_mail_recap';

$route['send-mail-invite-participant1'] 			 		 	= 'cronmail/cron_mail_invite_participant1';
$route['send-mail-invite-participant2'] 			 		 	= 'cronmail/cron_mail_invite_participant2';
$route['send-mail-invite-contact-donor1'] 			 		 	= 'cronmail/cron_mail_invite_contact_donor1';
$route['send-mail-invite-contact-donor2'] 			 		 	= 'cronmail/cron_mail_invite_contact_donor2';
$route['send-mail-admin-cron-start-end'] 			 		 	= 'cronmail/cron_mail_admin_start_end';


/*-------------------------------------------CRON ROUTING END---------------------------------------------------*/

/*-------------------------------------------PLAYER FRONT PANEL START---------------------------------------------*/
$seg_array = $this->uri->segment(1);
if($seg_array!='admin')
{
$route['Frontplayer/ajax_donation_post'] 			 		 	= 'frontplayer/ajax_donation_post';
$route['donate'] 				    	 						= 'frontplayer/defaultPaymentLandingPage';
$route['donate/personal-info/(:any)'] 				    	 	= 'frontplayer/defaultPaymentPersonalInfo/$1';
$route['donatePaymentAjax']										= 'frontplayer/donatePaymentAjax';
$route['donate/payment-info'] 				    	 			= 'frontplayer/defaultPaymentInfo';
$route['donate/payment-info-success']							= 'frontplayer/defPaySucSection';
$route['donate/payment-info-failure']							= 'frontplayer/defPayFailureSection';

$route['support/(:any)'] 				    	 	= 'frontplayer/fundraiser_webpage/$1';


$route['(:any)/(:any)'] 				    	 				= 'frontplayer/home/$1/$2';
$route['(:any)/(:any)/support/(:any)'] 				    	 	= 'frontplayer/home/$1/$2/$3';
$route['(:any)/(:any)/payment'] 				 				= 'frontplayer/donorPayment/$1/$2';
$route['(:any)/(:any)/payment/support/(:any)'] 				 	= 'frontplayer/donorPayment/$1/$2/$3';
$route['(:any)/(:any)/donate'] 				 	 				= 'frontplayer/donation_paid_stripe/$1/$2';
$route['(:any)/(:any)/donate/support/(:any)'] 				 	= 'frontplayer/donation_paid_stripe/$1/$2/$3';
$route['(:any)/(:any)/donate-paypal'] 				 	 		= 'frontplayer/donation_paid_paypal/$1/$2';
$route['(:any)/(:any)/donate-paypal/support/(:any)'] 			= 'frontplayer/donation_paid_paypal/$1/$2/$3';
$route['(:any)/(:any)/thanks/(:any)'] 			 		 		= 'frontplayer/donation_success/$1/$2/$3';
$route['(:any)/(:any)/post-message'] 			 				= 'frontplayer/donation_post/$1/$2';
$route['(:any)/(:any)/share'] 			 		 				= 'frontplayer/donation_share/$1/$2';
//$route['donate/payment-info'] 				    	 			= 'frontplayer/defaultPaymentInfo';
}


/*-------------------------------------------PLAYER FRONT PANEL END-----------------------------------------------*/

/*-------------------------------------------HOME START-----------------------------------------------------------*/
$route['home'] 				    	 			 				= 'front/home';
/*-------------------------------------------HOME End-------------------------------------------------------------*/

$route['translate_uri_dashes'] = FALSE;
//echo '<pre>';print_r($route);die;