@charset "utf-8";
/* CSS Document */
@font-face {
	font-family: 'Roboto';
	src: url('../fonts/Roboto-Regular.eot');
	src: url('../fonts/Roboto-Regular.eot?#iefix') format('embedded-opentype'),  url('../fonts/Roboto-Regular.woff') format('woff'),  url('../fonts/Roboto-Regular.ttf') format('truetype');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'robotobold';
	src: url('../fonts/roboto-bold.eot');
	src: url('../fonts/roboto-bold.eot?#iefix') format('embedded-opentype'),  url('../fonts/roboto-bold.woff2') format('woff2'),  url('../fonts/roboto-bold.woff') format('woff'),  url('../fonts/roboto-bold.ttf') format('truetype'),  url('../fonts/roboto-bold.svg#robotobold') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'robotomedium';
	src: url('../fonts/Roboto-Medium-webfont.eot');
	src: url('../fonts/Roboto-Medium-webfont.eot?#iefix') format('embedded-opentype'),  url('../fonts/Roboto-Medium-webfont.woff') format('woff'),  url('../fonts/Roboto-Medium-webfont.ttf') format('truetype'),  url('../fonts/Roboto-Medium-webfont.svg#robotomedium') format('svg');
	font-weight: normal;
	font-style: normal;
}
/* general start*/
body {
	color: #333;
	font-family: 'Roboto';
	font-size: 14px;
	line-height: 22px;
	font-weight: normal;
	font-style: normal;
	margin: 0;
	padding: 0;
	margin-top: 50px;
	background-color: #f4f4f4;
}
sub, sup {
	font-size: 75%;
	position: relative;
	vertical-align: baseline;
}
sup {
	top: 10px;
	left: 10px;
}
sub {
	bottom: 10px;
	right: 10px
}
ul {
	margin: 0;
	padding: 0;
	list-style-type: none;
}
ul li {
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6 {
	padding: 0;
	margin: 0;
	font-family: 'robotobold';
	font-weight: normal;
	font-style: normal;
}
h1 {
	font-size: 40px;
	line-height: 42px;
	color: #333;
}
h2 {
	font-size: 36px;
	line-height: 38px;
	color: #333;
}
h3 {
	font-size: 30px;
	line-height: 32px;
	color: #333;
}
h4 {
	font-size: 26px;
	line-height: 30px;
	color: #333;
}
h5 {
	font-size: 20px;
	line-height: 24px;
	color: #333;
}
p {
	padding: 0 0 20px 0;
	margin: 0;
}
a {
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	transition: all 0.2s linear;
	color: #333;
	outline: none;
	text-decoration: none;
}
a:hover, a:focus {
	color: #000;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	transition: all 0.2s linear;
	text-decoration: none !important;
	outline: none !important;
}
a > img {
	line-height: 0;
}
select, button, input {
	box-shadow: none;
	outline: none;
}
select:hover, button:hover, input:hover, select:focus, button:focus, input:focus {
	box-shadow: none;
	outline: none;
}
select {
	-webkit-appearance: none;
	-moz-appearance: none;
	-ms-appearance: none;
	-o-appearance: none;
	appearance: none;
	background-image: url(images/select-arrow.png) !important;
	background-repeat: no-repeat;
	background-position: right 15px center !important;
}
ul.inline_b li {
	display: inline-block;
	vertical-align: middle;
}
.spacer {
	clear: both;
	font-size: 0;
	line-height: 0;
}
.nomar {
	margin: 0;
}
.nomar_R {
	margin-right: 0;
}
.nomar_L {
	margin-left: 0;
}
.nomar_B {
	margin-bottom: 0;
}
.nopad {
	padding: 0;
}
.nopad_R {
	padding-right: 0;
}
.nopad_L {
	padding-left: 0 !important;
}
.nopad_B {
	padding-bottom: 0;
}
.nobor {
	border: none !important;
}
.nobor_L {
	border-left: none;
}
.nobor_R {
	border-right: none;
}
.nobor_T {
	border-top: none;
}
.nobor_B {
	border-bottom: none;
}
.left {
	float: left;
}
.right {
	float: right;
}
.middle {
	margin: 0 auto;
}
.alignleft {
	text-align: left;
	margin: 0;
}
.alignright {
	text-align: right;
	margin: 0;
}
.aligncenter {
	text-align: center;
	margin: 0;
}
.wrapper {
	width: 100%;
}
.header, header {
	width: 100%;
	background: #00CCFF;
	padding: 20px 0;
}
.footer, footer {
	width: 100%;
	background: #333;
	padding: 20px 0;
}
.site, .site-branding {
	margin: 0;
}
.site-inner {
	width: 100%;
	max-width: 100%;
	margin: 0;
}
.site-header {
	padding: 0;
}
body:not(.custom-background-image)::before, body:not(.custom-background-image)::after {
	height: auto;
}
.site-branding, .site-header-menu, .header-image {
	margin-bottom: 0;
	margin-top: 0;
}
.main-navigation, .site-main {
	margin: 0;
}
.site-header-main {
	display: block;
}
.site-content {
	padding: 0;
}
.content-area {
	float: none;
	margin-right: 0;
	width: 100%;
}
.site-footer {
	display: block;
	padding: 0;
}
.widget {
	font-size: inherit;
	line-height: inherit;
	border: none;
	padding: 0;
	margin: 0;
}
.sort-filter-ico > a {
	font-size: 12px;
	font-weight: normal;
	margin-left: 10px;
}
/* 
 * 	Core Owl Carousel CSS File
 *	v1.3.3
 */

.owl-carousel .owl-wrapper:after {
	content: ".";
	display: block;
	clear: both;
	visibility: hidden;
	line-height: 0;
	height: 0;
}
.owl-carousel {
	display: none;
	position: relative;
	width: 100%;
	-ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper {
	display: none;
	position: relative;
	-webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper-outer {
	overflow: hidden;
	position: relative;
	width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight {
	-webkit-transition: height 500ms ease-in-out;
	-moz-transition: height 500ms ease-in-out;
	-ms-transition: height 500ms ease-in-out;
	-o-transition: height 500ms ease-in-out;
	transition: height 500ms ease-in-out;
}
.owl-carousel .owl-item {
	float: left;
}
.owl-controls .owl-page, .owl-controls .owl-buttons div {
	cursor: pointer;
}
.owl-controls {
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
.grabbing {
	cursor: url(grabbing.png) 8 8, move;
}
.owl-carousel .owl-wrapper, .owl-carousel .owl-item {
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility: hidden;
	-ms-backface-visibility: hidden;
	backface-visibility: hidden;
	-webkit-transform: translate3d(0, 0, 0);
	-moz-transform: translate3d(0, 0, 0);
	-ms-transform: translate3d(0, 0, 0);
	transform: translate3d(0, 0, 0);
}
.owl-origin {
	-webkit-perspective: 1200px;
	-webkit-perspective-origin-x : 50%;
	-webkit-perspective-origin-y : 50%;
	-moz-perspective : 1200px;
	-moz-perspective-origin-x : 50%;
	-moz-perspective-origin-y : 50%;
	perspective : 1200px;
}
.owl-fade-out {
	z-index: 10;
	-webkit-animation: fadeOut .7s both ease;
	-moz-animation: fadeOut .7s both ease;
	animation: fadeOut .7s both ease;
}
.owl-fade-in {
	-webkit-animation: fadeIn .7s both ease;
	-moz-animation: fadeIn .7s both ease;
	animation: fadeIn .7s both ease;
}
.owl-backSlide-out {
	-webkit-animation: backSlideOut 1s both ease;
	-moz-animation: backSlideOut 1s both ease;
	animation: backSlideOut 1s both ease;
}
.owl-backSlide-in {
	-webkit-animation: backSlideIn 1s both ease;
	-moz-animation: backSlideIn 1s both ease;
	animation: backSlideIn 1s both ease;
}
.owl-goDown-out {
	-webkit-animation: scaleToFade .7s ease both;
	-moz-animation: scaleToFade .7s ease both;
	animation: scaleToFade .7s ease both;
}
.owl-goDown-in {
	-webkit-animation: goDown .6s ease both;
	-moz-animation: goDown .6s ease both;
	animation: goDown .6s ease both;
}
.owl-fadeUp-in {
	-webkit-animation: scaleUpFrom .5s ease both;
	-moz-animation: scaleUpFrom .5s ease both;
	animation: scaleUpFrom .5s ease both;
}
.owl-fadeUp-out {
	-webkit-animation: scaleUpTo .5s ease both;
	-moz-animation: scaleUpTo .5s ease both;
	animation: scaleUpTo .5s ease both;
}
@-webkit-keyframes empty {
 0% {
opacity: 1
}
}
@-moz-keyframes empty {
 0% {
opacity: 1
}
}
@keyframes empty {
 0% {
opacity: 1
}
}
@-webkit-keyframes fadeIn {
 0% {
opacity:0;
}
 100% {
opacity:1;
}
}
@-moz-keyframes fadeIn {
 0% {
opacity:0;
}
 100% {
opacity:1;
}
}
@keyframes fadeIn {
 0% {
opacity:0;
}
 100% {
opacity:1;
}
}
@-webkit-keyframes fadeOut {
 0% {
opacity:1;
}
 100% {
opacity:0;
}
}
@-moz-keyframes fadeOut {
 0% {
opacity:1;
}
 100% {
opacity:0;
}
}
@keyframes fadeOut {
 0% {
opacity:1;
}
 100% {
opacity:0;
}
}
@-webkit-keyframes backSlideOut {
 25% {
opacity: .5;
-webkit-transform: translateZ(-500px);
}
 75% {
opacity: .5;
-webkit-transform: translateZ(-500px) translateX(-200%);
}
 100% {
opacity: .5;
-webkit-transform: translateZ(-500px) translateX(-200%);
}
}
@-moz-keyframes backSlideOut {
 25% {
opacity: .5;
-moz-transform: translateZ(-500px);
}
 75% {
opacity: .5;
-moz-transform: translateZ(-500px) translateX(-200%);
}
 100% {
opacity: .5;
-moz-transform: translateZ(-500px) translateX(-200%);
}
}
@keyframes backSlideOut {
 25% {
opacity: .5;
transform: translateZ(-500px);
}
 75% {
opacity: .5;
transform: translateZ(-500px) translateX(-200%);
}
 100% {
opacity: .5;
transform: translateZ(-500px) translateX(-200%);
}
}
@-webkit-keyframes backSlideIn {
 0%, 25% {
opacity: .5;
-webkit-transform: translateZ(-500px) translateX(200%);
}
 75% {
opacity: .5;
-webkit-transform: translateZ(-500px);
}
 100% {
opacity: 1;
-webkit-transform: translateZ(0) translateX(0);
}
}
@-moz-keyframes backSlideIn {
 0%, 25% {
opacity: .5;
-moz-transform: translateZ(-500px) translateX(200%);
}
 75% {
opacity: .5;
-moz-transform: translateZ(-500px);
}
 100% {
opacity: 1;
-moz-transform: translateZ(0) translateX(0);
}
}
@keyframes backSlideIn {
 0%, 25% {
opacity: .5;
transform: translateZ(-500px) translateX(200%);
}
 75% {
opacity: .5;
transform: translateZ(-500px);
}
 100% {
opacity: 1;
transform: translateZ(0) translateX(0);
}
}
@-webkit-keyframes scaleToFade {
 to {
opacity: 0;
-webkit-transform: scale(.8);
}
}
@-moz-keyframes scaleToFade {
 to {
opacity: 0;
-moz-transform: scale(.8);
}
}
@keyframes scaleToFade {
 to {
opacity: 0;
transform: scale(.8);
}
}
@-webkit-keyframes goDown {
 from {
-webkit-transform: translateY(-100%);
}
}
@-moz-keyframes goDown {
 from {
-moz-transform: translateY(-100%);
}
}
@keyframes goDown {
 from {
transform: translateY(-100%);
}
}
 @-webkit-keyframes scaleUpFrom {
 from {
opacity: 0;
-webkit-transform: scale(1.5);
}
}
@-moz-keyframes scaleUpFrom {
 from {
opacity: 0;
-moz-transform: scale(1.5);
}
}
@keyframes scaleUpFrom {
 from {
opacity: 0;
transform: scale(1.5);
}
}
 @-webkit-keyframes scaleUpTo {
 to {
opacity: 0;
-webkit-transform: scale(1.5);
}
}
@-moz-keyframes scaleUpTo {
 to {
opacity: 0;
-moz-transform: scale(1.5);
}
}
@keyframes scaleUpTo {
 to {
opacity: 0;
transform: scale(1.5);
}
}
.owl-theme .owl-controls {
	margin-top: 10px;
	text-align: center;
}
.owl-theme .owl-controls .owl-buttons div {
	color: #FFF;
	display: inline-block;
	zoom: 1;
 *display: inline;
	margin: 5px;
	padding: 3px 10px;
	font-size: 12px;
	-webkit-border-radius: 30px;
	-moz-border-radius: 30px;
	border-radius: 30px;
	background: #869791;
	filter: Alpha(Opacity=50);
	opacity: 0.5;
}
.owl-theme .owl-controls.clickable .owl-buttons div:hover {
	filter: Alpha(Opacity=100);
	opacity: 1;
	text-decoration: none;
}
.owl-theme .owl-controls .owl-page {
	display: inline-block;
	zoom: 1;
 *display: inline;
}
.owl-theme .owl-controls .owl-page span {
	display: block;
	width: 12px;
	height: 12px;
	margin: 5px 7px;
	filter: Alpha(Opacity=50);
	opacity: 0.5;
	-webkit-border-radius: 20px;
	-moz-border-radius: 20px;
	border-radius: 20px;
	background: #869791;
}
.owl-theme .owl-controls .owl-page.active span, .owl-theme .owl-controls.clickable .owl-page:hover span {
	filter: Alpha(Opacity=100);
	opacity: 1;
}
.owl-theme .owl-controls .owl-page span.owl-numbers {
	height: auto;
	width: auto;
	color: #FFF;
	padding: 2px 10px;
	font-size: 12px;
	-webkit-border-radius: 30px;
	-moz-border-radius: 30px;
	border-radius: 30px;
}
.owl-item.loading {
	min-height: 150px;
	background: url(AjaxLoader.gif) no-repeat center center
}
.morris-hover {
	position: absolute;
	z-index: 1000
}
.morris-hover.morris-default-style {
	border-radius: 10px;
	padding: 6px;
	color: #666;
	background: rgba(255,255,255,0.8);
	border: solid 2px rgba(230,230,230,0.8);
	font-family: sans-serif;
	font-size: 12px;
	text-align: center
}
.morris-hover.morris-default-style .morris-hover-row-label {
	font-weight: bold;
	margin: 0.25em 0
}
.morris-hover.morris-default-style .morris-hover-point {
	white-space: nowrap;
	margin: 0.1em 0
}
#wrapper {
	padding-left: 0;
}
#page-wrapper {
	width: 100%;
	padding: 0;
	background-color: #fff;
}
.huge {
	font-size: 50px;
	line-height: normal;
}
 @media(min-width:768px) {
#wrapper {
	padding-left: 225px;
}
#page-wrapper {
	padding: 10px;
}
}
/* Top Navigation */

.top-nav {
	padding: 0 20px 0 0;
}
.top-nav>li {
	display: inline-block;
	float: left;
}
.top-nav>li>a {
	padding-top: 15px;
	padding-bottom: 15px;
	line-height: 20px;
	color: #999;
}
.top-nav>li>a:hover, .top-nav>li>a:focus, .top-nav>.open>a, .top-nav>.open>a:hover, .top-nav>.open>a:focus {
	color: #fff;
	background-color: #000;
}
.top-nav>.open>.dropdown-menu {
	float: left;
	position: absolute;
	margin-top: 0;
	border: 1px solid rgba(0,0,0,.15);
	border-top-left-radius: 0;
	border-top-right-radius: 0;
	background-color: #fff;
	-webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
	box-shadow: 0 6px 12px rgba(0,0,0,.175);
}
.top-nav>.open>.dropdown-menu>li>a {
	white-space: normal;
}
ul.message-dropdown {
	padding: 0;
	max-height: 250px;
	overflow-x: hidden;
	overflow-y: auto;
}
li.message-preview {
	width: 275px;
	border-bottom: 1px solid rgba(0,0,0,.15);
}
li.message-preview>a {
	padding-top: 15px;
	padding-bottom: 15px;
}
li.message-footer {
	margin: 5px 0;
}
ul.alert-dropdown {
	width: 200px;
}

/* Side Navigation */

@media(min-width:768px) {
.side-nav {
	position: fixed;
	top: 51px;
	left: 225px;
	width: 225px;
	margin-left: -225px;
	border: none;
	border-radius: 0;
	overflow-y: auto;
	background-color: #242536;
	bottom: 0;
	overflow-x: hidden;
	padding-bottom: 40px;
}
.side-nav>li>a {
	width: 225px;
}
.side-nav li a:hover,  .side-nav li a:focus {
	outline: none;
	background-color: #000 !important;
}
}
.side-nav>li>ul {
	padding: 0;
}
.side-nav>li>ul>li>a {
	display: block;
	padding: 10px 15px 10px 38px;
	text-decoration: none;
	color: #999;
}
.side-nav>li>ul>li>a:hover {
	color: #fff;
}
/* Flot Chart Containers */

.flot-chart {
	display: block;
	height: 400px;
}
.flot-chart-content {
	width: 100%;
	height: 100%;
}
/* Custom Colored Panels */

.huge {
	font-size: 40px;
}
.panel-green {
	border-color: #5cb85c;
}
.panel-green > .panel-heading {
	border-color: #5cb85c;
	color: #fff;
	background-color: #5cb85c;
}
.panel-green > a {
	color: #5cb85c;
}
.panel-green > a:hover {
	color: #3d8b3d;
}
.panel-red {
	border-color: #d9534f;
}
.panel-red > .panel-heading {
	border-color: #d9534f;
	color: #fff;
	background-color: #d9534f;
}
.panel-red > a {
	color: #d9534f;
}
.panel-red > a:hover {
	color: #b52b27;
}
.panel-yellow {
	border-color: #f0ad4e;
}
.panel-yellow > .panel-heading {
	border-color: #f0ad4e;
	color: #fff;
	background-color: #f0ad4e;
}
.panel-yellow > a {
	color: #f0ad4e;
}
.panel-yellow > a:hover {
	color: #df8a13;
}
/* general end*/
.navbar-brand {
	padding: 0;
	width: 225px;
}
.navbar-brand img {
	width: 225px;
}
.navbar-inverse {
	background-color: #242536;
}
.cd {
	height: 26px;
	width: 26px;
	line-height: 26px;
	text-transform: uppercase;
	color: #fff;
	background: #3aca60;
	border-radius: 100%;
	display: inline-block;
	text-align: center;
}
.navbar-right > li > a > i.fa {
	font-size: 20px;
	color: #fff;
	line-height: 26px;
}
.top-nav > li > a {
	padding: 12px 8px;
}
#page-wrapper {
	padding: 0;
	background: #f4f4f4;
}
.bg_wht {
	background: #fff;
	border: 1px solid #e6e6e6;
	border-radius: 4px;
}
.full_top_wrp {
	padding: 20px 0 0;
	position:relative;
}
.breadcrumb {
	background: #fff;
	border-radius: 0;
	padding: 0 15px 20px;
	margin: 0;
	border-bottom: 1px solid #efefef;
}
.email-view-popup-wrapper, .email-edit-temp-wrapper {width:770px;}
.top_pro {
	display: table;
	width: 100%;
}
.top_pro_img {
	display: inline-block;
	vertical-align: middle;
	width: 60px;
	height: 60px;
	border-radius: 100%;
	overflow: hidden;
}
.top_pro_img img {
	width: 60px;
}
.top_pro_con {
	display: table-cell;
	vertical-align: middle;
	padding-left: 10px;
}
.top_pro_wrp h4 {
	font-size: 22px;
	line-height: 22px;
	color: #333333;
	margin-bottom: 5px;
}
.dlr {
	font-family: Arial;
}
.top_pro_wrp h4 .dlr {
	font-weight: bold;
}
textarea {
	resize: none;
}
.top_pro_wrp p {
	padding: 0;
}
.top_pro_wrp {
	padding: 20px 0;
}
.top_pro_prc p {
	font-size: 26px;
	line-height: 26px;
	color: #333;
}
.top_pro_prc h4 {
	margin-bottom: 8px;
}
.grn1 {
	color: #7ed321 !important;
}
.btn_round {
	padding: 10px 20px;
	border-radius: 30px;
	font-size: 16px;
	line-height: 16px;
	font-family: 'robotobold';
	font-weight: normal;
	font-style: normal;
	color: #fff;
	background: #3aca60;
	display: inline-block;
	border: 1px solid #3aca60;
}
.btn_round:hover {
	background: #242536;
	color: #fff;
	border: 1px solid #242536;
}
.top_pro_donation .btn_round {
	margin-bottom: 10px;
}
.breadcrumb > li > a {
	font-weight: normal;
}
.breadcrumb > .active {
	font-family: 'robotobold';
	font-weight: normal;
	font-style: normal;
	color: #333;
}
.breadcrumb > li + li::before {
	color: #333;
	content: "\f105";
	font-family: 'FontAwesome';
}
.grn2 {
	color: #3aca60;
	text-decoration: underline;
	display: inline-block;
}
.tab_mnu {
	padding: 0 15px;
}
.tab_mnu > ul > li {
	display: inline-block;
	margin-right: 20px;
	vertical-align: top;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.tab_mnu > ul > li > a {
	display: block;
	padding: 10px 0 7px;
	border-bottom: 3px solid #fff;
	font-weight: normal;
}
.tab_mnu > ul > li > a:hover {
	color: #3aca60;
}
.tab_mnu > ul > li.active > a {
	color: #333;
	font-family: 'robotobold';
	font-weight: normal;
	font-style: normal;
	border-bottom: 3px solid #3aca60;
}
.top_pro_wrp > .clearfix > .col-xs-12:nth-child(2), .top_pro_wrp > .clearfix > .col-xs-12:nth-child(3) {
	border-right: 3px solid #ececec;
}
.tit1_sec {
	padding-bottom: 15px;
	border-bottom: 1px solid #9b9b9b;
}
.tit1_sec h2 {
	color: #333;
	font-size: 19px;
	line-height: 20px;
	margin-top: 8px;
}
.mar_t_15 {
	margin-top: 15px;
}
.tit1_sec .right a {
	padding: 5px 16px;
	background: #fff;
	color: #333;
	display: inline-block;
	font-size: 14px;
	line-height: 18px;
}
.tit1_sec .right {
	border-radius: 5px;
	border: 2px solid #dcdcdc;
	overflow: hidden;
}
.tit1_sec .right a.active {
	background: #3aca60;
	color: #fff;
}
.tot_pad {
	padding: 15px;
}
.filter_sec_inner li label {
	display: block;
	text-align: left;
	font-weight: normal;
	margin-bottom: 5px;
	font-size: 14px;
	line-height: 16px;
	height: 20px;
}
.filter_sec_inner ul > li {
	display: inline-block;
	width: 100px;
	margin-right: 20px;
	text-align: center;
}
.filter_sec_inner .datepicker {
	border: 1px solid #3aca60;
	padding: 0 5px;
	height: 34px;
	border-radius: 20px;
	width: 100%;
	text-align: center;
}
.filter_sec_inner ul > li > a {
	padding: 7px 15px;
	width: 100%;
	display: inline-block;
	border: 1px solid #9b9b9b;
	border-radius: 20px;
	color: #333;
}
.filter_sec_inner ul > li > a:hover {
	color: #fff;
	background: #9b9b9b;
}
.filter_sec_inner ul > li:nth-child(2) {
	margin-right: 40px;
}
.btn1, .filter_sec_inner ul > li > a.btn1 {
	padding: 7px 15px;
	display: inline-block;
	border: 1px solid #3aca60;
	border-radius: 20px;
	color: #333;
}
.btn1:hover, .filter_sec_inner ul > li > a.btn1:hover {
	background: #3aca60;
}
.filter_sec h3 {
	margin-bottom: 15px;
	font-size: 16px;
	line-height: 18px;
	color: #333;
}
.filter_sec {
	padding-top: 15px;
	padding-bottom: 30px;
	border-bottom: 1px solid #cccccc;
}
.total_info {
	padding-top: 30px;
}
.total_info_con {
	padding: 10px;
	border: 3px solid #cccccc;
	border-radius: 5px;
}
.total_info_con p {
	padding: 0 0 5px;
	color: #6c728a;
}
.total_info_con span {
	font-size: 20px;
	line-height: 20px;
	color: #333;
	font-family: 'robotobold';
	font-weight: normal;
	font-style: normal;
}
.navbar-inverse .navbar-nav > .active > a {
	background-color: #151524;
	border-left: 3px solid #3aca60;
}
.navbar-inverse .navbar-nav > li > a {
	color: #fff;
	border-left: 3px solid #242536;
}
.side-nav li a:hover, .side-nav li a:focus {
	background-color: #151524 !important;
	border-left: 3px solid #3aca60;
}
.donations_tbl {
	padding-top: 10px;
}
.table > thead > tr > th {
	border-bottom: 1px solid #eeeeee;
	vertical-align: middle;
	color: #ffb946;
	padding: 10px 5px;
}
.table > tbody > tr > td {
	border-bottom: 1px solid #eeeeee;
	vertical-align: middle;
	color: #333;
	padding: 10px 5px;
}
.table > tbody > tr > td small {
	color: #9d9d9d;
}
.table {
	margin: 0;
}
.tit2_sec h3 {
	font-size: 16px;
	line-height: 18px;
}
.tit2_sec {
	margin-bottom: 10px;
	padding-bottom: 10px;
	border-bottom: 1px solid #ccc;
}
.tit2_sec.nobor {
	border-bottom: none;
	margin-bottom: 15px;
	padding: 0;
}
.tit2_sec .left > h3 {
	margin-top: 3px;
}
#page-wrapper > .container-fluid {
	padding-bottom: 15px;
}
.sh_day_info h3 {
	font-size: 18px;
	color: #7f8c8d;
	line-height: 22px;
	margin-bottom: 15px;
	font-style: normal;
	font-weight: normal;/*font-family: 'robotoregular';*/
}
.sh_day_info ul li {
	display: inline-block;
	vertical-align: top;
	text-align: center;
	margin-right: 20px;
	color: #9ea7b3;
	width: 90px;
}
.sh_day_info ul li img {
	width: 90px;
}
.sh_day_info ul li p {
	padding: 0;
	margin-top: 5px;
}
.sh_day_info {
	padding-bottom: 10px;
	border-bottom: 1px solid #dee1e2;
}
.table > tbody > tr > td:first-child span {
	float:left;
	width: 40px;
	height:40px;
	overflow:hidden;
	border-radius: 100%;
	overflow: hidden;
}
.table > tbody > tr > td:first-child > div{
	display:inline-block !important;
	float:left;
	width:80%;
}
.table > tbody > tr > td:first-child span img {
	width: 40px;
	border-radius: 100%;
}
.table > tbody > tr > td:first-child strong {
	display: table-cell;
	vertical-align: middle;
	padding-left: 10px;
}
.add_plr_wrp h4 {
	font-size: 17px;
	line-height: 18px;
	margin-bottom: 15px;
}
.frm .form-group {
	position: relative;
}
.frm .form-group small {
	position: absolute;
	top: 33px;
	right: 10px;
	color: #9b9b9b;
}
.ico_fld {
	position: relative;
}
.ico_fld .fa {
	position: absolute;
	left: 10px;
	top: 38px;
	color: #9b9b9b;
}
.ico_fld .form-control {
	padding-left: 30px;
}
hr {
	border-color: #ccc;
}
.frm_btn_grp .btn_round {
	padding: 10px 30px;
	margin-right: 10px;
}
.undr_lin {
	display: inline-block;
	text-decoration: underline;
}
.undr_lin:hover {
	text-decoration: none;
	color: #3aca60;
}
.upld_img {
	height: 120px;
	width: 120px;
	border-radius: 100%;
	border: 1px solid #979797;
	position: relative;
	overflow: hidden;
}
.upld_img img {
	width: 120px;
	height: auto;
}
.up_file {
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	opacity: 0;
	cursor: pointer;
}
.modal-body .tit1_sec {
	margin-bottom: 25px;
}
.chng_up_file {
	opacity: 0;
	position: relative;
	z-index: 2;
	cursor: pointer;
	width: 130px;
	left: 0;
	right: 0;
	margin: auto;
}
.chng_up_file_wrp {
	position: relative;
	margin-top: 10px;
	overflow: hidden;
}
.chng_up_file_wrp a {
	position: absolute;
	color: #3aca60;
	font-family: 'robotobold';
	font-weight: normal;
	font-style: normal;
	left: 0;
	right: 0;
	top: 0;
	text-decoration: underline;
	margin: 0 auto;
	width: 120px;
}
.modal-content {
	border-radius: 0;
}
.chng_up_file_wrp:hover a {
	color: #333;
}
.fund_con > p:last-child {
	padding-bottom: 0;
}
.fund_con span {
	display: block;
	font-family: 'robotobold';
	font-weight: normal;
	font-style: normal;
}
.add_plr_wrp .rew_don h4 {
	margin-bottom: 5px;
}
.rew_don_inr {
	border: 1px solid #979797;
	display: table;
	width: 100%;
	margin-bottom: 30px;
}
.rew_don_rdo {
	display: table-cell;
	vertical-align: middle;
	width: 15px;
	text-align: center;
}
.rew_don_con {
	display: table-cell;
	vertical-align: middle;
	padding-left: 10px;
}
.rew_don_inr label {
	display: block;
	padding: 10px;
	margin: 0;
	cursor: pointer;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.rew_don_inr span {
	display: block;
	font-weight: normal;
}
.rew_don_inr label:hover {
	background: #f1f1f1;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.login_frm {
	width: 500px;
	margin: 100px auto 0;
	position: relative;
	left: -112px;
}
.panel-info > .panel-heading {
	background-color: #242536;
	border-color: #242536;
	color: #fff;
	border-radius: 0;
}
.btn2 {
	padding: 5px 20px;
	display: inline-block;
	border: 1px solid #242536;
	color: #242536;
}
.btn2:hover {
	background: #242536;
	color: #fff;
}
.login_frm .input-group-addon, .login_frm .form-control {
	border-radius: 0;
}
.form-control {
	padding: 0 12px;
}
/*Added on 25-07-2017*/
.chk-email-alert li:before {
	content: '\f00c';
	font-family: "FontAwesome";
	color: limegreen;
	margin-right: 10px;
}
.chk-email-alert li {
	padding: 5px 0;
}
.input-group {
	position: relative;
}
label.error {
	font-weight: normal;
	font-style: normal;
	color: #fff;
	font-size: 11px;
	line-height:11px;
	padding:2px 5px;
	background:#f00;
	position: absolute;
	top: 100%;
	left: 0;
}
label.error:after{
	content: "\f148";
	font-family: 'FontAwesome';
	display:inline-block;
	margin-left:4px;
}
.err_msg_div{
	background: #f00 ;
    margin: 10px auto;
    padding: 5px 10px;
    text-align: center;
    width: 300px;
	color:#fff;
}
.login_frm .err_msg_div{
	margin: 10px auto 0;
}
.form-horizontal > .form-group:last-child{
	margin-bottom:0 !important;
}
.full_top_wrp .msg_div{
	position:absolute;
	right:0;
	top:20px;
	padding:0 15px;
	font-size:20px;
	line-height:22px;
	color:#3aca60;
}
.tit1_sec{
	position:relative;
}
.ms{
	position:absolute;
	top:100%;
	left:0;
	width:100%;
	background:#3aca60;
	color:#333;
	padding:6px 10px;
	font-size:12px;
	line-height:14px;
	text-align:center;
}
.div_loading{
	position:fixed;
	top:50%;
	left:50%;
	color:#fff;
	padding:5px 15px;
	border:1px solid #fff;
	border-radius:5px;
	font-size:20px;
	line-height:22px;
	-webkit-transform:translate(-50%,-50%);
	-moz-transform:translate(-50%,-50%);
	-ms-transform:translate(-50%,-50%);
	-o-transform:translate(-50%,-50%);
	transform:translate(-50%,-50%);
	z-index:11111;
}
.list-style{overflow:hidden; margin-bottom:10px; padding:10px; border:1px solid #ccc;}
.list-style a i{ color:#333; margin-right:5px;}
.rem, .fa.rem{
	position:absolute;
	right:0;
	top:0;
	heidght:24px;
	width:24px;
	line-height:22px;
	background:#fff;
	color:#333;
	text-align:center;
	cursor:pointer;
}

/* Media Query*/

@media only screen and (min-width: 992px) and (max-width:1169px) {
.top_pro_prc p {
	font-size: 22px;
	line-height: 22px;
}
.top_pro_wrp .col-xs-12.text-right {
	text-align: left;
	margin-top: 10px;
}
.sh_day_info ul li {
	width: 80px;
}
.sh_day_info ul li img {
	width: 80px;
}
.morris-area-chart_inr svg {
	height: 210px !important;
}
.filter_sec_inner ul > li {
	margin-right: 8px;
}
}
@media only screen and (min-width: 768px) and (max-width: 991px) {
.top_pro, .top_pro_prc {
	padding: 10px;
	border: 1px solid #ccc;
	margin-bottom: 10px;
}
.table > tbody > tr > td:first-child strong {
	display: block;
	padding-left: 0;
}
.table > thead > tr > th, .table > tbody > tr > td {
	padding: 10px 3px;
}
.tab_mnu > ul > li {
	margin-right: 15px;
}
.sh_day_info ul li {
	width: 50px;
	margin-right: 15px;
}
.sh_day_info ul li img {
	width: 50px;
}
.morris-area-chart_inr svg {
	height: 210px !important;
}
.sh_day_info h3 {
	font-size: 14px;
	line-height: 14px;
}
.total_info > .row > .col-sm-6 {
	margin-bottom: 15px;
}
.filter_sec_inner ul > li:nth-child(2) {
	margin-right: 20px;
}
}
@media screen and (max-width: 767px) {
.table tbody td .form-control {
	width: 20%;
	display: inline !important;
}
.table thead {
	display: none;
}
.table tbody td {
	display: block;
 padding: .6rem;
	min-width: 320px;
	text-align: left;
}
.table tbody td:before {
	content: attr(data-th);
	font-weight: bold;
	display: block;
	width: 130px;
}
.table tfoot td {
	display: block;
}
.top-nav {
	position: absolute;
	right: 45px;
	top: 0;
}
.navbar-brand img {
	width: 215px;
}
.navbar-inverse {
	background-color: #151524;
}
.btn_round.pull-right {
	float: none !important;
	margin-top: 10px;
}
.table > tbody > tr {
	border: 1px solid #ccc;
	display: block;
	margin-bottom: 10px;
}
.table > tbody > tr > td {
	border: 1px solid #f1f1f1;
	padding: 5px;
}
.donations_tbl {
	padding-top: 0;
}
.tit1_sec .left {
	float: none;
}
.tit1_sec {
	border-bottom: none;
}
.top_pro, .top_pro_prc {
	padding: 10px;
	border: 1px solid #ccc;
	margin-bottom: 10px;
}
.sh_day_info h3 {
	font-size: 16px;
	line-height: 16px;
}
.sh_day_info ul li {
	width: 50px;
	margin-right: 15px;
}
.sh_day_info ul li img {
	width: 50px;
}
.morris-area-chart_inr svg {
	height: 210px !important;
}
.tit1_sec .right {
	display: inline-block;
	float: none;
	margin-top: 10px;
}
.filter_sec_inner ul > li {
	display: inline-block;
	margin-right: 0;
	text-align: left;
	width: 100%;
	margin-bottom: 5px;
}
.filter_sec_inner li label {
	display: inline-block;
	width: 100px;
}
.filter_sec_inner .datepicker {
	display: inline-block;
	text-align: center;
	width: 150px;
}
.filter_sec_inner ul > li:nth-child(4) > label, .filter_sec_inner ul > li:nth-child(5) > label, .filter_sec_inner ul > li:nth-child(6) > label {
	display: none;
}
.total_info > .row > .col-lg-2 {
	margin-bottom: 15px;
}
.filter_sec_inner ul > li:nth-child(2) {
	margin-right: 0;
}
}
@media screen and (max-width: 375px) {
.donations_tbl {
	overflow: auto;
}
}
