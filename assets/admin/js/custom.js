// JavaScript Document

jQuery(document).ready(function($){
	
	//Date time picker
 	 $(".datepicker").datepicker({ dateFormat: 'mm-dd-yy' });
	
	//Morris Line
	/*Morris.Line({
		element: 'morris-area-chart',
		data: [
			{ y: '2006', a: 100},
			{ y: '2007', a: 75},
			{ y: '2008', a: 50},
			{ y: '2009', a: 75},
			{ y: '2010', a: 50},
			{ y: '2011', a: 75},
			{ y: '2012', a: 100}
		],
		xkey: 'y',
		ykeys: ['a'],
		labels: ['Series A']
	});*/
	$(".img_vid").click(function(){
		var tab_active = $(this).text().trim().toLowerCase();
		var view_html='';
		if(!$(this).hasClass("btn-active"))
		{
			if(tab_active == 'video'){
				view_html+='<br><input type="text" id="youtube_fil" name="youtube_fil" class="form-control" placeholder="Enter YouTube Link (i.e. aAdioIs17LM)">';
				view_html+='<input type="button" id="img_vid_btn" name="img_vid_btn" class="btn33" value="Add Video" onclick="img_vid_add()">';
				view_html+='<div class="text-center">';
				view_html+="Copy the code that appears after '=' symbol from the <br/>youtube url, for example https://www.youtube.com/watch? <br/>v=aAdioIs17LM";
				view_html+='</div>';
				$("#vid_tab").addClass("btn-active");
				$("#img_tab").removeClass("btn-active");
			}else{
				view_html+='<div class="m-gap2-10 text-center">(Accepted Formats: jpg, jpeg, png, gif)</div>';
				view_html+='<label for="feature_img" class="file-upload text-center btn34"> Choose image from your computer</label>';
				view_html+='<input id="feature_img" name="feature_img" type="file" onchange="img_vid_add()"/>';
				view_html+='<div class="text-center">(Ideal Size: at least 1280 x 720 pixels)</div>';
				$("#img_tab").addClass("btn-active");
				$("#vid_tab").removeClass("btn-active");
			}
			$("#img_video").html(view_html);	
		}
		if(tab_active == 'video'){
		$("#media_type").val('V');	
		}else{
		$("#media_type").val('I');	
		}
	});
	$(".one_bulk").click(function(){
		var tab_active = $(this).text().trim().toLowerCase();
		var view_html='';

		if(tab_active == 'bulk upload'){
			view_html+='<div class="form-group file-btn ">';
			view_html+='<div class="m-gap2-10"><b>Bulk Upload</b></div>';
			view_html+='<label for="bulk_player" class="file-upload text-center btn34"> Upload file from your computer</label>';
			view_html+='<input type="file" id="bulk_player" name="bulk_player" class="do-not-ignore"/>';
			view_html+='<div class="text-center">(Accepted Formats: csv, excel)</div>';
			view_html+='</div>';
			$("#bulk_tab").addClass("btn-active");
			$("#onetime_tab").removeClass("btn-active");
		}else{
			view_html+='<div class="form-group">';
			view_html+='<label>Email Address</label>';
			view_html+='<input type="email" id="player_email" name="player_email" class="form-control required">';
			view_html+='</div>';
			view_html+='<div class="form-group">';
			view_html+='<label>First Name</label>';
			view_html+='<input type="text" id="player_fname" name="player_fname" class="form-control required">';
			view_html+='</div>';
			view_html+='<div class="form-group">';
			view_html+='<label>Last Name</label>';
			view_html+='<input type="text" id="player_lname" name="player_lname" class="form-control required">';
			view_html+='</div>';

			view_html+='<div class="form-group file-btn ">';
			view_html+='<div class="m-gap2-10 logo-gap"><b>Upload Profile Photo</b> (optional)</div>';
			view_html+='<label for="player_image" class="file-upload text-center btn34"> Upload photo from your computer</label>';
			view_html+='<input type="file" id="player_image" name="player_image" class="do-not-ignore"/>';
			view_html+='</div>';
			$("#onetime_tab").addClass("btn-active");
			$("#bulk_tab").removeClass("btn-active");
		}
		$("#upload_type").val(tab_active);
		$("#one_bulk").html(view_html);
		
	});
	
});

// from uploadImage 
function PreviewImage(no) {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);

	oFReader.onload = function (oFREvent) {
		document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
	};
}