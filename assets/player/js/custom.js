// JavaScript Document

jQuery(document).ready(function($){
	
	// Date time picker
 	$( ".datepicker" ).datepicker();
	
});

// from uploadImage 
function PreviewImage(no) {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);

	oFReader.onload = function (oFREvent) {
		document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
	};
}