<style>
body {
	background-color: #fff;
}
.error{ color:#F00; font-size:10px !important; }
</style>
<?php
if($fund_details[0]['fund_image']==""){
$fundimg =  base_url()."assets/images/noimage-150x150.jpg";   
}else{
$fundTimg = explode(".",$fund_details[0]['fund_image']);	
$fundimg  = base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
}
$dstmt    = substr($fund_details[0]["fund_username"],0,22);
//$fund_details[0]["fund_donor_statement"];
?>
<!-- Payment option start-->
<section class="inner-content-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-6 donation-wrapper">
        <p class="heading2 text-center"> <span class="brand"> <img src="<?php echo $fundimg;?>" alt="Brand-thumb"> <span><?php echo $fund_details[0]['fund_username'];?></span> </span> Personal Information</p>
        <div class="d-form-wrapper"> 
          <!-- Personal Information form -->
          <form id="personal-info" class="form-horizontal d-form personal" method="post" action="">
          <input type="hidden" name="hidfslug" value="<?php echo $fund_details[0]['fund_slug_name'];?>" />
          <input type="hidden" name="hidpslug" value="<?php echo $player_details[0]['player_slug_name'];?>" />
          <input type="hidden" name="donor_statement" value="<?php echo $dstmt;?>"/>
            <div class="form-content">
              <div class="form-group">
                <div class="col-xs-12 col-sm-6">
                  <label for="firstname">First Name <sup>*</sup></label>
                 <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" required/>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <label for="lastname">Last Name <sup>*</sup></label>
                  <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" required/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-12 col-sm-8">
                  <label for="email">Email Address <sup>*</sup></label>
                  <input type="email" class="form-control" id="donor_email" name="donor_email" placeholder="Email Address" required/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-12 col-sm-8">
                  <label for="mobile">Mobile phone number <sup>*</sup> <sub>(where you install your app)</sub></label>
                  <input type="text" class="form-control" id="donor_phone" name="donor_phone" placeholder="Phone Number" required/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-12">
                  <label for="add1">Street address 1 <sup>*</sup></label>
                  <input type="text" class="form-control" id="donor_addr" name="donor_addr" placeholder="Address" required/>
                </div>
              </div>
              <!--<div class="form-group">
                <div class="col-xs-12">
                  <label for="add2">Street address 2 <sup>*</sup></label>
                  <input id="add2" type="text" name="" value="" class="form-control">
                </div>
              </div>-->
              <div class="form-group">
                <div class="col-xs-12 col-sm-5">
                  <label for="city">City <sup>*</sup></label>
                   <input type="text" class="form-control" id="donor_city" name="donor_city" placeholder="City" required/>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <label for="state">State <sup>*</sup></label>
                  <input type="text" class="form-control" id="donor_state" name="donor_state" placeholder="State" required/>
                  <!--<select id="state" name="" value="" class="form-control">
                    <option>State 1</option>
                    <option>State 2</option>
                    <option>State 3</option>
                    <option>State 4</option>
                  </select>-->
                </div>
                <div class="col-xs-12 col-sm-3">
                  <label for="zip">Zip code <sup>*</sup></label>
                  <input type="text" class="form-control" id="donor_zip" name="donor_zip" placeholder="Zip" required/>
                </div>
              </div>
            </div>
            <div class="form-group text-center">
              <button type="sumbit" name="" value="" class="btn btn-default btn-donate">Next</button>
            </div>
          </form>
          <!-- Personal Information form ends --> 
          <!-- Payment Information form -->
          <form id="payment-info" class="form-horizontal d-form payment hide" method="post" action="">
          <input type="hidden" name="stripeToken" id="stripeToken" value="" />
            <div class="form-content">
              <div class="form-group">
                <div class="col-xs-12 s-amount">
                  <label for="d-amount" class="full-width">Select donation amount ($)</label>
                  <button class="btn amount" type="button" name="" value="20">$20</button>
                  <button class="btn amount" type="button" name="" value="50">$50</button>
                  <button class="btn amount selected" type="button" name="" value="100">$100</button>
                  <button class="btn amount" type="button" name="" value="250">$250</button>
                  <input type="text" name="donate_amt" id="donate_amt" value="" placeholder="Other" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-12">
                  <label for="p-method" class="full-width">Payment method</label>
                  <img src="<?php echo base_url()?>assets/images/cards.png" alt="Card logo"> </div>
              </div>
              <div class="form-group">
                <div class="col-xs-12">
                  <label>Enter credit card information</label>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-12 col-sm-8">
                  <label for="card">Card number <sup>*</sup></label>
                  <input type="text" class="form-control" id="card_number" name="card_number" placeholder="4584XXXXXXXX7804" required/>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <label for="cvv">CVV number <sup>*</sup></label>
                  <input type="text" class="form-control" id="card_cvv" name="card_cvv" placeholder="CVV" required/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-12">
                  <label for="exp-date">Expiration date <sup>*</sup></label>
                </div>
                <div class="col-xs-12 col-sm-5">
                  <?php $months = array("01","02","03","04","05","06","07","08","09","10","11","12"); ?>
                    <select id="card_expiry_month" name="card_expiry_month" class="form-control" required>
                        <option value="">Month</option>
                        <?php
                        $cmon = date("m");
                        foreach($months as $mon){
                        echo '<option value="'.$mon.'">'.$mon.'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <?php
				$cyear = date("Y");
				?>
				<select id="card_expiry_year" name="card_expiry_year" class="form-control" onchange="month()" required>
					<option value="">Year</option>
					<?php
					$cyear = date("Y");
					for($i=0;$i<=20;$i++){
					echo '<option value="'.($cyear+$i).'">'.($cyear+$i).'</option>';
					}
					?>
				</select>
                </div>
              </div>
            </div>
            <span id="payment-errors" class="" style="display:none;"></span>
            <div class="form-group text-center">
              <a href="javascript:void(0)" class="btn btn-default btn-donate prev">Previous</a>
              <button type="submit" class="btn btn-default green btn-donate">Donate</button>
            </div>
          </form>
          <!-- Payment Information form ends --> 
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-6 d-thumbnail">
        <table class="table">
          <tr>
            <td><img src="<?php echo base_url()?>assets/images/all_merchants.png" alt="Thumbnail1"></td>
            <td><img src="<?php echo base_url()?>assets/images/pizza_guys.png" alt="Thumbnail2"></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</section>
<!-- Payment option ends--> 

<form action="<?php echo base_url('donate/payment-info-success')?>" method="post" id="ActSfm"></form>

<form action="<?php echo base_url('donate/payment-info-failure')?>" method="post" id="ActFfm">
<input type="hidden" id="error_con" name="error_con" value="" />
</form>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 
<script type="text/javascript">
//set your publishable key
Stripe.setPublishableKey('pk_live_kFNkgP2dJPW917rPIvjABiMX');
//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        //enable the submit button
        $('.btn-donate').removeAttr("disabled");
        //display the errors on the form
        //$('#payment-errors').attr('hidden', 'false');
        $('#payment-errors').addClass('alert alert-danger');
        $("#payment-errors").html(response.error.message);
        $('#payment-errors').show();
    } else {
        //var form$ = $("#detailsform");
        //get token id
        var token = response['id'];
		$("#stripeToken").val(token);
        //insert the token into the form
        //form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        //submit form to the server
        //form$.get(0).submit();
		
		$.ajax({
			url:		"<?php echo base_url();?>donatePaymentAjax",
			type:		"POST",
			data: 	 	$("#payment-info, #personal-info").serializeArray(),
			dataType:	"json",
			beforeSend: function(){
			},
			success:function(results){
			if(results.status==1){			
			$( "#ActSfm" ).submit();	
			}else{
			$("#error_con").val(results.error_con);	
			$( "#ActFfm" ).submit();	
			}	
		  }
		});
		
    }
}
$(document).ready(function () {	
    $dval = $(".s-amount > button.selected").val();	
    $('#donate_amt').val($dval);
    $(".s-amount *").not("label").click(function() {
        $(".s-amount *").removeClass("selected");		
		$('#donate_amt').val($(this).val());
        $(this).addClass("selected");
    });
	$('.d-form-wrapper form').each(function () {
        $(this).validate({
            submitHandler: function (form) {				
			$('#payment-info').removeClass("hide");
			$('#personal-info').addClass("hide");
			  if($(form).attr('id') == 'payment-info'){
				/*$.ajax({
						url:		"<?php //echo base_url();?>donatePaymentAjax",
						type:		"POST",
						data: 	 	$("#payment-info, #personal-info").serializeArray(),
						dataType:	"json",
						beforeSend: function(){
						},
						success:function(results){
						if(results.msg==1){
						$(".ms").html("Successfully added!!");
						setTimeout(function() {
							$(".ms").html("");
							//$("#couponAddModal").modal("hide");
							//window.location.reload(true);
							//location.reload();
							//window.location.href='<?php //echo base_url();?>merchantcenter/location';
							$( "#hid_owner_frm2" ).submit();
							}, 3000);
						}
						else{
							$(".ms").html("Addition is failured");	
						}	
					  }
					});*/
					
					$('.btn-donate').attr("disabled", "disabled");
					//create single-use token to charge the user
						Stripe.createToken({
						number: $('#card_number').val(),
						cvc: $('#card_cvv').val(),
						exp_month: $('#card_expiry_month').val(),
						exp_year: $('#card_expiry_year').val()
					}, stripeResponseHandler);
					//submit from callback
					return false;				
			    }
			}
        });	
	});	
	
	$(".text-center a").click(function() {
	$('#payment-info').addClass("hide");
	$('#personal-info').removeClass("hide");
	});
});


/*function demostripeResponseHandler(){
	$.ajax({
		url:		"<?php //echo base_url();?>donate/payment-info",
		type:		"POST",
		data: 	 	$("#payment-info, #personal-info").serializeArray(),
		dataType:	"json",
		beforeSend: function(){
		},
		success:function(results){
			if(results.status==1){			
			$( "#ActSfm" ).submit();	
			}else{
			$("#error_con").val(results.error_con);	
			$( "#ActFfm" ).submit();	
			}
	   }
	});
}*/




</script> 
<script type="text/javascript">
function setDonation(strr)
{
    $("#donate_amt").val(strr);
}
function setDonation1(strr)
{
    $("#donate_amt1").val(strr);
}
function month()
{
$("#card_expiry_month").val('');
var cmonth = <?php echo $cmon.';' ?>
var today = new Date();
var year = today.getFullYear();
var chyear = $("#card_expiry_year").val();
if(chyear > year)
{
$("#card_expiry_month option").each(function()
{
$(this).removeAttr("disabled");
});
}
else
{
$("#card_expiry_month option").each(function()
{
if($(this).val() > cmonth)
{
$(this).removeAttr("disabled");
}
else
{
$(this).attr("disabled","disabled");
}

});
}
}
</script> 
