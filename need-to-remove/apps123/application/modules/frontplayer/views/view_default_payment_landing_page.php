<style>
body {
	background-color: #fff;
}

</style>
<!-- Payment option start-->
<section class="inner-content-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-6 donation-wrapper">
        <p class="heading2 text-center">Who would you like to support?</p>
        <div id="d-slider" class="carousel slide" data-ride="carousel"> 
          <!-- Indicators -->
          <ol class="carousel-indicators">          
            <?php for($i=0;$i<$counter;$i++){?>
             <li data-target="#d-slider" data-slide-to="<?php echo $i;?>"></li>            
           <?php } ?> 
          </ol>
          
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
          
           <?php  foreach($fund_details as $fval){  ?>
           <?php	  
		    if($fval['fund_image']==""){
			$fundimg =  base_url()."assets/images/noimage-150x150.jpg";   
			}else{
			$fundTimg = explode(".",$fval['fund_image']);	
			$fundimg  = base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
			}
            
		   ?>
            <div class="item text-center">
              <div class="brand"> <img src="<?php echo $fundimg;?>" alt="Brand-thumb"> <span><?php echo $fval['fund_username'];?></span> </div>
              <div><?php echo html_entity_decode($fval['fund_des']);?></div>
              <!--<button type="button" name="" value="" class="btn btn-default btn-donate" onclick="location.href='https://thanksforsupporting.com/apps/donate/personal-info';">Donate</button>-->
              <button type="button" name="" value="" class="btn btn-default btn-donate" 
              onclick="location.href='<?php echo base_url('donate/personal-info/'.$fval['fund_slug_name'])?>'">Donate</button>
            </div>
            <?php } ?>
            
            
            <!-- Left and right controls --> 
            <a class="left carousel-control" href="#d-slider" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#d-slider" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> </div>
        </div>
        <?php if($this->session->flashdata("error_msg")!=""){?>
          <span id="process-errors" class="alert alert-danger text-center alert-dismissible">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $this->session->flashdata("error_msg");?>
          </span>
        <?php } ?>  
      </div>
      
      <div class="col-xs-12 col-sm-4 col-md-6 d-thumbnail">
        <table class="table">
          <tr>
            <td><img src="<?php echo base_url()?>assets/images/all_merchants.png" alt="Thumbnail1"></td>
            <td><img src="<?php echo base_url()?>assets/images/pizza_guys.png" alt="Thumbnail2"></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</section>
<!-- Payment option ends-->


<script type="text/javascript">
$(document).ready(function () {
    $('.carousel-inner .item').first().addClass('active');
	$('.carousel-indicators > li').first().addClass('active');
});
</script> 
