<!-- Green Bar start-->
<?php
if($player_details[0]['player_image']=="")
{
	$playerimg =  base_url("assets/images/noimage-150x150.jpg");
	$playerimg1 =  base_url("assets/images/noimage-150x150.jpg");
}
else
{
	$playerTimg = explode(".",$player_details[0]['player_image']);
	$playerimg  =  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);
	$playerimg1  =  base_url("assets/player_image/".$player_details[0]['player_image']);
}

if($fund_details[0]['fund_image']=="")
{
	$fundimg =  base_url("assets/images/noimage-150x150.jpg");
}
else
{
	$fundTimg = explode(".",$fund_details[0]['fund_image']);
	$fundimg  =  base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);
}

$player_name 	 = $player_details[0]['player_fname'].' '.$player_details[0]['player_lname'];
$fund_enddate	 = $fund_details[0]["fund_end_dt"];
$today_date		 = date("Y-m-d");
$fundraiser_name = $fund_details[0]['fund_username'];
$fundraiser_slog = $fund_details[0]['fund_slogan'];
$ended_days_in 	 = (strtotime($fund_enddate)-strtotime($today_date));
$description	=	html_entity_decode($fund_details[0]['fund_des']);

if($ended_days_in < 0)
{
	echo '<script>window.location.href="'.base_url($fslug.'/'.$pslug).'"</script>';
}

$share_url = base_url($fslug.'/'.$pslug);

$function_call = "onclick=".'"'."javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=600'); return false;".'"';

$fb_share = '<a href="https://www.facebook.com/sharer/sharer.php?u='.$share_url.'&picture='.$playerimg1.'&title='.$player_name.'&description='.strip_tags($description).'" '.$function_call.' data-hasqtip="0" oldtitle="Share on Facebook" aria-describedby="qtip-0" class="pro_wsh" data-toggle="tooltip" data-placement="left" title="Facebook">Facebook</a>';
?>
<style type="text/css">
.subject-wrapper .item-title {
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}
.btn-green, .btn-blue {
	border: 1px solid <?php echo $fund_details[0]['fund_color_rgb'];?>;
	background-color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}
.btn-green:hover {
	color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
}
.btn-green-border {
	border: 1px solid <?php echo $fund_details[0]['fund_color_rgb'];?>;
	color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
}
.btn-green-border:hover {
	background-color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}

</style>
<script src="<?php echo base_url('assets/js/jquery.validate.js');?>"></script>
<script src="<?php echo base_url('assets/js/additional-methods.js');?>"></script>
<section class="subject-wrapper" style="<?php echo 'background-color:'.$fund_details[0]['fund_color_rgb'] ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-3 col-md-2">
	  <img src="<?php echo $fundimg;?>" alt="Subject Image" class="img-circle"/>
	  </div>
      <div class="col-xs-9  col-md-10 subject-title">
	  <span class="item-title"><?php echo $fundraiser_name; ?></span>
	  <p class="item-info"><?php echo $fundraiser_slog; ?></p>
      </div>
    </div>
  </div>
</section>
<!-- Green Bar ends-->

<!-- Inner Content start-->
<section class="bg-white inner-content-wrapper">
  <div class="container">
	  <div class="row">
		<div class="col-xs-12 text-center page-title add-img">
		<img src="<?php echo $playerimg;?>" class="img-circle" alt="<?php echo $player_name;?>"/>
		<span class="mem-name"><?php echo $player_name;?></span>
		</div>
		<div class="progress-meter">
			<span class="meter-block first" data-title="Donate">1</span>
			<span class="meter-block middle" data-title="Thank you">2</span>
			<span class="meter-block last active" data-title="Share">3</span>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-lg-8 middle-wrapper">
			<div class="msg-thankyou text-center">Complete your support by sharing this project</div>
			<div class="msg-email-confirm text-center">Share via Email, Facebook & Messages</div>
			<?php
			if($this->session->flashdata('msg'))
			{
				echo'<div class="alert alert-success alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> '.$this->session->flashdata('msg').'
					</div>';
				$this->session->unset_userdata('msg');
			}

			$msg = "I am helping raise money, which is essential for its success. I am hoping you can help them reach their goal by:

			1. Making a donation, and/or
			2. Spreading the word to your contacts using the share feature on the fundraising page.
			3. Visit/Share URL ".$player_url."
			Your donation will help with ".$fundraiser_name.".
			Special Thank You Gift

			As a special thank you for donating to their cause, you will receive a free one-year subscription to the GeoCouponAlerts coupon app (a $20 value that can save you $100's currently available in the greater Sacramento and Salinas areas).
			Thanks in advance,
			ThanksForSupporting";

			$title = urlencode($subject);
			$url = urlencode($player_url);
			?>


			<div class="text-center social-wrapper">
			<!--<div class="sharethis-inline-share-buttons"></div>-->
			<button type="button" data-network="email" data-description="<?php echo $msg; ?>" data-message="<?php echo $msg; ?>" class="st-custom-button btn btn-primary btn-email">Email</button>
			<!-- <button type="button" data-network="sms" class="st-custom-button btn btn-primary btn-msg">SMS</button> -->
			<!-- <button type="button" data-network="facebook" data-title="<?php echo $title; ?>" class="st-custom-button btn btn-primary btn-fb">Facebook</button> -->
            <!-- <a href="" class="st-custom-button btn btn-primary btn-fb">Facebook</a> -->
            <?php
            $fb_share_update = '<a href="https://www.facebook.com/sharer/sharer.php?u='.$share_url.'&picture='.$playerimg1.'&quote=Please help by donating or spreading the word." class="btn btn-primary btn-fb" title="Facebook">Facebook</a>';
            echo $fb_share_update; ?>
			</div>
			<div class="text-center button-wrapper">
			<span>Thank you for your support</span><br>
			<a href="<?php echo base_url($fslug.'/'.$pslug); ?>" style="text-decoration: underline;" class="btn-back">Back</a>
			</div>
		  </div>
	  </div>
  </div>
</section>
<!-- Inner Content ends-->
<!-- Scripts -->
<script type="text/javascript">
$(document).ready(function ($) {
	$("#share_email_form").validate({
		rules:{
			your_name:{
				required:true
			},
			your_email:{
				required:true
			},
			share_emails:{
				required:true
			}
		}
	});
	$("#share_sms_form").validate({
		rules:{
			your_sname:{
				required:true
			},
			your_semail:{
				required:true
			},
			share_numbers:{
				required:true
			}
		}
	});
});
</script>
<script>
jQuery(document).click(function (e) {
	// For Email Share
	if (!jQuery(e.target).hasClass('btn-email') && jQuery(e.target).parents('#email-share').length === 0)
	{
		jQuery('#email-share').removeClass('in');
	}

	// For FB Share
	/*if (!jQuery(e.target).hasClass('btn-fb') && jQuery(e.target).parents('#fb-share').length === 0)
	{
		jQuery('#fb-share').removeClass('in');
	}*/

	// For SMS Share
	if (!jQuery(e.target).hasClass('btn-sms') && jQuery(e.target).parents('#sms-share').length === 0)
	{
		jQuery('#sms-share').removeClass('in');
	}
});
</script>
