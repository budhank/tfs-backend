<!-- Green Bar start-->
<?php
if($player_details[0]['player_image']==""){
$playerimg =  base_url()."assets/images/noimage-150x150.jpg";
}else{
$playerTimg = explode(".",$player_details[0]['player_image']);
$playerimg  =  base_url()."assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1];
}

if($fund_details[0]['fund_image']==""){
$fundimg =  base_url()."assets/images/noimage-150x150.jpg";
}else{
$fundTimg = explode(".",$fund_details[0]['fund_image']);
$fundimg  =  base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];
}
$player_fname		=	$player_details[0]['player_fname'];
$player_lname		=	$player_details[0]['player_lname'];
$player_name		=	$player_fname.' '.$player_lname;
$fund_enddate		=	$fund_details[0]["fund_end_dt"];
$today_date			=	date("Y-m-d");
//$goal				=	$player_details[0]["player_goal"];
$goal				=	$fund_details[0]["fund_individual_goal"];
$raised				=	$total_payments[0]["raised_amt"];
$remaining			=	$goal - $raised;
$total_donors 		=	$total_donors[0]["total_donor"];
$description		=	html_entity_decode($fund_details[0]['fund_des']);
$fundraiser_name	=	$fund_details[0]['fund_username'];
$fundraiser_slogan	=	$fund_details[0]['fund_slogan'];

if($goal > 0){
$average_percent	=	ceil((($raised/$goal)*100));
}else{
$average_percent	=	0;
}
$ended_days_in 		=	ceil(strtotime($fund_enddate)-strtotime($today_date));
if($ended_days_in < 0)
{
$campaign_msg		=	"Campaign has ended";
$campaign_msg1		=	"<span>Campaign Over</span>";
}
elseif($ended_days_in == 0)
{
$campaign_msg		=	"Campaign ends today";
$campaign_msg1		=	"<span>Todays</span> Left ";
}
elseif($ended_days_in == 1)
{
$campaign_msg		=	"Campaign ends tomorrow";
$campaign_msg1		=	"<span>Tomorrows</span> Left ";
}
else
{
	//echo ((strtotime($fund_enddate)-strtotime($today_date))/86400);
$campaign_days_left =	ceil((strtotime($fund_enddate)-strtotime($today_date))/86400);
$campaign_msg		=	"Campaign ends in ".$campaign_days_left." days";
$campaign_msg1		=	"<span>".$campaign_days_left." Days</span> Left ";
}

if($this->uri->segment(4))
{
$payment_url = base_url().$fslug.'/'.$pslug.'/payment/support/'.$this->uri->segment(4);
}
else
{
$payment_url = base_url().$fslug.'/'.$pslug.'/payment';
}
?>
<style type="text/css">
.subject-wrapper .item-title {
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}
.btn-green, .btn-blue {
	border: 1px solid <?php echo $fund_details[0]['fund_color_rgb'];?>;
	background-color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}
.btn-green:hover {
	color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
}
.btn-green-border {
	border: 1px solid <?php echo $fund_details[0]['fund_color_rgb'];?>;
	color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
}
.btn-green-border:hover {
	background-color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}
</style>
<section class="subject-wrapper" style="<?php echo 'background-color:'.$fund_details[0]['fund_color_rgb'] ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-3 col-md-3">
	  <img src="<?php echo $fundimg;?>" alt="Subject Image" class="img-circle"/>
	  </div>
      <div class="col-xs-9  col-md-9 subject-title">
	  <span class="item-title"><?php echo $fundraiser_name; ?></span>
	  <p class="item-info"><?php echo $fundraiser_slogan; ?></p>
      </div>
    </div>
  </div>
</section>
<!-- Green Bar ends-->

<!-- Home Content start-->
<!-- Row 1 starts -->
<section class="content-wrapper content1">
  <div class="container">
    <div class="row bg-white">
      <div class="col-xs-12 col-sm-4 text-center team-mem-info">
		  <div class="img-circle ply-thmb" style="background-image:url('<?php echo $playerimg;?>');"></div>
		  <span class="mem-name"><?php echo ucwords($player_name);?></span>
	      <p class="mem-speech">Thank you for helping me out, I appreciate your kindness and support.</p>
      </div>
      <div class="col-xs-12 col-sm-8 team-detail">
        <div class="row">
          <div class="col-xs-4 detail-info first text-center">
            <p class="bold">$<?php echo number_format($goal);?></p>
            <p class="bold-sub">Goal</p>
          </div>
          <div class="col-xs-4 detail-info middle text-center">
            <p class="bold">$<?php echo number_format($raised);?></p>
            <p class="bold-sub">Raised</p>
          </div>
          <div class="col-xs-4 detail-info last text-center">
            <p class="bold" style="color:#d0021d">
			<?php
			if($remaining < 0)
			{
			echo "$0";
			}
			else
			{
			echo "$".number_format($remaining);
			}
			?>
			</p>
            <p class="bold-sub">Remaining</p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 fund-ratio">
            <div class="progress"> <span class="progress-rate"><?php echo $average_percent; ?>% Funded</span>
              <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $average_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $average_percent; ?>%;"> </div>
            </div>
            <span><?php echo $campaign_msg; ?></span> </div>
        </div>
        <div class="row">
			<div class="col-xs-12 text-center mem-specific-content">
			<?php
			if($ended_days_in < 0){
			}
			else
			{
			?>
			<a href="<?php echo $payment_url;?>" class="btn btn-green">Support <?php echo ucfirst($player_fname);?></a>
            <a class="btn btn-default btn-green-border" href="<?php echo base_url().$fslug.'/'.$pslug.'/share'; ?>">Share</a>
			<?php
			}
			?>
			</div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Row 1 ends -->
<!-- Row 2 starts -->
<section class="content-wrapper content2">
  <div class="container reward-content-wrapper">
    <div class="row">
      <div class="col-xs-12 col-sm-8">
         <div class="row">
          <div class="bg-white reward-wrapper">
      <span class="ribon"><img src="<?php echo base_url();?>assets/images/reward-ribbon.png" alt="Reward Ribon"/></span>
      <img src="<?php echo base_url('assets/images/Geo_Phones_mob-2.png');?>" alt="Reward Thumb" class="reward-thumbnail pull-left"/>
        <div class="reward-detail" align="center">
         <!-- <p>Get a Coupon App for free with every donation</p>
          <span>Save $1000's when you donate</span> -->
			<span style="font-size:22px;">Get the Premium Edition of the</span>
			<p style="font-size: 30px; margin:0;">GeoCoupons App</p>
            <span style="font-size:22px;">with every donation!</span>
            <p style="font-size: 18px; margin:0; font-weight:normal">(a $20 value)</p>

          </div>
          <div class="clearfix"></div>
      </div>
     <div class="bg-white reward-wrapper">
     <?php /*?>  <span class="ribon"><img src="<?php echo base_url();?>assets/images/reward-ribbon.png" alt="Reward Ribon"/></span>
      <img src="<?php echo base_url('assets/images/Geo_Phones_mob-1.png');?>" alt="Reward Thumb" class="reward-thumbnail pull-left"/>
        <div class="reward-detail">
         <!-- <p>Get a Coupon App for free with every donation</p>
          <span>Save $1000's when you donate</span> -->
			<p style="font-size: 24px;">Your Gift Has Been Emailed to You </p>
			<span>Check your email for your GeoCouponAlerts activation code</span>
			<p style="font-size: 30px; margin:30px 0;">Start Saving with Your Coupon App in 3 Easy Steps</p>
			<ul>
			<li>Download the GeoCouponAlerts app</li>
			<li>Enter your activation code (in your email)</li>
			<li>Use coupons & save money</li>
			</ul>
          </div>
		  <?php */?>
          <div class="clearfix"></div>
        <div class="bg-white reward-wrapper" style="border-top: 20px solid #e1e1e1; margin-top: 30px;">
        <!-- Slider start -->
		<?php
		if(count($fund_sliders) > 0)
		{
		?>
        <div id="slider-preview" class="slide slide-wrapper" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
			<?php
			$i = 1;
			foreach($fund_sliders as $fund_slide)
			{
			$clas="";
			if($i==1)
			{
			$clas=" active";
			}
			if($fund_slide["media_type"]=="V")
			{
			?>
            <div class="item<?php echo $clas; ?>">
			<img src="https://img.youtube.com/vi/<?php echo $fund_slide['fund_feature_image'];?>/maxresdefault.jpg" />
			<img src="<?php echo base_url(); ?>assets/images/play.png" alt="play" class="btn-play slivideo" rel="<?php echo $fund_slide['fund_feature_image'];?>">
			</div>
			<?php
			}
			else
			{
			$proimg  = base_url()."assets/fundraiser_feature_image/".$fund_slide['fund_feature_image'];
			?>
			<div class="item<?php echo $clas; ?>">
			<img src="<?php echo $proimg;?>"/>
			</div>
			<?php
			}
			$i++;
			}
			?>
          </div>
          <!-- Indicators -->
		  <?php if(count($fund_sliders) > 1):?>
          <ul class="carousel-indicators thumbnail-wrapper">
			<?php
			$j = 1;
			foreach($fund_sliders as $fund_slide)
			{
			$clas="";
			if($j==1)
			{
			$clas='class="active"';
			}
			if($fund_slide["media_type"]=="V")
			{
			?>
			<li data-target="#slider-preview" data-slide-to="<?php echo $j; ?>" <?php echo $clas; ?>>
			<img src="https://img.youtube.com/vi/<?php echo $fund_slide['fund_feature_image'];?>/mqdefault.jpg"/>
			</li>
			<?php
			}
			else
			{
			$filename = pathinfo($fund_slide['fund_feature_image'],PATHINFO_FILENAME);
			$extension = pathinfo($fund_slide['fund_feature_image'],PATHINFO_EXTENSION);
			$proimg  = base_url()."assets/fundraiser_feature_image/thumb/".$filename.'_thumb.'.$extension;
			?>
			<li data-target="#slider-preview" data-slide-to="<?php echo $j; ?>" <?php echo $clas; ?>>
			<img src="<?php echo $proimg;?>" />
			</li>
			<?php
			}
			?>

			<?php
			$j++;
			}
			?>
          </ul>
		  <?php endif;?>
        </div>
		<?php
		}
		?>
        <!-- Slider ends -->
       <div style="padding:20px;"> <?php echo $description; ?></div>
      </div>

      </div>
          </div> 
      </div>
      <div class="col-xs-12 col-sm-4">
          <div class="count-wrapper">
        <div class="bg-white">
          <div class="col-xs-6 supporter-count"> <span><?php echo $total_donors; ?></span> Supporters </div>
          <div class="col-xs-6 days-count">
		  <?php echo $campaign_msg1; ?> </div>
          <div class="clearfix"></div>
        </div>
        <div class="right-container">
		<?php
		if(count($messages)>0)
		{
		?>
        <div class="right-content-wrapper bg-white">
          <h3 class="text-center">Messages</h3>
          <div class="msg-content-wrapper">
			<?php
			foreach($messages as $msg)
			{
				$time_diff = (time()-strtotime($msg['post_date']));
				if($time_diff > 86400)
				{
					$date=date("d M, Y",strtotime($msg['post_date']));
				}
				else
				{
					$date=date("g",strtotime($msg['post_date'])).' hrs ago';
				}
			?>
			<div class="msg-item">
			<?php
			if($msg['fb_id']!='')
			{
			$profile_url="http://graph.facebook.com/".$msg['fb_id']."/picture?width=100&height=100";
			}
			else
			{
			$profile_url=base_url("assets/images/".$msg['profile_img']);
			}
			?>
			  <img src="<?php echo $profile_url; ?>" class="img-circle" />
              <div class="msg-sender">
			  <span><?php echo ucwords($msg['donor_name']);?></span> <?php echo $date;?>
			  </div>
              <p><?php if(strlen($msg['message'])>200){ echo substr($msg['message'],0,200).'..'; }else{ echo $msg['message']; }?></p>
            </div>
			<?php
			}
			?>
          </div>
        </div>
		<?php
		}
		?>
		<?php
		if($ended_days_in < 0){
		}
		else
		{
		?>
        <div class="right-content-wrapper text-center bg-white" style="margin-top:20px;">
          <h3 class="text-center">Support <?php echo ucfirst($player_fname);?></h3>
          <span>choose amount</span>
			<?php /*please maintain label for and input id same*/ ?>
			<div class="switch-account">
			<input type="hidden" name="donate_amount1" id="donate_amount1" value="20"/>
			<label for="donate_amount1" class="btn btn-green-border amtt">$20</label>
			<input type="hidden" name="donate_amount2" id="donate_amount2" value="35"/>
			<label for="donate_amount2" class="btn btn-green-border amtt">$35</label>
			<input type="hidden" name="donate_amount3" id="donate_amount3" value="50"/>
			<label for="donate_amount3" class="btn btn-green-border amtt">$50</label>
			<input type="hidden" name="donate_amount4" id="donate_amount4" value="75"/>
			<label for="donate_amount4" class="btn btn-green-border amtt">$75</label>
			<input type="hidden" name="donate_amount5" id="donate_amount5" value="100"/>
			<label for="donate_amount5" class="btn btn-green-border amtt">$100</label>
			<input type="hidden" name="donate_amount6" id="donate_amount6" value=""/>
			<label for="donate_amount6" class="btn btn-green-border amtt">Other</label>
			</div>
			<!--<button type="submit" name="donateSubmit" class="btn btn-green">Support <?php //echo ucfirst($player_fname);?></button>-->
		</div>
		<?php
		}
		?>
      </div>
      </div>
      </div>
    </div>
  </div>
</section>
<!-- Row 2 ends -->

<!-- Row 3 starts -->

<div id="myModal" class="modal fade video-play-wrapper">
	<div class="modal-dialog">
		<div class="modal-content">
			<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
			<div class="modal-body">
				<iframe id="sliderVideo" width="570" height="330" src="" frameborder="0"></iframe>
			</div>
		</div>
	</div>
</div>
<form action="<?php echo $payment_url;?>" method="post" id="supportamt" name="supportamt">
<input type="hidden" name="donate_amount" id="donate_amount" value="">
</form>
<script type="text/javascript">
$(document).ready(function ($) {
	$(".slivideo").click(function(){
		var video_id = $(this).attr('rel');
		var url = "https://www.youtube.com/embed/"+video_id+"?fs=0&rel=0&autoplay=1";
		$("#sliderVideo").attr('src',url);
		$("#myModal").modal('show');
	});
	$(".amtt").click(function(){
		var fr = $(this).attr('for');
		var amt = $('#'+fr).val();
		$('#donate_amount').val(amt);
		$('#supportamt').submit();
	});
    document.title = "<?php echo $fundraiser_name; ?>";
    $('meta[name="title"]').attr("content", "<?php echo $fundraiser_name; ?>");
    //<meta property="og:title" content="testing">
    $('meta[property="og:title"]').remove();
    $('head').append( '<meta property="og:title" content="<?php echo $fundraiser_name; ?>">' );
    $('meta[property="og:url"]').remove();
    $('head').append( '<meta property="og:url" content="'+document.URL+'">' );
});
</script>
<!-- Row 3 ends -->
<!-- Home Content ends-->
