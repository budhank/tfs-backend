<!-- Green Bar start-->
<?php
if($player_details[0]['player_image']=="")
{
	$playerimg =  base_url("assets/images/noimage-150x150.jpg");   
}
else
{
	$playerTimg = explode(".",$player_details[0]['player_image']);	
	$playerimg  =  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
}

if($fund_details[0]['fund_image']=="")
{
	$fundimg =  base_url("assets/images/noimage-150x150.jpg");   
}
else
{
	$fundTimg = explode(".",$fund_details[0]['fund_image']);	
	$fundimg  =  base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
}

$player_name 	= $player_details[0]['player_fname'].' '.$player_details[0]['player_lname'];
$fund_enddate	=	$fund_details[0]["fund_end_dt"];
$donor_statement=	($fund_details[0]["fund_donor_statement"] ? $fund_details[0]["fund_donor_statement"] : "Fund Statement");
$today_date		=	date("Y-m-d");
//$player_goal	= $player_details[0]['player_goal'];
$player_goal	= $fund_details[0]['fund_individual_goal'];
$fundraiser_name= $fund_details[0]['fund_username'];
$fundraiser_slog= $fund_details[0]['fund_slogan'];
$player_raised	= $player_raised[0]["raised_amt"];
$player_remain	= $player_goal - $player_raised;
$total_donor	= $player_donors[0]["total_donor"];
if($total_donor	== 0)
{
	$total_donor= 1;
}
$player_average	= ceil($player_raised/$total_donor);
$ended_days_in 	= (strtotime($fund_enddate)-strtotime($today_date));

if($ended_days_in < 0)
{
	echo '<script>window.location.href="'.base_url($fslug.'/'.$pslug).'"</script>';
}

if($this->uri->segment(5))
{
$form_url = base_url($fslug.'/'.$pslug.'/donate/support/'.$this->uri->segment(5));
$form_url1 = base_url($fslug.'/'.$pslug.'/donate-paypal/support/'.$this->uri->segment(5));
}
else
{
$form_url = base_url($fslug.'/'.$pslug.'/donate');
$form_url1 = base_url($fslug.'/'.$pslug.'/donate-paypal');
}
?>
<style type="text/css">
.subject-wrapper .item-title {
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}
.btn-green, .btn-blue {
	border: 1px solid <?php echo $fund_details[0]['fund_color_rgb'];?>;
	background-color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
	color: <?php echo $fund_details[0]['fund_font_color'];?>;
}
.btn-green:hover {
	color: <?php echo $fund_details[0]['fund_color_rgb'];?>;
}
</style>
<section class="subject-wrapper" style="<?php echo 'background-color:'.$fund_details[0]['fund_color_rgb'] ?>">
  <div class="container">
    <div class="row">
      <div class="col-xs-3 col-md-3"><img src="<?php echo $fundimg;?>" alt="Subject Image" class="img-circle"/></div>
      <div class="col-xs-9  col-md-9 subject-title"> 
	  <span class="item-title"><?php echo $fundraiser_name; ?></span>
	  <p class="item-info"><?php echo $fundraiser_slog; ?></p>
      </div>
    </div>
  </div>
</section>
<!-- Green Bar ends--> 

<!-- Home Content start--> 
<section class="bg-white inner-content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center page-title add-img">
			<img src="<?php echo $playerimg;?>" class="img-circle" alt="Team member"/> 
			<span class="mem-name"><?php echo $player_name;?></span>
			</div>
			<div class="progress-meter">
			<span class="meter-block first active" data-title="Donate">1</span>
			<span class="meter-block middle" data-title="Thank you">2</span>
			<span class="meter-block last" data-title="Share">3</span>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-12 col-sm-8 col-md-6 tab-form-wrapper">
                <p class="bold">Payment type</p>
                <p>Please select your payment type below</p>
                <ul class="nav nav-tabs tab-payment">
                    <li class="active">
<a class="p1" data-toggle="tab" href="#p-m-1"><img src="<?php echo base_url();?>assets/images/card.png" alt="Card">&nbsp;Pay with Credit Card</a>
                        <span class="divider">OR</span>
                    </li>
                    <li>
<a class="p2" data-toggle="tab" href="#p-m-2"><img src="<?php echo base_url();?>assets/images/PayPal.png" alt="Paypal">&nbsp;Check Out</a>
                        <span>The safer, easier way to pay</span>
                    </li>
                </ul>
                <div class="tab-content form-content-wrapper">
                  <div id="p-m-1" class="tab-pane fade in active">
                        <form action="<?php echo $form_url; ?>" id="detailsform" name="detailsform" method="post">
							<input type="hidden" name="paytype" value="stripe"/>
                            <input type="hidden" name="donor_statement" value="<?php echo $donor_statement;?>"/>
                            <div class="form-group" style="margin-bottom:0;">
                            <div class="col-xs-12 col-sm-6">
                            <label for="donate_amt">Amount ($)</label><br>
                            <input type="text" class="form-control" id="donate_amt" name="donate_amt" value="<?php echo $donate_amt; ?>" placeholder="Enter Valid Amount" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                             <span class="r-label">Select donation amount</span>
                             <div class="radio-style">
                             <input type="radio" id="donate_avg_amt" class="radio_item" name="donate_pay_amt" value="<?php echo ($player_average > 0)? $player_average :35; ?>" onclick="setDonation(this.value);"/> 
                            <label for="donate_avg_amt" class="label_item"><i class="fa fa-check-circle"></i>
                           <div> Donate average  <span>$<?php echo ($player_average > 0)? $player_average :35; ?></span></div> 
                            </label>
                            <input type="radio" id="donate_rest_amt" class="radio_item" name="donate_pay_amt" value="<?php echo abs($player_remain); ?>" onclick="setDonation(this.value);"/>
                            <label for="donate_rest_amt"  class="label_item"><i class="fa fa-check-circle"></i>
                             <div> Donate remaining<span> <?php echo ($player_remain < 0)? '$0':'$'.abs($player_remain);?></span> </div>

                            </label>
                            </div>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                   <label>Note: A processing fee equal to <?php echo $settings_processingfees[0]['payment_processing_fee'] ?>% plus $<?php echo $settings_processingfees[0]['payment_processing_extra'] ?> will be added to your donation.</label>
                                  </div>
                                <div class="clearfix"></div>
                            </div>



                            <div class="form-group">
                            <div class="col-xs-12">
                            <label for="donor_email">Supporter's email</label><br>
                            <input type="email" class="form-control" id="donor_email" name="donor_email" placeholder="Email Address" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="firstname">First Name</label><br>
                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                            <label for="lastname">Last Name</label><br>
                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12">
                            <label for="donor_email">Address</label><br>
                            <input type="text" class="form-control" id="donor_addr" name="donor_addr" placeholder="Address" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="firstname">City</label><br>
                            <input type="text" class="form-control" id="donor_city" name="donor_city" placeholder="City" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                            <label for="lastname">State</label><br>
                            <input type="text" class="form-control" id="donor_state" name="donor_state" placeholder="State" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="lastname">Zip</label><br>
                            <input type="text" class="form-control" id="donor_zip" name="donor_zip" placeholder="Zip" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                            <label for="firstname">Phone Number</label><br>
                            <input type="text" class="form-control" id="donor_phone" name="donor_phone" placeholder="Phone Number" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="card_number">Card number</label><br>
                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="4584XXXXXXXX7804" required/>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                            <label for="card_expiry_month">Expiration</label><br>
                            <?php $months = array("01","02","03","04","05","06","07","08","09","10","11","12"); ?>
                            <select id="card_expiry_month" name="card_expiry_month" class="form-control" required>
                            <option value="">Month</option>
                            <?php
                            $cmon = date("m");
                            foreach($months as $mon)
                            {
                            echo '<option value="'.$mon.'">'.$mon.'</option>';
                            }
                            ?>
                            </select>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                            <?php
                            $cyear = date("Y");
                            ?>
                            <select id="card_expiry_year" name="card_expiry_year" class="form-control" style="margin-top:24px;" onchange="month()" required>
                            <option value="">Year</option>
                            <?php
                            $cyear = date("Y");
                            for($i=0;$i<=20;$i++)
                            {
                            echo '<option value="'.($cyear+$i).'">'.($cyear+$i).'</option>';
                            }
                            ?>
                            </select>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="card_country">Card address country</label><br>
                            <select id="card_country" name="card_country" class="form-control" required>
                            <option value="">Please select the address</option>
                            <?php
                            foreach($country_list as $country_li)
                            {
                            echo '<option value="'.$country_li["code"].'">'.$country_li["country"].'</option>';	
                            }
                            ?>
                            </select>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                            <label for="card_cvv">CVV</label><br>
                            <input type="text" class="form-control" id="card_cvv" name="card_cvv" placeholder="CVV" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group text-center">
                            <input type="submit" class="btn btn-default btn-green" id="payBtn" value="Submit"/><br>
                            <a href="<?php echo base_url().'player/'.$pslug;?>" style="text-decoration: underline;">Cancel</a>
                            <div class="clearfix"></div>
                            <span id="payment-errors" class="" style="display:none;"></span>
                            </div>
                        </form>
                    </div>
                    <div id="p-m-2" class="tab-pane fade">
                        <form action="<?php echo $form_url1; ?>" id="paypalform" name="paypalform">
							<input type="hidden" name="paytype" value="paypal"/>
                            <input type="hidden" name="donor_statement" value=""/>
                            <div class="form-group" style="margin-bottom:0;">
                                <div class="col-xs-12 col-sm-6">
                                    <label for="donate_amt1">Amount ($)</label><br>
                                    <input type="text" class="form-control ferror1" id="donate_amt1" name="donate_amt" value="<?php echo $donate_amt; ?>" placeholder="Enter Valid Amount" required/>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                     <span class="r-label">Select donation amount</span>
                                     <div class="radio-style">
                                     <input type="radio" id="donate_avg_amt1" class="radio_item" name="donate_pay_amt" value="<?php echo ($player_average > 0)? $player_average :35; ?>" onclick="setDonation1(this.value);"/> 
                                    <label for="donate_avg_amt1" class="label_item"><i class="fa fa-check-circle"></i>
                                   <div> Donate average  <span>$<?php echo ($player_average > 0)? $player_average :35; ?></span></div> 
                                    </label>
                                    <input type="radio" id="donate_rest_amt1" class="radio_item" name="donate_pay_amt" value="<?php echo abs($player_remain); ?>" onclick="setDonation1(this.value);"/>
                                    <label for="donate_rest_amt1"  class="label_item"><i class="fa fa-check-circle"></i>
                                     <div> Donate remaining<span> <?php echo ($player_remain < 0)? '$0':'$'.abs($player_remain);?></span> </div>
                                    </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label>Note: A processing fee equal to <?php echo $settings_processingfees[0]['payment_processing_fee'] ?>% plus $<?php echo $settings_processingfees[0]['payment_processing_extra'] ?>  will be added to your donation.</label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                            <label for="donor_email">Supporter's email</label><br>
                            <input type="email" class="form-control ferror1" id="donor_email1" name="donor_email" placeholder="Email Address" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="firstname">First Name</label><br>
                            <input type="text" class="form-control ferror1" id="firstname1" name="firstname" placeholder="Firstname" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                            <label for="lastname">Last Name</label><br>
                            <input type="text" class="form-control ferror1" id="lastname1" name="lastname" placeholder="Lastname" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12">
                            <label for="donor_email">Address</label><br>
                            <input type="text" class="form-control ferror1" id="donor_addr1" name="donor_addr" placeholder="Address" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="firstname">City</label><br>
                            <input type="text" class="form-control ferror1" id="donor_city1" name="donor_city" placeholder="City" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                            <label for="lastname">State</label><br>
                            <input type="text" class="form-control ferror1" id="donor_state1" name="donor_state" placeholder="State" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12 col-sm-6">
                            <label for="lastname">Zip</label><br>
                            <input type="text" class="form-control ferror1" id="donor_zip1" name="donor_zip" placeholder="Zip" required/>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                            <label for="firstname">Phone Number</label><br>
                            <input type="text" class="form-control ferror1" id="donor_phone1" name="donor_phone" placeholder="Phone Number" required/>
                            </div>
                            <div class="clearfix"></div>
                            </div>
                            <div class="form-group text-center">
							<div id="paypal-button"></div>
                            <a href="<?php echo base_url().'player/'.$pslug;?>" style="text-decoration: underline;">Cancel</a>
                            <div class="clearfix"></div>
                            <span id="payment-errors1" class="" style="display:none;"></span>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
//set your publishable key
Stripe.setPublishableKey('pk_live_kFNkgP2dJPW917rPIvjABiMX');

/*Stripe.setPublishableKey('pk_test_ltQBkG6gsxVHEwnAcT0FQjTN');*/

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
	if (response.error) {
		//enable the submit button
		$('#payBtn').removeAttr("disabled");
		//display the errors on the form
		//$('#payment-errors').attr('hidden', 'false');
		$('#payment-errors').addClass('alert alert-danger');
		$("#payment-errors").html(response.error.message);
		$('#payment-errors').show();
	} else {
		var form$ = $("#detailsform");
		//get token id
		var token = response['id'];
		//insert the token into the form
		form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
		//submit form to the server
		form$.get(0).submit();
	}
}

$(document).ready(function () {
	$('.ferror1').on('keyup',function () {
		var id = $(this).attr('id');
		//var field_msg = [];
		var field_msg = {'donate_amt1':'Amount','donor_email1':'Email','firstname1':'First Name','lastname1':'Last Name','donor_addr1':'Address','donor_city1':'City','donor_state1':'State','donor_zip1':'Zip','donor_phone1':'Phone'};
		if($(this).val()!=''){
			$("#lbl_"+$(this).attr('id')).remove();
		}else{
			$("#"+id).after('<label for="'+id+'" id="lbl_'+id+'" generated="true" class="error1">'+field_msg[id]+' Required</label>');
		}
	});
	$("#detailsform").validate({
		rules:{
			donate_amt:{
				required:true,
				number:true,
				min: 20
			},
			donor_email:{
				required:true
			},
			firstname:{
				required:true
			},
			lastname:{
				required:true
			},
			donor_addr:{
				required:true
			},
			donor_city:{
				required:true
			},
			donor_state:{
				required:true
			},
			donor_zip:{
				required:true
			},
			donor_phone:{
				required:true,
				digits:true
			},
			card_number:{
				required:true,
				digits:true
			},
			card_country:{
				required:true
			},
			card_cvv:{
				required:true,
				digits:true
			},
			card_expiry_month:{
				required:true
			},
			card_expiry_year:{
				required:true
			}
		},
		messages:{
			donate_amt:{
				required:"Amount Required",
				number:"Enter Valid Amount",
				min: "Donate Minimum $20 Amount"
			},
			donor_email:{
				required:"Email Required"
			},
			firstname:{
				required:"First Name Required"
			},
			lastname:{
				required:"Last Name Required"
			},
			donor_addr:{
				required:"Address Required"
			},
			donor_city:{
				required:"City Required"
			},
			donor_state:{
				required:"State Required"
			},
			donor_zip:{
				required:"Zip Required"
			},
			donor_phone:{
				required:"Phone no Required",
				digits:"Invalid Phone no"
			},
			card_number:{
				required:"Card Number Required",
				digits:"Invalid Card Number"
			},
			card_country:{
				required:"Card Country Required"
			},
			card_cvv:{
				required:"Cvv Required",
				digits:"Invalid CVV"
			},
			card_expiry_month:{
				required:"Month Required"
			},
			card_expiry_year:{
				required:"Year Required"
			}
		},
		submitHandler: function(form) {
			//disable the submit button to prevent repeated clicks
			$('#payBtn').attr("disabled", "disabled");
			//create single-use token to charge the user
			Stripe.createToken({
				number: $('#card_number').val(),
				cvc: $('#card_cvv').val(),
				exp_month: $('#card_expiry_month').val(),
				exp_year: $('#card_expiry_year').val()
			}, stripeResponseHandler);
			//submit from callback
			return false;
		}
	});
});
</script>
<script type="text/javascript">
function setDonation(strr)
{
	$("#donate_amt").val(strr);
}
function setDonation1(strr)
{
	$("#donate_amt1").val(strr);
}
function month()
{
	$("#card_expiry_month").val('');
	var cmonth = <?php echo $cmon.';' ?>
	var today = new Date();
	var year = today.getFullYear();
	var chyear = $("#card_expiry_year").val();
	if(chyear > year)
	{
		$("#card_expiry_month option").each(function()
		{
			$(this).removeAttr("disabled");
		});
	}
	else
	{
		$("#card_expiry_month option").each(function()
		{
			if($(this).val() > cmonth)
			{
			$(this).removeAttr("disabled");	
			}
			else
			{
			$(this).attr("disabled","disabled");	
			}
		
		});
	}
}
</script>
<!-- Home Content ends-->
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
function isValid() {
	var field_arr = ['donate_amt1','donor_email1','firstname1','lastname1','donor_addr1','donor_city1','donor_state1','donor_zip1','donor_phone1'];
	var field_msg = ['Amount','Email','First Name','Last Name','Address','City','State','Zip','Phone'];
	var fval = '';
	var error = 0;
	$('.error1').remove();
	$.each( field_arr, function( index, value ){
		fval = $("#"+value).val();	
		if(fval==''){
			$('#'+value).after('<label for="'+value+'" id="lbl_'+value+'" generated="true" class="error1">'+field_msg[index]+' Required</label>');
			error++;
		}
	});
	if(error != 0){
		return false;
	}else{
		return true;
	}
}

function validateButton(actions) {
	return isValid() ? actions.enable() : actions.disable();
}
function disableButton(actions) {
	return actions.disable();
}

function onChangeForm(handler) {
	document.querySelector('#paypalform').addEventListener('change', handler);
}

function toggleValidationMessage() {
	isValid();
}

function toggleButton(actions) {
	return (isValid()) ? actions.enable() : actions.disable();
}

paypal.Button.render({
	env: 'production',
	client: {
	production: 'AbLdeDZsx1TSJP6jA2gUUY1BzOq8lMWXEfPCFQsBU5ZF-rquk7cckH_EuoCjnosG'
	},
	/*client: {
	sandbox: 'AbRxnYBCCL8rumIxCOGhwF4nieSmKY00ZHtTRDKOxM26WLXVEzaK-2FpK5CreCgI016fYm9rX0a4lLBf'
	},*/
	locale: 'en_US',
	style: {
		size: 'medium',
		color: 'gold',
		shape: 'pill',
		label: 'checkout',
		tagline: 'false'
	},
	validate: function(actions) {
		disableButton(actions);
		onChangeForm(function(){
			toggleButton(actions);
		});
	},
	onClick: function() {
		toggleValidationMessage();
	},
	payment: function (data, actions) {

	        var donatevalue = parseFloat($('#donate_amt1').val());
            var additional  = parseFloat('3.50');
            var extra  = parseFloat('0.30');
            var total = (( donatevalue * additional / 100 ) + extra + donatevalue).toFixed(2);
		return actions.payment.create({
			transactions: [{amount: {total: total,currency: 'USD'}}]
		});
	},
	onAuthorize: function (data, actions) {
		return actions.payment.execute().then(function () {
		console.log(data);
		$('#payment-errors1').addClass('alert alert-success');
		$("#payment-errors1").html('Thank you for your payment!');
		$('#payment-errors1').show();
		//$('#paypalform').submit();
		});
	},
	onCancel : function(data){
		console.log(data);
		$('#payment-errors1').addClass('alert alert-danger');
		$("#payment-errors1").html('Payment cancelled');
		$('#payment-errors1').show();
	},
	onError : function (err){
        console.log(err);
		$('#payment-errors1').addClass('alert alert-danger');
		$("#payment-errors1").html('Error: Some errors occured');
		$('#payment-errors1').show();
	}
}, '#paypal-button');
</script>