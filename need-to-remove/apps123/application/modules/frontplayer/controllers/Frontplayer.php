<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontplayer extends CI_Controller{
    public $data=array();
	public function __construct(){
		parent::__construct();
		$this->data['found_flag'] = 1;
		$this->load->helper('url');
		$this->load->library('stripegateway');
		$this->load->model('Frontplayer_model');
	}

	public function index()
	{
		/*echo "Front View for players";
		die;
		if($this->session->userdata('player_id')!="")
		{
			redirect('player/'.$this->session->userdata('player_urlname').'/home');
		}
		else
		{
			redirect('player/login');
		}*/
	}

	public function home($fslug,$pslug,$msg_id='')
	{
		$this->session->unset_userdata('donation_id');
		if($pslug!='')
		{
			$player_details= $this->Frontplayer_model->view('tbl_player',array("player_slug_name"=>$pslug));			
			if(count($player_details)>0)
			{
				if($msg_id!='')
				{
					$where_up = array('id'=>$msg_id);
					$check_msg = $this->Frontplayer_model->view_check('tbl_email_share_reports',$where_up);
					if($check_msg > 0)
					{
						$dt_arrayup = array('visited' => 1,'visited_time' => date("Y-m-d H:i:s"));
						$this->Frontplayer_model->edit('tbl_email_share_reports',$dt_arrayup,$where_up);
					}
				}
				$fund_id 		= $player_details[0]['player_added_by'];
				$player_id 		= $player_details[0]['id'];
				$fund_details	= $this->Frontplayer_model->view('tbl_fundraiser',array("id"=>$fund_id));
				$fund_sliders	= $this->Frontplayer_model->view('tbl_rel_fund_feature_images',array("fund_id"=>$fund_id));
				$total_donors 	= $this->Frontplayer_model->fundraiser_donors($fund_id);
				$total_payments	= $this->Frontplayer_model->total_player_fund($pslug);
				$messages		= $this->Frontplayer_model->player_messages($pslug);
				
				$this->data["player_details"]   = $player_details;
				$this->data["fund_details"]     = $fund_details;
				$this->data["fund_sliders"]     = $fund_sliders;
				$this->data["total_donors"]     = $total_donors;
				$this->data["total_payments"]   = $total_payments;
				$this->data["messages"]   		= $messages;
				$this->data["fslug"]            = $fslug;
				$this->data["pslug"]            = $pslug;
				$this->load->view('../../includes_front_player/header1',$this->data);        
				$this->load->view('view_dashboard',$this->data);
				$this->load->view('../../includes_front_player/footer',$this->data);
			}
			else
			{
				$this->data['error_con']  = '404 Page not found!!';
				$this->data['found_flag'] = 0;	
				$this->load->view('../../includes_front_player/header1',$this->data);  
				$this->load->view('error',$this->data);
				//$this->load->view('../../includes_front_player/footer');
			}
		}
	}

	public function donorPayment($fslug,$pslug,$msg_id='')
	{
		if($fslug!='' && $pslug!='')
		{
			$player_details= $this->Frontplayer_model->view('tbl_player',array("player_slug_name"=>$pslug));			
			if(count($player_details)>0)
			{
				$fund_details  = $this->Frontplayer_model->view('tbl_fundraiser',array("id"=>$player_details[0]['player_added_by']));
				$player_raised = $this->Frontplayer_model->playersinfo($pslug);
				$player_donors = $this->Frontplayer_model->player_donors($pslug);
				$country_list  = $this->Frontplayer_model->view('tbl_mst_country',array("status"=>1));
                $this->data['settings_processingfees'] = $this->Frontplayer_model->view('tbl_settings',array('id'=>1));
				$this->data["player_details"]   = $player_details;
				$this->data["fund_details"]     = $fund_details;
				$this->data["player_raised"]    = $player_raised;
				$this->data["player_donors"]    = $player_donors;
				$this->data["country_list"]     = $country_list;
				$this->data["fslug"]            = $fslug;
				$this->data["pslug"]            = $pslug;
				$this->data["donate_amt"]		= $this->input->post('donate_amount');
				$this->load->view('../../includes_front_player/header',$this->data);        
				$this->load->view('view_payment',$this->data);
				$this->load->view('../../includes_front_player/footer',$this->data);
			}
			else
			{
				$this->data['error_con']  = '404 Page not found!!';
				$this->data['found_flag'] = 0;	
				$this->load->view('../../includes_front_player/header',$this->data);  
				$this->load->view('error',$this->data);
			}
		}
	}
	
	public function donation_paid_stripe($fslug,$pslug,$msg_id='')
	{
		if($fslug!='' AND $pslug!='')
		{
            $settings_processingfees = $this->Frontplayer_model->view('tbl_settings',array('id'=>1));

			if(!empty($_POST['stripeToken']))
			{

                $donatevalue = $this->input->post('donate_amt');
                $additional  = $settings_processingfees[0]['payment_processing_fee'];
                $extra  = $settings_processingfees[0]['payment_processing_extra'];
                $total = (( $donatevalue * $additional / 100 ) + $extra + $donatevalue);

				$data = array(
				'number'	=> $this->input->post('card_number'),
				'exp_month' => $this->input->post('card_expiry_month'),
				'exp_year'	=> $this->input->post('card_expiry_year'),
				'amount'	=>  number_format($total, 2, '.', '') * 100,
				"description"=> $this->input->post('donor_statement'),
				'token'		=> $this->input->post('stripeToken'),
				'email'		=> $this->input->post('donor_email')   
				);

				$return = $this->stripegateway->checkout($data);
/*				print_r($return);
				exit;*/
				if(is_array($return)){
				if($return['status']=='succeeded'){
					
					$finfo = $this->Frontplayer_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
					$pinfo = $this->Frontplayer_model->view('tbl_player',array('player_slug_name'=>$pslug));
					$cinfo = $this->Frontplayer_model->get_activation_code();

					if(count($cinfo)>0){
					$code_id = $cinfo[0]['id'];
					$activation_code = $cinfo[0]['activation_code'];	
					}else{
					$code_id = 0;
					$activation_code = '';	
					}

					$transaction_id		= $return['balance_transaction'];
					$card_type			= $return['source']['brand'];
					$card_last4			= $return['source']['last4'];
					$fundraiser_id		= $finfo[0]['id'];
					$fundraiser_slug	= $finfo[0]['fund_slug_name'];
					//$fundraiser_name	= $finfo[0]['fund_fname'].' '.$finfo[0]['fund_lname'];
					$fundraiser_name	= $finfo[0]['fund_username'];
					$fund_donor_statement	= $finfo[0]['fund_donor_statement'];
					$fundraiser_slog		= $finfo[0]['fund_slogan'];
					$fundraiser_bgcolor		= $finfo[0]['fund_color_rgb'];
					if($finfo[0]['fund_image']==""){
					$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
					}else{
					$fundTimg 		= explode(".",$finfo[0]['fund_image']);	
					$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
					}
					
					$fundraiser_taxid	= (trim($finfo[0]['fund_beneficiary_tax_id'])!='')?$finfo[0]['fund_beneficiary_tax_id']:'Not Provided';
					$player_id			= $pinfo[0]['id']; 
					$player_slug		= $pinfo[0]['player_slug_name']; 
					$player_first		= $pinfo[0]['player_fname']; 
					$player_last		= $pinfo[0]['player_lname'];
                    $payment_processing_fee		= $settings_processingfees[0]['payment_processing_fee'].'%';
                    $payment_processing_extra		= '$'.$settings_processingfees[0]['payment_processing_extra'];
					$firstname			= $this->input->post('firstname');
					$lastname			= $this->input->post('lastname');
					$donor_addr			= $this->input->post('donor_addr');
					$donor_city			= $this->input->post('donor_city');
					$donor_state		= $this->input->post('donor_state');
					$donor_zip			= $this->input->post('donor_zip');
					$donor_phone		= $this->input->post('donor_phone');
					$card_number		= $this->input->post('card_number');
					$short_card_number	= $this->short_card_number($card_number);
					$donor_email		= $this->input->post('donor_email');
                    //Donate value as per settings
                    $donate_amt			= number_format($total, 2, '.', '');
                    //Donate value as per settings
					$donate_datetime	= date("d M Y H:i:s");
					$start_fundraiser	= 'https://www.ThanksForSupporting.com';
					
					$dt_array = array(
						'txn_id'	  => $transaction_id,
						'fund_id'	  => $fundraiser_id,
						'fund_slug'	  => $fundraiser_slug,
						'player_id'	  => $player_id,
						'player_slug' => $player_slug,
						'code_id'	  => $code_id,
						'used_code'	  => $activation_code,
						'donation_amt'=> $donate_amt,
                        'donation'=> $donatevalue,
                        'payment_processing_fee'=> $additional,
                        'payment_processing_extra'=> $extra,
						'donor_fname' => $firstname,
						'donor_lname' => $lastname,
						'donor_email' => $donor_email,
						'donor_addr'  => $donor_addr,
						'donor_city'  => $donor_city,
						'donor_state' => $donor_state,
						'donor_zip'	  => $donor_zip,
						'donor_phone' => $donor_phone,
						'donation_date'=>date("Y-m-d H:i:s")
					);
					
					$this->Frontplayer_model->add('tbl_donors_payment',$dt_array);
					
					$inserted_id = $this->Frontplayer_model->last_insert_id();
					$this->session->set_userdata('donation_id',$inserted_id);
					$this->session->set_userdata('suc_msg','Thank you for your donation');
					$this->session->set_userdata('suc_msg1','Your transaction was successful. A confirmation message has been sent to your email');
					
					$temp_dt = $this->Frontplayer_model->view('tbl_mail_template',array("template_slug"=>'donation-receipt-to-donor-from-tfs'));
					
					$recpt_html_header	= $temp_dt[0]['template_header'];
					$recpt_html_footer	= $temp_dt[0]['template_footer'];
					$temp_subject	= $temp_dt[0]['template_subject'];
					$temp_body		= $recpt_html_header.html_entity_decode($temp_dt[0]['template_content']).$recpt_html_footer;
					$search_key 	= array('[FundraiserSlogan]','[FundraiserImage]','[DonorFirst]','[DonorLast]','[FundraiserName]','[Fundraiser TaxID]','[PlayerFirst]','[PlayerLast]','[DonationAmount]','[DonationDateTime]','[CreditCardType]','[LastFourDigits]','[TransactionID]','[DonationFromName]','[StartFundraiser]','[PaymentProcessingFee]','[PaymentProcessingFeeExtra]','[DonorCreditCardStatement]','[selected_color]');
					$search_val		= array($fundraiser_slog,$fundimg,$firstname,$lastname,$fundraiser_name,$fundraiser_taxid,$player_first,$player_last,$donate_amt,$donate_datetime,$card_type,'('.$short_card_number.')',$transaction_id,$firstname.' '.$lastname,$start_fundraiser,$payment_processing_fee,$payment_processing_extra,$fund_donor_statement,$fundraiser_bgcolor);
					$mail_content	= str_replace($search_key,$search_val,$temp_body);
					
					$config = array();
					$config['useragent']= "CodeIgniter";
					$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config['protocol'] = "smtp";
					$config['smtp_host']= "localhost";
					$config['smtp_port']= "25";
					$config['mailtype'] = 'html';
					$config['charset']	= 'iso-8859-1';
					$config['newline']  = "\r\n";
					$config['wordwrap'] = TRUE;
					
					$this->email->initialize($config);   
					$this->email->from('support@thanksforsupporting.com','ThanksForSupporting');  
					$this->email->to($donor_email); 
					$this->email->subject("ThanksForSupporting - ".$temp_subject); 
					$this->email->message($mail_content);
					$bval=$this->email->send();
					
					$temp_dt1		= $this->Frontplayer_model->view('tbl_mail_template',array("template_slug"=>'thank-you-gift-to-donor-from-gca'));

					$temp_subject1	= $temp_dt1[0]['template_subject'];
					$gift_html_header	= $temp_dt1[0]['template_header'];
					$gift_html_footer	= $temp_dt1[0]['template_footer'];
					$temp_body1		= html_entity_decode($temp_dt1[0]['template_content']);
					$search_key1 	= array('[DonorFirst]','[DonorLast]','[FundraiserName]','[ActivationCode]');
					$search_val1	= array($firstname,$lastname,$fundraiser_name,$activation_code);
					$mail_content1	= $gift_html_header.str_replace($search_key1,$search_val1,$temp_body1).$gift_html_footer;

					$config1 = array();
					$config1['useragent']= "CodeIgniter";
					$config1['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config1['protocol'] = "smtp";
					$config1['smtp_host']= "localhost";
					$config1['smtp_port']= "25";
					$config1['mailtype'] = 'html';
					$config1['charset']	= 'iso-8859-1';
					$config1['newline']  = "\r\n";
					$config1['wordwrap'] = TRUE;
					
					$this->email->initialize($config1);   
					$this->email->from('support@geocouponalerts.com','GeoCouponAlerts');  
					$this->email->to($donor_email); 
					$this->email->subject("GeoCouponAlerts - ".$temp_subject1); 
					$this->email->message($mail_content1);
					$bval1=$this->email->send();
					if($bval1){
						$where_upc = array('id'=>$code_id);
						$cdt_arrayup = array('consumed' => 1,'consumed_by' => $inserted_id,'consumed_dt' => date("Y-m-d H:i:s"));
						$this->Frontplayer_model->edit('tbl_activation_codes',$cdt_arrayup,$where_upc);	
					}

					if($msg_id!='')
					{
						$where_up = array('id'=>$msg_id);
						$check_msg = $this->Frontplayer_model->view_check('tbl_email_share_reports',$where_up);
						if($check_msg > 0)
						{
						$dt_arrayup = array('donated' => 1,'donated_time' => date("Y-m-d H:i:s"));
						$this->Frontplayer_model->edit('tbl_email_share_reports',$dt_arrayup,$where_up);
						}
					}

					redirect($fslug.'/'.$pslug.'/thanks/'.$inserted_id);	
				}
				}else{
                    $this->data['error_con']  = $return;
					$this->load->view('error',$this->data);
				}
			}else{
				$this->data['error_con']  = 'Token Expired';	
				$this->load->view('error',$this->data);
			}
		}
	}
	
	public function donation_paid_paypal($fslug,$pslug,$msg_id='')
	{
		if($fslug!='' AND $pslug!='')
		{
			if($_POST['paytype']=='paypal')
			{
					$finfo = $this->Frontplayer_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
					$pinfo = $this->Frontplayer_model->view('tbl_player',array('player_slug_name'=>$pslug));
					$cinfo = $this->Frontplayer_model->get_activation_code();
					$settings_processingfees = $this->Frontplayer_model->view('tbl_settings',array('id'=>1));
					if(count($cinfo)>0){
					$code_id = $cinfo[0]['id'];
					$activation_code = $cinfo[0]['activation_code'];	
					}else{
					$code_id = 0;
					$activation_code = '';	
					}

					$transaction_id		= $return['balance_transaction'];
					$card_type			= $return['source']['brand'];
					$card_last4			= $return['source']['last4'];
					$fundraiser_id		= $finfo[0]['id'];
					$fundraiser_slug	= $finfo[0]['fund_slug_name'];
					//$fundraiser_name	= $finfo[0]['fund_fname'].' '.$finfo[0]['fund_lname'];
					$fundraiser_name	= $finfo[0]['fund_username'];
					$fundraiser_slog		= $finfo[0]['fund_slogan'];
					$fund_donor_statement	= $finfo[0]['fund_donor_statement'];
					$fundraiser_bgcolor		= $finfo[0]['fund_color_rgb'];
					
					if($finfo[0]['fund_image']==""){
					$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
					}else{
					$fundTimg 		= explode(".",$finfo[0]['fund_image']);	
					$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
					}
					
					$fundraiser_taxid	= (trim($finfo[0]['fund_beneficiary_tax_id'])!='')?$finfo[0]['fund_beneficiary_tax_id']:'Not Provided';
					$player_id			= $pinfo[0]['id']; 
					$player_slug		= $pinfo[0]['player_slug_name']; 
					$player_first		= $pinfo[0]['player_fname']; 
					$player_last		= $pinfo[0]['player_lname'];
                    $payment_processing_fee		= $settings_processingfees[0]['payment_processing_fee'].'%';
                    $payment_processing_extra		= '$'.$settings_processingfees[0]['payment_processing_extra'];
					$firstname			= $this->input->post('firstname');
					$lastname			= $this->input->post('lastname');
					$donor_addr			= $this->input->post('donor_addr');
					$donor_city			= $this->input->post('donor_city');
					$donor_state		= $this->input->post('donor_state');
					$donor_zip			= $this->input->post('donor_zip');
					$donor_phone		= $this->input->post('donor_phone');
					$donor_email		= $this->input->post('donor_email');

					//Donate value as per settings
                    $donatevalue = $this->input->post('donate_amt');
                    $additional  = $settings_processingfees[0]['payment_processing_fee'];
                    $extra  = $settings_processingfees[0]['payment_processing_extra'];
                    $total = (( $donatevalue * $additional / 100 ) + $extra + $donatevalue);
					$donate_amt			= number_format($total, 2, '.', '');
                   //Donate value as per settings

					$donate_datetime	= date("d M Y H:i:s");
					$start_fundraiser	= 'https://www.ThanksForSupporting.com';
					
					$dt_array = array(
						'txn_id'	  => $transaction_id,
						'fund_id'	  => $fundraiser_id,
						'fund_slug'	  => $fundraiser_slug,
						'player_id'	  => $player_id,
						'player_slug' => $player_slug,
						'code_id'	  => $code_id,
						'used_code'	  => $activation_code,
						'donation_amt'=> $donate_amt,
                        'donation'=> $donatevalue,
                        'payment_processing_fee'=> $additional,
                        'payment_processing_extra'=> $extra,
						'donor_fname' => $firstname,
						'donor_lname' => $lastname,
						'donor_email' => $donor_email,
						'donor_addr'  => $donor_addr,
						'donor_city'  => $donor_city,
						'donor_state' => $donor_state,
						'donor_zip'	  => $donor_zip,
						'donor_phone' => $donor_phone,
						'donation_date'=>date("Y-m-d H:i:s")
					);
					
					$this->Frontplayer_model->add('tbl_donors_payment',$dt_array);
					
					$inserted_id = $this->Frontplayer_model->last_insert_id();
					$this->session->set_userdata('donation_id',$inserted_id);
					$this->session->set_userdata('suc_msg','Thank you for your donation');
					$this->session->set_userdata('suc_msg1','Your transaction was successful. A confirmation message has been sent to your email');
					
					$temp_dt = $this->Frontplayer_model->view('tbl_mail_template',array("template_slug"=>'donation-receipt-to-donor-from-tfs'));
					
					$recpt_html_header	= $temp_dt[0]['template_header'];
					$recpt_html_footer	= $temp_dt[0]['template_footer'];
					$temp_subject	= $temp_dt[0]['template_subject'];
					$temp_body		= $recpt_html_header.html_entity_decode($temp_dt[0]['template_content']).$recpt_html_footer;
					$search_key 	= array('[FundraiserSlogan]','[FundraiserImage]','[DonorFirst]','[DonorLast]','[FundraiserName]','[Fundraiser TaxID]','[PlayerFirst]','[PlayerLast]','[DonationAmount]','[DonationDateTime]','[CreditCardType]','[LastFourDigits]','[TransactionID]','[DonationFromName]','[StartFundraiser]','[PaymentProcessingFee]','[PaymentProcessingFeeExtra]','[DonorCreditCardStatement]','[selected_color]');
					$search_val		= array($fundraiser_slog,$fundimg,$firstname,$lastname,$fundraiser_name,$fundraiser_taxid,$player_first,$player_last,$donate_amt,$donate_datetime,$card_type,'('.$short_card_number.')',$transaction_id,$firstname.' '.$lastname,$start_fundraiser,$payment_processing_fee,$payment_processing_extra,$fund_donor_statement,$fundraiser_bgcolor);
					$mail_content	= str_replace($search_key,$search_val,$temp_body);
					
					$config = array();
					$config['useragent']= "CodeIgniter";
					$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config['protocol'] = "smtp";
					$config['smtp_host']= "localhost";
					$config['smtp_port']= "25";
					$config['mailtype'] = 'html';
					$config['charset']	= 'iso-8859-1';
					$config['newline']  = "\r\n";
					$config['wordwrap'] = TRUE;
					
					$this->email->initialize($config);   
					$this->email->from('support@thanksforsupporting.com','ThanksForSupporting');  
					$this->email->to($donor_email); 
					$this->email->subject("ThanksForSupporting - ".$temp_subject); 
					$this->email->message($mail_content);
					$bval=$this->email->send();
					
					$temp_dt1		= $this->Frontplayer_model->view('tbl_mail_template',array("template_slug"=>'thank-you-gift-to-donor-from-gca'));

					$temp_subject1	= $temp_dt1[0]['template_subject'];
					$gift_html_header	= $temp_dt1[0]['template_header'];
					$gift_html_footer	= $temp_dt1[0]['template_footer'];
					$temp_body1		= html_entity_decode($temp_dt1[0]['template_content']);
					$search_key1 	= array('[DonorFirst]','[DonorLast]','[FundraiserName]','[ActivationCode]');
					$search_val1	= array($firstname,$lastname,$fundraiser_name,$activation_code);
					$mail_content1	= $gift_html_header.str_replace($search_key1,$search_val1,$temp_body1).$gift_html_footer;

					$config1 = array();
					$config1['useragent']= "CodeIgniter";
					$config1['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config1['protocol'] = "smtp";
					$config1['smtp_host']= "localhost";
					$config1['smtp_port']= "25";
					$config1['mailtype'] = 'html';
					$config1['charset']	= 'iso-8859-1';
					$config1['newline']  = "\r\n";
					$config1['wordwrap'] = TRUE;
					
					$this->email->initialize($config1);   
					$this->email->from('support@geocouponalerts.com','GeoCouponAlerts');  
					$this->email->to($donor_email); 
					$this->email->subject("GeoCouponAlerts - ".$temp_subject1); 
					$this->email->message($mail_content1);
					$bval1=$this->email->send();
					if($bval1){
						$where_upc = array('id'=>$code_id);
						$cdt_arrayup = array('consumed' => 1,'consumed_by' => $inserted_id,'consumed_dt' => date("Y-m-d H:i:s"));
						$this->Frontplayer_model->edit('tbl_activation_codes',$cdt_arrayup,$where_upc);	
					}

					if($msg_id!='')
					{
						$where_up = array('id'=>$msg_id);
						$check_msg = $this->Frontplayer_model->view_check('tbl_email_share_reports',$where_up);
						if($check_msg > 0)
						{
						$dt_arrayup = array('donated' => 1,'donated_time' => date("Y-m-d H:i:s"));
						$this->Frontplayer_model->edit('tbl_email_share_reports',$dt_arrayup,$where_up);
						}
					}

					redirect($fslug.'/'.$pslug.'/thanks/'.$inserted_id);	
			}else{
				$this->data['error_con']  = 'Insuffiecient Data Provided';	
				$this->load->view('error',$this->data);
			}
		}
	}
	
	public function donation_success($fslug,$pslug,$don_id)
	{
		if($fslug!='' AND $pslug!='')
		{
			$player_details= $this->Frontplayer_model->view('tbl_player',array("player_slug_name"=>$pslug));			
			if(count($player_details)>0)
			{
				$fund_details  = $this->Frontplayer_model->view('tbl_fundraiser',array("id"=>$player_details[0]['player_added_by']));
				$country_list  = $this->Frontplayer_model->view('tbl_mst_country',array("status"=>1));
				$this->data["player_details"]   = $player_details;
				$this->data["fund_details"]     = $fund_details;
				$this->data["country_list"]     = $country_list;
				$this->data["fslug"]            = $fslug;
				$this->data["pslug"]            = $pslug;			
				$this->data["donation_id"]		= $don_id;			
				$this->load->view('../../includes_front_player/header',$this->data);        
				$this->load->view('view_paymentsucess',$this->data);
				$this->load->view('../../includes_front_player/footer',$this->data);
			}
			else
			{
				$this->data['error_con']  = '404 Page not found!!';
				$this->data['found_flag'] = 0;	
				$this->load->view('../../includes_front_player/header',$this->data);  
				$this->load->view('error',$this->data);
			}
		}
	}
	
	public function donation_post($fslug,$pslug)
	{
		if($fslug!='' AND $pslug!='')
		{
			$share_message	=	$this->input->post('share_message');
			$donation_id	=	$this->input->post('donation_id');
			$dt_array		=	array();
			if($donation_id!='' AND $donation_id!=NULL)
			{
				$dt_array['donation_id']=	$donation_id;
			}
			//die;
			$dt_array['profile_img']	=	'no_image.png';
			$dt_array['message']		=	$share_message;
			$dt_array['post_date']		=	date('Y-m-d H:i:s');
			$dt_array['player_slug']	=	$pslug;
			$dt_array['fb_status']		=	0;
			$msg_sts  		=	$this->Frontplayer_model->add('tbl_messages',$dt_array);
			if($msg_sts)
			{
				//$this->session->unset_userdata('donation_id');
				$r = array('valid' => 1,'msg'=>'success');
			}
			else
			{
				$r = array('valid' => 0,'msg'=>'failed');
			}
			echo json_encode($r);
		}
	}
	
	public function ajax_donation_post()
	{
		$fb_id			=	$this->input->post('fb_id');
		$fb_image		=	$this->input->post('fb_image');
		$donation_id	=	$this->input->post('don_id');
		$dt_array		=	array();
		if($donation_id!='' AND $donation_id!=NULL)
		{
			$where 					=	array('donation_id'=>$donation_id);
			$dt_array['fb_id']		=	$fb_id;
			$dt_array['profile_img']=	$fb_image;
			$dt_array['fb_status']	=	1;
			$msg_sts  		=	$this->Frontplayer_model->edit('tbl_messages',$dt_array,$where);
			if($msg_sts)
			{
				$this->session->unset_userdata('donation_id');
				$r = array('valid' => 1,'msg'=>'success');
			}
			else
			{
				$r = array('valid' => 0,'msg'=>'failed1');
			}
		}
		else
		{
			$r = array('valid' => 0,'msg'=>'failed2');
		}
		
		echo json_encode($r);
	}
	
	public function donation_share($fslug,$pslug)
	{
		if($fslug!='' AND $pslug!='')
		{
			$this->session->unset_userdata('donation_id');
			
			$player_details 		= $this->Frontplayer_model->view('tbl_player',array("player_slug_name"=>$pslug));
			$fund_details 			= $this->Frontplayer_model->view('tbl_fundraiser',array("fund_slug_name"=>$fslug));
			//$fundraiser_name		= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name		= $fund_details[0]['fund_username'];
			$fundraiser_slog		= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor		= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor	= $fund_details[0]['fund_font_color'];
			
			if($fund_details[0]['fund_image']==""){
				$fundimg =  base_url()."assets/images/noimage-150x150.jpg";   
			}else{
				$fundTimg = explode(".",$fund_details[0]['fund_image']);	
				$fundimg  =  base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
			}
			
			if(count($player_details)>0)
			{
				$firstname	= 'ThanksForSupporting';
				$lastname	= ' ';
				
				$player_url			= base_url($fslug.'/'.$pslug);
				$player_share_url	= base_url($fslug.'/'.$pslug);
				$start_fundraiser	= base_url("start-fundraiser");

				$template = $this->Frontplayer_model->view('tbl_mail_template',array('template_slug'=>'share-request-for-donation-to-donercontact-from-donor'));
				$share_html_header = $template[0]["template_header"];
				$share_html_footer = $template[0]["template_footer"];
				$template_subject = $template[0]["template_subject"];
				$template_body	= $share_html_header.html_entity_decode(htmlspecialchars_decode($template[0]['template_content'])).$share_html_footer;
				
				$search_key 	= array('[FundraiserSlogan]','[FundraiserImage]','[FundraiserName]','[FundraiserSlogan]','[ShareFirst]','[ShareLast]','[PlayerURL]','[PlayerShareLink]','[StartFundraiser]','[UseOfDonations]','[bgcolor]','[fontcolor]','[selected_color]');
				$search_val		= array($fundraiser_slog,$fundimg,$fundraiser_name,$fundraiser_slog,$firstname,$lastname,$player_url,$player_share_url,$start_fundraiser,$fundraiser_name,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_bgcolor);
				$mail_content	= str_replace($search_key,$search_val,$template_body);
				$from = 'invite@thanksforsupporting.com';
				$subject = "ThanksForSupporting - ".$template_subject;
				$mail_body = $mail_content;
				
				$fund_details  = $this->Frontplayer_model->view('tbl_fundraiser',array("id"=>$player_details[0]['player_added_by']));
				$this->data["player_details"]= $player_details;
				$this->data["fund_details"] = $fund_details;
				$this->data["from"] 		= $from;
				$this->data["subject"] 		= $subject;
				$this->data["mail_body"] 	= $mail_body;
				$this->data["player_url"]	= $player_url;
				$this->data["fslug"]        = $fslug;
				$this->data["pslug"]        = $pslug;
				$this->load->view('../../includes_front_player/header',$this->data);        
				$this->load->view('view_playershare',$this->data);
				$this->load->view('../../includes_front_player/footer',$this->data);

			}
			else
			{
				$this->data['error_con']  = '404 Page not found!!';
				$this->data['found_flag'] = 0;	
				$this->load->view('../../includes_front_player/header',$this->data);  
				$this->load->view('error',$this->data);
			}
		}
	}
	
	public function short_card_number($cn)
	{
		$cnumber = str_repeat('X',(strlen($cn)-4)).substr($cn, -4);
		return $cnumber;
	}
	
	public function logout($fslug,$pslug)
	{
		$this->session->unset_userdata('donor_id');
		$this->session->unset_userdata('donor_fname');
		$this->session->unset_userdata('donor_lname');
		$this->session->unset_userdata('donor_email');
		$this->session->set_userdata('err_msg', 'Successfully Logout !');
		redirect(base_url($fslug.'/'.$pslug));
	}
	
	/*-----------------------------------Player Section Start---------------------------------*/
	
	private function createThumb($path, $filename,$data,$w, $h=0, $crop=false)
	{
        $config['image_library']    = "gd2";      
        $config['source_image']     = $data['full_path'];      
        $config['create_thumb']     = TRUE;      
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['new_image']    	= $path.$filename;

        $width= $data['image_width'];
        $height= $data['image_height'];

        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }

        $config['width']  = $newwidth; 
        $config['height'] = $newheight;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize()){
        return $this->image_lib->display_errors();
        } 
        $this->image_lib->clear();
        return false; 
	}
    
    //---------------------
    //Done by Rupak
    //Dated 27/08/2018
    //---------------------
	public function defaultPaymentLandingPage(){
		$this->data['found_flag']    = '';
		$country_list   = $this->Frontplayer_model->view('tbl_mst_country',array("status"=>1));
		$fund_details	= $this->Frontplayer_model->func_common_listing_where('tbl_fundraiser',array('active_fundraiser'=>1),'fund_username','ASC');
		$this->data["country_list"]  		= $country_list;
		$this->data["counter"]              = (count($fund_details)>0)?count($fund_details):array();;
		$this->data["fund_details"]  		= $fund_details;
		$this->load->view('../../includes_front_player/header_default_payment',$this->data);        
		$this->load->view('view_default_payment_landing_page',$this->data);
		$this->load->view('../../includes_front_player/footer_default_payment',$this->data);		
	}
	
	public function defaultPaymentPersonalInfo($fslug){		
		if($this->uri->segment(3)!=''){
		$fund_details   = $this->Frontplayer_model->view('tbl_fundraiser',array("fund_slug_name"=>$fslug));
		$player_details = $this->Frontplayer_model->view('tbl_player',array("player_added_by"=>$fund_details[0]['id'],"free_trial"=>1));
		if(count($player_details)==0){
		$this->session->set_flashdata('error_msg',"This fundraiser has no default player!!");	
		redirect('donate');	
		}else{
		$this->data["fund_details"]  		= (count($fund_details)>0)?$fund_details:array();
		$this->data["player_details"]  		= (count($player_details)>0)?$player_details:array();
		$this->load->view('../../includes_front_player/header_default_payment',$this->data);        
		$this->load->view('view_default_payment_personalinfo_page',$this->data);
		$this->load->view('../../includes_front_player/footer_default_payment',$this->data);
		}
		}else{
		redirect('donate');	
		}
	}
	public function donatePaymentAjax(){
		//echo '<pre>'.print_r($_REQUEST).'</pre>';

            $settings_processingfees = $this->Frontplayer_model->view('tbl_settings',array('id'=>1));
			if(!empty($_POST['stripeToken']))
			{
                $donatevalue = $this->input->post('donate_amt');
                $additional  = $settings_processingfees[0]['payment_processing_fee'];
                $extra  = $settings_processingfees[0]['payment_processing_extra'];
                $total = (( $donatevalue * $additional / 100 ) + $extra + $donatevalue);

				$fslug = $this->input->post('hidfslug');
				$pslug = $this->input->post('hidpslug');
				$data = array(
				'number'	  => $this->input->post('card_number'),
				'exp_month'   => $this->input->post('card_expiry_month'),
				'exp_year'	  => $this->input->post('card_expiry_year'),
				/*'amount'	=> ($this->input->post('donate_amt')*100),*/
				'amount'	  => number_format($total, 2, '.', '') * 100,
				"description" => $this->input->post('donor_statement'),
				'token'		  => $this->input->post('stripeToken'),
				'email'		  => $this->input->post('donor_email')   
				);
				$return = $this->stripegateway->checkout($data);
				if(is_array($return)){
				if($return['status']=='succeeded'){
					
					$finfo = $this->Frontplayer_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
					$pinfo = $this->Frontplayer_model->view('tbl_player',array('player_slug_name'=>$pslug));
					$cinfo = $this->Frontplayer_model->get_activation_code();

					if(count($cinfo)>0){
					$code_id = $cinfo[0]['id'];
					$activation_code = $cinfo[0]['activation_code'];	
					}else{
					$code_id = 0;
					$activation_code = '';	
					}

					$transaction_id		= $return['balance_transaction'];
					$card_type			= $return['source']['brand'];
					$card_last4			= $return['source']['last4'];
					$fundraiser_id		= $finfo[0]['id'];
					$fundraiser_slug	= $finfo[0]['fund_slug_name'];
					//$fundraiser_name	= $finfo[0]['fund_fname'].' '.$finfo[0]['fund_lname'];
					$fundraiser_name	= $finfo[0]['fund_username'];
					$fund_donor_statement	= $finfo[0]['fund_donor_statement'];
					$fundraiser_slog		= $finfo[0]['fund_slogan'];
					$fundraiser_bgcolor		= $finfo[0]['fund_color_rgb'];
					if($finfo[0]['fund_image']==""){
					$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
					}else{
					$fundTimg 		= explode(".",$finfo[0]['fund_image']);	
					$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
					}
					
					$fundraiser_taxid	= (trim($finfo[0]['fund_beneficiary_tax_id'])!='')?$finfo[0]['fund_beneficiary_tax_id']:'Not Provided';
					$player_id			= $pinfo[0]['id']; 
					$player_slug		= $pinfo[0]['player_slug_name']; 
					$player_first		= $pinfo[0]['player_fname']; 
					$player_last		= $pinfo[0]['player_lname'];
                    $payment_processing_fee		= $settings_processingfees[0]['payment_processing_fee'].'%';
                    $payment_processing_extra		= '$'.$settings_processingfees[0]['payment_processing_extra'];

					$firstname			= $this->input->post('firstname');
					$lastname			= $this->input->post('lastname');
					$donor_addr			= $this->input->post('donor_addr');
					$donor_city			= $this->input->post('donor_city');
					$donor_state		= $this->input->post('donor_state');
					$donor_zip			= $this->input->post('donor_zip');
					$donor_phone		= $this->input->post('donor_phone');
					$card_number		= $this->input->post('card_number');
					$short_card_number	= $this->short_card_number($card_number);
					$donor_email		= $this->input->post('donor_email');
					$donate_amt			= number_format($total, 2, '.', '');
					$donate_datetime	= date("d M Y H:i:s");
					$start_fundraiser	= 'https://www.ThanksForSupporting.com';
					
					$dt_array = array(
						'txn_id'	  => $transaction_id,
						'fund_id'	  => $fundraiser_id,
						'fund_slug'	  => $fundraiser_slug,
						'player_id'	  => $player_id,
						'player_slug' => $player_slug,
						'code_id'	  => $code_id,
						'used_code'	  => $activation_code,
						'donation_amt'=> $donate_amt,
                        'donation'=> $donatevalue,
                        'payment_processing_fee'=> $additional,
                        'payment_processing_extra'=> $extra,
						'donor_fname' => $firstname,
						'donor_lname' => $lastname,
						'donor_email' => $donor_email,
						'donor_addr'  => $donor_addr,
						'donor_city'  => $donor_city,
						'donor_state' => $donor_state,
						'donor_zip'	  => $donor_zip,
						'donor_phone' => $donor_phone,
						'donation_date'=>date("Y-m-d H:i:s")
					);
					
					$this->Frontplayer_model->add('tbl_donors_payment',$dt_array);
					
					$inserted_id = $this->Frontplayer_model->last_insert_id();
					$this->session->set_userdata('donation_id',$inserted_id);
					$this->session->set_userdata('suc_msg','Thank you for your donation');
					$this->session->set_userdata('suc_msg1','Your transaction was successful. A confirmation message has been sent to your email');
					
					$temp_dt = $this->Frontplayer_model->view('tbl_mail_template',array("template_slug"=>'donation-receipt-to-donor-from-tfs'));
					
					$recpt_html_header	= $temp_dt[0]['template_header'];

					$recpt_html_footer	= $temp_dt[0]['template_footer'];
					$temp_subject	= $temp_dt[0]['template_subject'];
					$temp_body		= $recpt_html_header.html_entity_decode($temp_dt[0]['template_content']).$recpt_html_footer;
					$search_key 	= array('[FundraiserSlogan]','[FundraiserImage]','[DonorFirst]','[DonorLast]','[FundraiserName]','[Fundraiser TaxID]','[PlayerFirst]','[PlayerLast]','[DonationAmount]','[DonationDateTime]','[CreditCardType]','[LastFourDigits]','[TransactionID]','[DonationFromName]','[StartFundraiser]','[PaymentProcessingFee]','[PaymentProcessingFeeExtra]','[DonorCreditCardStatement]','[selected_color]');
					$search_val		= array($fundraiser_slog,$fundimg,$firstname,$lastname,$fundraiser_name,$fundraiser_taxid,$player_first,$player_last,$donate_amt,$donate_datetime,$card_type,'('.$short_card_number.')',$transaction_id,$firstname.' '.$lastname,$start_fundraiser,$payment_processing_fee,$payment_processing_extra,$fund_donor_statement,$fundraiser_bgcolor);
					$mail_content	= str_replace($search_key,$search_val,$temp_body);
					
					$config = array();
					$config['useragent']= "CodeIgniter";
					$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config['protocol'] = "smtp";
					$config['smtp_host']= "localhost";
					$config['smtp_port']= "25";
					$config['mailtype'] = 'html';
					$config['charset']	= 'iso-8859-1';
					$config['newline']  = "\r\n";
					$config['wordwrap'] = TRUE;
					
					$this->email->initialize($config);   
					$this->email->from('support@thanksforsupporting.com','ThanksForSupporting');  
					$this->email->to($donor_email); 
					$this->email->subject("ThanksForSupporting - ".$temp_subject); 
					$this->email->message($mail_content);
					$bval=$this->email->send();
					
					$temp_dt1		= $this->Frontplayer_model->view('tbl_mail_template',array("template_slug"=>'thank-you-gift-to-donor-from-gca'));

					$temp_subject1	= $temp_dt1[0]['template_subject'];
					$gift_html_header	= $temp_dt1[0]['template_header'];
					$gift_html_footer	= $temp_dt1[0]['template_footer'];
					$temp_body1		= html_entity_decode($temp_dt1[0]['template_content']);
					$search_key1 	= array('[FundraiserImage]','[DonorFirst]','[DonorLast]','[FundraiserName]','[ActivationCode]');
					$search_val1	= array($fundimg,$firstname,$lastname,$fundraiser_name,$activation_code);
					$mail_content1	= $gift_html_header.str_replace($search_key1,$search_val1,$temp_body1).$gift_html_footer;

					$config1 = array();
					$config1['useragent']= "CodeIgniter";
					$config1['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config1['protocol'] = "smtp";
					$config1['smtp_host']= "localhost";
					$config1['smtp_port']= "25";
					$config1['mailtype'] = 'html';
					$config1['charset']	= 'iso-8859-1';
					$config1['newline']  = "\r\n";
					$config1['wordwrap'] = TRUE;
					
					$this->email->initialize($config1);   
					$this->email->from('support@geocouponalerts.com','GeoCouponAlerts');  
					$this->email->to($donor_email); 
					$this->email->subject("GeoCouponAlerts - ".$temp_subject1); 
					$this->email->message($mail_content1);
					$bval1=$this->email->send();
					if($bval1){
						$where_upc = array('id'=>$code_id);
						$cdt_arrayup = array('consumed' => 1,'consumed_by' => $inserted_id,'consumed_dt' => date("Y-m-d H:i:s"));
						$this->Frontplayer_model->edit('tbl_activation_codes',$cdt_arrayup,$where_upc);	
					}

					if(isset($msg_id) && $msg_id!='')
					{
						$where_up = array('id'=>$msg_id);
						$check_msg = $this->Frontplayer_model->view_check('tbl_email_share_reports',$where_up);
						if($check_msg > 0)
						{
						$dt_arrayup = array('donated' => 1,'donated_time' => date("Y-m-d H:i:s"));
						$this->Frontplayer_model->edit('tbl_email_share_reports',$dt_arrayup,$where_up);
						}
					}
					//redirect($fslug.'/'.$pslug.'/thanks/'.$inserted_id);					
					/*$this->load->view('../../includes_front_player/header_default_payment',$this->data);
					$this->load->view('view_success',$this->data);
					$this->load->view('../../includes_front_player/footer_default_payment',$this->data);*/
					$data['status'] = 1;
		            echo json_encode($data);
				  }
				}else{
                    //$this->data['error_con']  = $return;
					$data['status']    = 0;
					$data['error_con'] = $return;					
		            echo json_encode($data);
					/*$this->load->view('../../includes_front_player/header_default_payment',$this->data);
					$this->load->view('view_error',$this->data);
					$this->load->view('../../includes_front_player/footer_default_payment',$this->data);*/
				}
			}else{
				//$this->data['error_con']  = 'Token Expired';
				$data['status']    = 0;
				$data['error_con'] ='Token Expired';					
		        echo json_encode($data);
				/*$this->load->view('../../includes_front_player/header_default_payment',$this->data);	
				$this->load->view('view_error',$this->data);
				$this->load->view('../../includes_front_player/footer_default_payment',$this->data);*/
			}
		
	}	
	public function defPayFailureSection(){
		$this->data['error_con']  = $this->input->post('error_con');
		$this->load->view('../../includes_front_player/header_default_payment',$this->data);
		$this->load->view('view_error',$this->data);
		$this->load->view('../../includes_front_player/footer_default_payment',$this->data);
	}
	public function defPaySucSection(){
		$this->load->view('../../includes_front_player/header_default_payment',$this->data);
		$this->load->view('view_success',$this->data);
		$this->load->view('../../includes_front_player/footer_default_payment',$this->data);	
	}
	public function defaultPaymentInfo(){
		/*$this->data['found_flag']    = '';
		$this->data['error_con']  = 'Token Expired';
		$this->load->view('../../includes_front_player/header_default_payment',$this->data);        
		$this->load->view('view_error',$this->data);
		$this->load->view('../../includes_front_player/footer_default_payment',$this->data);*/
		$data['error_con'] = 'Token Expired';
		$data['status'] = 0;
		echo json_encode($data);
	}
	//-----------
	// Ends here
	//-----------
}