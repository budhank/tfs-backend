<?php //echo $active_page; ?>
</div>
<div class="div_loading" id="div_loading" style="display:none">loading...</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {   
        
        var active_tab='<?php if($active_page){echo $active_page;} ?>';        
        if(active_tab == 'dboard')
        {
            $('#act_dboard').addClass('active');
            $('#act_fundraiser').removeClass('active');
			$('#act_etemplate').removeClass('active');
			$('#act_payment').removeClass('active');
			$('#act_settings').removeClass('active');
        }
        else if(active_tab == 'fundraiser')
        {
            $('#act_fundraiser').addClass('active');
			$('#act_dboard').removeClass('active');
			$('#act_etemplate').removeClass('active');
			$('#act_payment').removeClass('active');
			$('#act_settings').removeClass('active');	
        }
		else if(active_tab == 'emailtemp')
        {
            $('#act_etemplate').addClass('active');
			$('#act_fundraiser').removeClass('active');	
			$('#act_dboard').removeClass('active');
			$('#act_payment').removeClass('active');
			$('#act_settings').removeClass('active');
        }
		else if(active_tab == 'payments')
        {
            $('#act_payment').addClass('active');	
			$('#act_etemplate').removeClass('active');
			$('#act_fundraiser').removeClass('active');
			$('#act_dboard').removeClass('active');
			$('#act_settings').removeClass('active');
        }
		else if(active_tab == 'settings')
        {
            $('#act_settings').addClass('active');	
			$('#act_payment').removeClass('active');	
			$('#act_etemplate').removeClass('active');
			$('#act_fundraiser').removeClass('active');
			$('#act_dboard').removeClass('active');
        }
        /*else if(active_tab == 'codes')
        {
            $('#business').removeClass('active');
            $('#user').removeClass('active');
            $('#report').removeClass('active');
            $('#codes').addClass('active');	
        }*/    
        //var active_tab='<? //if(isset($array2['active_page'])){ echo $array2['active_page']; } else if($active_page){echo $active_page;} ?>';
    });
</script>
</body>
</html>
