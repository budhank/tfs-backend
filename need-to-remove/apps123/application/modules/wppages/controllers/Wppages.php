<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wppages extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('Wppages_model');
	}
	
	public function index(){
		redirect('admin/login');
	}
	
	public function get_page()
	{
		
	    $this->load->helper('file');
		$this->load->helper('wp_pages_helper');
		$page_id = base64_decode($this->input->post('page_id'));
		echo get_page_content($page_id);
	}
}
