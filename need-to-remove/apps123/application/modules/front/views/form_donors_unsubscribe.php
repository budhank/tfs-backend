<div class="add_plr_wrp">
	<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 text-center">
			<div class="frm">
				<h2 class="title">I have been Unsubscribed</h2>
			</div>
		</div>
	</div>
	<hr>
	<div class="frm_btn_grp text-center">
		<a href="<?php echo base_url() . "admin/"; ?>" class="btn-unsub">Ok</a>
	</div>
	</div>
</div>
<style>
.btn-unsub{ 
	background: #000;
    color: #fff;
    padding: 10px 30px;
	border-radius: 6px;	
    font-size: 17px;
    text-transform: uppercase;
}
.btn-unsub:hover{
	background: #444;
	color: #fff;
}
</style>