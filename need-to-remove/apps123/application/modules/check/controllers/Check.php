<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Check extends CI_controller{

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$id = $this->session->userdata('id');
		/*if(empty($id)){
		redirect('admin/login');
		}*/
	}
	   
    public function index(){
		redirect('admin/login');
	}
	
	public function start_fundraiser(){
		$this->load->model('admin/admin_model');
		$a=$this->session->userdata('err_msg');
		if(isset($a)){
		$data['err_msg']=$a;
			$this->session->unset_userdata('err_msg');
		}else{
			$data['err_msg']=0;
		}
		if(isset($_POST["signup_btn"])){
			$fname		= $this->input->post('fname');
			$lname		= $this->input->post('lname');
			$username	= $this->input->post('username');
			$email 		= $this->input->post('email');
			$password1	= $this->input->post('password');
			$password	= $this->input->post('password');
			
			$this->form_validation->set_rules('username', 'User Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('password', 'Last Name', 'trim|required|xss_clean');
			if ($this->form_validation->run() != FALSE)
			{
				$encrypt_key = $this->config->item('encryption_key');
				$password    = $this->encrypt->encode($password, $encrypt_key);
				
				$this->admin_model->add('tbl_fundraiser',array(
				'fund_fname'=>$fname,
				'fund_lname'=>$lname,
				'fund_username'=>$username,
				'fund_email'=>$email,
				'fund_pass'=>$password,
				'slug_change'=>'1'
				));
				
				//-------------------------------------------  
				//Fundraiser Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($username);  
				
				$string 	   = str_replace(' ', '-', $fname_slug); // Replaces all spaces with hyphens.
				$slug		   = preg_replace('/[^A-Za-z0-9\-#$&^*()+=!~\']/', '', $string); // Removes special chars.
				$jSlug		   = strtolower(rtrim($slug,'-'));
				$bgcolor	   = '#581b6c';
				$selected_color	= '#581b6c';
				$fontcolor	   = '#fff';

				$fundURL = url_title($jSlug);
				if($this->admin_model->isUrlExists('tbl_fundraiser',$fundURL)==1){
				   $fundURL = $fundURL.'-'.$lastInsId; 
				}
				//$fund_slug_name = $fundURL;
				$x = $this->db->update('tbl_fundraiser', array('fund_slug_name'=>$fundURL), array('id'=>$lastInsId));
				//---------------------------------------------------------------------------------------------------
				if($x)
				{
					$admin_details 		= $this->admin_model->view('tbl_admin',array('id'=>1));
					$admin_email 		= $admin_details[0]["contact_email"];
					$template 			= $this->admin_model->view('tbl_mail_template',array('template_slug'=>'create-fundraiser'));
					$login_url			= base_url("admin/login");
					$start_fundraiser	= base_url("start-fundraiser");
					$template_head		= $template[0]["template_header"];
					$template_body		= html_entity_decode($template[0]["template_content"]);
					$template_foot		= $template[0]["template_footer"];
					$template_subject	= $template[0]["template_subject"];
					
					
					$search_key 		= array('[FundraiserFirst]','[FundraiserEmail]','[Password]','[LoginUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
					$search_val			= array($fname,$email,$password1,$login_url,$start_fundraiser,$bgcolor,$fontcolor,$selected_color);
					$email_content		= $template_head.str_replace($search_key,$search_val,$template_body).$template_foot;
					
					$config = array();
					$config['useragent']= "CodeIgniter";
					$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
					$config['protocol'] = "mail";
					//$config['smtp_host']= "ssl://smtp.gmail.com";
					//$config['smtp_port']= "465";
					$config['mailtype'] = 'html';
					$config['charset']	= 'iso-8859-1';
					$config['newline']  = "\r\n";
					$config['wordwrap'] = TRUE;
					
					$this->email->initialize($config);   
					$this->email->from($admin_email);  
					$this->email->to($email); 
					$this->email->subject($template_subject); 
					$this->email->message($email_content);
					$bval=$this->email->send();
					if($bval){
						redirect('admin/login');
					}
				}
			}
		}

		$this->load->view('../../includes/header2');
		$this->load->view('view_create_fundraiser',$data);
	}
	
	public function forget_password(){
		$this->load->model('admin/admin_model');
		$data['aaaa'] = '';
		if(isset($_POST["forget_btn"])){
			$email 		= $this->input->post('email');
			$password 	= time();
			$slug		= $this->input->post('login_slug');
			$this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email|xss_clean');
			if ($this->form_validation->run() != FALSE)
			{
				$encrypt_key = $this->config->item('encryption_key');
				$query		=	$this->admin_model->all_user_login_check($email,$slug);
				if(count($query)>0)
				{
					if($query[0]['role']=='fundraiser'){
						$slugf	=	str_replace("-fundraiser","",$slug);
						$where	=	array('fund_email' => $email,'fund_slug_name' => $slugf);
						$query	=	$this->admin_model->encry_login_check('tbl_fundraiser',$where);

						if(count($query)>0){
							$encrypt_key = $this->config->item('encryption_key');
							$password1   = $this->encrypt->encode($password, $encrypt_key);
							$xx = $this->admin_model->edit('tbl_fundraiser',array('fund_pass'=>$password1),array('fund_email' => $email,'fund_slug_name' => $slugf));
							
							if($xx)
							{
								$login_url			= base_url("admin/login");
								$email_content		= "Hi, $email <br/> Your password is $password. Click <a href='".$login_url."'> HERE </a> to login and use your password.";
								
								$config = array();
								$config['useragent']= "CodeIgniter";
								$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
								$config['protocol'] = "smtp";
								$config['smtp_host']= "localhost";
								$config['smtp_port']= "25";
								$config['mailtype'] = 'html';
								$config['charset']	= 'iso-8859-1';
								$config['newline']  = "\r\n";
								$config['wordwrap'] = TRUE;
								
								$this->email->initialize($config);   
								$this->email->from('login@thanksforsupporting.com');  
								$this->email->to($email); 
								$this->email->subject("ThanksForSupporting - Forget Password Request"); 
								$this->email->message($email_content);
								$bval=$this->email->send();
								$this->session->set_userdata('succ_msg',"Password reset successfully. Please check your email !");
							}
						}else{
							$this->session->set_userdata('err_msg', 'Invalid Email Address');	
						}
						
					}elseif($query[0]['role']=='player'){
						$slugp	=	str_replace("-player","",$slug);
						$where	=	array('player_email' => $email,'player_slug_name' => $slugp);
						$query	=	$this->admin_model->encry_login_check('tbl_player',$where);
						
						if(count($query)>0){
							$encrypt_key = $this->config->item('encryption_key');
							$password1   = $this->encrypt->encode($password, $encrypt_key);
							$xx = $this->admin_model->edit('tbl_fundraiser',array('fund_pass'=>$password1),array('player_email' => $email,'player_slug_name' => $slugp));
							if($xx){
								$login_url			= base_url("admin/login");
								$email_content		= "Hi, $email <br/> Your new password is $password. Click <a href='".$login_url."'> HERE </a> to login and use your new password.";
								
								$config = array();
								$config['useragent']= "CodeIgniter";
								$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
								$config['protocol'] = "smtp";
								$config['smtp_host']= "localhost";
								$config['smtp_port']= "25";
								$config['mailtype'] = 'html';
								$config['charset']	= 'iso-8859-1';
								$config['newline']  = "\r\n";
								$config['wordwrap'] = TRUE;
								
								$this->email->initialize($config);   
								$this->email->from('login@thanksforsupporting.com');  
								$this->email->to($email); 
								$this->email->subject("ThanksForSupporting - Forget Password Request"); 
								$this->email->message($email_content);
								$bval=$this->email->send();
								$this->session->set_userdata('succ_msg',"Password reset successfully. Please check your email !");
							}else{
								$this->session->set_userdata('err_msg', 'Invalid Email Address');
							}
						}else{
							$this->session->set_userdata('err_msg', 'Invalid Email Address');
						}
					}else{
						$this->session->set_userdata('err_msg', 'Invalid Email Address');
					}
				}else{
					$this->session->set_userdata('err_msg', 'Invalid Email Address');
				}
			}else{
				$this->session->set_userdata('err_msg', 'Invalid Email Address');
			}
		}
		$this->load->view('../../includes/header2');
		$this->load->view('view_forget_password',$data);
	}
	
	public function login(){

	    if($this->session->userdata('id')!="" || $this->session->userdata('fundraiser_id')!="" ||  $this->session->userdata('player_id')!="") {
            redirect('admin/home');
        }
	     if($this->session->userdata('fundraiser_id')!="" && $this->session->userdata('id')=="" )
            redirect($this->session->userdata('fundraiser_urlname').'/admin/home');

         if($this->session->userdata('player_id')!="" && $this->session->userdata('id')=="" && $this->session->userdata('fundraiser_id')=="")
            redirect($this->session->userdata('fundraiser_urlname').'/'.$this->session->userdata('player_urlname').'/admin/home');

		$a=$this->session->userdata('err_msg');
		if(isset($a)){
		   $data['err_msg']=$a;
		   $this->session->unset_userdata('err_msg');
	     }else{
		   $data['err_msg']=0;
		 }
		$this->load->view('../../includes/header2');
		$this->load->view('view_login',$data);
	}
	
	public function validUser(){
		$this->load->model('admin/admin_model');
		$html = '';
		$username=$this->input->post('username');
		$where=array('username' => $username);
		$query=$this->admin_model->all_user_check($username);
		$valid = '';
		if(count($query) > 0)
		{
			$html .= '<div class="input-group" style="margin-bottom: 40px;">
						<label>Password <span>(min.6 char)</span></label>
						<input id="password" type="password" class="form-control required pass" name="password"/>
					</div>';
			if(count($query) > 1)
			{		
			$html .= '<div class="input-group">
						<label>User Type</label>
						<select id="login-slug" class="form-control required" name="login_slug">
						<option value=""> - Select User - </option>';
			foreach($query as $qry){
			$html .= '<option value="'.$qry["slug"].'">'.$qry["name"].' ('.ucfirst($qry["role"]).')</option>';	
			}
			$html .= '</select>
					</div>';
			}
			else{
			$html .= '<input type="hidden" name="login_slug" value="'.$query[0]["slug"].'">';
			}
			$html .= '<div class="form-group">
						<div class="col-sm-12 controls">
							<input type="submit" name="login_btn" id="login_btn" class="btn4" value="LOGIN" />
						</div>
					</div>
					<div class="log-foot blue-txt"><a href="'.base_url('forget-password').'">Forgot Password?</a></div>';
			$msg = '';
			$valid = 1;
		}else{
			$html .='<div class="form-group">
						<div class="col-sm-12 controls">
							<input type="button" name="signup_btn" id="btn-nxt" class="btn4" value="Verify" />
						</div>
					</div>
					<div class="log-foot blue-txt"><a href="'.base_url('forget-password').'">Forgot Password?</a></div>';
			$msg = 'Username or Password is wrong !';
			$valid = 0;
		}
		echo json_encode(array('msg'=>$msg,'html'=>$html,'valid'=>$valid)); 
	}
	
	public function verifyUser(){
		$this->load->model('admin/admin_model');
		$html = '';
		$email=$this->input->post('email');
		$where=array('email' => $email);
		$query=$this->admin_model->all_user_check_email($email);
		$valid = '';
		if(count($query) > 0)
		{
			$html .='<div class="input-group">
						<label>'.$email.' is valid email address.</label>
						<input type="hidden" name="email" id="email" class="form-control" value="'.$email.'"/>
					</div>';
			if(count($query) > 1)
			{		
				$html .= '<div class="input-group">
							<label>User Type</label>
							<select id="login-slug" class="form-control required" name="login_slug">
							<option value=""> - Select User - </option>';
				foreach($query as $qry){
				$html .= '<option value="'.$qry["slug"].'">'.$qry["name"].' ('.ucfirst($qry["role"]).')</option>';	
				}
				$html .= '</select></div>';
			}
			else{
				$html .= '<input type="hidden" name="login_slug" value="'.$query[0]["slug"].'">';
			}
			$html .='<div class="form-group">
						<div class="col-sm-12 controls">
							<input type="submit" name="forget_btn" id="forget_btn" class="btn3" value="CONFIRM"/>
						</div>
					</div>';
			$msg = '';
			$valid = 1;
		}else{
			$html .='<div class="input-group">
						<label>Email address</label>
						<input type="email" name="email" id="email" class="form-control"/>
					</div>
					<div class="form-group">
						<div class="col-sm-12 controls">
							<input type="button" name="verify_btn" id="verify_btn" class="btn3" value="Verify">
						</div>
					</div>';
			$msg = 'Email address is wrong !';
			$valid = 0;
		}
		echo json_encode(array('msg'=>$msg,'html'=>$html,'valid'=>$valid)); 
	}
	
	public function fund_username_check(){
		$this->load->model('admin/admin_model');
		$username = $this->input->post('username');
		if($username!=''){
			$query=$this->admin_model->fundraiser_username_check($username);
			if($query>0){
				echo 'false';
			}else{
				echo 'true';
			}	
		}else{
			echo 'false';
		}
	}
	
	public function fund_email_check(){
		$this->load->model('admin/admin_model');
		$email = $this->input->post('email');
		if($email!=''){
			$query=$this->admin_model->fundraiser_email_check($email);
			if($query>0){
				echo 'false';
			}else{
				echo 'true';
			}	
		}else{
			echo 'false';
		}
	}
	
	public function adminlogin(){ 

		$this->load->model('admin/admin_model');
		$encrypt_key = $this->config->item('encryption_key');


/*        $encrypted_password = 'ExbANn3ZxhtiMvk8wDayIEPRdeytc0Hix+cNp1EhEHiaNh2bogbTUu7521ysz9YfUrXLFjjExq7aFgBiINg6rQ==';


        $decrypted_string = $this->encrypt->decode($encrypted_password, $encrypt_key);

        echo $decrypted_string;
        exit;*/


		$username	=	$this->input->post('username');
		$password	=	$this->input->post('password');
		$slug		=	$this->input->post('login_slug');
		$query		=	$this->admin_model->all_user_login_check($username,$slug);
		if(count($query)>0){
			if($query[0]['role']=='admin'){
				
				$where=array('username' => $username);
				$query=$this->admin_model->encry_login_check('tbl_admin',$where);	          
				if(count($query)>0){
					$dpass = $query[0]['password'];
					if($password==$this->encrypt->decode($dpass,$encrypt_key)){
							$this->session->set_userdata('id', $query[0]['id']);
							$this->session->set_userdata('username', $query[0]['username']);
							$this->session->set_userdata('password', $query[0]['password']);
							$this->session->set_userdata('contact_email', $query[0]['contact_email']);
							redirect("admin/home");
					}else{
							$this->session->set_userdata('err_msg', 1);	
							redirect("admin/login");
					}
				}
				else{
					$this->session->set_userdata('err_msg', 1);	
					redirect("admin/login"); 
				}
			}elseif($query[0]['role']=='fundraiser'){
				$slugf	=	str_replace("-fundraiser","",$slug);
				$where	=	array('fund_email' => $username,'fund_slug_name' => $slugf);
				$query	=	$this->admin_model->encry_login_check('tbl_fundraiser',$where);

				if(count($query)>0)
				{
					$dpass = $query[0]['fund_pass'];  
					$fslug = $query[0]['fund_slug_name'];
					if($password==$this->encrypt->decode($dpass,$encrypt_key))
					{
						$this->session->set_userdata('fundraiser_id', $query[0]['id']);
						$this->session->set_userdata('fundraiser_fullname', $username);
						$this->session->set_userdata('fundraiser_urlname', $fslug);
						$this->session->set_userdata('fundraiser_username', $query[0]['fund_email']);
						$this->session->set_userdata('fundraiser_password', $query[0]['fund_pass']);
						$this->session->set_userdata('fundraiser_contact_email', $query[0]['fund_email']);
						redirect($fslug."/admin/home");
					}
					else
					{
						$this->session->set_userdata('err_msg', 1);	
						redirect("admin/login");
					}
				}
				else
				{
					$this->session->set_userdata('err_msg', 1);	
					redirect(base_url("admin/login"));
				}
				
			}elseif($query[0]['role']=='player'){
				$slugp	=	str_replace("-player","",$slug);
				$where	=	array('player_email' => $username,'player_slug_name' => $slugp);
				$query	=	$this->admin_model->encry_login_check('tbl_player',$where);
				$this->session->unset_userdata('player_id');
				if(count($query)>0){
					$dpass		= 	$query[0]['player_pass'];
					$pslug 		=	$query[0]['player_slug_name'];
					$fund_id	= 	$query[0]['player_added_by'];
					$queryf		=	$this->admin_model->view('tbl_fundraiser',array('id'=>$fund_id));
					$fslug		=	$queryf[0]['fund_slug_name'];
					
					if($password==$this->encrypt->decode($dpass,$encrypt_key)){
						$this->admin_model->edit('tbl_player',array('logged_in'=>1,'logged_dt'=>date("Y-m-d H:i:s")),array('player_slug_name'=>$pslug));
						$this->session->set_userdata('player_id', $query[0]['id']);
						$this->session->set_userdata('fundraiser_id', $fund_id);
						$this->session->set_userdata('player_fullname', $username);
						$this->session->set_userdata('player_urlname', $pslug);
						$this->session->set_userdata('fundraiser_urlname', $fslug);
						$this->session->set_userdata('player_username', $query[0]['player_email']);
						$this->session->set_userdata('player_password', $query[0]['player_pass']);
						$this->session->set_userdata('player_contact_email', $query[0]['player_email']);
						redirect($fslug.'/'.$pslug."/admin/home");
					}else{
						$this->session->set_userdata('err_msg', 1);	
						redirect("admin/login");
					}
				}else{
					$this->session->set_userdata('err_msg', 1);	
					redirect("admin/login");
				}
			}else{
				$this->session->set_userdata('err_msg', 1);	
				redirect("admin/login");	
			}
			
		}else{
			$this->session->set_userdata('err_msg', 1);	
			redirect("admin/login");
		}
	}
}