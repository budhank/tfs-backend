<style>

	#wrapper{padding-left:0 !important;}

		.login_frm{left:0 !important;margin: 0px auto 0 !important; width: auto !important;}

	.navbar{margin-bottom: 0 !important;}

	.blue-bg{ background:#161523;}

	.white-bg{ background:#fff;}
	#page-wrapper,.mainbox ,.container,.blue-bg, .white-bg{height:92vh}

</style>

<script src="<?php echo base_url('assets/admin/js/jquery.validate.js')?>"></script>

<script src="<?php echo base_url('assets/admin/js/additional-methods.js')?>"></script>

<div id="page-wrapper">

    <div class="mainbox login_frm log-page">

        <div class="container ">

			<div class="col-sm-5 blue-bg">

			<div class="log-txt log-gap">

			<div class="logo-log"><img src="http://thanksforsupporting.com/apps/assets/admin/images/Logo_Clear_white.png"></div>

				<h4>Login Now To: </h4>

				<ul class="log-ul-info">

					<li>1) &nbsp; &nbsp; &nbsp; Start a Fundraiser </li>

					<li>2) &nbsp; &nbsp; &nbsp;  Customize Your Webpage </li>

					<li>3) &nbsp; &nbsp; &nbsp; Invite Participants/Donors </li>

					<li>4) &nbsp; &nbsp; &nbsp;  View Donation Reports </li>

				</ul>

			</div></div>

			  <div class="col-sm-7 white-bg">

			<div class="log-sec log-gap">

           <h3 class="heading-log">

				

                <div class="panel-title">Login to Account</div>

            </h3>

			<?php

			$err = '';

			if(isset($err_msg) && $err_msg==1){

			$err = 'Username or Password is wrong !';

			unset($err_msg);

			}

			?>  

			<div class="err_msg_div" style="background-color:transparent; color:#f00;"><?php echo $err ?></div>

			<div class="panel-body">

				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

				<form id="loginform" name="loginform" class="form-horizontal" role="form" method="post" action="<?php echo base_url('login/adminlogin')?>">

				

					<div class="input-group">

						<label>Email address</label>

						<input id="username" type="text" class="form-control required" name="username"/>

					</div>

					<div id="pass-select-wrap">

					<div class="form-group">

						<div class="col-sm-12 controls">

							<input type="button" name="signup_btn" id="btn-nxt" class="btn4" value="Verify" />

						</div>

						<div class="log-foot blue-txt"><a href="<?php echo base_url('forget-password');?>" style="text-decoration: none;" >Forgot Password?</a></div>

					</div>

					</div>

				</form>

				<div class="log-foot">

				Don’t have an Account? Click here to 

				<a href="<?php echo base_url('start-fundraiser');?>" >Create an account</a>

				</div>

			</div>

        </div>

			</div>

			</div>

			

    </div>

</div>

<script type="text/javascript">

$(document).ready(function() {   

	$("#loginform").validate();

	$("#btn-nxt").click(function(){
		var sts = $("#loginform").valid();
		if(sts==true){
		$.ajax({

			type	: 'POST',

			url		: "<?php echo base_url('check/validUser');?>",

			data	: {username: $('#username').val()},

			dataType: 'JSON',

			success	: function (res) {
				$(".err_msg_div").html(res.msg);
				if(res.valid==1){
				$("#pass-select-wrap").html(res.html);
				}
			}
		});
		}
	});

//	//log-sec //white-bg
//
//	var hgt = $('.blue-bg').outerHeight();
//
//	$('.white-bg').css('min-height',hgt);
//
//	//console.log(hgt);

});

</script>    

