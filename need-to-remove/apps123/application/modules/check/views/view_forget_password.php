<style>
#wrapper{padding-left:0 !important;}
.login_frm{left:0 !important;margin: 0px auto 0 !important; width: auto !important;}
#page-wrapper,.mainbox ,.container,.blue-bg, .white-bg{height:92vh}	
.navbar{margin-bottom: 0 !important;}
.blue-bg{ background:#147dbe;}
.white-bg{ background:#fff;}
/*-------------------------------*/
</style>

<script src="<?php echo base_url('assets/admin/js/jquery.validate.js')?>"></script>

<script src="<?php echo base_url('assets/admin/js/additional-methods.js')?>"></script>

	<div class="container">

		<div class="col-sm-5 blue-bg">

			<div class="log-txt log-gap">

				<div class="logo-log"><img src="<?php echo base_url('assets/admin/images/Logo_Clear_white.png');?>"></div>

				<h4>Raising Money Has<br> Never Been So Easy </h4>

				<ul class="log-ul-info">

					<li>1) &nbsp; &nbsp; &nbsp;  Create an Account</li>

					<li>2) &nbsp; &nbsp; &nbsp; Complete the Fundraiser Form</li>

					<li>3) &nbsp; &nbsp; &nbsp; Invite Participants</li>

					<li>4) &nbsp; &nbsp; &nbsp; Receive Donations </li>

					<li>5) &nbsp; &nbsp; &nbsp; GIFT IS GIVEN TO DONORS </li>
					
				</ul>

			</div>
		</div>

		<div class="col-sm-7 white-bg">

			   <div class="log-sec log-gap">

				<h3 class="heading-log">

					<div class="panel-title" style="margin-bottom: 0;">Forget Password</div>

				</h3>

				<?php
				$text = '';
				$color = '';
				if($this->session->userdata('err_msg')){
				$text = $this->session->userdata('err_msg');
				$color = '#ff000';
				$this->session->unset_userdata('err_msg');
				}
				if($this->session->userdata('succ_msg')){
				$text = $this->session->userdata('succ_msg');
				$color = '#3ACA5F';
				$this->session->unset_userdata('succ_msg');
				}
				?>  

				<div class="err_msg_div" style="background-color:transparent; color:<?php echo $color; ?>;"><?php echo $text ?></div>

				<div class="panel-body">

					<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

					<form id="forgetform" name="forgetform" class="form-horizontal" role="form" method="post" action="<?php echo base_url('forget-password');?>">
						<div id="formforgot">
							<div class="input-group">
								<label>Email address</label>
								<input type="email" name="email" id="email" class="form-control"/>
							</div>
							<div class="form-group">
								<div class="col-sm-12 controls">
									<input type="button" name="verify_btn" id="verify_btn" class="btn3" value="Verify">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<div id="terms" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content terms">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {     

	$("#forgetform").validate({
		rules: {
			email: {
				required  : true,
				email: true,
			}
		}
	});
	
	$("#verify_btn").click(function(){
		var sts = $("#forgetform").valid();
		if(sts==true){
		$.ajax({

			type	: 'POST',

			url		: "<?php echo base_url('check/verifyUser');?>",

			data	: {email: $('#email').val()},

			dataType: 'JSON',

			success	: function (res) {
				$(".err_msg_div").html(res.msg);
				if(res.valid==1){
				$("#formforgot").html(res.html);	
				}
			}
		});
		}
	});
});
</script>
<script type="text/javascript">
function displayPage(page_id){
	var url = "<?php echo base_url('wppages/get_page');?>";
	$.ajax({
		type: 'POST',
		url: url,
		data: {page_id: page_id},
		success: function (msg) {
				$(".modal-body").html(msg);
				$("#terms").modal('show');
		}
	});
}
</script>   