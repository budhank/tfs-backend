<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Fundraiser</title>
    <link href="<?php echo base_url()?>assets/fundraiser/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fundraiser/css/jquery-ui.css">
    <link href="<?php echo base_url()?>assets/fundraiser/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/fundraiser/css/custom_style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" rel="stylesheet" type="text/css">
    
    <script src="<?php echo base_url()?>assets/fundraiser/js/jquery-3.1.1.min.js"></script>    
    <script src="<?php echo base_url()?>assets/fundraiser/js/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>assets/fundraiser/js/custom.js"></script>
    <script src="<?php echo base_url()?>assets/fundraiser/js/bootstrap.min.js"></script>
	<script  type="text/javascript">
    $(function () {
  	$('[data-toggle="tooltip"]').tooltip()
	})
	</script>
</head>
    <body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="<?php echo base_url()?>assets/admin/images/logo.png" alt=""></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li><a href="#"><i class="fa fa-bell-o"></i></a></li>
                <li><a href="#"><span class="cd">CD</span></a></li>
            </ul>
			<?php
			$fslug = $this->session->userdata('fundraiser_urlname');
			?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php if($this->session->userdata('fundraiser_id')!=""){ ?>
				<ul class="nav navbar-nav side-nav">
				<li <?php if($this->uri->segment(3)=="report"){ echo "class='active'";}?>>
				<a href="<?php echo base_url($fslug.'/admin/report')?>">Dashboard</a>
				</li>
				<!--<li <?php //if($this->uri->segment(3)=="players"){ echo "class='active'";}?>>
				<a href="<?php //echo base_url($fslug.'/admin/players')?>">Players</a>
				</li>-->
				<!--<li <?php if(in_array($this->uri->segment(3),array("home","organization","bankinfo","fundraiser","participants","startup"))){ echo "class='active'";}?>>
				<a href="<?php echo base_url($fslug.'/admin/home')?>">Setup Guide</a>
				</li>-->
				<!--<li <?php if(in_array($this->uri->segment(4),array("list","graph"))){ echo "class='active'";}?>>
				<a href="<?php echo base_url($fslug.'/admin/report/list');?>">Status of Invites</a>
				</li>
				<li <?php //if($this->uri->segment(3)=="manage-donations" || $this->uri->segment(3)=="manage-rewards"){ echo "class='active'";}?>>
				<a href="<?php //echo base_url($fslug.'/admin/manage-donations')?>">Manage Rewards</a>
				</li>-->
				<li <?php if($this->uri->segment(3)=="donors"){ echo "class='active'";}?>>
				<a href="<?php echo base_url($fslug.'/admin/donors');?>">Donors</a>
				</li>
				<li>
				<a href="<?php echo base_url($fslug.'/admin/logout')?>">Log Out</a>
				</li>
                </ul>
                <?php } ?>
            </div>
            <!-- /.navbar-collapse -->
        </nav>     
    