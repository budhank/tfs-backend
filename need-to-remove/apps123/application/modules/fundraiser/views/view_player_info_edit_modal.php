<?php

	if($pdetails['player_image']==""){
	$proimg =  base_url()."assets/images/noimage-150x150.jpg";   
	}else{
	$protimg = explode(".",$pdetails['player_image']);   
	$proimg =  base_url()."assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1];  
	}
?>

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Update Participants</h4>
	</div>
      <div class="modal-body">
		<form action="<?php echo base_url($fslug.'/admin/participants');?>" id="frm_participants_info" name="frm_participants_info" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="player_id" value="<?php echo $pdetails['id'];?>">
			<div class="form-group">
			<div style="margin:auto;">
			<span class="pic"><img style="width: 150px;" id="uploadPreview1" src="<?php echo $proimg;?>" /></span>
			</div>
			<div class="chng_up_file_wrp">
			<input style="margin: 0;" class="chng_up_file" id="uploadImage1" placeholder="Change" type="file" name="player_image" onchange="PreviewImage(1);" />
			<a style="right: initial;" href="JavaScript:Void(0);">Upload</a> 
			</div>
			</div>
			<div class="form-group"><label for="player_fname">First</label>
			<input type="text" id="player_fname" name="player_fname" class="form-control" value="<?php echo $pdetails['player_fname'];?>" ></div>
			<div class="form-group"><label for="player_lname">Last</label>
			<input type="text" id="player_lname" name="player_lname" class="form-control" value="<?php echo $pdetails['player_lname'];?>" ></div>
			<div class="form-group">
			<label for="player_email">Email Address</label>
			<input type="email" id="player_email" name="player_email" class="form-control" value="<?php echo $pdetails['player_email'];?>"></div>
			<div class="form-group"><input type="submit" name="participant_submit" value="Update" class="btn34"></div>
		</form>      
      </div>
   



