<script src="<?php echo base_url('assets/fundraiser/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/additional-methods.js')?>"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		<li><a href="<?php echo base_url($fslug.'/admin/players'); ?>">All Players</a></li>
		<li class="active"><?php echo $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]; ?></li>
		</ul>
		<?php include(APPPATH.'modules/fundraiser/views/view_top_section.php');?>
		<div class="tab_mnu">
		<ul>
		<li><a href="<?php echo base_url($fslug.'/admin/players/summary/'.$pslug);?>">SUMMARY</a></li>
		<li><a href="<?php echo base_url($fslug.'/admin/players/donors/'.$pslug);?>">DONATION/DONORS</a></li>
		<li><a href="<?php echo base_url($fslug.'/admin/players/emailreportgraph/'.$pslug);?>">EMAIL SHARE REPORTS</a></li>
		<li class="active"><a href="<?php echo base_url($fslug.'/admin/players/setting/'.$pslug);?>">SETTINGS</a></li>
		</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Settings</h2></div>
			</div>
			<div class="total_info">
			<?php
			$fundraiser_id = $this->session->userdata('fundraiser_id');
			$player_id = $player_details[0]["id"];
			$player_status = $player_details[0]["player_status"];
			if($player_status == 1)
			{
				$status = "Deactivate Profile";
				$sts = 0;
			}
			else
			{
				$status = "Activate Profile";
				$sts = 1;
			}
			?>
				<p><a href="javascript:void(0)" onclick="changestatus(<?php echo $fundraiser_id; ?>,<?php echo $player_id; ?>,<?php echo $sts;?>)" class="grn2"><?php echo $status; ?></a></p>
				<p><a href="#" class="grn2">Delete Profile</a></p>
			</div>
		</div>
	</div>
</div>
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function openProfile()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fslug.'/admin/getPlayerModalAjax'); ?>",
	data      :"pslug=<?php echo $pslug;?>",
	dataType  :"html",
	beforeSend: function(){
		$('#div_loading').show();
		$("#edit_pro").modal('show');
	},
	success: function(response){
		$('#div_loading').hide();
		$("#edit_pro").html(response);
		$("#edit_pro").modal('show');               
	}
    });
}
function changestatus(fid,pid,sts)
{
	$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fslug.'/admin/playerStatus'); ?>",
	data      :{fid:fid,pid:pid,sts:sts},
	dataType  :"json",
	success: function(response){ 
		if(response.valid == 1 )
		{
			location.reload();	
		}
		else
		{
			alert(response.msg);	
		}
	}
    });
}
</script>