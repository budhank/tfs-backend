<script src="<?php echo base_url('assets/fundraiser/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/additional-methods.js')?>"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		<li><a href="<?php echo base_url($fslug.'/admin/players'); ?>">All Players</a></li>
		<li class="active"><?php echo $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]; ?></li>
		</ul>
		<?php include(APPPATH.'modules/fundraiser/views/view_top_section.php');?>
		<div class="tab_mnu">
		<ul>
		<li><a href="<?php echo base_url($fslug.'/admin/players/summary/'.$pslug);?>">SUMMARY</a></li>
		<li class="active"><a href="<?php echo base_url($fslug.'/admin/players/donors/'.$pslug);?>">DONATION/DONORS</a></li>
		<li><a href="<?php echo base_url($fslug.'/admin/players/emailreportgraph/'.$pslug);?>">EMAIL SHARE REPORTS</a></li>
		<li><a href="<?php echo base_url($fslug.'/admin/players/setting/'.$pslug);?>">SETTINGS</a></li>
		</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Donations/Donors</h2></div>
			</div>
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:20%">Donor Name</th>
							<th style="width:20%">Amount</th>
							<th style="width:20%">Message</th>
							<th style="width:20%">Thanks Mail</th>
							<th style="width:20%" class="text-right">
							<a href="#" style="color: #333">
							<i class="fa fa-list-ul">&nbsp;&nbsp; Sort</i>
							</a>
							</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					if(count($donors_list)>0)
					{
					foreach($donors_list as $donors_li)
					{
					?>
						<tr>
							<td data-th="Donor Name"><?php echo $donors_li["donor_fname"].' '.$donors_li["donor_lname"];?></td>
							<td data-th="Amount"><strong>$<?php echo $donors_li["donation_amt"]; ?></strong></td>
							<td data-th="Message"></td>
							<td data-th="Mail">
							<a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($donors_li["id"]);?>" style="text-decoration:underline;">Send a Thank you Email</a>
							</td>
							<td data-th="" class="text-right">
							<small><?php echo date("d-m-Y H:i:s A",strtotime($donors_li["donation_date"]));?></small>
							</td>
						</tr>
					<?php
					}
					}
					else
					{
					?>
						<tr>
							<td colspan="5" class="text-center">No Donation Found</td>
						</tr>
					<?php	
					}
					?>
					</tbody>
					<tfoot>
					<?php 
					if(count($donors_list)>10)
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<a class="grn2" href="#">+Load more</a>
							</td>
						</tr>
					<?php	
					}
					else
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<span class="grn2">Thats all</span>
							</td>
						</tr>
					<?php
					}
					?>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<div id="mail_pro" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function openProfile()
{
$.ajax({
            type      :"POST",
            url       :"<?php echo base_url($fslug.'/admin/getPlayerModalAjax'); ?>",
            data      :"pslug=<?php echo $pslug;?>",
            dataType  :"html",
            beforeSend: function(){
				$('#div_loading').show();
                $("#edit_pro").modal('show');
            },
            success: function(response){
				$('#div_loading').hide();
                $("#edit_pro").html(response);
                $("#edit_pro").modal('show');               
            }
    });
}
function thanksent(ref)
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url($fslug.'/admin/getMailStructure'); ?>",
		data      :"pslug=<?php echo $pslug;?>&donor="+ref.rel,
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#mail_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#mail_pro").html(response);
			$("#mail_pro").modal('show');               
		}
    });
}
</script>