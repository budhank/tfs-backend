<script src="<?php echo base_url()?>assets/fundraiser/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/fundraiser/js/additional-methods.js"></script>
<style>
.youtube:before {
    content: "\f04b";
	font-family:"FontAwesome";
    position: absolute;
    width: 30px;
    height: 30px;
    z-index: 10;
	top:0;
	bottom:0;
	left:0;
	right:0;
	margin:auto;
	color:#F00;
	font-size:30px;
}
.rem, .fa.rem {
    position: absolute;
    right: 0;
    top: 0;
    heidght: 24px;
    width: 24px;
    line-height: 22px;
    background: #fff;
    color: #333;
    text-align: center;
    cursor: pointer;
}
</style>
<div id="page-wrapper">
    <div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		<li class="active">Profile</li>
		</ul>
        <div class="top_pro_wrp">
            <div class="clearfix">
                <?php 
				//echo '<pre>';print_r($fundraiserInfo);
                   if($fundraiserInfo[0]['fund_image']==""){
                    $proimg =  base_url()."assets/images/noimage-150x150.jpg";   
                   }else{
					$protimg = explode(".",$fundraiserInfo[0]['fund_image']);   
                    $proimg =  base_url()."assets/fundraiser_image/thumb/".$protimg[0].'_thumb.'.$protimg[1];   
                   } 
                   if($fundraiserInfo[0]['fund_feature_image']==""){
                    $featureimg =  base_url()."assets/images/no-image570x200.jpg";   
                   }else{
					$featuretimg = explode(".",$fundraiserInfo[0]['fund_feature_image']);     
                    $featureimg =  base_url()."assets/fundraiser_feature_image/thumb/".$featuretimg[0].'_thumb.'.$featuretimg[1];
                   }
                ?>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <div class="top_pro">
                        <div class="top_pro_img"><img src="<?php echo $proimg;?>" alt=""></div>
                        <div class="top_pro_con">
                            <h4>
                                <?php echo $fundraiserInfo[0]['fund_fname'].' '.$fundraiserInfo[0]['fund_lname'];?>
                            </h4>
                            <p>
                                <?php echo $fundraiserInfo[0]['fund_city'];?>,
                                <?php echo $fundraiserInfo[0]['state_ini'];?>
                            </p>
                        </div>
                    </div>
                </div>
				<?php
				$org_goal = $fundraiserInfo[0]['fund_org_goal'];
				$raised_amt = $fundraiserInfo[0]['raised_amt'];
				$remaining_goal = ($org_goal - $raised_amt);
				?>
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <div class="top_pro_prc">
                        <h4>
						<span class="glyphicon glyphicon-usd"></span> 
                        <?php echo number_format($org_goal);?>
                        </h4>
                        <p>Org Goal</p>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <div class="top_pro_prc">
                        <h4>
						<span class="glyphicon glyphicon-usd"></span> 
						<?php echo number_format($raised_amt);?>
						</h4>
                        <p>Raised</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                    <div class="top_pro_prc">
                        <h4>
						<span class="glyphicon glyphicon-usd"></span> 
						<?php echo number_format($remaining_goal);?>
						</h4>
                        <p>Remaining</p>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 text-right">
                    <div class="top_pro_donation">
                        <a href="javascript:void(0)" class="grn2 mar_t_15" onclick="openProfile();">Edit info</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="bg_wht mar_t_15 tot_pad" style="position:relative;">
                    <div class="tit2_sec clearfix nobor">
                        <div class="left">
                            <h3>Fundraiser Image / Video</h3>                            
                        </div>
                        <div class="left" style="padding-left:45px;">
                            <?php
							$base_msg = $this->session->userdata('base_msg');
                            if(isset($base_msg) && $base_msg!=""){
                			echo '<div style="color:#F00">'.$base_msg.'!</div>';
               	 			$this->session->set_userdata('base_msg','');
                			} 
                            ?>
					    </div>
                        <div class="right">
                        <!--<a href="javascript:void(0)" class="grn2" onclick="openFundFeatureImg();">Edit</a>-->
                        <a href="javascript:void(0)" class="grn2" onclick="openFundFeatureImgAdd();">Add Media</a>
                        </div>
                    </div>
                    <?php
					//echo "<pre>";
					//print_r($fundraiserImgInfo);
					//echo "</pre>";
					$str = '';
					if(count($fundraiserImgInfo)>0)
					{
						foreach($fundraiserImgInfo as $fval)
						{
							if($fval['media_type']=='I'){
								$protimg = explode(".",$fval['fund_feature_image']);   
								$proimg  = base_url()."assets/fundraiser_feature_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
								$str = '<img src="'.$proimg.'" class="thumbnail" />';
							}
							else{
								$str = '<a class="youtube" href="http://youtube.com/watch?v='.$fval['fund_feature_image'].'"><img src="https://img.youtube.com/vi/'.$fval['fund_feature_image'].'/mqdefault.jpg" class="thumbnail" /></a>';
							}
						?>
						<div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center" style="margin-top:15px;margin-bottom:15px;overflow:hidden;height:150px;">
							<?php echo $str;?>
						 <a href="javascript:void(0);" onclick="del_media(<?php echo $fval['id'];?>)" class="fa fa-times rem"></a>                 
						</div>
						<?php
						}
					}
					else
					{
						?>
                        <div style="text-align:center; color:#F00;">No image/video found!!</div>
                        <?
					}
					?>
                    <div class="clearfix"></div>
                </div>
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="tit2_sec clearfix nobor">
                        <div class="left">
                            <h3>Fundraiser Description</h3>
                        </div>
                        <div class="right">                            
                            <a href="javascript:void(0)" class="grn2" onclick="openFundDes();">Edit</a>
                        </div>
                    </div>
                    <div class="fund_con">
                        <p><?php echo html_entity_decode($fundraiserInfo[0]['fund_des']);?></p>                        
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="tit2_sec clearfix nobor">
                        <div class="left">
                            <h3>Fundraiser Dates</h3>
                        </div>
                        <div class="right">                            
                            <a href="javascript:void(0)" class="grn2" onclick="openFundDates();">Edit</a>
                        </div>
                    </div>
					<div class="fund_con">
                        <p>Start Date <span><?php if($fundraiserInfo[0]['fund_start_dt']!='0000-00-00'){ echo date('F\ j\, Y',strtotime($fundraiserInfo[0]['fund_start_dt'])); }else{ echo 'Nil';};?></span></p>
                        <p>End Date <span><?php if($fundraiserInfo[0]['fund_end_dt']!='0000-00-00'){ echo date('F\ j\, Y',strtotime($fundraiserInfo[0]['fund_end_dt'])); }else{ echo 'Nil';};?></span></p>
                    </div>
                </div>
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="tit2_sec clearfix nobor">
                        <div class="left">
                            <h3>Fundraiser Contact</h3>
                        </div>
                        <div class="right">                            
                            <a href="javascript:void(0)" class="grn2" onclick="openContact();">Edit</a> 
                        </div>
                    </div>
                    <div class="fund_con">
                        <p>Name <span><?php echo $fundraiserInfo[0]['fund_fname'].' '.$fundraiserInfo[0]['fund_lname'];?></span></p>
                        <p>Email Address <span><?php echo $fundraiserInfo[0]['fund_email'];?></span></p>
                        <p>Phone <span><?php echo $fundraiserInfo[0]['fund_contact'];?></span></p>
                    </div>
                </div>
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="tit2_sec clearfix nobor">
                        <div class="left">
                            <h3>Bank Details</h3>
                        </div>
                        <div class="right">
                            <a href="javascript:void(0)" class="grn2" onclick="openBankDetails();">Edit</a>
                        </div>
                    </div>
                    <div class="fund_con">
                        <p>Bank Name<span><?php echo ($fundraiserInfo[0]['fund_bank_name']!="")?$fundraiserInfo[0]['fund_bank_name']:'Nil';?></span></p>
                        <p>Bank Routing Number<span><?php echo ($fundraiserInfo[0]['fund_routing_number']!="")?$fundraiserInfo[0]['fund_routing_number']:'Nil';?></span></p>
                        <p>Bank Account Number<span><?php echo ($fundraiserInfo[0]['fund_account_number']!="")?$fundraiserInfo[0]['fund_account_number']:'Nil';?></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form name="hid_media_fm" id="hid_media_fm" action="" method="post">
<input type="hidden" id="hid_fid" name="hid_fid" value="" />
<input type="hidden" name="flug" value="<?php echo $fundraiserInfo[0]['fund_slug_name'];?>" />
</form>
<script type="text/javascript">
function openProfile()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/getFundraiserInfoOpenAjax'); ?>",
	data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
	dataType  :"html",
	beforeSend: function(){
		$("#fundInfoModal").modal('show');
		$('#div_loading').show();
	},
	success: function(response){
		$('#div_loading').hide();               
		$("#fundInfoModal").html(response);
		$("#fundInfoModal").modal('show');               
	}
    });
}
    
function openFundDates()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/getFundraiserDateOpenAjax'); ?>",
	data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
	dataType  :"html",
	beforeSend: function(){
		$("#fundDateModal").modal('show');
		$('#div_loading').show();
	},
	success: function(response){ 
		$('#div_loading').hide();               
		$("#fundDateModal").html(response);
		$("#fundDateModal").modal('show');               
	}
    });        
}
    
function openContact()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/getFundraiserContactOpenAjax'); ?>",
	data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
	dataType  :"html",
	beforeSend: function(){
		$("#fundContModal").modal('show');
		$('#div_loading').show();
	},
	success: function(response){ 
		$('#div_loading').hide();               
		$("#fundContModal").html(response);
		$("#fundContModal").modal('show');               
	}
    });      
}

function openBankDetails()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/getFundraiserBankDetailOpenAjax'); ?>",
	data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
	dataType  :"html",
	beforeSend: function(){
		$("#fundBankModal").modal('show');
		$('#div_loading').show();
	},
	success: function(response){ 
		$('#div_loading').hide();               
		$("#fundBankModal").html(response);
		$("#fundBankModal").modal('show');               
	}
    });	
}

function openFundDes()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/getFundraiserDesOpenAjax'); ?>",
	data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
	dataType  :"html",
	beforeSend: function(){
		$("#fundDesModal").modal('show');
		$('#div_loading').show();
	},
	success: function(response){ 
		$('#div_loading').hide();               
		$("#fundDesModal").html(response);
		$("#fundDesModal").modal('show');               
	}
    });	
}

function openFundFeatureImg()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/getFundraiserFetImgOpenAjax'); ?>",
	data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
	dataType  :"html",
	beforeSend: function(){
		$("#fundFetModal").modal('show');
		$('#div_loading').show();
	},
	success: function(response){ 
		$('#div_loading').hide();               
		$("#fundFetModal").html(response);
		$("#fundFetModal").modal('show');               
	}
    });	
}

function openFundFeatureImgAdd()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/getFundraiserFetImgOpenAddAjax'); ?>",
	data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
	dataType  :"html",
	beforeSend: function(){
		$("#fundFetModal").modal('show');
		$('#div_loading').show();
	},
	success: function(response){ 
		$('#div_loading').hide();               
		$("#fundFetModal").html(response);
		$("#fundFetModal").modal('show');               
	}
    });	
}
function del_media(v)
{
	var r = confirm("Are you sure to delete this image/video?");
	if (r == true) 
	{
		$('#hid_media_fm').attr('action', '<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/delMedia');?>');
		$('#hid_fid').val(v);
		$('#hid_media_fm').submit();
	}
}
</script>

<div id="fundInfoModal" class="modal fade" role="dialog"></div>
<div id="fundDateModal" class="modal fade" role="dialog"></div>
<div id="fundContModal" class="modal fade" role="dialog"></div>
<div id="fundBankModal" class="modal fade" role="dialog"></div>
<div id="fundDesModal" class="modal fade" role="dialog"></div>
<div id="fundFetModal"  class="modal fade" role="dialog"></div>