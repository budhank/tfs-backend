		<div id="page-wrapper">
				<div class="full_top_wrp bg_wht">
					<ul class="breadcrumb">
					  <li class="active">Profile</li>
					</ul>
					<div class="top_pro_wrp">
						<div class="clearfix">
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
								<div class="top_pro">
									<div class="top_pro_img"><img src="images/pro_pic2.jpg" alt=""></div>
									<div class="top_pro_con">
										<h4>Celtics</h4>
										<p>23 Nice Ave, Boston</p>
									</div>
								</div>
							</div>
							<div class="col-md-2 col-sm-12 col-xs-12">
								<div class="top_pro_prc">
									<h4><span class="glyphicon glyphicon-usd"></span> 3000</h4>
									<p>Org Goal</p>
								</div>
							</div>
							<div class="col-md-2 col-sm-12 col-xs-12">
								<div class="top_pro_prc">
									<h4><span class="glyphicon glyphicon-usd"></span> 1500</h4>
									<p>Raised</p>
								</div>
							</div>
							<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
								<div class="top_pro_prc">
									<h4><span class="glyphicon glyphicon-usd"></span> 1500</h4>
									<p>Remaining</p>
								</div>
							</div>
							<div class="col-lg-3 col-xs-12 text-right">
								<div class="top_pro_donation">
									<a href="#" class="grn2 mar_t_15" data-toggle="modal" data-target="#edit_pro_info">Edit info</a>
								</div>
							</div>
						</div>
					</div>
				</div>
          		<div class="container-fluid">
          			<div class="row">
          				<div class="col-md-7 col-sm-12 col-xs-12">
          					<div class="bg_wht mar_t_15 tot_pad">
          						<div class="tit2_sec clearfix">
									<div class="left"><h3>Fundraiser Image / Video</h3></div>
									<div class="right"><a href="#" class="grn2" data-toggle="modal" data-target="#edit_found_imgvdo">Edit</a></div>
								</div>
          						<div class="fund_img"><img class="img-responsive" src="images/fund_img1.jpg" alt=""></div>
         					</div>
         					<div class="bg_wht mar_t_15 tot_pad">
          						<div class="tit2_sec clearfix">
									<div class="left"><h3>Fundraiser Description</h3></div>
									<div class="right"><a href="#" class="grn2" data-toggle="modal" data-target="#edit_desc">Edit</a></div>
								</div>
         						<div class="fund_con">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
									in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
									in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
          						</div>
         					</div>
          				</div>
          				<div class="col-md-5 col-sm-12 col-xs-12">
          					<div class="bg_wht mar_t_15 tot_pad">
          						<div class="tit2_sec clearfix">
									<div class="left"><h3>Fundraiser Dates</h3></div>
									<div class="right"><a href="#" class="grn2" data-toggle="modal" data-target="#edit_date">Edit</a></div>
								</div>
         						<div class="fund_con">
									<p>Start Date <span>August 1, 2017</span></p>
         							<p>End Date <span>September 15, 2017</span></p>
          						</div>
         					</div>
         					<div class="bg_wht mar_t_15 tot_pad">
          						<div class="tit2_sec clearfix">
									<div class="left"><h3>Fundraiser Contact</h3></div>
									<div class="right"><a href="#" class="grn2" data-toggle="modal" data-target="#edit_found_contact">Edit</a></div>
								</div>
         						<div class="fund_con">
									<p>Name <span>John Lewis</span></p>
        							<p>Email Address <span>john.lewis@gmail.com</span></p>
         							<p>Phone <span>+61 556 789 568</span></p>
          						</div>
         					</div>
          				</div>
          			</div>
				</div>
        </div>
    