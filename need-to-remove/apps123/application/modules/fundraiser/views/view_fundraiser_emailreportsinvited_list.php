<script src="<?php echo base_url('assets/fundraiser/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/additional-methods.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/raphael-min.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/morris-0.4.1.min.js')?>"></script>
<style type="text/css">
.price_sec h1{line-height: 45px;}
.price_sec ul{
	text-align:center;
	margin-bottom:15px;
	}
.price_sec ul > li{
	display:inline-block;
	width:20%;
	padding:5px 0;
	border-right:1px solid #ccc;
}
.price_sec ul > li:last-child{
	border-right:none;
	}
.price_sec ul > li p{
	padding-bottom:0;
	text-transform: capitalize;
	color:#c4c4c4;
	font-weight:bold;
	}
.price_sec ul > li.active h1{
	color:red;
	}
.price_sec .p__bar_sec{
	padding: 0 15%;
	}
.price_sec .p__bar_sec p{
	padding-bottom:0;
	font-size: 12px;
	font-weight: bold;
	}
.price_sec .p__bar_sec .progress{
	margin-bottom:0;
	height:5px;
	border-radius:10px;
	}

.price_sec .p__bar_sec .progress .progress-bar-success{
	background-color:#7ad400;
	}
.morris-area-chart_sec{
	margin-top:15px;
	position:relative;
	}
.morris-area-chart_sec .chrt__overloop{
	position:absolute;
	left:0;
	top:0;
	width:100%;
	height:100%;
	background:rgba(244,244,244,0.8);
	z-index:2222;
	}
.morris-area-chart_sec .chrt__overloop > div{
	margin-top: 120px !important;
	}

.morris-area-chart_sec .chrt__over{
	width:100%;
	background:rgba(244,244,244,0.8);
	z-index:2222;
	}
.morris-area-chart_sec .chrt__over > div{
	margin-top: 120px !important;
	}
</style>
<?php
$fund_enddate		=	$fund_details[0]["fund_end_dt"];
$today_date			=	date("Y-m-d");
$goal				=	$fund_details[0]["fund_org_goal"];
if(count($total_payments)>0){
$raised				=	$total_payments[0]["raised_amt"];
}else{
$raised				=	0;	
}	

$remaining			=	$goal - $raised;

if($goal > 0){
$average_percent	=	ceil((($raised/$goal)*100));	
}else{
$average_percent	=	0;	
}
$ended_days_in 		=	ceil(strtotime($fund_enddate)-strtotime($today_date));
if($ended_days_in < 0)
{	
$campaign_msg		=	"<p>Campaign has ended</p>";
}
elseif($ended_days_in == 0)
{	
$campaign_msg		=	"<p>Campaign ends today</p>";	
}
elseif($ended_days_in == 1)
{	
$campaign_msg		=	"<p>Campaign ends tomorrow</p>";
}
else
{
$campaign_days_left =	ceil((strtotime($fund_enddate)-strtotime($today_date))/86400);
$campaign_msg		=	"<p>Campaign ends in ".$campaign_days_left." days</p>";	
}
?>
<div id="page-wrapper">
  <div class="container-fluid">
  <h5 style="margin-top:15px;">Fundraising Progress</h5>
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="price_sec">
        <ul>
          <li>
            <h1>$<?php echo number_format($goal);?></h1>
            <p>goal</p>
          </li>
          <li>
            <h1>$<?php echo number_format($raised);?></h1>
            <p>raised</p>
          </li>
          <li class="active">
			<h1>
			<?php 
			if($remaining < 0)
			{
			echo "$0";	
			}
			else
			{
			echo "$".number_format($remaining);	
			}
			?>
			</h1>
            <p>remaning</p>
          </li>
        </ul>
        <div class="p__bar_sec">
          <p><?php echo $average_percent; ?>% Funded</p>
          <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $average_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $average_percent; ?>%;"> </div>
          </div>
          <?php echo $campaign_msg; ?>
		  </div>
      </div>
    </div>
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="tit1_sec clearfix">
        <div class="left">

          <h2>Status of Contact Invites</h2>

        </div>
        <a href="#" class="grn2 right nobor" style="margin-top: 5px; margin-left: 10px;"><strong>Filters</strong></a>


          <div class="right"> 
		   <!-- <a href="<?php //echo base_url('/admin/fundraiser/'.$fslug.'/report/graph/invited');?>">Graph View</a>
		  <a href="<?php //echo base_url('/admin/fundraiser/'.$fslug.'/report/list/invited');?>" class="active">List View</a>
		  --> </div>


      </div>
      <div class="total_info">
        <div class="row">
          <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="total_info_con text-center">
              <p>Participants</p>
              <span><?php echo $report_chart["participants"];?></span> </div>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="total_info_con text-center">
              <p>Accounts</p>
              <span><?php echo $report_chart["accounts"];?></span> </div>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="total_info_con text-center">
              <p>Contacts</p>
              <span><?php echo $report_chart["contacts"];?></span> </div>
          </div>
		  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="total_info_con text-center">
              <p>Donors</p>
              <span><?php echo $report_chart["donors"];?></span> </div>
          </div>
		  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="total_info_con text-center">
              <p>Shares</p>
              <span><?php echo $report_chart["shares"];?></span> </div>
          </div>
        </div>
      </div>
      <div class="donations_tbl">
        <table class="table table-hover table-condensed">
          <thead>
            <tr>
              <th style="width:15%">Participants Name</th>
              <th style="width:10%">Contact</th>
              <th style="width:10%">Invited</th>
                <th style="width:10%">1st Reminder</th>
              <th style="width:10%">2nd Reminder</th>
              <th style="width:14%">Donated</th>
              <th class="text-right"> <a href="#" style="color: #333"><i class="fa fa-list-ul"></i>&nbsp;&nbsp; Sort</a> </th>
            </tr>
          </thead>
          <tbody>
			<?php
			if(count($report_list)==0)
			{
			?>
			<tr>
			<td colspan="9" ><div class="chrt__over">
			<div style="width:50%; margin:auto; text-align:center;">
			<h5>You haven't invited any of your contacts to donate yet. Invite your contacts to start receiving donations.</h5>
			<form action="<?php echo base_url($fslug.'/admin/participants'); ?>" method="POST">
			<input type="submit" name="info_submit" class="btn34" value="Invite Your Contacts" style="margin-top:12px; width:300px;padding:6px 25px;">
			</form>  
			</div>
			</div>
			</td>
			</tr>
			<?php
			}else{
			?>
			<?php
			if(count($report_list)>0){
			    //print_r($report_list);
				foreach($report_list as $report_li){
			?>
            <tr>
			<td style="vertical-align: top;" >

               <?php
                $sql   = "SELECT * from tbl_player_contacts where contact_added_by='".$report_li["player_slug_name"]."'";
                //echo $sql;
                $query = $this->db->query($sql);
                if($query->num_rows() > 0)
                {
                    $resultArrcontact = $query->result_array();
                    ?>
                    <table >
                       <tbody>
                        <?php
                        foreach($resultArrcontact as $key=>$value)
                        {

                            ?>
                            <tr><td><?=$report_li["player_fname"].' '.$report_li["player_lname"]?></td>

                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                <?php  } else{?>
                    <?php echo $report_li["player_fname"].' '.$report_li["player_lname"];?>
                <?php } ?>
            </td>
                <td>
                    <?php
                    $sql   = "SELECT * from tbl_player_contacts where contact_added_by='".$report_li["player_slug_name"]."'";
                    //echo $sql;
                    $query = $this->db->query($sql);
                    if($query->num_rows() > 0)
                    {
                        $resultArrcontact = $query->result_array();
                    ?>
                    <table >


                        <tbody>
                        <?php
                        foreach($resultArrcontact as $key=>$value)
                        {

                        ?>
                        <tr><td><?=$value['contact_email']?></td>

                        </tr>
                            <? } ?>
                        </tbody>
                    </table>
                    <?php  } else { echo "-";?>

                        <?php } ?>

                </td>
                <td style="vertical-align: top;">


                    <?php
                    $sql   = "SELECT * from tbl_player_contacts where contact_added_by='".$report_li["player_slug_name"]."'";
                    //echo $sql;
                    $query = $this->db->query($sql);
                    if($query->num_rows() > 0)
                    {
                        $resultArrcontact = $query->result_array();
                        ?>
                        <table >
                            <tbody>
                            <?php
                            foreach($resultArrcontact as $key=>$value)
                            {

                                ?>
                                <tr><td>

                                        <?=date('m-d-Y',strtotime($value["created"]));?>
                                    </td>

                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    <?php  } else { echo "-";?>

                    <?php } ?>


                </td>
                <td style="vertical-align: top;">
                    <?php
                    if($report_li["reminder"] == 1 && $report_li["reminder_dt"]!='0000-00-00 00:00:00'){
                        echo date("m-d-Y",strtotime($report_li["reminder_dt"]));
                    }else{ echo "-"; }
                    ?>
                </td>
                <td style="vertical-align: top;">
                    <?php
                    if($report_li["sec_reminder"] == 1 && $report_li["sec_reminder_dt"]!='0000-00-00 00:00:00'){
                        echo date("m-d-Y",strtotime($report_li["sec_reminder_dt"]));
                    }else{ echo "-"; }
                    ?>
                </td>


                <td style="vertical-align: top;">
                    <?php
                    $sql   = "SELECT * from tbl_player_contacts where contact_added_by='".$report_li["player_slug_name"]."'";
                    //echo $sql;
                    $query = $this->db->query($sql);
                    if($query->num_rows() > 0)
                    {
                        $resultArrcontact = $query->result_array();
                        ?>
                        <table >
                            <tbody>
                            <?php
                            foreach($resultArrcontact as $key=>$value)
                            {
                                ?>
                                <tr><td>
                                        <?php
                                $sql   = "SELECT * from tbl_email_share_reports where to_id='".$value["id"]."' and donated =1";
                                $query2 = $this->db->query($sql);
                                $resultArrcontact2 = $query2->result_array();

                                if($query2->num_rows() > 0)
                                {
                                    if($resultArrcontact2[0]["donated_time"]!='0000-00-00 00:00:00'){
                                        echo date("m-d-Y",strtotime($resultArrcontact2[0]["donated_time"]));
                                    }else{ echo "-"; }

                                }
                                else
                                        echo "-";
                                ?>

                                    </td>

                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    <?php  } else { echo "-";?>

                    <?php } ?>



                    </td>


			<td class="text-right"><small></small></td>
            </tr>
			<?php
			}
			}
			?>
			</tbody>
			<tfoot>
			<?php 
			/*if(count($report_list)>10)
			{
			?>
            <tr>
              <td colspan="9" class="text-center"><a class="grn2" href="#">+Load more</a></td>
            </tr>
			<?php	
			}
			else
			{*/
			?>
            <tr>
              <td colspan="9" class="text-center"><span class="grn2">Thats all</span></td>
            </tr>
			<?php
			/*}*/
			}
			?>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
