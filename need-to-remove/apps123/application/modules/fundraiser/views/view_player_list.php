<?php $fslug = $this->session->userdata('fundraiser_urlname'); ?>
		<div id="page-wrapper">
				<div class="full_top_wrp bg_wht">
					<ul class="breadcrumb">
					  <li class="active">Players</li>
					</ul>
				</div>
          		<div class="container-fluid">
          			<div class="bg_wht mar_t_15 tot_pad">
						<div class="tit1_sec clearfix">
						<div class="left"><h2>All Players</h2></div>
						<a href="<?php echo base_url($fslug.'/admin/players/add')?>" class="btn_round pull-right">Add new player</a>
						</div>
       					
        				<div class="donations_tbl">
        					<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<th style="width:20%">Player Name</th>
										<th style="width:12.5%" class="text-right">Goal</th>
										<th style="width:12.5%" class="text-right">Raised</th>
										<th style="width:12.5%" class="text-right">Remaining</th>
										<th style="width:18%" class="text-right">&nbsp;</th>
										<th style="width:8%" class="text-right">
										<a href="#" style="color: #333"><i class="fa fa-list-ul">&nbsp;&nbsp; Sort</i></a>
										</th>
									</tr>
								</thead>
								<tbody id="b_content">
								<?php
								if(count($players_list)>0)
								{
								foreach($players_list as $players_li)
								{
								if($players_li['player_image']==""){
								$proimg =  base_url()."assets/images/noimage-150x150.jpg";   
								}else{
								$protimg = explode(".",$players_li['player_image']);   
								$proimg =  base_url()."assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1];  
								} 
								$org_goal = $players_li['player_goal'];
								$raised_amt = $players_li['raised_amt'];
								$remaining_goal = ($org_goal - $raised_amt);
								?>
									<tr>
										<td data-th="Player Name">
										<span><img src="<?php echo $proimg;?>"/></span>
										<strong style="margin-top:10px;">
										<a href="<?php echo base_url($fslug.'/admin/players/summary/'.$players_li["player_slug_name"]);?>"><?php echo $players_li["player_fname"].' '.$players_li["player_lname"];?></a>
										</strong>
										</td>
										<td data-th="Goal" class="text-right">$<?php echo number_format($org_goal,2,'.',',');?></td>
										<td data-th="Raised" class="text-right">$<?php echo number_format($raised_amt,2,'.',',');?></td>
										<td data-th="Remaining" class="text-right">$<?php echo number_format($remaining_goal,2,'.',',');?></td>
										<td data-th="">
										<a href="<?php echo base_url($fslug.'/admin/players/asktoinvite/'.$players_li["player_slug_name"]);?>"><b>Invite</b></a> / 
										<a href="<?php echo base_url($fslug.'/admin/players/remindtoinvite/'.$players_li["player_slug_name"]);?>"><b>Reminder</b></a> / 
										<a href="<?php echo base_url($fslug.'/admin/players/secremindtoinvite/'.$players_li["player_slug_name"]);?>"><b>2nd Reminder</b></a>
										</td>
										<td data-th="" class="text-right"><small><?php echo date("M d, Y",strtotime($players_li["created"]));?></small></td>
									</tr>
								<?php
								}
								}
								else
								{
								?>
									<tr>
										<td colspan="6" class="text-center">No Records Found</td>
									</tr>
								<?php	
								}
								?>
								</tbody>
								<tfoot id="f_content">
								<?php
								if(count($players_list) >= 10)
								{
								?>
									<tr>
										<td colspan="6" class="text-center">
										<a class="grn2" id="ldmore" href="javascript:void(0);" onclick="loadmore();">Load more</a>
										</td>
									</tr>
								<?php
								}
								else
								{
								?>
									<tr>
										<td colspan="6" class="text-center"><a class="grn2" href="#">That's All</a></td>
									</tr>
								<?php
								}
								?>
								</tfoot>
							</table>
        				</div>
          			</div>
				</div>
        </div>
<script type="text/javascript">
var cnt = 0;
function loadmore()
{
cnt++;
$.ajax({
	url:'<?php echo base_url($fslug.'/admin/playerloadmore');?>',
	type:'POST',
	data:'limit='+cnt,            
	dataType:"text",
	beforeSend: function(){
	   $('#ldmore').text('loading...');
	},
	success:function(results){
		if(results!=0){
		$('#ldmore').text('Load more');
		$('#b_content').append(results);
		}
		else
		{
		$('#f_content').html('<tr><td colspan="6" class="text-center"><a class="grn2" href="#">That\'s All</a></td></tr>');
		}            
	}
});
}
</script> 