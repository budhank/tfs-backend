<form name="frm_popup_fund_fimg" id="frm_popup_fund_fimg" method="post" enctype="multipart/form-data" action="">
<input type="hidden" name="hid_ffid" id="hid_ffid" value="<?php echo $fundraiserInfo[0]['id'];?>">
<input type="hidden" name="hid_mtype" id="hid_mtype" value="I" />
				   
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Fundraiser image / video upload Section</h2></div>
                <div class="ms" style="display:none; padding-bottom:3px;"></div>
			</div>
			<div class="add_plr_wrp">
				<div class="row">
					<div class="col-xs-12">
						<div class="edit_found_img">
							<!--<img src="<?php //echo $featureimg;?>" class="img-responsive" alt="">-->
                            <div class="col-md-4 col-sm-3 col-xs-12">Select media type</div>
                            <div class="col-sm-4 col-xs-12">
                            <select name="sel_type" id="sel_type" class="form-control" onchange="setMedia(this.value)">
                            	<option value="I" selected="selected">Image</option>
                                <option value="V">Video</option>
                            </select>
                            </div>
                            <div class="col-xs-4 col-xs-offset-4" style="margin-top:10px;">
                            	<span id="sp_img">
								<input type="file" name="proimage" id="proimage" required/>
								</span>
                            	<span id="sp_you" style="display:none;">
                                	<input class="form-control" type="text" name="youtube_fil" id="youtube_fil" required/><i>aAdioIs17LM</i>
                                </span>
                                
                            </div>
						</div>
					</div>
				</div>
                <div id="sp_you_msg" style="text-align:left; padding-top:10px; color:#F00; font-size:11px;">
				<i>Image dimension must be greater than or equal to 600 x 600</i>
				</div>
				<hr>
				<div class="frm_btn_grp">
                    <button type="submit" name="Save" class="btn_round">Save</button>
                    <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
			</div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function ($) {
//VALIDATE USER EMAIL
$.validator.addMethod("imagedimension", function(value, element, params)
{
	var val_height = params[0];
	var val_width = params[1];
	
    var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0]);
		formData.append('valid_height', val_height);
		formData.append('valid_width', val_width); 
		
    var eReport = ''; //error report
	var sts = '';
    $.ajax(
    {
		url:"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/checkfile');?>", 
        type:'POST',
        data: formData,    
        contentType: false,            
        processData:false,     
        async:false,
        dataType:"json",
        success: function(res)
        {
            if (res.valid !== 'true')
            {
				sts = false;
            }
            else
            {
               sts = true;
            }
        },
        error: function(xhr, textStatus, errorThrown)
        {
            sts = false;
        }
    });
	return sts;

}, "Image Dimension must be greater than {0}x{1}.");
$("#frm_popup_fund_fimg").validate({
	rules: {
		proimage:{
		extension: "jpg|jpeg|png|gif",
		imagedimension: [600,600] //height and width
		},
	},
	messages:{
		 proimage:
		 {
			extension:"Please upload valid file type.",
		 }
	},
	submitHandler: function(form){
        var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0]);         
        formData.append('hid_fid', $('#hid_ffid').val()); 
        formData.append('hid_mtype', $('#hid_mtype').val()); 
		formData.append('youtube_fil', $('#youtube_fil').val());   
        $.ajax({
		url:"<?php echo base_url($fundraiserInfo[0]['fund_slug_name'].'/admin/fundraiserFeatureAddAjax');?>", 
        type:'POST',
        data: formData,    
        contentType: false,            
        processData:false,     
        dataType:"json",
        beforeSend: function(){
           // $('#div_loading').html('<div class="loading_inn">loading...</div>');
        },
        success:function(results){
			$('.ms').show();	
            if(results.msg==1){			
            $('.ms').html('Successfully added!!');
            setTimeout(function() {
                location.reload();
                }, 3000);
            }
            else{
                $('.ms').html('Insertion is failured');
            }            
        }
       });
	}
	});
});

function setMedia(v)
{
	if(v == 'V'){
		$("#proimage").attr('required','');
		$("#sp_img").hide();
		$("#sp_you").show();
		$("#hid_mtype").val(v);
		$("#sp_you_msg").html("<i>Copy the code that appears after '=' symbol from the youtube url, for example https://www.youtube.com/watch?v=aAdioIs17LM</i>");
		
	} else {
		$("#proimage").attr('required','');
		$("#sp_img").show();
		$("#sp_you").hide();
		$("#hid_mtype").val(v);
		$("#sp_you_msg").html("<i>Image dimension must be greater than or equal to 600 x 600</i>");
	}
}

</script> 
