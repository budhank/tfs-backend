<div class="row">
								<div class="col-sm-12">
								<div class="tit1_sec clearfix">
									<div class="left">
										<h2>Donations/Donors</h2>
									</div>
								</div>
                                </div>
                                <div class="col-sm-12">
								<div class="">
									<table class="table table-hover table-condensed">
										<thead>
											<tr>
												<th style="width:20%">Player</th>
												<th style="width:20%">Doner</th>
												<th style="width:20%">Amount</th>
												<th style="width:20%">Thanks Mail</th>
												<th style="width:20%" class="text-right">
													<a href="#" style="color: #333">
														<i class="fa fa-list-ul">&nbsp;&nbsp; Sort</i>
													</a>
												</th>
											</tr>
										</thead>
										<tbody>
											<?php
											if ($donors_list && count($donors_list) > 0) {
												foreach ($donors_list as $donors_li) {
													?>
											<tr>
												<td data-th="Donor Name"><?php echo $donors_li["donor_fname"] . ' ' . $donors_li["donor_lname"]; ?></td>
												<td data-th="Amount"><strong>$<?php echo $donors_li["donation_amt"]; ?></strong></td>
												<td data-th="Message"></td>
												<td data-th="Mail">
													<a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($donors_li["id"]); ?>" style="text-decoration:underline;">Send a Thank you Email</a>
												</td>
												<td data-th="" class="text-right">
													<small><?php echo date("d-m-Y H:i:s A", strtotime($donors_li["donation_date"])); ?></small>
												</td>
											</tr>
											<?php
												}
											} else {
												?>
											<tr>
												<td colspan="5" class="text-center">No Donation Found</td>
											</tr>
											<?php
											}
											?>
										</tbody>
										<tfoot>
											
										</tfoot>
									</table>
								</div>
                                </div>
							</div>