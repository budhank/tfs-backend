<script src="<?php echo base_url('assets/fundraiser/js/jquery.validate.js') ?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/additional-methods.js') ?>"></script>
<link href="<?php echo base_url('assets/css/jquery.colorpicker.bygiro.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/jquery.colorpicker.bygiro.js'); ?>"></script>

<style>
    .btn-radius {
        border-radius: 40px;
        padding: 4px 12px;
    }

    .dropdown-menu.dropdown-radius-bg {
        padding: 3px 15px;
    }

    .dropdown-menu.dropdown-radius-bg>li {
        border-bottom: solid 1px #ddd;
    }

    .dropdown-menu.dropdown-radius-bg>li:nth-last-child(1) {
        border-bottom: 0px;
    }

    .dropdown-menu.dropdown-radius-bg>li>a {
        padding: 6px 3px;
    }

    .dropdown-menu.dropdown-radius-bg>li>a:hover,
    .dropdown-radius-bg.dropdown-menu>li>a:focus {
        color: #4a90e2;
        text-decoration: none;
        background-color: transparent;
    }

    .btn-ste {
        background: #fff;
        border: 1px solid #242536;
        border-radius: 8px;
        color: #333;
        display: inline-block;
        padding: 5px 8px;
        -webkit-transition: all 0.2s linear;
        -moz-transition: all 0.2s linear;
        -o-transition: all 0.2s linear;
        transition: all 0.2s linear;
    }

    .btn-ste:hover {
        color: #fff;
        background: #242536;
        -webkit-transition: all 0.2s linear;
        -moz-transition: all 0.2s linear;
        -o-transition: all 0.2s linear;
        transition: all 0.2s linear;
    }

    .bg_wht .btn-danger {
        padding: 1px 6px;
    }

</style>

<?php
$desc_fund = '<p>ABOUT US <br /> <br /> Write something about your organization here. Mention what your primary purpose is and how you help the youth and/or community. You can mention your overall organization goals, past achievements, how long you have been around, your outlook for the upcoming season/year, etc. <br /> <br /> PURPOSE OF FUNDRAISER<br /> <br /> In the next paragraph, mention the primary purpose of the fundraiser. Tell them exactly how the fundraiser will help you achieve your upcoming goals. Mention specifically what the funds will be used for whether it be for traveling to competitions, new equipment, new training facilities, uniforms, instruments, etc. <br /> <br /> OUR GIFT TO YOU<br /> <br /> Thank you in advance for taking the time to read about our organization and for supporting our cause by making a donation and/or sharing this opportunity with your personal networks. <br /> <br /> As a special thank you for donating to our cause, you will receive a free one-year subscription to the GeoCouponAlerts<sup>TM</sup> coupon app (a $20 value) that can save you $100&rsquo;s with merchants in the greater Sacramento and Salinas areas.&rdquo;</p>';
$fslug = $this->session->userdata('fundraiser_urlname');
?>
<div id="page-wrapper" class="nw-admin">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 dw-lt">
                <div class="bg_wht mar_t_15 tot_pad">
                    <?php
					//print_r($fundraiserInfo);
					$fund_start_dt = $fundraiserInfo[0]['fund_start_dt'];
					$fund_end_dt = $fundraiserInfo[0]['fund_end_dt'];
					if ($fundraiserInfo[0]['fund_image'] == "") {
						$fundraiser_image =  base_url("assets/images/noimage-fund.jpg");
					} else {
						$playerTimg = explode(".", $fundraiserInfo[0]['fund_image']);
						if (file_exists("assets/fundraiser_image/thumb/" . $playerTimg[0] . '_thumb.' . $playerTimg[1]))
							$fundraiser_image  =  base_url("assets/fundraiser_image/thumb/" . $playerTimg[0] . '_thumb.' . $playerTimg[1]);
						else
							$fundraiser_image =  base_url("assets/images/noimage-fund.jpg");
					}
					?>
                    <div class="top_usr_img">
                        <img src="<?php echo $fundraiser_image; ?>" alt="">
                    </div>
                    <div class="top_usr_con">
                        <h4><?php
							//echo ucfirst($fundraiserInfo[0]["fund_fname"]).' '.ucfirst($fundraiserInfo[0]["fund_lname"]);
							//echo ($fundraiserInfo[0]["fund_username"] != '') ? ucfirst($fundraiserInfo[0]["fund_username"]) : 'Your Organization Name';
							echo $fundraiserInfo[0]["fund_username"];
							?></h4>
                        <span><?php //echo ($fundraiserInfo[0]["fund_slogan"]) ? $fundraiserInfo[0]["fund_slogan"] : 'Brief Slogan About Organization'; ?>
                            <?php echo $fundraiserInfo[0]["fund_slogan"];?>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">

                <?php
				//echo "<pre>"; print_r($fundraiserInfo);echo "</pre>"; 
				$fund_enddate		=	$fundraiserInfo[0]["fund_end_dt"];
				$today_date			=	date("Y-m-d");
				//$goal				=	$player_details[0]["player_goal"];	
				$goal				=	$fundraiserInfo[0]["fund_org_goal"];
				$raised				=	$fundraiserInfo[0]["raised_amt"];;
				/*
		        if(count($total_payments)>0){
		        $raised				=	$total_payments[0]["raised_amt"];
		        }else{
		        $raised				=	0;	
		        }	
		        */
				$remaining			=	$goal - $raised;

				if ($goal > 0) {
					$average_percent	=	ceil((($raised / $goal) * 100));
				} else {
					$average_percent	=	0;
				}
				$ended_days_in 		=	ceil(strtotime($fund_enddate) - strtotime($today_date));
				if ($ended_days_in < 0) {
					$campaign_msg		=	"<p>Campaign has ended</p>";
				} elseif ($ended_days_in == 0) {
					$campaign_msg		=	"<p>Campaign ends today</p>";
				} elseif ($ended_days_in == 1) {
					$campaign_msg		=	"<p>Campaign ends tomorrow</p>";
				} else {
					$campaign_days_left =	ceil((strtotime($fund_enddate) - strtotime($today_date)) / 86400);
					$campaign_msg		=	"<p>Campaign ends in " . $campaign_days_left . " days</p>";
				}
				?>
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="price_sec">
                        <ul>
                            <li>
                                <h1>$<?php echo number_format($goal); ?></h1>
                                <p>goal</p>
                            </li>
                            <li>
                                <h1>$<?php echo number_format($raised); ?></h1>
                                <p>raised</p>
                            </li>
                            <li class="active">
                                <h1>
                                    <?php
									if ($remaining < 0) {
										echo "$0";
									} else {
										echo "$" . number_format($remaining);
									}
									?>
                                </h1>
                                <p>remaning</p>
                            </li>
                        </ul>
                        <div class="p__bar_sec">
                            <p><?php echo $average_percent; ?>% Funded</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $average_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $average_percent; ?>%;"> </div>
                            </div>
                            <?php echo $campaign_msg; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">

            <div class="col-sm-3 dw-lt">
                <div class="bg_wht mar_t_15 tot_pad setup-steps">
                    <h3 class="ptitle">Setup Steps</h3>
                    <?php
					$step1 = false;
					$step2 = false;
					$step3 = false;
					$step4 = false;
					$step5 = false;
					$step6 = false;
					//echo "<pre>";print_r($fundraiserInfo[0]);echo "</pre>";
					if ($fundraiserInfo[0]["step"] >= 1) $step1 = true;
					if ($fundraiserInfo[0]["step"] >= 2) $step2 = true;
					if ($fundraiserInfo[0]["step"] >= 3) $step3 = true;
					if ($fundraiserInfo[0]["step"] >= 4) $step4 = true;				
					if ($fundraiserInfo[0]["step"] >= 5) $step5 = true;				
					
					//if (count($players_list) > 0) $step5 = true;
					?>
                    <ul class="nav tab-title">
                        <li class="<?php echo ($active_tab == 'home') ? 'active' : '';?> <?php echo ($step1) ? 'green-check' : '';?>"> <a href="<?php echo base_url($fslug . '/admin/home'); ?>">
                                <div class="stp-info"><b>Step 1 - Your Info</b><span>Information about you</span></div>
                                <div class="go-btn">Go</div>
                                <div class="right-btn"><i class="fa fa-angle-right" aria-hidden="true"></i> </div>
                                <?php if($step1){?>

                                <div class="green-right-btn"></div>
                                <?php }?>
                            </a>
                        </li>
                        <li class="<?php echo ($active_tab == 'organization') ? 'active' : '' ?> <?php echo ($step2 != '') ? 'green-check' : '' ?>">
                            <a href="<?php echo base_url($fslug . '/admin/organization'); ?>">
                                <div class="stp-info"><b>Step 2 - Organization Info</b><span>Information about the organization</span></div>
                                <div class="go-btn">Go</div>
                                <div class="right-btn"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                                <?php if($step2){?>
                                <div class="green-right-btn"></div>
                                <?php }?>
                            </a>
                        </li>
                        <li class="<?php echo ($active_tab == 'bankinfo') ? 'active' : '' ?> <?php echo ($step3 != '') ? 'green-check' : '' ?>">
                            <a href="<?php echo base_url($fslug . '/admin/bankinfo'); ?>">
                                <div class="stp-info"><b>Step 3 - Payment Info</b><span>Where and how to send donations</span></div>
                                <div class="go-btn">Go</div>
                                <div class="right-btn"><i class="fa fa-angle-right" aria-hidden="true"></i> </div>
                                <?php if($step3){?>
                                <div class="green-right-btn"></div>
                                <?php }?>
                            </a>
                        </li>
                        <li class="<?php echo ($active_tab == 'fundraiser') ? 'active' : '' ?> <?php echo ($step4 != '') ? 'green-check' : ''; ?>">
                            <a href="<?php echo base_url($fslug . '/admin/fundraiser'); ?>">
                                <div class="stp-info"><b>Step 4 - Fundraiser Info</b><span>When the fundraiser will occur</span></div>
                                <div class="go-btn">Go</div>
                                <div class="right-btn"><i class="fa fa-angle-right" aria-hidden="true"></i> </div>
                                <?php if($step4){?>
                                <div class="green-right-btn"></div>
                                <?php }?>
                            </a>
                        </li>
                        <li class="<?php echo ($active_tab == 'participants') ? 'active' : '' ?> <?php echo ($step5 != '') ? 'green-check' : ''; ?>">
                            <a href="<?php echo base_url($fslug . '/admin/participants'); ?>">
                                <div class="stp-info"><b>Step 5 - Participants Info</b><span>Who will be helping raise money</span></div>
                                <div class="go-btn">Go</div>
                                <div class="right-btn"><i class="fa fa-angle-right" aria-hidden="true"></i> </div>
                                <?php if($step5){?>
                                <div class="green-right-btn"></div>
                                <?php }?>
                            </a>
                        </li>

                    </ul>
                    <div class="text-center">
                        <?php 
					if(strtotime($fundraiserInfo[0]['fund_end_dt']) >= strtotime(date('Y-m-d'))){
					if(strtotime($fundraiserInfo[0]['fund_start_dt']) <= strtotime(date('Y-m-d'))){
$fund_start_dt=date_create($fundraiserInfo[0]['fund_start_dt']);
$fund_end_dt=date_create($fundraiserInfo[0]['fund_end_dt']);
$remaining = date_diff($fund_start_dt,$fund_end_dt);
//echo "<pre>"; print_r($remaining);
						?>
                        <div class="text-steps"><i>Days Remaining:<?php echo $remaining->days;?> Days</i></div>
                        <?php }else{?>
                        <a <?php if ($step5) : ?>data-toggle="modal" data-target="#myParticipant" href="#" class="grn-btn" <?php else : ?> onclick="return false;" class="grn-btn disabled" <?php endif; ?>>Start Fundraiser</a>
                        <div class="text-steps"><i>You must complete all set up steps above before you can start the fundraiser</i></div>

                        <?php
					}
						}else{?>
                        <div class="text-steps"><i>Expired</i></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="bg_wht mar_t_15 tot_pad other-resources">
                    <h3 class="ptitle">Other Resources</h3>
                    <ul class="nav tab-title">
                        <li> <a href="#">
                                <div class="stp-info"><b>Announcement Email</b><span>Look for upcoming fundraiser email</span></div>
                                <div class="go-btn black">Go</div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('donate/personal-info/' . $fslug); ?>" target="_blank">
                                <div class="stp-info"><b>Fundraiser Webpage</b><span>View the fundraising webpage</span></div>
                                <div class="go-btn black">Go</div>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('donate/personal-info/' . $fslug); ?>">
                                <div class="stp-info"><b>Share on Facebook</b><span>Post your fundraiser on Facebook</span></div>
                                <div class="go-btn black">Go</div>
                            </a>
                        </li>

                    </ul>

                </div>
                <div class="bg_wht mar_t_15 tot_pad login-credentials">
                    <h3 class="ptitle">Login Credentials</h3>
                    <div class="top_usr_img ">
                        <div class="row text-left">
                            <div class="col-sm-4 ">Login</div>
                            <?php //echo "<pre>"; print_r($fundraiserInfo);echo "</pre>";
							?>
                            <div class="col-sm-8"><?php echo $fundraiserInfo[0]["fund_email"]; ?></div>
                        </div>
                        <div class="row text-left">
                            <div class="col-sm-4 ">Password</div>
                            <div class="col-sm-8"><?php
													$encrypt_key = $this->config->item('encryption_key');
													$player_passw = $this->encrypt->decode($fundraiserInfo[0]["fund_pass"], $encrypt_key);
													echo $player_passw;
													?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" data-toggle="modal" data-target="#edit_login_credential" class="grn-btn">Change Password</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="tab-content">
                        <div id="step1" class="tab-pane fade <?php echo ($active_tab == 'home') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <h2>Step 1 - Enter Your Contact Information</h2>
                                    </div>
                                </div>
                                <div class="col-sm-8 lt-form-sec">

                                    <div class="lt-form-wrap m-gap2-15">
                                        <form action="<?php echo base_url($fslug . '/admin/home'); ?>" id="frm_contact_info" name="frm_contact_info" method="POST">
                                            <input type="hidden" name="step" value="<?php echo ($fundraiserInfo[0]["step"])?$fundraiserInfo[0]["step"]:1;?>">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control required" id="fund_fname" name="fund_fname" value="<?php echo $fundraiserInfo[0]["fund_fname"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control required" id="fund_lname" name="fund_lname" value="<?php echo $fundraiserInfo[0]["fund_lname"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control required" id="fund_email" name="fund_email" value="<?php echo $fundraiserInfo[0]["fund_email"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input type="text" class="form-control required" id="fund_contact" name="fund_contact" value="<?php echo $fundraiserInfo[0]["fund_contact"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Responsible Party</label>
                                            </div>
                                            <div class="form-group">
                                                <label class="container">I agree to act as the Coordinator for the Fundraiser. As such, I am
                                                    the party responsible for the Fundraiser and the only authorized
                                                    person who can communicate with ThanksForSupporting <sub>TM</sub>on
                                                    behalf of the Organization. By clicking below, I acknowledge that I
                                                    have read and agree to the <a href="javascript:void(0);" onclick="displayPage('<?php echo base64_encode("11230"); ?>')">Terms &amp; Conditions</a>
                                                    <input type="checkbox" class="checkbox" id="fund_terms" name="fund_terms" value="y" <?php if ($fundraiserInfo[0]["fund_terms"] == 'y') {		echo 'checked';} ?> />
                                                    <span class="checkmark"></span> </label>
                                            </div>
                                            <div class="sec-line"></div>
                                            <div class="btn-gp m-gap2-25">
                                                <input type="submit" name="contact_submit" class="btn33" value="Next">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="m-gap2-15">Description</h4>
                                    <p>Enter your personal contact information in this form. This is the information we will use to contact you about your Fundraiser, if necessary. </p>
                                    <p> When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
                                </div>
                            </div>
                        </div>
                        <div id="step2" class="tab-pane fade <?php echo ($active_tab == 'organization') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <h2>Step 2 - Organization/Beneficiary Information</h2>
                                    </div>
                                </div>
                                <div class="col-sm-8 lt-form-sec">
                                    <div class="lt-form-wrap m-gap2-15">
                                        <form action="<?php echo base_url($fslug . '/admin/organization'); ?>" id="frm_orgz_info" name="frm_orgz_info" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="step" value="<?php echo ($fundraiserInfo[0]["step"]>2)?$fundraiserInfo[0]["step"]:2;?>">
                                            <div class="form-group">
                                                <label>Organization Name</label>
                                                <input type="text" class="form-control required" id="fund_username" name="fund_username" value="<?php echo $fundraiserInfo[0]["fund_username"]; ?>" placeholder="i.e. Rocky Mountain Baseball">
                                            </div>
                                            <div class="form-group">
                                                <label>Short Description (optional)</label>
                                                <input type="text" class="form-control" id="fund_slogan" name="fund_slogan" value="<?php echo $fundraiserInfo[0]["fund_slogan"]; ?>" placeholder="i.e. 2017 SIC State Champions">
                                            </div>
                                            <div class="form-group">
                                                <label>Beneficiary Tax ID (optional – helpful if donations are tax deductible)</label>
                                                <input type="text" class="form-control" id="fund_beneficiary_tax_id" name="fund_beneficiary_tax_id" value="<?php echo $fundraiserInfo[0]["fund_beneficiary_tax_id"]; ?>" placeholder="Beneficiary Tax ID">
                                            </div>
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control required" id="fund_city" name="fund_city" value="<?php echo $fundraiserInfo[0]["fund_city"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>State</label>
                                                <select id="fund_state" name="fund_state" class="form-control required">
                                                    <option value="">Select</option>
                                                    <?php
													foreach ($stateList as $sval) {
														?>
                                                    <option value="<?php echo $sval['state_initial']; ?>" <?php if ($fundraiserInfo[0]['fund_state'] == $sval['state_initial']) {
														echo 'selected="selected"';
														} ?>><?php echo $sval['name']; ?></option>
                                                    <?php
													}
													?>
                                                </select>
                                            </div>
                                            <div class="form-group file-btn ">
                                                <div class="m-gap2-10 logo-gap"><b>Logo</b> (Accepted Formats: jpg, jpeg, png, gif)</div>
                                                <label for="fund_image" class="file-upload btn34"> Choose image from your computer</label>
                                                <input type="file" id="fund_image" name="fund_image" class="do-not-ignore" />
                                                <div>(Ideal Size: at least 100 x 100 pixels)</div>
                                            </div>
                                            <?php
											if ($fundraiserInfo[0]['fund_image'] == "") {
												$proimg =  base_url("assets/images/noimage-150x150.jpg");
											} else {
												$protimg = explode(".", $fundraiserInfo[0]['fund_image']);
												$proimg =  base_url("assets/fundraiser_image/thumb/" . $protimg[0] . '_thumb.' . $protimg[1]);
											}
											?>
                                            <div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px;overflow:hidden;height:150px;">
                                                <img src="<?php echo $proimg; ?>" id="fund_logo" />
                                            </div>
                                            <div class="form-group">
                                                <label>Select Primary Color</label>
                                                <div class="span6">
                                                    <div class="input-group myColorPicker"> <span class="input-group-addon myColorPicker-preview" style="background-color: <?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>;">&nbsp;</span>
                                                        <input type="text" class="form-control" name="selected_color" id="myselected_color" value="<?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>">
                                                    </div>
                                                </div>
                                                <div class="span6"></div>
                                            </div>
                                            <div class="color-selector">
                                                <div style="margin-bottom: 15px;"><b>Select Text Color</b> (that goes over Primary Color)</div>
                                                <input id="black_color" type="radio" name="fund_font_color" value="#000000" <?php echo ($fundraiserInfo[0]['fund_font_color'] == '#000000') ? 'checked' : '' ?> />
                                                <label class="color-col mybg bg-black" for="black_color" style="background-color: <?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>;padding: 5px 8px;">Black Text</label><br>
                                                <input id="white_color" type="radio" name="fund_font_color" value="#FFFFFF" <?php echo ($fundraiserInfo[0]['fund_font_color'] == '#FFFFFF') ? 'checked' : '' ?> />
                                                <label class="color-col mybg bg-white" for="white_color" style="background-color: <?php echo $fundraiserInfo[0]['fund_color_rgb']; ?>; color:#fff; padding: 5px 8px;">White Text</label>
                                            </div>
                                            <div class="sec-line"></div>
                                            <div class="btn-gp m-gap2-25"> <a href="<?php echo base_url($fslug . '/admin/home'); ?>" class="btn35">Back</a>
                                                <input type="submit" name="orgz_submit" class="btn33" value="Next">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="m-gap2-15">Description</h4>
                                    <p>Enter the information of the Beneficiary Organization that will receive the Donations from the Fundraiser. </p>
                                    <p> Be sure to upload a current Logo and choose the color that best represents your Organization. The color you choose will appear on the system-generated invitation emails and web pages.</p>
                                    <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
                                </div>
                            </div>
                        </div>
                        <div id="step3" class="tab-pane fade <?php echo ($active_tab == 'bankinfo') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <h2>Step 3 - Payment Information</h2>
                                    </div>
                                </div>
                                <div class="col-sm-8 lt-form-sec">
                                    <div class="lt-form-wrap m-gap2-15">
                                        <form action="<?php echo base_url($fslug . '/admin/bankinfo'); ?>" id="frm_bank_info" name="frm_bank_info" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="step" value="<?php echo ($fundraiserInfo[0]["step"]>3)?$fundraiserInfo[0]["step"]:3;?>">
                                            <input type="hidden" class="form-control" id="fund_pay_type" name="fund_pay_type" value="<?php echo $fundraiserInfo[0]['fund_pay_type']; ?>">
                                            <div class="form-group">
                                                <label>Description to Appear on Donor Credit Card Statement<br>(limited to 22 characters)</label>
                                                <input type="text" class="form-control required" id="fund_donor_statement" name="fund_donor_statement" value="<?php echo (isset($fundraiserInfo[0]['fund_donor_statement']) && $fundraiserInfo[0]['fund_donor_statement'] != "") ? $fundraiserInfo[0]['fund_donor_statement'] : $fundraiserInfo[0]["fund_username"]; ?>">
                                            </div>
                                            <div class="form-group file-btn tab-video" style="margin-bottom:20px;">
                                                <div>At the end of the Fundraiser, how would you like to receive the funds?</div>
                                                <!--<div class="or-div">OR</div>-->

                                                <div id="paypal" data-id="P" class="file-upload tab1 text-center btn36 pay-type <?php echo ($fundraiserInfo[0]['fund_pay_type'] == 'P') ? 'btn-active' : ''; ?>">Deposit to a PayPal account</div>
                                                <div id="chq" data-id="C" class="file-upload tab2 text-center btn36 pay-type <?php echo ($fundraiserInfo[0]['fund_pay_type'] == 'C') ? 'btn-active' : ''; ?>">Mail me a Check</div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div id="paypal-info" class="p-info-w" <?php echo ($fundraiserInfo[0]['fund_pay_type'] == 'C') ? 'style="display:none;"' : ''; ?>>
                                                <div class="form-group">
                                                    <label>PayPal Account</label>
                                                    <input type="text" class="form-control" name="fund_paypal_acc" value="<?php echo (isset($fundraiserInfo[0]['fund_paypal_acc']) && $fundraiserInfo[0]['fund_paypal_acc'] != "") ? $fundraiserInfo[0]['fund_paypal_acc'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div id="bank-info" class="b-info-w" <?php echo ($fundraiserInfo[0]['fund_pay_type'] == 'P') ? 'style="display:none;"' : ''; ?>>

                                                <div>
                                                    <h4>Where should we mail the check?</h4>
                                                    <div class="form-group">
                                                        <label>Make check payable to</label>
                                                        <input type="text" class="form-control required" id="make_check_payable_to" name="make_check_payable_to" value="<?php echo (isset($fundraiserInfo[0]['make_check_payable_to']) && $fundraiserInfo[0]['make_check_payable_to'] != "") ? $fundraiserInfo[0]['make_check_payable_to'] : ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Street Address</label>
                                                        <textarea class="form-control required" name="street_address" id="street_address"><?php echo (isset($fundraiserInfo[0]['street_address']) && $fundraiserInfo[0]['street_address'] != "") ? $fundraiserInfo[0]['street_address'] : ''; ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" class="form-control required" id="city" name="city" value="<?php echo (isset($fundraiserInfo[0]['city']) && $fundraiserInfo[0]['city'] != "") ? $fundraiserInfo[0]['city'] : ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <select id="fund_state" name="fund_state" class="form-control required">
                                                            <option value="">Select</option>
                                                            <?php
															foreach ($stateList as $sval) {
																?>
                                                            <option value="<?php echo $sval['state_initial']; ?>" <?php if ($fundraiserInfo[0]['fund_state'] == $sval['state_initial']) { echo 'selected="selected"';
																} ?>><?php echo $sval['name']; ?></option>
                                                            <?php
															}
															?>
                                                        </select>

                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" class="form-control required" id="zip" name="zip" value="<?php echo (isset($fundraiserInfo[0]['zip']) && $fundraiserInfo[0]['zip'] != "") ? $fundraiserInfo[0]['zip'] : ''; ?>">
                                                    </div>
                                                </div>
                                                <!--<div class="send-chech">
	                        <div class="form-group">
	                          <label>Bank Name</label>
	                          <input type="text" class="form-control required" id="fund_bank_name" name="fund_bank_name" value="<?php echo (isset($fundraiserInfo[0]['fund_bank_name']) && $fundraiserInfo[0]['fund_bank_name'] != "") ? $fundraiserInfo[0]['fund_bank_name'] : ''; ?>">
	                        </div>
	                        <div class="form-group">
	                          <label>Bank Routing Number (9-digit number)</label>
	                          <input type="text" class="form-control required" id="fund_routing_number" name="fund_routing_number" value="<?php echo (isset($fundraiserInfo[0]['fund_routing_number']) && $fundraiserInfo[0]['fund_routing_number'] != "") ? $fundraiserInfo[0]['fund_routing_number'] : ''; ?>">
	                        </div>
	                        <div class="form-group">
	                          <label>Bank Account Number</label>
	                          <input type="text" class="form-control required" id="fund_account_number" name="fund_account_number"  value="<?php echo (isset($fundraiserInfo[0]['fund_account_number']) && $fundraiserInfo[0]['fund_account_number'] != "") ? $fundraiserInfo[0]['fund_account_number'] : ''; ?>">
	                        </div>
	                        <div class="form-group"><img src="<?php echo base_url('assets/images/routing-number.jpg') ?>" /></div>
	                      </div>-->
                                            </div>
                                            <div class="sec-line m-top-60"></div>
                                            <div class="btn-gp m-gap2-25"> <a href="<?php echo base_url($fslug . '/admin/organization'); ?>" class="btn35">Back</a>
                                                <input type="submit" name="bank_submit" class="btn33" value="Next">
                                                <div class="clearfix"></div>
                                                <a href="#" class="btn40">Cancel</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="m-gap2-15">Description</h4>
                                    <p>Enter the bank information for the Organization where the Donations will be deposited after the Fundraiser ends.</p>
                                    <p> It is critical that you enter the correct information here.</p>
                                    <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
                                </div>
                            </div>
                        </div>
                        <div id="step4" class="tab-pane fade <?php echo ($active_tab == 'fundraiser') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <h2>Step 4 - Fundraiser Information</h2>
                                    </div>
                                </div>
                                <div class="col-sm-8 lt-form-sec">
                                    <div class="lt-form-wrap m-gap2-15">
                                        <form action="<?php echo base_url($fslug . '/admin/fundraiser'); ?>" id="frm_fundraiser_info" name="frm_fundraiser_info" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="step" value="<?php echo ($fundraiserInfo[0]["step"]>4)?$fundraiserInfo[0]["step"]:4;?>">
                                            <input type="hidden" name="media_type" id="media_type" class="form-control valid" value="I">
                                            <div class="form-group">
                                                <label>Overall Donations Goal ($)</label>
                                                <input id="fund_org_goal" name="fund_org_goal" type="text" class="form-control" value="<?php echo number_format($fundraiserInfo[0]['fund_org_goal']); ?>" placeholder="<?php echo number_format("10000"); ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Donation Goal Per Participant ($)</label>
                                                <input id="fund_individual_goal" name="fund_individual_goal" type="text" class="form-control" value="<?php echo number_format($fundraiserInfo[0]['fund_individual_goal']); ?>" placeholder="<?php echo number_format("10000"); ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <div class="file-btn">
                                                    <div class="input-group-date">
                                                        <input type="text" class="form-control datepicker" id="fund_start_dt" name="fund_start_dt" readonly="readonly" value="<?php echo date('m-d-Y', strtotime($fundraiserInfo[0]['fund_start_dt'])); ?>" placeholder="MM-DD-YYYY">
                                                        <span class="glyphicon glyphicon-calendar"></span> </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>End Date</label>
                                                <div class="file-btn">
                                                    <div class="input-group-date">
                                                        <input type="text" class="form-control datepicker" id="fund_end_dt" name="fund_end_dt" readonly="readonly" value="<?php echo date('m-d-Y', strtotime($fundraiserInfo[0]['fund_end_dt'])); ?>" placeholder="MM-DD-YYYY">
                                                        <span class="glyphicon glyphicon-calendar"></span> </div>
                                                </div>
                                            </div>
                                            <div class="form-group file-btn tab-video">
                                                <div> <b>Upload Image/Video (photo of team, class, etc.)</b></div>
                                                <div for="file-upload" class="file-upload tab1 img_vid text-center btn36 btn-active" id="img_tab">Image</div>
                                                <div for="file-upload" class="file-upload tab2 img_vid text-center btn36" id="vid_tab">Video</div>
                                            </div>
                                            <div class="form-group file-btn " id="img_video">
                                                <div class="m-gap2-10">(Accepted Formats: jpg, jpeg, png, gif)</div>
                                                <label for="feature_img" class="file-upload text-center btn34"> Choose image from your computer</label>
                                                <input id="feature_img" name="feature_img" type="file" onchange="img_vid_add()" />
                                                <div>(Ideal Size: at least 1280 x 720 pixels)</div>
                                            </div>
                                            <div id="img_vid_list">
                                                <?php
												$str = '';
												if (count($fundraiserImgInfo) > 0) {
													foreach ($fundraiserImgInfo as $fval) {
														if ($fval['media_type'] == 'I') {
															$protimg = explode(".", $fval['fund_feature_image']);
															$proimg  = base_url() . "assets/fundraiser_feature_image/thumb/" . $protimg[0] . '_thumb.' . $protimg[1];
															$str = '<img src="' . $proimg . '"/>';
														} else {
															$str = '<a class="youtube" href="http://youtube.com/watch?v=' . $fval['fund_feature_image'] . '"><img src="https://img.youtube.com/vi/' . $fval['fund_feature_image'] . '/mqdefault.jpg" /></a>';
														}
														?>
                                                <div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px; overflow:hidden;height:150px;"> <?php echo $str; ?> <a href="javascript:void(0);" onclick="del_media(<?php echo $fval['id']; ?>)" class="fa fa-times rem"></a> </div>
                                                <?php
													}
												}
												?>
                                            </div>
                                            <div class="form-group">
                                                <label>Fundraiser Description</label>
                                                <?php
												$settings_qry = $this->db->query("SELECT fund_des_static,fund_des_editable FROM tbl_settings");
												$fund_des_array = $settings_qry->result_array();
												//echo "<pre>";print_r($fund_des_array);echo "</pre>";
												if ($fundraiserInfo[0]['fund_des'] != "") {
													if ($fundraiserInfo[0]['fund_des_static'] != "") { ?>
                                                <input type="hidden" value="<?php echo $fundraiserInfo[0]['fund_des_static']; ?>" name="fund_des_static">
                                                <div class="tab-cont"><?php echo $fundraiserInfo[0]['fund_des_static']; ?></div>

                                                <?php } else { ?>
                                                <input type="hidden" value="<?php echo $fund_des_array[0]['fund_des_static']; ?>" name="fund_des_static">
                                                <div class="tab-cont"><?php echo $fund_des_array[0]['fund_des_static']; ?></div>
                                                <?php } ?>
                                                <textarea name="fund_des" id="fund_des" class="form-control editme" rows="5">
                                                <?php echo $fundraiserInfo[0]['fund_des']; ?>
                                                </textarea>
                                                <?php } else { ?>
                                                <input type="hidden" value="<?php echo $fund_des_array[0]['fund_des_static']; ?>" name="fund_des_static">
                                                <div class="tab-cont"><?php echo $fund_des_array[0]['fund_des_static']; ?></div>
                                                <textarea name="fund_des" id="fund_des" class="form-control editme" rows="5">
                                                <?php echo $fund_des_array[0]['fund_des_editable']; ?>
                                                </textarea>
                                                <?php }  ?>
                                            </div>
                                            <div class="sec-line m-top-60"></div>
                                            <div class="btn-gp m-gap2-25"> <a href="<?php echo base_url($fslug . '/admin/bankinfo'); ?>" class="btn35">Back</a>
                                                <input type="submit" name="fundraiser_submit" class="btn33" value="Next">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="m-gap2-15">Description</h4>
                                    <p>Here is where you enter the details of your Fundraiser. </p>
                                    <p> The Overall Donations Goals is the combined amount you are hoping to raise overall.</p>
                                    <p>The Donation Goal Per Participant is the amount you hope each Participant is able to raise.</p>
                                    <p>You must upload at least one photo or YouTube video. </p>
                                    <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
                                </div>
                            </div>
                        </div>
                        <div id="step5" class="tab-pane fade <?php echo ($active_tab == 'participants') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <div class="pull-left">
                                            <h2>Participants You've Invited</h2>
                                        </div>

                                        <div class="pull-right"><a class="grn-btn-right" data-toggle="modal" data-target="#addParticipants" href="#">Add Participants</a></div>
                                    </div>

                                </div>
                                <div class="col-sm-12 lt-form-sec">

                                    <div class="lt-form-wrap">
                                        <div class="participants">
                                            <div class="row">
                                                <?php
												if ($this->session->userdata('up_msg')) {
													echo '<br /><p class="text-success">&nbsp;&nbsp;&nbsp;' . $this->session->userdata('up_msg') . '</p>';
													$this->session->unset_userdata('up_msg');
												}
												?>

                                            </div>
                                            <div id="b_content">
                                                <div class="ta-participants">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Email</th>
                                                                <th class="text-center">Invited</th>
                                                                <th>Reminder*</th>
                                                                <th class="text-center">Contacts</th>
                                                                <th class="text-center">Raised</th>
                                                                <th class="text-center">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
															if (count($players_list) > 0) {
																$sends = 0;
																$contacts = 0;
																$raised = 0;
																foreach ($players_list as $players_li) {
																	$contact_list = $this->fundraiser_model->view("tbl_player_contacts", array("contact_added_by" => $players_li["id"]));
																	if ($players_li['player_image'] == "") {
																		$proimg =  base_url() . "assets/images/noimage-150x150.jpg";
																	} else {
																		$protimg = explode(".", $players_li['player_image']);
																		$proimg =  base_url() . "assets/player_image/thumb/" . $protimg[0] . '_thumb.' . $protimg[1];
																	}
																	$org_goal = $players_li['player_goal'];
																	$raised_amt = $players_li['raised_amt'];
																	$remaining_goal = ($org_goal - $raised_amt);
																	?>
                                                            <tr>
                                                                <td>
                                                                    <span class="pic"><img src="<?php echo $proimg; ?>" /></span>
                                                                    <strong class="plr-fname"><?php echo $players_li["player_fname"] . ' ' . $players_li["player_lname"] ?></strong>
                                                                </td>
                                                                <td><?php echo $players_li["player_email"] ?></td>
                                                                <td class="text-center"> <?php if ($players_li["invitation"] == 0) { ?>
                                                                    <a href="<?php echo base_url($fslug . '/admin/players/asktoinvite/' . $players_li["player_slug_name"]); ?>" class="grn-btn-right">Send</a>
                                                                    <?php } else { ?>
                                                                    <span class="invite"><?php echo date('m-d-Y', strtotime($players_li["invitation_dt"])); ?></span>
                                                                    <?php $sends++;
																			} ?></td>
                                                                <td class="text-center"><?php if ($players_li["reminder"] == 0) { ?>
                                                                    <a href="<?php echo base_url($fslug . '/admin/players/remindtoinvite/' . $players_li["player_slug_name"]); ?>" class="grn-btn-right">Send</a>
                                                                    <?php } else { ?>
                                                                    <span class="invite"><?php echo date('m-d-Y', strtotime($players_li["reminder_dt"])); ?></span>
                                                                    <?php } ?></td>
                                                                <td class="text-center">
                                                                    <span class="invite">
                                                                        <?php $contacts += count($contact_list);
																			echo count($contact_list);
																			?></span>
                                                                </td>
                                                                <td class="text-right"><?php
																	$raised_amt = ($players_li["raised_amt"]) ? $players_li["raised_amt"] : 0;
																	$raised += $raised_amt;
																	echo $raised_amt;
																	?></td>
                                                                <td class="text-center"><a class="remove bg-danger" href="<?php echo base_url($fslug . '/admin/delete-participant/' . $players_li["player_slug_name"]); ?>">
                                                                        <span class="text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                                    </a></td>
                                                            </tr>
                                                            <?php
																}
																?>
                                                            <tr>
                                                                <th colspan="2">TOTALS</th>
                                                                <th class="text-center"><?php echo $sends; ?></th>
                                                                <th></th>
                                                                <th class="text-center"><?php echo $contacts; ?></th>
                                                                <th class="text-right">$0</th>
                                                                <th></th>
                                                            </tr>

                                                            <?php
															} else {
																?>
                                                            <tr>
                                                                <td colspan="6" class="comp-steps-hight">

                                                                    After completing Setup Steps 1-4, click the green <br>
                                                                    button below to invite Participants to get started.
                                                                    <div style="padding: 30px 0;">
                                                                        <a style="padding: 15px;" class="grn-btn-right" data-toggle="modal" data-target="#addParticipants" href="#">Add Participants</a>
                                                                    </div>
                                                                    <div style="font-size:16px">
                                                                        <b>WARNING:</b> <i>Children under the age of 13 are NOT permitted to use the <br>
                                                                            Platform or participate in the Fundraiser. If they are part of your school,<br>
                                                                            team, club or organization, invite their parent or guardian instead.</i></div>
                                                                </td>
                                                            </tr>

                                                            <?php
															}
															?>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="sec-line"></div>
                                        <div class="btn-gp m-gap2-25">
                                            <a href="<?php echo base_url($fslug . '/admin/participants'); ?>" class="btn35">Back</a>
                                            <a href="<?php echo base_url($fslug . '/admin/startup'); ?>" class="btn33">Next</a>
                                        </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="step6" class="tab-pane fade <?php echo ($active_tab == 'startup') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <h2>Step 6 - Invite Participants</h2>
                                    </div>
                                </div>
                                <div class="lt-form-wrap m-gap2-15">
                                    <?php
									if ($this->session->userdata('succ_msg')) {
										?>
                                    <div class="col-sm-12">
                                        <div class="congratulations-icn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></div>
                                        <div align="center">
                                            <h3>Congratulations</h3>The participants have been invited
                                        </div>
                                    </div>
                                    <div class="col-sm-12 lt-form-sec">
                                        <div class="sec-line m-gap2-15"></div>
                                        <div class="btn-gp text-center m-gap2-25"> <a href="<?php echo base_url($fslug . '/admin/report'); ?>" class="btn35">Go to Dashboard</a> </div>
                                    </div>

                                    <?php
										$this->session->unset_userdata('succ_msg');
									} else {
										?>
                                    <div class="col-sm-8">
                                        <div class="m-gap2-15" align="center"><img src="<?php echo base_url('assets/images/setup-complete.jpg'); ?>" /></div>
                                        <div class="form-group file-btn" style="margin:auto;">
                                            <div data-toggle="modal" data-target="#myParticipant" class="text-center btn34 m-gap2-25"> Start Fundraiser</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <h4 class="m-gap2-15">Description</h4>
                                        <p>Click on the Start Fundraiser button to have the system send out emails to each of the Participants you entered.</p>
                                        <p>The email will include instructions on how they can invite their contacts to donate to your cause.</p>
                                    </div>
                                    <?php
									}
									?>
                                </div>

                            </div>
                        </div>
                        <div id="step7" class="tab-pane fade <?php echo ($active_tab == 'report') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h2>Participants You've Invited</h2>
                                            </div>
                                            <div class="col-sm-8 d-board-fdr text-right">
                                                <div><a class="fbook-ico" href="#"><img src="https://thanksforsupporting.com/apps/assets/fundraiser/images/facebook.png" /></a></div>
                                                <div><a class="clist-ico" href="#"><img src="https://thanksforsupporting.com/apps/assets/fundraiser/images/content-list-ico.png" /></a></div>
                                                <div><a class="black-btn" data-toggle="modal" data-target="#addParticipants" href="#">Upload File<span>
                                                            (CSV, excel)</span></a></div>
                                                <div><a class="grn-btn-right" data-toggle="modal" data-target="#addParticipants" href="#">Add Participants</a></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12">
                                    <div class="ta-participants">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th class="text-center">Invited</th>
                                                    <th>Reminder*</th>
                                                    <th class="text-center">Contacts</th>
                                                    <th class="text-center">Raised</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
												if ($report_list && count($report_list) > 0) {
													//echo '<pre>';print_r($report_list);echo '</pre>';
													$sends = 0;
													$contacts = 0;
													$raised = 0;
													foreach ($report_list as $players_li) {
														$contact_list = $this->fundraiser_model->view("tbl_player_contacts", array("contact_added_by" => $players_li["player_slug_name"]));
														if ($players_li['player_image'] == "") {
															$proimg =  base_url() . "assets/images/noimage-150x150.jpg";
														} else {
															$protimg = explode(".", $players_li['player_image']);
															$proimg =  base_url() . "assets/player_image/thumb/" . $protimg[0] . '_thumb.' . $protimg[1];
														}
														$org_goal = $players_li['player_goal'];
														$raised_amt = $players_li['raised_amt'];
														$remaining_goal = ($org_goal - $raised_amt);
														if ($players_li["player_slug_name"] == '') {
															$player_slug_name = $players_li["player_fname"] . '-' . $players_li["player_lname"];
															$this->db->update('tbl_player', array('player_slug_name' => $player_slug_name), array('id' => $players_li["id"]));
															$players_li["player_slug_name"] = $player_slug_name;
														}
														?>
                                                <tr>
                                                    <td>
                                                        <span class="pic"><img src="<?php echo $proimg; ?>" /></span>
                                                        <strong class="plr-fname"><?php echo $players_li["player_fname"] . ' ' . $players_li["player_lname"] ?></strong>
                                                    </td>
                                                    <td><?php echo $players_li["player_email"] ?></td>
                                                    <td class="text-center"> <?php if ($players_li["invitation"] == 0) { ?>
                                                        <a href="<?php echo base_url($fslug . '/admin/players/asktoinvite/' . $players_li["player_slug_name"]); ?>" class="grn-btn-right">Send</a>
                                                        <?php } else { ?>
                                                        <span class="invite"><?php echo date('m-d-Y', strtotime($players_li["invitation_dt"])); ?></span>
                                                        <?php $sends++;
																} ?></td>
                                                    <td class="text-center"><?php if ($players_li["reminder"] == 0) { ?>
                                                        <a href="<?php echo base_url($fslug . '/admin/players/remindtoinvite/' . $players_li["player_slug_name"]); ?>" class="grn-btn-right">Send</a>
                                                        <?php } else { ?>
                                                        <span class="invite gray-text"><?php echo date('m-d-Y', strtotime($players_li["reminder_dt"])); ?></span>
                                                        <?php } ?></td>
                                                    <td class="text-center">
                                                        <span class="invite">
                                                            <?php $contacts += count($contact_list);
																echo count($contact_list);
															?></span>
                                                    </td>
                                                    <td class="text-right"><?php
														$raised_amt = ($players_li["raised_amt"]) ? $players_li["raised_amt"] : 0;
														$raised += $raised_amt;
														echo $raised_amt;
														?>

                                                    </td>
                                                    <td class="text-right">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-default dropdown-toggle btn-radius" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-right dropdown-radius-bg" aria-labelledby="dropdownMenuButton">
                                                                <li><a href="<?php echo base_url($fslug . '/admin/players/remindtoinvite/' . $players_li["player_slug_name"]); ?>">Resend</a></li>
                                                                <li><a href="javascript:void(0);" onclick="getPoup('<?php echo base_url($fslug . '/admin/edit/' . $players_li["player_slug_name"]); ?>');">Edit</a></li>
                                                                <li><a target="_blank" href="<?php echo base_url($fslug . '/' . $players_li["player_slug_name"]); ?>">Webpage</a></li>
                                                                <li><a href="<?php echo base_url($fslug . '/admin/contacts/' . $players_li["player_slug_name"]); ?>">Contacts</a></li>
                                                                <li><a href="<?php echo base_url($fslug . '/admin/donations/' . $players_li["player_slug_name"]); ?>">Donations</a></li>
                                                                <li><a href="<?php echo base_url($fslug . '/admin/delete/' . $players_li["player_slug_name"]); ?>">Delete</a></li>
                                                            </ul>
                                                        </div>


                                                    </td>
                                                </tr>
                                                <?php
													}
													?>
                                                <tr>
                                                    <th colspan="2">TOTALS</th>
                                                    <th class="text-center"><?php echo $sends; ?></th>
                                                    <th></th>
                                                    <th class="text-center"><?php echo $contacts; ?></th>
                                                    <th class="text-right">$<?php echo $raised; ?></th>
                                                    <th></th>
                                                </tr>
                                                <tr>

                                                    <td colspan="7"><i  class="gray-text">On this date, a reminder email will be sent to participants who have NOT yet logged in to their account. You may send a reminder manually at any time by clicking on the "Email Reminder" link. Doing so will override the scheduled reminder.</i></td>
                                                </tr>

                                                <?php
												} else {
													?>



                                                <tr>
                                                    <td colspan="6" class="comp-steps-hight">

                                                        After completing Setup Steps 1-4, click the green <br>
                                                        button below to invite Participants to get started.
                                                        <div style="padding: 30px 0;">
                                                            <a style="padding: 15px;" class="grn-btn-right" data-toggle="modal" data-target="#addParticipants" href="#">Add Participants</a>
                                                        </div>
                                                        <div style="font-size:16px">
                                                            <b>WARNING:</b> <i>Children under the age of 13 are NOT permitted to use the <br>
                                                                Platform or participate in the Fundraiser. If they are part of your school,<br>
                                                                team, club or organization, invite their parent or guardian instead.</i></div>
                                                    </td>
                                                </tr>

                                                <?php
												}
												?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div id="step8" class="tab-pane fade <?php echo ($active_tab == 'contacts') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <div class="pull-right"><a class="grn-btn-right" data-id="<?php echo ($player_info)?$player_info[0]['id']:''; ?>" data-toggle="modal" data-target="#myModalContact" href="#">Add Contact</a></div>
                                    </div>

                                </div>
                                <div class="col-sm-12">
                                    <div class="ta-participants">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <th style="width:20%">Name</th>
                                                    <th style="width:15%">Invited</th>
                                                    <th style="width:15%">Reminder* </th>
                                                    <th style="width:15%"> Last Chance*</th>
                                                    <th style="width:12%"></th>
                                                    <th style="width:2%" class="delete_btn"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
												$doneteds = array();
												if (count($contact_lists)) {
													foreach ($contact_lists as $contact_li) {
														$countSent = 0;
														$countDoneted = 0;
														$share_reports = $this->fundraiser_model->view('tbl_email_share_reports', array('to_id' => $contact_li["id"]));
														if (count($share_reports))
															$countSent = count($share_reports) - 1;

														$donors_payment = $this->fundraiser_model->view('tbl_donors_payment', array('donor_email' => $contact_li["contact_email"]));
														if (count($donors_payment)) {
															$doneteds[$donors_payment[0]['donor_email']] = $donors_payment[0];
															$countDoneted = count($donors_payment);
														}
														?>
                                                <tr>
                                                    <!-- <td><?php echo $contact_li["contact_name"]; ?></td> -->
                                                    <td><?php echo $contact_li["contact_email"]; ?></td>
                                                    <td>
                                                        <?php if ($countDoneted != 0) : ?>
                                                        Donated
                                                        <?php elseif ($contact_li["status"] == 3) : ?>
                                                        Opted Out
                                                        <?php else : ?>
                                                        <?php echo ($share_reports && count($share_reports)) ? date('m-d-Y', strtotime($share_reports[0]["sent_time"])) : ''; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($countDoneted != 0) : ?>
                                                        Donated
                                                        <?php elseif ($contact_li["status"] == 3) : ?>
                                                        Opted Out
                                                        <?php else : ?>
                                                        <span <?php if ($contact_li["status"] == 1) : ?>style="color:#cdcdcd;" <?php endif; ?>><?php echo ($share_reports) ? date('m-d-Y', strtotime('+5 days', strtotime($share_reports[0]["sent_time"]))) : ''; ?></span>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($countDoneted != 0) : ?>
                                                        Donated
                                                        <?php elseif ($contact_li["status"] == 3) : ?>
                                                        Opted Out
                                                        <?php else : ?>
                                                        <span <?php if ($contact_li["status"] == 1) : ?>style="color:#cdcdcd;" <?php endif; ?>><?php echo date('m-d-Y', strtotime('-3 days', strtotime($fund_end_dt))); ?></span>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php
																switch ($contact_li["status"]) {
																	case '2':
																		?>
                                                        <button type="button" class="btn-invyt btn-ste">Stopped</button>
                                                        <?php
																	break;
																case '3':
																	?>
                                                        Opted Out
                                                        <?php
																	break;
																default:
																	?>
                                                        <?php if ($countDoneted == 0) : ?>
                                                        <form action="<?php echo base_url($fslug . '/admin/contacts/' . $pslug . '/invitetoall'); ?>" method="POST">
                                                            <input type="hidden" name="contact_id" value="<?php echo $contact_li["id"]; ?>" />
                                                            <input type="hidden" name="goto" value="same" />
                                                            <button type="submit" class="btn-invyt btn-ste">
                                                                <?php if (count($share_reports) == 0) : ?>Invite<?php endif; ?>
                                                                <?php if (count($share_reports) > 0) : ?>Re-invite<?php endif; ?>
                                                            </button>
                                                        </form>
                                                        <?php else : ?>
                                                        <button type="button" class="btn-invyt btn-black">Donated</button>
                                                        <?php endif; ?>

                                                        <?php
																	break;
															}
															?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php if ($contact_li["status"] == 1 && $countDoneted == 0) : ?>
                                                        <form action="<?php echo base_url($fslug . '/' . $pslug . '/admin/invitetoall'); ?>" method="POST">
                                                            <input type="hidden" name="contact_id" value="<?php echo $contact_li["id"]; ?>" />
                                                            <input type="hidden" name="goto" value="same" />
                                                            <input type="hidden" name="action" value="status_change" />
                                                            <input type="hidden" name="status" value="2" />
                                                            <button type="submit" class="btn btn-danger">
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </button>
                                                        </form>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <?php
													}
												} else {
													?>
                                                <tr>
                                                    <td colspan="7" class="text-center">
                                                        <p>You haven’t invited any of your contacts to donate yet.
                                                            <br />Invite your contacts to start receiving donations.</p>
                                                        <button type="button" class="text-center btn34" data-toggle="modal" data-target="#myModalContact">
                                                            Invite Your Contacts</button>
                                                    </td>
                                                </tr>
                                                <?php
												}
												?>
                                            </tbody>

                                            <tfoot class="notif">
                                                <tr>
                                                    <td colspan="7">*Up to two reminder emails will be sent to contacts who have NOT donated prior to the scheduled reminder date. You may send a reminder manually at anytime by clicking on the “Email Re-Invite” button. Doing so will override the next scheduled reminder.
                                                    </td>
                                                </tr>
                                            </tfoot>

                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div id="step9" class="tab-pane fade <?php echo ($active_tab == 'donations') ? 'in active' : '' ?> ">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tit1_sec clearfix">
                                        <div class="left">
                                            <h2>Donations/Donors</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <th style="width:20%">Donor Name</th>
                                                    <th style="width:20%">Amount</th>
                                                    <th style="width:20%">Message</th>
                                                    <th style="width:20%">Thanks Mail</th>
                                                    <th style="width:20%" class="text-right">
                                                        <a href="#" style="color: #333">
                                                            <i class="fa fa-list-ul">&nbsp;&nbsp; Sort</i>
                                                        </a>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
											if ($donors_list && count($donors_list) > 0) {
												foreach ($donors_list as $donors_li) {
													?>
                                                <tr>
                                                    <td data-th="Donor Name"><?php echo $donors_li["donor_fname"] . ' ' . $donors_li["donor_lname"]; ?></td>
                                                    <td data-th="Amount"><strong>$<?php echo $donors_li["donation_amt"]; ?></strong></td>
                                                    <td data-th="Message"></td>
                                                    <td data-th="Mail">
                                                        <a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($donors_li["id"]); ?>" style="text-decoration:underline;">Send a Thank you Email</a>
                                                    </td>
                                                    <td data-th="" class="text-right">
                                                        <small><?php echo date("m-d-Y", strtotime($donors_li["donation_date"])); ?></small>
                                                    </td>
                                                </tr>
                                                <?php
												}
											} else {
												?>
                                                <tr>
                                                    <td colspan="5" class="text-center">No Donation Found</td>
                                                </tr>
                                                <?php
											}
											?>
                                            </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($active_tab=='donors'){?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tit1_sec clearfix">
                                <div class="left">
                                    <h2>Donations/Donors</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="">
                                <table class="table table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <th style="width:20%">Player</th>
                                            <th style="width:20%">Doner</th>
                                            <th style="width:20%">Amount</th>
                                            <th style="width:20%" class="text-right">
                                                <a href="#" style="color: #333">
                                                    <i class="fa fa-list-ul">&nbsp;&nbsp; Sort</i>
                                                </a>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
											//print_r($players_list);
											if ($donors_list_show && count($donors_list_show) > 0) {
												foreach ($donors_list_show as $donors_li) {
												if($donors_li['player_id']!=''){?>
                                        <tr>
                                            <td data-th="Donor Name"><?php echo $donors_li["player_fname"] . ' ' . $donors_li["player_lname"]; ?></td>
                                            <td data-th="Message"><?php echo $donors_li["donor_fname"].' '.$donors_li['donor_lname']; ?></td>
                                            <td data-th="Amount"><strong>$<?php echo $donors_li["donation"]; ?></strong></td>

                                            <td data-th="" class="text-right">
                                                <small><?php echo date("d-m-Y H:i:s A", strtotime($donors_li["created"])); ?></small>
                                            </td>
                                        </tr>
                                        <?php
												}}
											} else {
												?>
                                        <tr>
                                            <td colspan="5" class="text-center">No Donation Found</td>
                                        </tr>
                                        <?php
											}
											?>
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!--<div class="bg_wht mar_t_15 tot_pad" style="margin-top:150px;">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<p>Complete the Setup Steps below to get your fundraiser up and running in as little as 15 minutes.</p>
				</div>
			</div>
		</div>-->
    </div>
</div>

<?php
if ($active_tab == 'home') {
	?>
<div id="terms" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content terms">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>




<script type="text/javascript">
    $(document).ready(function($) {
        //VALIDATE IMAGE DIMENSION
        $.validator.addMethod("imagedimension", function(value, element, params) {
            var val_height = params[0];
            var val_width = params[1];

            var formData = new FormData();
            formData.append('file', $('input[type=file]')[0].files[0]);
            formData.append('valid_height', val_height);
            formData.append('valid_width', val_width);

            var eReport = ''; //error report
            var sts = '';
            $.ajax({
                url: "<?php echo base_url($fslug . '/admin/checkfile'); ?>",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                async: false,
                dataType: "json",
                success: function(res) {
                    if (res.valid !== 'true') {
                        sts = false;
                    } else {
                        sts = true;
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    sts = false;
                }
            });
            return sts;

        }, "Image Dimension must be greater than {0}x{1}.");

        $("#frm_contact_info").validate({
            rules: {
                fund_email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "<?php echo base_url('fundraiserEmailCheckerEditAjax'); ?>",
                        type: "post",
                        data: {
                            fund_email: function() {
                                return $("#fund_email").val();
                            }
                        }
                    }
                },
                fund_terms: {
                    required: true
                }
            },
            messages: {
                fund_email: {
                    remote: "The email id is already in use!",
                },
                fund_terms: {
                    required: "Please accept Terms & Conditions",
                }
            }
        });
    });

    function displayPage(page_id) {
        var url = "<?php echo base_url('wppages/get_page'); ?>";
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                page_id: page_id
            },
            success: function(msg) {
                $(".modal-body").html(msg);
                $("#terms").modal('show');
            }
        });
    }

</script>
<?php
}
?>
<?php
if ($active_tab == 'organization') {
	$color_rgba = str_replace(['rgb(', 'rgba(', ')'], '', $fundraiserInfo[0]['fund_color_rgb']);
	$color_dt = explode(",", $color_rgba, 4);
	$col_r = $color_dt[0];
	$col_g = $color_dt[1];
	$col_b = $color_dt[2];
	$col_a = $color_dt[3];
	?>
<script type="text/javascript">
    $(document).ready(function($) {
        $.validator.setDefaults({
            ignore: ':hidden:not(.do-not-ignore)'
        });
        $("#frm_orgz_info").validate({
            rules: {
                fund_image: {
                    extension: "png|jpe?g|gif"
                }
            },
            messages: {
                fund_image: {
                    extension: "Please upload valid file type."
                }
            }
        });

        $("#fund_image").change(function() {
            var stss = true;
            $('.crr').remove();
            var img = $("#fund_image").val();
            if (img != '') {
                var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    $('#fund_image').after('<label for="fund_image" generated="true" class="crr error">Please upload valid file type.</label>');
                } else {
                    stss = true;
                }
            } else {
                $('#fund_image').after('<label for="fund_image" generated="true" class="crr error">This field is required.</label>');
                stss = false;
            }
            if (stss == true) {
                var formData = new FormData();
                formData.append('fund_image', $('#fund_image')[0].files[0]);

                $.ajax({
                    url: "<?php echo base_url($fslug . '/admin/upload_fund_logo'); ?>",
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    beforeSend: function() {
                        // $('#div_loading').html('<div class="loading_inn">loading...</div>');
                    },
                    success: function(results) {
                        if (results.valid == 1) {
                            $("#fund_logo").attr('src', results.src);
                        } else {
                            $('#fund_image').after('<label for="fund_image" generated="true" class="crr error">' + results.msg + '</label>');
                        }
                    }
                });
            }
        });

        $('.myColorPicker').colorPickerByGiro({
            preview: '.myColorPicker-preview',
            format: 'rgba'
        });
        $('#col_r').val('<?php echo $col_r; ?>');
        $('#col_g').val('<?php echo $col_g; ?>');
        $('#col_b').val('<?php echo $col_b; ?>');
        $('#col_a').val('<?php echo $col_a; ?>');
        $('.cp-hex input').val('<?php echo $fundraiserInfo[0]['fund_color_hex']; ?>');
        $('.cp-hex input').trigger('keyup');
        $('.cp-trigger').click(function() {
            $('.mybg').css("background-color", $('#myselected_color').val());
        });
    });

</script>
<?php
}
?>
<?php
if ($active_tab == 'bankinfo') {
	?>
<script type="text/javascript">
    $(document).ready(function($) {
        $("#frm_bank_info").validate({
            rules: {
                fund_routing_number: {
                    required: function(element) {
                        return ($('#fund_pay_type').val() != '' && $('#fund_pay_type').val() == 'C') ? true : false;
                    },
                    routingnumber: true
                },
                fund_paypal_acc: {
                    required: function(element) {
                        return ($('#fund_pay_type').val() != '' && $('#fund_pay_type').val() == 'P') ? true : false;
                    }
                }
                /*,
                fund_donor_statement: {
                	required: true,
                	maxlength: 22
                }
                */
            },
            messages: {
                /*
                	fund_donor_statement: {
                		maxlength: 'Enter not more than 22 characters.'
                	}
                	*/
            }
        });
        $("#paypal").removeClass("btn-active");
        $("#paypal-info").hide();
        $('.pay-type').click(function() {
            if ($(this).attr('data-id') == 'C') {
                $("#paypal").removeClass("btn-active");
                $("#fund_pay_type").val("C");
                $(this).addClass("btn-active");
                $("#paypal-info").hide();
                $("#bank-info").show();
            } else {
                $("#chq").removeClass("btn-active");
                $("#fund_pay_type").val("P");
                $(this).addClass("btn-active");
                $("#paypal-info").show();
                $("#bank-info").hide();
            }
        });

    });

</script>
<?php
}
?>
<?php
if ($active_tab == 'fundraiser') {
	?>
<form name="hid_media_fm" id="hid_media_fm" action="" method="post">
    <input type="hidden" id="hid_fid" name="hid_fid" value="" />
    <input type="hidden" name="fslug" value="<?php echo $fslug; ?>" />
</form>
<script type="text/javascript">
    $(function() {
        $("#fund_start_dt").datepicker({
            dateFormat: 'mm-dd-yy',
            onSelect: function(selected) {
                var sdate = $(this).datepicker('getDate');
                if (sdate) {
                    sdate.setDate(sdate.getDate() + 1);
                }
                $("#fund_end_dt").datepicker("option", "minDate", sdate)
            }
        });

        $("#fund_end_dt").datepicker({
            dateFormat: 'mm-dd-yy',
        });

        var validator = $("#frm_fundraiser_info").submit(function() {
            tinyMCE.triggerSave(); // update underlying textarea before submit validation
        }).validate({
            ignore: "",
            rules: {
                fund_des: "required",
                fund_org_goal: {
                    required: true,
                    currency: ["$", false]
                },
                fund_individual_goal: {
                    required: true,
                    currency: ["$", false]
                },
                fund_start_dt: {
                    required: true
                },
                fund_end_dt: {
                    required: true
                }
            },
            messages: {
                fund_org_goal: {
                    currency: "Please enter valid amount.",
                },
                fund_individual_goal: {
                    currency: "Please enter valid amount.",
                }
            },
            errorPlacement: function(error, element) {
                // position error label after generated textarea
                if (element.is("textarea")) {
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element)
                }
            }
        });
        validator.focusInvalid = function() {
            // put focus on tinymce on submit validation
            if (this.settings.focusInvalid) {
                try {
                    var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                    if (toFocus.is("textarea")) {
                        tinyMCE.get(toFocus.attr("id")).focus();
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch (e) {
                    // ignore IE throwing errors when focusing hidden elements
                }
            }
        }

    });

    function img_vid_add() {
        var stss = true;
        var mtype = $('#media_type').val();
        if (mtype == 'V') {
            $('.crr').remove();
            var vid = $("#youtube_fil").val();
            if (vid != '') {
                stss = true;
            } else {
                $('#youtube_fil').after('<label for="youtube_fil" generated="true" class="crr error">This field is required.</label>');
                stss = false;
            }
        } else {
            $('.crr').remove();
            var img = $("#feature_img").val();
            if (img != '') {
                var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
                if ($.inArray($('#feature_img').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    $('#feature_img').after('<label for="feature_img" generated="true" class="crr error">Please upload valid file type.</label>');
                } else {
                    /*-----------------*/
                    var val_height = '720';
                    var val_width = '1280';

                    var formData = new FormData();
                    formData.append('file', $('#feature_img')[0].files[0]);
                    formData.append('valid_height', val_height);
                    formData.append('valid_width', val_width);

                    var eReport = ''; //error report
                    var sts = '';
                    $.ajax({
                        url: "<?php echo base_url($fslug . '/admin/checkfile'); ?>",
                        type: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: false,
                        dataType: "json",
                        success: function(res) {
                            if (res.valid !== 'true') {
                                $('#feature_img').after('<label for="feature_img" generated="true" class="crr error">Image Dimension must be greater than ' + val_width + 'x' + val_height + '.</label>');
                                sts = false;
                            } else {
                                sts = true;
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            $('#feature_img').after('<label for="feature_img" generated="true" class="crr error">Image Dimension must be greater than ' + val_width + 'x' + val_height + '.</label>');
                            sts = false;
                        }
                    });
                    stss = sts;
                    /*-----------------*/
                }
            } else {
                stss = false;
            }
        }
        if (stss == true) {
            var mtype = $('#media_type').val();
            var formData = new FormData();

            if (mtype == 'I') {
                formData.append('feature_img', $('#feature_img')[0].files[0]);
            } else {
                formData.append('youtube_fil', $('#youtube_fil').val());
            }
            formData.append('media_type', $('#media_type').val());

            $.ajax({
                url: "<?php echo base_url($fslug . '/admin/upload_media'); ?>",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",
                beforeSend: function() {
                    // $('#div_loading').html('<div class="loading_inn">loading...</div>');
                },
                success: function(results) {
                    if (results.valid == 1) {
                        $("#img_vid_list").prepend(results.html);
                        if (results.mtype == 'I') {
                            $('#feature_img').val('');
                        } else {
                            $('#youtube_fil').val('');
                        }
                    }
                }
            });
        }
    }

</script>
<script src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.min.js'); ?>"></script>
<script>
    tinymce.init({
        selector: 'textarea.editme',
        height: 250,
        menubar: false,
        plugins: [
            'advlist autolink lists charmap preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media contextmenu paste code'
        ],
        toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect fontsizeselect',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        onchange_callback: function(editor) {
            tinyMCE.triggerSave();
            $("#" + editor.id).valid();
        },
        encoding: "xml"
    });

</script>
<script type="text/javascript">
    function del_media(v) {
        var r = confirm("Are you sure to delete this image/video?");
        if (r == true) {
            $('#hid_media_fm').attr('action', '<?php echo base_url($fslug . '/admin/delMedia'); ?>');
            $('#hid_fid').val(v);
            $('#hid_media_fm').submit();
        }
    }

</script>
<?php
}
?>
<?php
if ($active_tab == 'participants') {
	?>
<div id="emailAllMailModal" class="modal fade" role="dialog"></div>
<script type="text/javascript">
    $(document).ready(function($) {
        $.validator.setDefaults({
            ignore: ':hidden:not(.do-not-ignore)'
        });
        $("#frm_participants_info").validate({
            rules: {
                player_fname: {
                    required: true
                },
                player_lname: {
                    required: true
                },
                player_email: {
                    required: true,
                    email: true
                },
                player_image: {
                    extension: "png|jpg|jpeg|gif"
                }
            },
            messages: {
                player_image: {
                    extension: "Please upload valid file type."
                }
            }
        });
        $("#bulk_player").change(function() {
            $('#frm_upload_info').validate({
                rules: {
                    bulk_player: {
                        extension: "csv|xls|csvx|xlsx"
                    }
                },
                messages: {
                    bulk_player: {
                        extension: "Please upload valid file type."
                    }
                }
            });
            $('#bulk_submit').trigger('click');
        });

        $("#emailall").click(function(e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url($fslug . '/admin/getEmailToAllAjax'); ?>",
                data: "fslug=<?php echo $fslug; ?>",
                dataType: "html",
                success: function(response) {
                    $("#emailAllMailModal").html(response);
                    $("#emailAllMailModal").modal('show');
                }
            });
        });
    });

</script>
<?php
}
?>


<!-- Edit Login Credential -->
<div id="edit_login_credential" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="tit1_sec clearfix">
                    <div class="left">
                        <h2>Edit Login Credentials</h2>
                        <div class="ms" style="display:none;"></div>
                    </div>
                </div>
                <div class="add_plr_wrp">
                    <form method="post" name="changesettings" id="changesettings">
                        <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="frm">
                                    <div class="form-group">
                                        <label>Change password for</label>
                                        <input type="text" class="form-control" id="fund_email" name="fund_email" value="<?php echo $this->session->userdata('fundraiser_contact_email'); ?>" readonly />
                                    </div>
                                    <div class="form-group">
                                        <label for="new-pass">New password</label>
                                        <input type="password" class="form-control" id="fund_pass" name="fund_pass" required />
                                    </div>
                                    <div class="form-group">
                                        <label for="re-pass">Retype new password</label>
                                        <input type="password" class="form-control" id="fund_pass_confirm" name="fund_pass_confirm" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="frm_btn_grp">
                            <input type="submit" class="btn_round" name="submit" value="Save" />
                            <a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#changesettings").validate({
            rules: {
                fund_pass: {
                    required: true
                },
                fund_pass_confirm: {
                    required: true,
                    equalTo: "#fund_pass"
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "<?php echo base_url('ajaxSettings'); ?>",
                    type: "POST",
                    data: $("#changesettings").serialize(),
                    dataType: "json",
                    success: function(results) {
                        if (results.valid == 1) {
                            $('.ms').html(results.msg);
                            $('.ms').show();
                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        } else {
                            $('.ms').html(results.msg);
                            $('.ms').show();
                        }
                    }
                });
                return false;
            }
        });


    });

</script>
<!-- Edit Login Credential ends -->
<?php
//if($active_tab=='startup'){
?>
<div id="myParticipant" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">AGE ACKNOWLEDGEMENT</h4>
            </div>
            <div class="modal-body">
                <div class="ms" style="display:none;"></div>
                <p>Children under the age of 13 are NOT permitted to use the Platform or participate in the Fundraiser under any circumstances. By clicking below, I
                    acknowledge and agree that I have not invited anyone under the age of 13 to participate in this Fundraiser. </p>
                <p><b>NOTE: </b>If there is a member of your Organization who is under 13 years of age, please invite their parents or legal guardians who are 18 years of age or older to participate on their behalf.</p>
            </div>
            <div class="modal-footer"> <a href="<?php echo base_url($fslug . '/admin/invitationtoall'); ?>" id="agree_send" class="btn34">Agree & Send Invitations</a>
                <div class="clearfix"></div>
                <div class="goback-btn " data-dismiss="modal">(go back)</div>
            </div>
        </div>
    </div>
</div>

<div id="addParticipants" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="participants_info">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Participants</h4>
            </div>
            <div class="modal-body">

                <form action="<?php echo base_url($fslug . '/admin/participants'); ?>" id="frm_participants_info" name="frm_participants_info" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <div style="margin:auto;">
                            <span class="pic"><img style="width: 150px;" id="uploadPreview1" src="<?php echo base_url('assets/images/no_image.png'); ?>" /></span>
                        </div>
                        <div class="chng_up_file_wrp">
                            <input style="margin: 0;" class="chng_up_file" id="uploadImage1" placeholder="Change" type="file" name="player_image" onchange="PreviewImage(1);" />
                            <a style="right: initial;" href="JavaScript:Void(0);">Upload</a>
                        </div>
                    </div>
                    <div class="form-group"><label for="player_fname">First</label>
                        <input type="text" id="player_fname" name="player_fname" class="form-control"></div>
                    <div class="form-group"><label for="player_lname">Last</label>
                        <input type="text" id="player_lname" name="player_lname" class="form-control"></div>
                    <div class="form-group">
                        <label for="player_email">Email Address</label>
                        <input type="email" id="player_email" name="player_email" class="form-control"></div>
                    <div class="form-group"><input type="submit" name="participant_submit" value="Add" class="btn34"></div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="myModalContact" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Contact</h4>
            </div>
            <div class="modal-body clearfix">
                <div class="add-new-email-row">
                    <form action="<?php echo base_url($fslug . "/admin/contacts/" . $pslug); ?>" method="POST" id="frm_contact_add" name="frm_contact_add" enctype="multipart/form-data" class="add-email-form">
                        <div class="col-xs-6 email-input">
                            <input type="text" class="form-control required email" id="contact_email" name="contact_email">
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 email-input-action text-center">
                            <button id="con_submit" name="con_submit" type="submit" class="btn34 btn-add-email">Add</button>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 email-input-action text-center">
                            <!--<button id="cus_mail" name="cus_mail" type="button" class="btn34 btn-add-email">Send Custom Mail</button>-->
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



<script type="text/javascript">
    function getPoup(url) {
        $.ajax({
            url: url,
            type: 'POST',
            success: function(results) {
                $('#participants_info').html(results);
                $('#addParticipants').modal('show');

            }
        });
    }

</script>
