<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fundraiser extends MY_FundraiserController{

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('fundraiser_model');
	}

	public function index($fslug)
	{
		if($this->session->userdata('fundraiser_id')!=""){
			redirect($this->session->userdata('fundraiser_urlname').'/admin/home');
		}else{
			redirect(base_url("admin/login"));
		}
	}

	/*public function home($fslug)
	{
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_dashboard');
		$this->load->view('../../includes_fundraiser/footer');
	}*/
	
	public function checkfile()
	{
		$getswidth = $this->input->post("valid_width");
		$getsheight = $this->input->post("valid_height");
		$image_info = getimagesize($_FILES["file"]["tmp_name"]);
		$image_width = $image_info[0];
		$image_height = $image_info[1];
		if($image_width >= $getswidth && $image_height >= $getsheight)
		{
			echo json_encode(array('valid'=>'true','msg'=>'Image Dimension Correct'));
		}
		else
		{
			echo json_encode(array('valid'=>'false','msg'=>'Image Dimension Exceeds'));
		}
		exit;
	}
	
	/*-----------------------------------Player Section Start---------------------------------*/

	public function players($fslug)
	{
		$limit = 0;
		$offset = 10;
		$fund_id = $this->session->userdata('fundraiser_id');
		$data["players_list"] = $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_player_list',$data);
		$this->load->view('../../includes_fundraiser/footer');
	}
	
	public function playerloadmore()
	{
		$offset = 10;
		$limit = $this->input->post('limit')*$offset;
		$fund_id = $this->session->userdata('fundraiser_id');
		$players_list = $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$tbr='';
		if(count($players_list)>0)
		{
		foreach($players_list as $players_li)
		{
		$org_goal = $players_li['player_goal'];
		$raised_amt = $players_li['raised_amt'];
		$remaining_goal = ($org_goal - $raised_amt);
		$tbr.= '<tr>
				<td data-th="Player Name">
				<span><img src="'.base_url().'assets/fundraiser/images/pro_pic3.png" alt=""></span>
				<strong>
				<a href="'.base_url().'fundraiser/'.$this->session->userdata('fundraiser_urlname').'/players/profile/'.$players_li["player_slug_name"].'">'.$players_li["player_fname"].' '.$players_li["player_lname"].'</a>
				</strong>
				</td>
				<td data-th="Title">'.$players_li["player_title"].'</td>
				<td data-th="Goal" class="text-right">$'.number_format($org_goal,2,'.',',').'</td>
				<td data-th="Raised" class="text-right">$'.number_format($raised_amt,2,'.',',').'</td>
				<td data-th="Remaining" class="text-right">$'.number_format($remaining_goal,2,'.',',').'</td>
				<td data-th="" class="text-right"><small>'.date("F d, Y",strtotime($players_li["created"])).'</small></td>
			    </tr>';
		}
		echo $tbr;
		}
		else
		{
		echo 0;	
		}
	}
	
	public function getMailStructure()
	{
		$pslug=$this->input->post('pslug');  
		$donor=$this->input->post('donor');  
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		if($donor!='')
		{
			$donor_details=$this->fundraiser_model->view('tbl_donors_payment',array("id"=>base64_decode($donor)));
		}
        $data['playerInfo']	= $player_details;  
        $data['donorInfo']	= $donor_details;  
        echo $this->load->view('view_player_mail_modal', $data, true); 
	}
	
	public function getMailfromReports()
	{
		$pslug=$this->input->post('pslug');
		$report=$this->input->post('report');  
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		if($report!='')
		{
			$report_details=$this->fundraiser_model->view('tbl_email_share_reports',array("id"=>base64_decode($report)));
		}
		$data['playerInfo']	= $player_details;  
		$data['reportInfo']	= $report_details;  
		echo $this->load->view('view_thanks_mail_modal', $data, true); 
	}
	
	public function sendingThanks()
	{
		$pid=$this->input->post('hid_pid'); 
		$did=$this->input->post('hid_did');
        if($pid!='' AND $did!='')
		{
			$player_details=$this->fundraiser_model->view('tbl_player',array("id"=>base64_decode($pid)));
			$player_name = $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"];
			$player_email = $player_details[0]["player_email"];
			$donor_details=$this->fundraiser_model->view('tbl_donors_payment',array("id"=>base64_decode($did)));
			$donor_name = $donor_details[0]["donor_fname"].' '.$donor_details[0]["donor_lname"];
			$donor_email = $donor_details[0]["donor_email"];
			
			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "smtp";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;
			$str = "<p>Dear $donor_name,</p>
			<p>Thank you so much for your donation. I really appreciate you thinking of me and you couldn't have chosen a more perfect present for me. Again, thanks very much.</p>";
			$this->email->initialize($config);   
			$this->email->from($player_email);  
			$this->email->to($donor_email); 
			$this->email->subject("Thanks for Supporting"); 
			$this->email->message($str);
			$bval=$this->email->send();
			echo json_encode(array('msg'=>1));
		}
		else
		{
			echo json_encode(array('msg'=>0));
		}
	}
	
	public function sendThanksfromReports()
	{
		$rid=$this->input->post('hid_rid'); 
		if($rid!='')
		{
			$report_details	= $this->fundraiser_model->view('tbl_email_share_reports',array("id"=>base64_decode($rid)));
			$from_email		= $report_details[0]["from_email"];
			$to_email		= $report_details[0]["to_email"];

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "smtp";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;
			$str = "<p>Dear Friend,</p>
			<p>Thank you so much for your donation. I really appreciate you thinking of me and you couldn't have chosen a more perfect present for me. Again, thanks very much.</p>";
			$this->email->initialize($config);   
			$this->email->from($from_email);  
			$this->email->to($to_email); 
			$this->email->subject("Thanks for Supporting"); 
			$this->email->message($str);
			$bval=$this->email->send();
			echo json_encode(array('msg'=>1));
		}
		else
		{
			echo json_encode(array('msg'=>0));
		}
	}
		
	public function addeditplayer($id=0)
	{
		if(isset($_POST["submit"]))
		{
		$this->form_validation->set_rules('player_email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('player_fname', 'First Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('player_lname', 'Last Name', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('player_pass', 'Password', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('player_goal', 'Goal', 'trim|required|xss_clean');	
		if ($this->form_validation->run() != FALSE)
		{
			$player_image= isset($_FILES['player_image']['name'])?$_FILES['player_image']['name']:"";
			
			//------------------------------------- 
            //***Player Image upload section***
            //-------------------------------------
			$fpath1= "";
			if($player_image!="") 
            {
				
                /* Create the config for upload library */				
				$config_img['upload_path']   = './assets/player_image/';
				$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config_img['max_size']      = '100000';
				$this->load->library('upload', $config_img);
				$this->upload->initialize($config_img);			 
				$this->upload->do_upload('player_image');			
				$data_img = $this->upload->data(); 
				if($data_img['is_image'] == 1)
				{ 
					$fpath1  = $data_img['file_name'];			
					$this->load->library('image_lib');
					$config_resize['image_library']   = 'gd2';	
					$config_resize['create_thumb']    = TRUE;
					$config_resize['maintain_ratio']  = TRUE;
					$config_resize['master_dim']      = 'height';
					$config_resize['quality']         = "100%";  
					$config_resize['source_image']    = './assets/player_image/'.$fpath1;
					$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
					$config_resize['height']          = 120;
					$config_resize['width']           = 1;
					$this->image_lib->initialize($config_resize);
					$this->image_lib->resize(); 
				}
            }
			
			$encrypt_key = $this->config->item('encryption_key');  
			$player_added_by = $this->session->userdata('fundraiser_id');
			$player_email = $this->input->post('player_email');
			$player_fname = $this->input->post('player_fname');
			$player_lname = $this->input->post('player_lname');
			$player_goal  = $this->input->post('player_goal');
			$player_pass  = $this->input->post('player_pass');
			$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
			
			$dt_array = array(
			'player_email' => $player_email,
			'player_fname' => $player_fname,
			'player_lname' => $player_lname,
			'player_goal'  => $player_goal,
			'player_pass'  => $player_passw,
			'player_image' => $fpath1,
			'player_added_by' => $player_added_by,
			'created' => date("Y-m-d H:i:s")
			);
			$this->fundraiser_model->add('tbl_player',$dt_array);
			
			//-------------------------------------------  
            //Player Slug Creation
            //-------------------------------------------
            $lastInsId     = $this->db->insert_id();    
            $fname_slug    = strip_tags($player_fname);
            $lname_slug    = strip_tags($player_lname);  
            $jSlug         = $fname_slug.'-'.$lname_slug;    
            $playerURL	   = strtolower(url_title($jSlug));
            if($this->fundraiser_model->isPlayerUrlExists('tbl_player',$playerURL,$lastInsId)==1){
               $playerURL = $playerURL.'-'.$lastInsId; 
            }
            $x = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
            //---------------------------------------------
			if($x)
			{
				$fund_details		= $this->fundraiser_model->view('tbl_fundraiser',array('id'=>$player_added_by));
				$fund_email 		= $fund_details[0]["fund_email"];
				$fundraiser_id		= $fund_details[0]['id'];
				$fundraiser_slug	= $fund_details[0]['fund_slug_name'];
				//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
				$fundraiser_name	= $fund_details[0]['fund_username'];
				$fundraiser_start	= $fund_details[0]['fund_start_dt'];
				$fundraiser_end		= $fund_details[0]['fund_end_dt'];
				$fundraiser_email	= $fund_details[0]['fund_email'];
				$fundraiser_no		= $fund_details[0]['fund_contact'];
				$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
				$selected_color		= $fund_details[0]['fund_color_rgb'];
				$player_login_url	= base_url('admin/login');
				
				$template = $this->fundraiser_model->view('tbl_mail_template',array('template_slug'=>'login-credentials-to-player-from-fundraiser'));

				$template_body		= html_entity_decode(htmlspecialchars_decode($template[0]["template_content"]));
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNo]','https://thanksforsupporting.com/fundraiser/player/','[selected_color]');
				$search_val			= array($player_fname,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$selected_color);
				$email_content		= str_replace($search_key,$search_val,$template_body);

				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com','ThanksforSupporting');  
				$this->email->to($player_email); 
				$this->email->subject('ThanksforSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();
			}
			$this->session->set_userdata('succ_msg', 'Player Added Successfully');
			redirect($this->session->userdata('fundraiser_urlname').'/admin/players');
		}
		}
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_addeditplayer');
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function playerSummary($fslug, $pslug)
	{
		$player_details = array();
		$donor_details = array();
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->playersInfo($pslug);
			$donor_details=$this->fundraiser_model->donors_list($pslug,10);
		}
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_player_summary',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function playerDonors($fslug, $pslug)
	{
	
		$player_details = array();
		$donor_details = array();
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->playersInfo($pslug);
			$donor_details=$this->fundraiser_model->donors_list($pslug,10);
		}
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_player_donors',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function playerEmailReportsGraph($fslug, $pslug)
	{
		$player_details = array();
		$player_id = 0;
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->playersInfo($pslug);
			$player_id = $player_details[0]['id'];
		}
		$report_chart = $this->fundraiser_model->email_reports($player_id);
		$data["player_details"] = $player_details;
		$data['report_chart'] 	= $report_chart;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_player_emailreports_graph',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function playerEmailReportsList($fslug, $pslug)
	{
		$player_details = array();
		$report_list = array();
		$player_id = 0;
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->playersInfo($pslug);
			$report_list = $this->fundraiser_model->view('tbl_email_share_reports',array('from_id'=>$player_details[0]['id']));
			$player_id = $player_details[0]['id'];
		}
		$report_chart = $this->fundraiser_model->email_reports($player_id);
		$data["player_details"] = $player_details;
		$data['report_list'] 	= $report_list;
		$data['report_chart'] 	= $report_chart;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_player_emailreports_list',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function playerSetting($fslug, $pslug)
	{
		$player_details = array();
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->playersInfo($pslug);
		}
		$data["player_details"] = $player_details;
		$data["fslug"] 			= $fslug;
		$data["pslug"] 			= $pslug;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_player_setting',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function playerStatus()
	{
		$fund_id=$this->input->post("fid");
		$play_id=$this->input->post("pid");
		$status=$this->input->post("sts");
		$response = array();
		if($fund_id!='' AND $play_id!='' AND $status!='')
		{
		$cnt=$this->fundraiser_model->view_check('tbl_player',array("player_added_by"=>$fund_id,'id'=>$play_id));
		if($cnt > 0)
		{
		$updt=$this->fundraiser_model->edit('tbl_player',array("player_status"=>$status),array('id'=>$play_id));	
		$response=array("valid"=>1,"msg"=>"Status Updated Successfully");
		}
		else
		{
		$response=array("valid"=>0,"msg"=>"Invalid Data Provided");	
		}
		}
		else
		{
		$response=array("valid"=>0,"msg"=>"Sufficient Data Not Provided");		
		}
		echo json_encode($response);
	}
	
	public function getPlayerModalAjax()
	{
		$pslug=$this->input->post('pslug');  
		if($pslug!='')
		{
			$player_details=$this->fundraiser_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
        $data['playerInfo']      = $player_details;  
        echo $this->load->view('view_player_info_edit_modal', $data, true); 
	}
	
	public function playerInfoEditAjax()
	{
        $player_id			= $_POST['hid_pid'];
        $player_image		= isset($_FILES['file']['name'])?$_FILES['file']['name']:"";
        $player_email		= $_POST['player_email'];
        $player_fname		= $_POST['player_fname'];
        $player_lname		= $_POST['player_lname'];
        $player_goal		= $_POST['player_goal'];
    
        $arr=$this->fundraiser_model->view('tbl_player',array('id'=>$player_id));
        if($player_image!="")
        {
            if(!empty($arr)){
                if(isset($arr[0]['player_image']) && $arr[0]['player_image']!=""){
                    if(file_exists("./assets/player_image/thumb/".$arr[0]['player_image'])){
                      unlink("./assets/player_image/thumb/".$arr[0]['player_image']);
                    }
                    if(file_exists("./assets/player_image/".$arr[0]['player_image'])){
                      unlink("./assets/player_image/".$arr[0]['player_image']);
                    }
                }
            }
            /* Create the config for upload library */
			
			$config_img['upload_path']   = './assets/player_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config_img['max_size']      = '100000';
            $this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
            { 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/player_image/'.$fpath1;
				$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
				$config_resize['height']          = 120;
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$player_image = $fpath1;
			}
        }
        else
		{
            $player_image = $arr[0]['player_image'];
        }
    
        $playdata = array('player_email'	=> $this->input->post('player_email'),
                          'player_fname'	=> $this->input->post('player_fname'),
                          'player_lname'	=> $this->input->post('player_lname'),
                          'player_goal'		=> $this->input->post('player_goal'),
                          'player_image'	=> $player_image
                          );
        $return = $this->db->update('tbl_player', $playdata, array('id'=>$this->input->post('hid_pid')));
		
        if($return){
            $data['msg']=1;
        }
        else{
            $data['msg']=0;
        }
        echo json_encode($data);
	}

	public function playerEmailCheckerAjax()
	{
		$where = array('player_email'=>$this->input->post('player_email')); 
		$query=$this->fundraiser_model->view_check('tbl_player',$where);
	   
		if($query>0)
		{
			echo 'false';
		}
		else
		{
			echo 'true';
		}
	}	
	/*-----------------------------------Player Section End---------------------------------*/

	/*-------------------------------Fundraiser Profile Section Start---------------------------------*/
	
	public function orgprofile($slug)
	{
		$data['active_page']	= 'fundraiser'; 
        $data['fundraiserInfo']	= $this->fundraiser_model->fundraiserinfo($slug);
		$getId					= $this->fundraiser_model->fundraiser_getID($slug);
		$data['fundraiserImgInfo']= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_detail',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function test($slug,$pslug='')
	{
        
		$action = $this->uri->segment(3); 

		$data['active_tab']			= $action; 
		$limit 						= 0;
		$offset 					= 10;
		$fund_id 					= $this->session->userdata('fundraiser_id');
		$getId						= $this->fundraiser_model->fundraiser_getID($slug);
        $data['fundraiserInfo'] = $fundraiserInfo	= $this->fundraiser_model->fundraiserinfo($slug);		
		$data['fundraiserImgInfo']	= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->fundraiser_model->fundraiser_playersInfo($fund_id);
		$data["fslug"] = $slug;
		$data["pslug"] = $pslug;
		$data["donors_list"] = '';
		$data["player_info"] = '';
		
		 switch($action){
			case 'contacts':
			$data["player_info"] = $player_info = $this->fundraiser_model->playersInfo($pslug);
				$sub_action = $this->uri->segment(5);  
				switch($sub_action){
					case 'invitetoall':
					 $this->invitetoall($fundraiserInfo,$player_info,$slug,$pslug);
					break;
				}
			
			if(isset($_POST["con_submit"]))
			{
				$this->contact_submit($slug,$pslug,$player_info);					
			}
			//echo $pslug; exit;
			$data["contact_lists"] =  $contact_lists = $this->fundraiser_model->view('tbl_player_contacts',array('contact_added_by'=>trim($pslug)));

			break;
			case 'donations':				
			$data["donors_list"] = $donor_details=$this->fundraiser_model->donors_list($pslug);	
			break;
			case 'donors':				
			$data["donors_list_show"] = $donor_details=$this->fundraiser_model->fund_collection_list($fund_id);	
			
			break;
		
		  case 'invitationtoall':
				$this->invitationtoall($slug);
		  break;
		  case 'upload_fund_logo':
		  $this->upload_fund_logo($slug);
		  break;
		  case 'checkfile':
		  $this->checkfile();
		  break;
		  case 'upload_media':
		  $this->upload_media();
		  break;
		  case 'players':
			$fslug = $this->uri->segment(1); 
			$subAction = $this->uri->segment(4);
			$pslug = $this->uri->segment(5);
			switch($subAction){
					case 'asktoinvite':
					$this->asktoinvite($fslug,$pslug);
					break;
					case 'remindtoinvite':
					$this->remindtoinvite($fslug,$pslug);
					break;
					case 'secremindtoinvite':
					$this->secremindtoinvite($fslug,$pslug);
					break;	
					case 'secremindtoinvite':
					$this->secremindtoinvite($fslug,$pslug);
					break;	
				}
		  break;
		   case 'report':
			$data["report_list"] = $this->fundraiser_model->fundraiser_players($fund_id);
			break;		 
			 case 'home':
			 if(isset($_POST["fund_email"]))
				{
					$this->form_validation->set_rules('fund_fname', 'First Name', 'trim|required|xss_clean');
					$this->form_validation->set_rules('fund_lname', 'Last Name', 'trim|required|xss_clean');
					$this->form_validation->set_rules('fund_email', 'Email', 'trim|required|valid_email');	
					$this->form_validation->set_rules('fund_contact', 'Phone', 'trim|required|xss_clean');	
					
					if ($this->form_validation->run() != FALSE)
					{
						$fundata       = array();
						$fundata["fund_fname"]= $this->input->post('fund_fname');
						$fundata["fund_lname"]= $this->input->post('fund_lname');
						$fundata["fund_email"]= $this->input->post('fund_email');
						$fundata["fund_contact"]= $this->input->post('fund_contact');
						$fundata["fund_terms"]= $this->input->post('fund_terms');
						$fundata["step"]= $this->input->post('step');
						$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->session->userdata('fundraiser_id')));
						if($return){
							redirect($slug.'/admin/organization');
						}
					}
				}
			 break;
			 case 'organization':
				if(isset($_POST["orgz_submit"]))
				{
					$this->form_validation->set_rules('fund_username', 'Organization Name', 'trim|required|xss_clean');
					//$this->form_validation->set_rules('fund_slogan', 'Short Description', 'trim|required|xss_clean');
					$this->form_validation->set_rules('fund_city', 'City', 'trim|required|xss_clean');	
					$this->form_validation->set_rules('fund_state', 'State', 'trim|required|xss_clean');
					
					if ($this->form_validation->run() != FALSE)
					{
						$fund_id        = $this->session->userdata('fundraiser_id');
						$qslug			= $slug;
						$fund_username  = $_POST['fund_username'];
						$fund_slogan    = $_POST['fund_slogan'];
						$fund_city      = $_POST['fund_city'];
						$fund_state     = $_POST['fund_state'];
						$col_r     		= $_POST['col_r'];
						$col_g     		= $_POST['col_g'];
						$col_b     		= $_POST['col_b'];
						$col_a     		= $_POST['col_a'];
						$fund_color_hex = $_POST['hex_color'];
						$fund_color_rgb	= 'rgba('.$col_r.', '.$col_g.','.$col_b.','.$col_a.')';
						$fund_font_color= (isset($_POST['fund_font_color']) && $_POST['fund_font_color']!='')?$_POST['fund_font_color']:'';
						$fund_beneficiary_tax_id    = $_POST['fund_beneficiary_tax_id'];
						
						$fundata = array();	
						//------------------------------------Fundraiser Slug Creation--------------------------------
						$slug_check		= $this->fundraiser_model->view_check('tbl_fundraiser',array('fund_slug_name'=>$slug,'slug_change'=>'0'));
						if($slug_check > 0)
						{
							$lastInsId     = $fund_id;    
							$fname_slug    = strip_tags($fund_username);  
							
							$string 	   = str_replace(' ', '-', $fname_slug); // Replaces all spaces with hyphens.
							$st_slug	   = preg_replace('/[^A-Za-z0-9\-#$&^*()+=!~\']/', '', $string); // Removes special chars.
							$jSlug		   = strtolower(rtrim($st_slug,'-'));
							$slug		   = url_title($jSlug);
							if($this->fundraiser_model->isUrlExists('tbl_fundraiser',$slug)==1){
							   $slug = $slug.'-'.$lastInsId; 
							}
							$fundata['fund_slug_name'] = $slug;
							$fundata['slug_change'] = '1';
						}
						//---------------------------------------------------------------------------------------------
						
						$fundata['fund_username']			= $fund_username;
						$fundata['fund_slogan']				= $fund_slogan;
						$fundata['fund_city']				= $fund_city;
						$fundata['fund_state']				= $fund_state;
						$fundata['fund_color_hex']			= $fund_color_hex;
						$fundata['fund_color_rgb']			= $fund_color_rgb;
						$fundata['fund_font_color']			= $fund_font_color;
						$fundata['fund_beneficiary_tax_id']	= $fund_beneficiary_tax_id;
						$fundata["step"]= $this->input->post('step');
						$return = $this->db->update('tbl_fundraiser',$fundata,array('id'=>$fund_id));
						if($return){
							redirect($slug.'/admin/bankinfo');
						}else{
							$slug = $qslug;
						}
					}
				}
			 break;
			 case 'bankinfo':
			 //print_r($this->input->post()); exit;
			 $inputData= $this->input->post();
					if(isset($inputData["bank_submit"]) && $inputData["bank_submit"] != '')
					{
					if($inputData["fund_pay_type"] == 'P'){
					$this->form_validation->set_rules('fund_pay_type', 'Payment Type', 'trim|required|xss_clean');
					$this->form_validation->set_rules('fund_paypal_acc', 'Paypal Account', 'trim|required|xss_clean');	
					$this->form_validation->set_rules('fund_donor_statement', 'Donor Statement', 'trim|required|xss_clean');
					}else{
					$this->form_validation->set_rules('make_check_payable_to', 'Make check payable to', 'trim|required|xss_clean');
					$this->form_validation->set_rules('city', 'CITY', 'trim|required|xss_clean');
					$this->form_validation->set_rules('fund_state', 'STATE', 'trim|required|xss_clean');
					$this->form_validation->set_rules('zip', 'ZIP', 'trim|required|xss_clean');
					$this->form_validation->set_rules('street_address', 'Street Address', 'trim|required|xss_clean');
					}

					if ($this->form_validation->run() != FALSE)
					{
					unset($inputData['bank_submit']);
					$fundata = $inputData;
					//print_r($fundata); exit;
					$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->session->userdata('fundraiser_id')));
					if($return){
						redirect($slug.'/admin/fundraiser');
					}
					}	
					}

			 break;
			 case 'fundraiser':
				if(isset($_POST["fundraiser_submit"]))
				{
					$this->form_validation->set_rules('fund_org_goal', 'Overall Donations Goal', 'trim|required|xss_clean');
					$this->form_validation->set_rules('fund_individual_goal', 'Donation Goal Per Participant', 'trim|required|xss_clean');
					$this->form_validation->set_rules('fund_start_dt', 'Start Date', 'trim|required|xss_clean');	
					$this->form_validation->set_rules('fund_end_dt', 'End Date', 'trim|required|xss_clean');	
					$this->form_validation->set_rules('fund_des', 'Fundraiser Description', 'trim|required');	
					if ($this->form_validation->run() != FALSE){
						$fund_id	   = $this->session->userdata('fundraiser_id');
						$fund_des_static 	   = $this->input->post('fund_des_static');
						$fund_des 	   = $this->input->post('fund_des');
						$step 	   = $this->input->post('step');
						$fund_start_dt = $this->input->post('fund_start_dt');
						$ex_ful_st_dt  = explode('-',$fund_start_dt);
						$ex_ful_st_dt  = $ex_ful_st_dt[2].'-'.$ex_ful_st_dt[0].'-'.$ex_ful_st_dt[1];
						$fund_end_dt   =  $this->input->post('fund_end_dt');
						$ex_ful_en_dt  = explode('-',$fund_end_dt);
						$ex_ful_en_dt  = $ex_ful_en_dt[2].'-'.$ex_ful_en_dt[0].'-'.$ex_ful_en_dt[1];
						$fund_org_goal = str_replace(["$",","],"",$this->input->post('fund_org_goal'));
						$fund_individual_goal = str_replace(["$",","],"",$this->input->post('fund_individual_goal'));
						$fundata    = array(
						'fund_start_dt'			=> $ex_ful_st_dt,
						'fund_end_dt'			=> $ex_ful_en_dt,
						'fund_org_goal'         => $fund_org_goal,
						'fund_des'        		=> $fund_des,
						'fund_des_static'       => @$fund_des_static,
						'fund_individual_goal'  => $fund_individual_goal,
						'step'=>$step
						);
						$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$fund_id));
						if($return){
							redirect($slug.'/admin/participants');
						}
					}
				}	
			 break;
			 case 'participants':
				if(isset($_POST["participant_submit"]))
				{
					$this->form_validation->set_rules('player_email', 'Email', 'trim|required|valid_email');
					$this->form_validation->set_rules('player_fname', 'First Name', 'trim|required|xss_clean');
					$this->form_validation->set_rules('player_lname', 'Last Name', 'trim|required|xss_clean');		
					if ($this->form_validation->run() != FALSE){
						$player_image= isset($_FILES['player_image']['name'])?$_FILES['player_image']['name']:"";
						//------------------------------------- 
						//***Player Image upload section***
						//-------------------------------------
						$fpath1= "";
						if($player_image!="") 
						{
							/* Create the config for upload library */				
							$config_img['upload_path']   = './assets/player_image/';
							$config_img['allowed_types'] = 'gif|jpg|png|jpeg';
							$config_img['max_size']      = '100000';
							$this->load->library('upload', $config_img);
							$this->upload->initialize($config_img);			 
							$this->upload->do_upload('player_image');			
							$data_img = $this->upload->data(); 
							if($data_img['is_image'] == 1)
							{ 
								$fpath1  = $data_img['file_name'];			
								$this->load->library('image_lib');
								$config_resize['image_library']   = 'gd2';	
								$config_resize['create_thumb']    = TRUE;
								$config_resize['maintain_ratio']  = TRUE;
								$config_resize['master_dim']      = 'height';
								$config_resize['quality']         = "100%";  
								$config_resize['source_image']    = './assets/player_image/'.$fpath1;
								$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
								$config_resize['height']          = 120;
								$config_resize['width']           = 1;
								$this->image_lib->initialize($config_resize);
								$this->image_lib->resize(); 
							}
						}
						
						$encrypt_key = $this->config->item('encryption_key');  
						$player_added_by = $this->session->userdata('fundraiser_id');
						$this->db->update('tbl_fundraiser', array('step'=>6), array('id'=>$player_added_by));
						$player_email = $this->input->post('player_email');
						$player_fname = $this->input->post('player_fname');
						$player_lname = $this->input->post('player_lname');
						$player_id = ($this->input->post('player_id'))?$this->input->post('player_id'):'';
						$player_pass  = time();
						$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
						
						$dt_array = array(
						'player_email' => $player_email,
						'player_fname' => $player_fname,
						'player_lname' => $player_lname,
						'player_pass'  => $player_passw,
						'player_image' => $fpath1,
						'player_added_by' => $player_added_by,
						'created' => date("Y-m-d H:i:s")
						);
						if($player_id) 
						{
							$this->fundraiser_model->edit('tbl_player',$dt_array,array('id'=>$player_id));
							$lastInsId     = $player_id;    
						}
							else 
							{
								$this->fundraiser_model->add('tbl_player',$dt_array);
								$lastInsId     = $this->db->insert_id();    
							}
						
						//-------------------------------------------  
						//Player Slug Creation
						//-------------------------------------------
						
						$fname_slug    = strip_tags($player_fname);
						$lname_slug    = strip_tags($player_lname);  
						$jSlug         = $fname_slug.'-'.$lname_slug;    
						$playerURL	   = strtolower(url_title($jSlug));
						if($this->fundraiser_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
						   $playerURL = $playerURL.'-'.$lastInsId; 
						}
						$return = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
						if($return){
							$this->session->set_userdata('up_msg','Added successfully');
							//redirect($slug.'/admin/participants');
							redirect($_SERVER['HTTP_REFERER']);
						}
					}
				}
			 break;
			 }
		
		//echo '<pre>';print_r($data);die;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('fundraiser_dashboard_test',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
    
    public function home($slug)
	{
        
		if(isset($_POST["fund_email"]))
		{
			$this->form_validation->set_rules('fund_fname', 'First Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_lname', 'Last Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_email', 'Email', 'trim|required|valid_email');	
			$this->form_validation->set_rules('fund_contact', 'Phone', 'trim|required|xss_clean');	
			
			if ($this->form_validation->run() != FALSE)
			{
				$fundata       = array();
				$fundata["fund_fname"]= $this->input->post('fund_fname');
				$fundata["fund_lname"]= $this->input->post('fund_lname');
				$fundata["fund_email"]= $this->input->post('fund_email');
				$fundata["fund_contact"]= $this->input->post('fund_contact');
				$fundata["fund_terms"]= $this->input->post('fund_terms');
				$fundata["step"]= $this->input->post('step');
				echo "<pre>"; print_r($fundata); echo "</pre>"; exit;
				$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->session->userdata('fundraiser_id')));
				if($return){
					redirect($slug.'/admin/organization');
				}
			}
		}
		$data['active_tab']			= 'home'; 
		$limit 						= 0;
		$offset 					= 10;
		$fund_id 					= $this->session->userdata('fundraiser_id');
		$getId						= $this->fundraiser_model->fundraiser_getID($slug);
        $data['fundraiserInfo']		= $this->fundraiser_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);

		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	public function organization_tab($slug)
	{
		
		if(isset($_POST["orgz_submit"]))
		{
			$this->form_validation->set_rules('fund_username', 'Organization Name', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('fund_slogan', 'Short Description', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_city', 'City', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('fund_state', 'State', 'trim|required|xss_clean');
			
			if ($this->form_validation->run() != FALSE)
			{
				$fund_id        = $this->session->userdata('fundraiser_id');
				$qslug			= $slug;
				$fund_username  = $_POST['fund_username'];
				$fund_slogan    = $_POST['fund_slogan'];
				$fund_city      = $_POST['fund_city'];
				$fund_state     = $_POST['fund_state'];
				$col_r     		= $_POST['col_r'];
				$col_g     		= $_POST['col_g'];
				$col_b     		= $_POST['col_b'];
				$col_a     		= $_POST['col_a'];
				$fund_color_hex = $_POST['hex_color'];
				$fund_color_rgb	= 'rgba('.$col_r.', '.$col_g.','.$col_b.','.$col_a.')';
				$fund_font_color= $_POST['fund_font_color'];
				$fund_beneficiary_tax_id    = $_POST['fund_beneficiary_tax_id'];
				
				$fundata = array();	
				//------------------------------------Fundraiser Slug Creation--------------------------------
				$slug_check		= $this->fundraiser_model->view_check('tbl_fundraiser',array('fund_slug_name'=>$slug,'slug_change'=>'0'));
				if($slug_check > 0)
				{
					$lastInsId     = $fund_id;    
					$fname_slug    = strip_tags($fund_username);  
					
					$string 	   = str_replace(' ', '-', $fname_slug); // Replaces all spaces with hyphens.
					$st_slug	   = preg_replace('/[^A-Za-z0-9\-#$&^*()+=!~\']/', '', $string); // Removes special chars.
					$jSlug		   = strtolower(rtrim($st_slug,'-'));
					$slug		   = url_title($jSlug);
					if($this->fundraiser_model->isUrlExists('tbl_fundraiser',$slug)==1){
					   $slug = $slug.'-'.$lastInsId; 
					}
					$fundata['fund_slug_name'] = $slug;
					$fundata['slug_change'] = '1';
				}
				//---------------------------------------------------------------------------------------------
				
				$fundata['fund_username']			= $fund_username;
				$fundata['fund_slogan']				= $fund_slogan;
				$fundata['fund_city']				= $fund_city;
				$fundata['fund_state']				= $fund_state;
				$fundata['fund_color_hex']			= $fund_color_hex;
				$fundata['fund_color_rgb']			= $fund_color_rgb;
				$fundata['fund_font_color']			= $fund_font_color;
				$fundata['fund_beneficiary_tax_id']	= $fund_beneficiary_tax_id;
				$return = $this->db->update('tbl_fundraiser',$fundata,array('id'=>$fund_id));
				if($return){
					redirect($slug.'/admin/bankinfo');
				}else{
					$slug = $qslug;
				}
			}
		}
		$data['active_tab']			= 'organization'; 
		$limit 						= 0;
		$offset 					= 10;
		$fund_id 					= $this->session->userdata('fundraiser_id');
		$getId						= $this->fundraiser_model->fundraiser_getID($slug);
        $data['fundraiserInfo']		= $this->fundraiser_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	public function bankinfo_tab($slug)
	{
		if(isset($_POST["bank_submit"]))
		{
			if($this->input->post('fund_pay_type')=='P'){
				$this->form_validation->set_rules('fund_pay_type', 'Payment Type', 'trim|required|xss_clean');
				$this->form_validation->set_rules('fund_paypal_acc', 'Paypal Account', 'trim|required|xss_clean');	
				$this->form_validation->set_rules('fund_donor_statement', 'Donor Statement', 'trim|required|xss_clean');
			}else{
				//$this->form_validation->set_rules('fund_bank_name', 'Bank Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('make_check_payable_to', 'Make check payable to', 'trim|required|xss_clean');
				$this->form_validation->set_rules('city', 'CITY', 'trim|required|xss_clean');
				$this->form_validation->set_rules('state', 'STATE', 'trim|required|xss_clean');
				$this->form_validation->set_rules('zip', 'ZIP', 'trim|required|xss_clean');
				$this->form_validation->set_rules('street_address', 'Street Address', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('fund_donor_statement', 'Donor Statement', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('fund_routing_number', 'Bank Routing Number', 'trim|required|xss_clean');	
				//$this->form_validation->set_rules('fund_account_number', 'Bank Account Number', 'trim|required|xss_clean');		
			}
			
			if ($this->form_validation->run() != FALSE)
			{
				$fundata       = array();
				
				if($this->input->post('fund_pay_type')=='P'){
					$fundata["fund_pay_type"]		= $this->input->post('fund_pay_type');
					$fundata["fund_paypal_acc"]		= $this->input->post('fund_paypal_acc');
					$fundata["fund_donor_statement"]= $this->input->post('fund_donor_statement');
					$fundata["make_check_payable_to"]		= "";
					$fundata["city"]		= "";
					$fundata["state"]		= "";
					$fundata["zip"]		= "";
					$fundata["street_address"]		= "";
				}else{
					/*$fundata["fund_pay_type"]		= $this->input->post('fund_pay_type');
					$fundata["fund_bank_name"]		= $this->input->post('fund_bank_name');
					$fundata["fund_donor_statement"]= $this->input->post('fund_donor_statement');
					$fundata["fund_routing_number"]	= $this->input->post('fund_routing_number');
					$fundata["fund_account_number"]	= $this->input->post('fund_account_number');*/
					$fundata["fund_paypal_acc"]		= "";
					$fundata["fund_pay_type"]		= $this->input->post('fund_pay_type');
					$fundata["fund_donor_statement"]= $this->input->post('fund_donor_statement');
					$fundata["make_check_payable_to"]		= $this->input->post('make_check_payable_to');
					$fundata["city"]		= $this->input->post('city');
					$fundata["state"]		= $this->input->post('state');
					$fundata["zip"]		= $this->input->post('zip');
					$fundata["street_address"]		= $this->input->post('street_address');
				}
				$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->session->userdata('fundraiser_id')));
				if($return){
					redirect($slug.'/admin/fundraiser');
				}
			}	
		}
		$data['active_tab']			= 'bankinfo'; 
		$limit 						= 0;
		$offset 					= 10;
		$fund_id 					= $this->session->userdata('fundraiser_id');
		$getId						= $this->fundraiser_model->fundraiser_getID($slug);
        $data['fundraiserInfo']		= $this->fundraiser_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	public function fundraiser_tab($slug)
	{
		if(isset($_POST["fundraiser_submit"]))
		{
			$this->form_validation->set_rules('fund_org_goal', 'Overall Donations Goal', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_individual_goal', 'Donation Goal Per Participant', 'trim|required|xss_clean');
			$this->form_validation->set_rules('fund_start_dt', 'Start Date', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('fund_end_dt', 'End Date', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('fund_des', 'Fundraiser Description', 'trim|required');	
			if ($this->form_validation->run() != FALSE)
			{
				$fund_id	   = $this->session->userdata('fundraiser_id');
				$fund_des_static 	   = $this->input->post('fund_des_static');
				$fund_des 	   = $this->input->post('fund_des');
				$fund_start_dt = $this->input->post('fund_start_dt');
				$ex_ful_st_dt  = explode('-',$fund_start_dt);
				$ex_ful_st_dt  = $ex_ful_st_dt[2].'-'.$ex_ful_st_dt[0].'-'.$ex_ful_st_dt[1];
				$fund_end_dt   =  $this->input->post('fund_end_dt');
				$ex_ful_en_dt  = explode('-',$fund_end_dt);
				$ex_ful_en_dt  = $ex_ful_en_dt[2].'-'.$ex_ful_en_dt[0].'-'.$ex_ful_en_dt[1];
				$fund_org_goal = str_replace(["$",","],"",$this->input->post('fund_org_goal'));
                $fund_individual_goal = str_replace(["$",","],"",$this->input->post('fund_individual_goal'));

				$fundata    = array(
				'fund_start_dt'			=> $ex_ful_st_dt,
				'fund_end_dt'			=> $ex_ful_en_dt,
				'fund_org_goal'         => $fund_org_goal,
				'fund_des'        		=> $fund_des,
				'fund_des_static'       => @$fund_des_static,
				'fund_individual_goal'  => $fund_individual_goal);
				$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$fund_id));
				if($return){
					redirect($slug.'/admin/participants');
				}
			}
		}
		$data['active_tab']			= 'fundraiser'; 
		$limit 						= 0;
		$offset 					= 10;
		$fund_id 					= $this->session->userdata('fundraiser_id');
		$getId						= $this->fundraiser_model->fundraiser_getID($slug);
        $data['fundraiserInfo']		= $this->fundraiser_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	public function participants_tab($slug)
	{
		if(isset($_POST["participant_submit"]))
		{
			$this->form_validation->set_rules('player_email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('player_fname', 'First Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('player_lname', 'Last Name', 'trim|required|xss_clean');		
			if ($this->form_validation->run() != FALSE){
				$player_image= isset($_FILES['player_image']['name'])?$_FILES['player_image']['name']:"";
				//------------------------------------- 
				//***Player Image upload section***
				//-------------------------------------
				$fpath1= "";
				if($player_image!="") 
				{
					/* Create the config for upload library */				
					$config_img['upload_path']   = './assets/player_image/';
					$config_img['allowed_types'] = 'gif|jpg|png|jpeg';
					$config_img['max_size']      = '100000';
					$this->load->library('upload', $config_img);
					$this->upload->initialize($config_img);			 
					$this->upload->do_upload('player_image');			
					$data_img = $this->upload->data(); 
					if($data_img['is_image'] == 1)
					{ 
						$fpath1  = $data_img['file_name'];			
						$this->load->library('image_lib');
						$config_resize['image_library']   = 'gd2';	
						$config_resize['create_thumb']    = TRUE;
						$config_resize['maintain_ratio']  = TRUE;
						$config_resize['master_dim']      = 'height';
						$config_resize['quality']         = "100%";  
						$config_resize['source_image']    = './assets/player_image/'.$fpath1;
						$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
						$config_resize['height']          = 120;
						$config_resize['width']           = 1;
						$this->image_lib->initialize($config_resize);
						$this->image_lib->resize(); 
					}
				}
				
				$encrypt_key = $this->config->item('encryption_key');  
				$player_added_by = $this->session->userdata('fundraiser_id');
				$player_email = $this->input->post('player_email');
				$player_fname = $this->input->post('player_fname');
				$player_lname = $this->input->post('player_lname');
				$player_pass  = time();
				$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
				
				$dt_array = array(
				'player_email' => $player_email,
				'player_fname' => $player_fname,
				'player_lname' => $player_lname,
				'player_pass'  => $player_passw,
				'player_image' => $fpath1,
				'player_added_by' => $player_added_by,
				'created' => date("Y-m-d H:i:s")
				);
				$this->fundraiser_model->add('tbl_player',$dt_array);
				
				//-------------------------------------------  
				//Player Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($player_fname);
				$lname_slug    = strip_tags($player_lname);  
				$jSlug         = $fname_slug.'-'.$lname_slug;    
				$playerURL	   = strtolower(url_title($jSlug));
				if($this->fundraiser_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
				   $playerURL = $playerURL.'-'.$lastInsId; 
				}
				$return = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
				if($return){
					$this->session->set_userdata('up_msg','Added successfully');
					//redirect($slug.'/admin/startup');
				}
			}
		}
		$data['active_tab']			= 'participants'; 
		$limit 						= 0;
		$offset 					= 50;
		$fund_id 					= $this->session->userdata('fundraiser_id');
		$getId						= $this->fundraiser_model->fundraiser_getID($slug);
        $data['fundraiserInfo']		= $this->fundraiser_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	public function bulk_player_upload($fslug)
	{
		$ext = pathinfo($_FILES['bulk_player']['name'],PATHINFO_EXTENSION);
		$fund_id = $this->session->userdata('fundraiser_id');
		if($ext=='xls' || $ext=='xlsx')
		{
			$this->load->library('excel');
			//Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/)	 
			$configUpload['upload_path'] = FCPATH.'assets/uploads/excel/';
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '5000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('bulk_player');	
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension=$upload_data['file_ext'];    // uploded file extension
			
			$objReader		= PHPExcel_IOFactory::createReader('Excel2007');// For excel 2007 	  
			//Set to read only
			$objReader->setReadDataOnly(true);
			//Load excel file
			$objPHPExcel	= $objReader->load(FCPATH.'assets/uploads/excel/'.$file_name);		 
			$totalrows		= $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows available in excel      	 
			$objWorksheet	= $objPHPExcel->setActiveSheetIndex(0);
			$encrypt_key	= $this->config->item('encryption_key');
			//loop from first data untill last data
			$return = false; 
			for($i=2;$i<=$totalrows;$i++)
			{	
				$player_fname = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$player_lname = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
				$player_email = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
				$player_added_by = $fund_id;
				$player_pass  = time();
				$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
				if(filter_var($player_email, FILTER_VALIDATE_EMAIL)){
				$dt_array = array(
				'player_email' => $player_email,
				'player_fname' => $player_fname,
				'player_lname' => $player_lname,
				'player_pass'  => $player_passw,
				'player_added_by' => $player_added_by,
				'created' => date("Y-m-d H:i:s")
				);
				$this->fundraiser_model->add('tbl_player',$dt_array);
				//-------------------------------------------  
				//Player Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($player_fname);
				$lname_slug    = strip_tags($player_lname);  
				$jSlug         = $fname_slug.'-'.$lname_slug;    
				$playerURL	   = strtolower(url_title($jSlug));
				if($this->fundraiser_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
				   $playerURL = $playerURL.'-'.$lastInsId; 
				}
				$return = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
				}
			}
			if($return){
				unlink('./assets/uploads/excel/'.$file_name); //File Deleted After uploading in database .
				$this->session->set_userdata('up_msg','Added successfully');
			}
		}
		else
		{
			$dir_path = FCPATH.'assets/uploads/excel/';
			$configUpload['upload_path'] = $dir_path;
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '5000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('bulk_player');	
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension=$upload_data['file_ext'];    // uploded file extension
			
			$encrypt_key	= $this->config->item('encryption_key');
			
			$filepath=$dir_path.$file_name;
			$ntag='\r\n';
			$estag='\"';

			$sqlPl="LOAD DATA LOCAL INFILE '".addslashes($filepath)."' INTO TABLE `tbl_player_exception` 
			FIELDS TERMINATED BY ',' 
			OPTIONALLY ENCLOSED BY '".$estag."' 
			LINES TERMINATED BY '".$ntag."' 
			IGNORE 1 LINES (player_fname, player_lname, player_email)";
			$this->db->query($sqlPl);
			$qry = $this->db->query('SELECT * FROM tbl_player_exception');
			$qry_arr = $qry->result_array();
			$return = false; 
			foreach($qry_arr as $arr){
				$player_fname = $arr["player_fname"];
				$player_lname = $arr["player_lname"];
				$player_email = $arr["player_email"];
				$player_added_by = $fund_id;
				$player_pass  = time();
				$player_passw = $this->encrypt->encode($player_pass,$encrypt_key);
				if(filter_var($player_email, FILTER_VALIDATE_EMAIL)){
				$dt_array = array(
				'player_email' => $player_email,
				'player_fname' => $player_fname,
				'player_lname' => $player_lname,
				'player_pass'  => $player_passw,
				'player_added_by' => $player_added_by,
				'created' => date("Y-m-d H:i:s")
				);
				$this->fundraiser_model->add('tbl_player',$dt_array);
				$this->fundraiser_model->remove('tbl_player_exception',array('id'=>$arr["id"]));
				//-------------------------------------------  
				//Player Slug Creation
				//-------------------------------------------
				$lastInsId     = $this->db->insert_id();    
				$fname_slug    = strip_tags($player_fname);
				$lname_slug    = strip_tags($player_lname);  
				$jSlug         = $fname_slug.'-'.$lname_slug;    
				$playerURL	   = strtolower(url_title($jSlug));
				if($this->fundraiser_model->isPlayerUrlExists('tbl_player',$playerURL)==1){
				   $playerURL = $playerURL.'-'.$lastInsId; 
				}
				$return = $this->db->update('tbl_player', array('player_slug_name'=>$playerURL), array('id'=>$lastInsId));
				}
			}
			if($return){
				unlink('./assets/uploads/excel/'.$file_name); //File Deleted After uploading in database .
				$this->session->set_userdata('up_msg','Added successfully');
			}
		}
		redirect($fslug.'/admin/participants');
	}
	public function getEmailToAllAjax($fslug)
	{
        $data['fslug']		= $fslug;
        echo $this->load->view('view_custom_mail_modal', $data, true);
	}
	public function email_all_participants($fslug)
	{
		$getId	=$this->fundraiser_model->fundraiser_getID($fslug);
		$fund_id=$getId['0']['id'];
		$players=$this->fundraiser_model->view('tbl_player',array('player_added_by'=>$fund_id));
		$response=array(); 
		if(count($players)>0){
		$recepients = array();
		foreach($recepients as $recp){
		if(filter_var($recp['player_email'], FILTER_VALIDATE_EMAIL)) {
		$recepients[]=$recp['player_email'];
		}
		}
		
		if(!empty($recepients)){
		$header ='<!DOCTYPE html> 
				<html lang="en"> 
				<head> 
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
				<title>ThanksForSupporting</title> 
				<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
				</head> 
				<body> 
				<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;"> 
				<tr> 
				<td align="center">
				<table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0"> 
				<tr> 
				<td style="padding:0 30px;"><div style="text-align: center; border-bottom: 1px solid #ccc; padding:10px 0 20px">
				<img src="'.base_url('assets/images/logo_thanks-for-supporting.png').'"  /></div>
				</td> 
				</tr> 
				</table>
				</td> 
				</tr> 
				</table>';
		$subject=$this->input->post('mail_subject');
		$body   =$this->input->post('mail_content');
		$footer ='</body>
				</html>';
		
				
		$email_content = $header.$body.$footer;
		
		
		//echo $email_content;die;
		$config = array();
		$config['useragent']= "CodeIgniter";
		$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
		$config['protocol'] = "smtp";
		$config['smtp_host']= "localhost";
		$config['smtp_port']= "25";
		$config['mailtype'] = 'html';
		$config['charset']	= 'iso-8859-1';
		$config['newline']  = "\r\n";
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);   
		$this->email->from('invite@thanksforsupporting.com');
		$this->email->to($recepients); 
		$this->email->subject('ThanksForSupporting - '.$subject); 
		$this->email->message($email_content);
		$bval=$this->email->send();	
		if($bval){
			$this->session->set_userdata('up_msg','Successfully send');
		}else{
			$this->session->set_userdata('up_msg','Oops! Something went wrong');
		}
		}else{
			$this->session->set_userdata('up_msg','Oops! Something went wrong');
		}
		}else{
			$this->session->set_userdata('up_msg','Oops! Something went wrong');
		}
		redirect($fslug.'/admin/participants');
	}
	public function startup_tab($slug)
	{
		$data['active_tab']			= 'startup'; 
		$limit 						= 0;
		$offset 					= 10;
		$fund_id 					= $this->session->userdata('fundraiser_id');
		$getId						= $this->fundraiser_model->fundraiser_getID($slug);
        $data['fundraiserInfo']		= $this->fundraiser_model->fundraiserinfo($slug);
		$data['fundraiserImgInfo']	= $this->fundraiser_model->fundraiserImgInfo($getId['0']['id']);
		$data['stateList']          = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
		$data["players_list"]		= $this->fundraiser_model->fundraiser_playersInfo($fund_id,$limit,$offset);
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_detail1',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	public function upload_media()
	{
		$fund_id  = $this->session->userdata('fundraiser_id');
		$response = array('msg'=>'','html'=>'','mtype'=>'','valid'=>1);

		$media_type    = $this->input->post('media_type'); 
		
		if($media_type == 'I'){
			$feature_image     = isset($_FILES['feature_img']['name'])?$_FILES['feature_img']['name']:"";
			if($feature_image!="")
			{
				$config_img['upload_path']   = './assets/fundraiser_feature_image/';
				$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config_img['max_size']      = '100000';
				$this->load->library('upload', $config_img);
				$this->upload->initialize($config_img);			 
				$this->upload->do_upload('feature_img');			
				$data_img = $this->upload->data(); 
				if($data_img['is_image'] == 1)
				{ 
					$fpath1  = $data_img['file_name'];	
					$fpath1_ex = explode(".",$fpath1);
					$fpath1  = $fpath1_ex[0].'.'.strtolower($fpath1_ex[1]);
					$this->load->library('image_lib');
					$config_resize['image_library']   = 'gd2';	
					$config_resize['create_thumb']    = TRUE;
					$config_resize['maintain_ratio']  = TRUE;
					$config_resize['master_dim']      = 'height';
					$config_resize['quality']         = "100%";  
					$config_resize['source_image']    = './assets/fundraiser_feature_image/'.$fpath1;
					$config_resize['new_image']       = './assets/fundraiser_feature_image/thumb/'.$fpath1;
					$config_resize['height']          = 150;  //600
					$config_resize['width']           = 1;
					$this->image_lib->initialize($config_resize);
					$this->image_lib->resize(); 
					$fund_feature_image = $fpath1;
				}
				$fundata = array('fund_id'=>$fund_id,'media_type'=>$media_type,'fund_feature_image'=>$fund_feature_image);
				$img  = $this->db->insert('tbl_rel_fund_feature_images', $fundata);
				$last_id = $this->db->insert_id();
				$protimg = explode(".",$fund_feature_image);   
				$proimg  = base_url()."assets/fundraiser_feature_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
				$str = '<img src="'.$proimg.'" />';
				$html = '<div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px; overflow:hidden;height:150px;">
				<img src="'.$proimg.'" />
				<a href="javascript:void(0);" onclick="del_media('.$last_id.')" class="fa fa-times rem"></a>                 
				</div>';
				$response = array('msg'=>'uploaded','html'=>$html,'mtype'=>'I','valid'=>1);
			}
		}else{
			$fundata = array('fund_id'=>$fund_id,'media_type'=>$media_type,'fund_feature_image'=>$this->input->post('youtube_fil'));
			$vid  = $this->db->insert('tbl_rel_fund_feature_images', $fundata);
			$last_id = $this->db->insert_id();
			$html = '<div class="col-xs-12 col-sm-6 col-md-4 fund_img text-center thumbnail" style="margin-top:15px;margin-bottom:15px; overflow:hidden;height:150px;">
				<a class="youtube" href="http://youtube.com/watch?v='.$this->input->post('youtube_fil').'"><img src="https://img.youtube.com/vi/'.$this->input->post('youtube_fil').'/mqdefault.jpg" /></a>
				<a href="javascript:void(0);" onclick="del_media('.$last_id.')" class="fa fa-times rem"></a>                 
				</div>';
			$response = array('msg'=>'uploaded','html'=>$html,'mtype'=>'V','valid'=>1);
		}
		echo json_encode($response);
		exit;
	}
	public function upload_fund_logo($fslug)
	{
		$fund_id	= $this->session->userdata('fundraiser_id');
		$fund_image	= isset($_FILES['fund_image']['name'])?$_FILES['fund_image']['name']:"";
		$arr		= $this->fundraiser_model->view('tbl_fundraiser',array('id'=>$fund_id));
		$res		= array('msg'=>'','valid'=>0);
		if($fund_image!="")
		{
			if(!empty($arr)){
				if(isset($arr[0]['fund_image']) && $arr[0]['fund_image']!=""){
					if(file_exists("./assets/fundraiser_image/thumb/".$arr[0]['fund_image'])){
					  unlink("./assets/fundraiser_image/thumb/".$arr[0]['fund_image']);
					}
					if(file_exists("./assets/fundraiser_image/".$arr[0]['fund_image'])){
					  unlink("./assets/fundraiser_image/".$arr[0]['fund_image']);
					}
				}
			}

			$config_img['upload_path']   = './assets/fundraiser_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);		
			
			if($this->upload->do_upload('fund_image'))
			{
				$data_img = $this->upload->data(); 
				if($data_img['is_image'] == 1)
				{ 
					$fpath1  = $data_img['file_name'];			
					$this->load->library('image_lib');
					$config_resize['image_library']   = 'gd2';	
					$config_resize['create_thumb']    = TRUE;
					$config_resize['maintain_ratio']  = TRUE;
					$config_resize['master_dim']      = 'height';
					$config_resize['quality']         = "100%";  
					$config_resize['source_image']    = './assets/fundraiser_image/'.$fpath1;
					$config_resize['new_image']       = './assets/fundraiser_image/thumb/'.$fpath1;
					$config_resize['height']          = 170;
					$config_resize['width']           = 1;
					$this->image_lib->initialize($config_resize);
					$this->image_lib->resize(); 
					$fund_image = $fpath1;
				}
				$fundimg 	= explode(".",$fund_image);   
				$fundimage	=  base_url("assets/fundraiser_image/thumb/".$fundimg[0].'_thumb.'.$fundimg[1]);
				$fundata    = array('fund_image'=> $fund_image);
				$where		= array('id'=>$fund_id);
				$return = $this->fundraiser_model->edit('tbl_fundraiser', $fundata, $where);
				if($return){
					$res = array('msg'=>'uploaded','valid'=>1,'src'=>$fundimage);
				}else{
					$res = array('msg'=>'Error occured','valid'=>0);
				}
			}else{
				$res = array('msg'=>$this->upload->display_errors(),'valid'=>0);
			}
		}
		echo json_encode($res);
		exit;
	}
	public function invitationtoall($fslug)
	{
		if($fslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->fundraiser_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= date("F j, Y",strtotime($fund_details[0]['fund_start_dt']));
			$fundraiser_end		= date("F j, Y",strtotime($fund_details[0]['fund_end_dt']));
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_org_goal'];
			$player_goal		= $fund_details[0]['fund_individual_goal'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			if($fund_details[0]['fund_image']==""){
				$fundimg 		= base_url()."assets/images/noimage-150x150.jpg";   
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		= base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
			}
			
			$player_listing		= $this->fundraiser_model->view('tbl_player',array('player_added_by'=>$fundraiser_id));
			foreach($player_listing as $player_li){
				$pslug				= $player_li['player_slug_name'];
				$player_first		= $player_li['player_fname'];
				$player_last		= $player_li['player_lname'];
				$player_email		= $player_li['player_email'];
				$player_password	= $player_li['player_pass'];
				
				$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
				$player_login_url	= base_url('admin/login');
				$start_fundraiser	= base_url("start-fundraiser");

				$template = $this->fundraiser_model->view('tbl_mail_template',array('template_slug'=>'asking-to-invite-donors-to-player-from-fundraiser'));
				$html_header = '<!DOCTYPE html>
				<html lang="en">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>ThanksForSupporting</title>
				<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
				</head>
				<body>
				<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato; font-size: 20px;">
				<tr>
				<td align="center"><table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0">
				<tr>
				<td style="padding:0 30px;"><table border="0" width="640" cellpadding="0" cellspacing="0">
				<tr>
				<td><img src="http://thanksforsupporting.com/apps/assets/images/logo_thanks-for-supporting.png" /></td>
				<td style="padding:0 10px;"><img src="'.$fundimg.'" style="width: 100px;height: 100px;border-radius: 50%;"/></td>
				<td><strong>'.$fundraiser_name.'</strong> <br>
				<div style="font-size: 14px;">'.$fundraiser_slog.'</div></td>
				</tr>
				</table></td>
				</tr>
				</table></td>
				</tr>
				</table>';
				$html_footer = '<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;">
				<tr>
				<td style="padding:5px 30px 30px; text-align: center; color:#9f9f9f; font-size: 16px"> This email was sent to you because we want to make the world a better place.<br/>
				You can choose to opt out of future opportunities to help others by clicking here. </td>
				</tr>
				</table>
				</td>
				</tr>
				</table>
				</body>
				</html>'.'<img src="'.base_url('getimagefund/'.$player_li['id']).'" style="display:none;"/>';

				$template_body		= html_entity_decode($template[0]["template_content"]);
				$template_subject	= $template[0]["template_subject"];
				$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
				$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_bgcolor);
				$email_content		= $html_header.str_replace($search_key,$search_val,$template_body).$html_footer;
				//echo $email_content;die;
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
				$this->email->to($player_email); 
				$this->email->subject('ThanksForSupporting - '.$template_subject); 
				$this->email->message($email_content);
				$bval=$this->email->send();	
				if($bval){
				$this->fundraiser_model->edit('tbl_player',array("invitation"=>1,"invitation_dt"=>date("Y-m-d H:i:s")),array("id"=>$player_li["id"]));
				}
			}
			
			$response = "Invitation sent";
			$this->session->set_userdata('succ_msg', $response);
		}else{
			$response = "Sufficient data not provided";
		}
		redirect($fslug.'/admin/startup');
	}
	public function delParticipant($fslug,$pslug)
	{
		$cnt=$this->fundraiser_model->view_check('tbl_player',array('player_slug_name'=>$pslug));
		if($cnt > 0)
		{
			$pdetails = $this->fundraiser_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$pid = $pdetails[0]["id"];
			$this->fundraiser_model->remove('tbl_player',array('id'=>$pid));
			$this->fundraiser_model->remove('tbl_player_contacts',array('contact_added_by'=>$pslug));
			$this->fundraiser_model->remove('tbl_donors_payment',array('player_id'=>$pid));
			$this->session->set_userdata('up_msg','Deleted successfully');
			
		}
		redirect($_SERVER['HTTP_REFERER']);
		//redirect($fslug.'/admin/participants');
	}
	
	
	public function editParticipant($fslug,$pslug)
	{
		$cnt=$this->fundraiser_model->view_check('tbl_player',array('player_slug_name'=>$pslug));
		if($cnt > 0)
		{
			
			$pdetails = $this->fundraiser_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$pid = $pdetails[0]["id"];
			$data['pdetails'] = $pdetails[0];
			$data['fslug'] = $fslug;
			echo $this->load->view('view_player_info_edit_modal', $data, true);
			exit;
			
			$this->session->set_userdata('up_msg','Deleted successfully');
			
		}
		redirect($_SERVER['HTTP_REFERER']);
		//redirect($fslug.'/admin/participants');
	}	
	public function fundraiserReportsGraph($fslug)
	{
		$fund_dt = $this->fundraiser_model->fundraiser_getID($fslug);
		$fund_id = $fund_dt[0]["id"];
		$report_chart = $this->fundraiser_model->participants_report($fund_id);
		$total_donations = $this->fundraiser_model->total_player_fund($fslug);
		$data['total_payments'] = $total_donations;
		$data['report_chart'] 	= $report_chart;
		$data['fund_details'] 	= $fund_dt;
		$data["fslug"] 			= $fslug;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundrasier_emailreports_graph',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function graph_json($fslug)
	{
		$fund_dt = $this->fundraiser_model->fundraiser_getID($fslug);
		$fund_id = $fund_dt[0]["id"];
		$jsondata = $this->fundraiser_model->fundraiser_graph_json($fund_id);
		echo $jsondata;  
	}
	
	public function fundraiserReportsList($fslug)
	{
		$report_list = array();
		$limit 	= 0;
		$offset	= 999999999999;
		$fund_dt = $this->fundraiser_model->fundraiser_getID($fslug);
		$fund_id = $fund_dt[0]["id"];
		$report_list = $this->fundraiser_model->fundraiser_players($fund_id,$limit,$offset);
		$report_chart = $this->fundraiser_model->participants_report($fund_id);
		$total_donations = $this->fundraiser_model->total_player_fund($fslug);
		$data['total_payments'] = $total_donations;
		$data['report_list'] 	= $report_list;
		$data['report_chart'] 	= $report_chart;
		$data['fund_details'] 	= $fund_dt;
		$data["fslug"] 			= $fslug;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_fundraiser_emailreports_list',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function getFundraiserInfoOpenAjax()
	{
        $slug=$this->input->post('fslug');        
        $data['stateList']           = $this->fundraiser_model->func_common_listing('tbl_mst_state','name', 'ASC');
        $data['fundraiserInfo']      = $this->fundraiser_model->fundraiserinfo($slug);  
        echo $this->load->view('view_fundraiser_info_edit_modal', $data, true);
	}  
    
	public function fundraiserInfoEditAjax(){

		$fund_id        = $_POST['hid_fid'];
		$fund_image     = isset($_FILES['file']['name'])?$_FILES['file']['name']:"";
		$fund_username  = $_POST['fund_username'];
		$fund_city      = $_POST['fund_city'];
		$fund_state     = $_POST['fund_state'];
		$fund_org_goal  = $_POST['fund_individual_goal'];

		$arr=$this->fundraiser_model->view('tbl_fundraiser',array('id'=>$fund_id));
		if($fund_image!="")
		{
			if(!empty($arr)){
				if(isset($arr[0]['fund_image']) && $arr[0]['fund_image']!=""){
					if(file_exists("./assets/fundraiser_image/thumb/".$arr[0]['fund_image'])){
					  unlink("./assets/fundraiser_image/thumb/".$arr[0]['fund_image']);
					}
					if(file_exists("./assets/fundraiser_image/".$arr[0]['fund_image'])){
					  unlink("./assets/fundraiser_image/".$arr[0]['fund_image']);
					}
				}
			}

			$config_img['upload_path']   = './assets/fundraiser_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config_img['max_size']      = '100000';
            $this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
            { 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/fundraiser_image/'.$fpath1;
				$config_resize['new_image']       = './assets/fundraiser_image/thumb/'.$fpath1;
				$config_resize['height']          = 170;
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$player_image = $fpath1;
			}
		}else{
			$fund_image = $arr[0]['fund_image'];
		}

		$fundata    = array('fund_username'         => $this->input->post('fund_username'),
                            'fund_slogan'           => $this->input->post('fund_slogan'),
                            'fund_city'             => $this->input->post('fund_city'),
                            'fund_state'            => $this->input->post('fund_state'),
                            'fund_org_goal'         => $this->input->post('fund_org_goal'),
                            'fund_image'            => $fund_image,
                            'fund_individual_goal'  => $this->input->post('fund_individual_goal')
                           );
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		//$data['fslug']=$fundraiserURL;
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}    

	public function getFundraiserDateOpenAjax()
	{
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->fundraiser_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_date_edit_modal', $data, true);
	}   

	public function fundraiserDateEditAjax(){
		$fund_start_dt = $this->input->post('fund_start_dt');
		$fund_end_dt   =  $this->input->post('fund_end_dt');
		$ex_ful_st_dt  = explode('-',$fund_start_dt);
		$ex_ful_st_dt  = $ex_ful_st_dt[2].'-'.$ex_ful_st_dt[0].'-'.$ex_ful_st_dt[1];
		$ex_ful_en_dt  = explode('-',$fund_end_dt);
		$ex_ful_en_dt  = $ex_ful_en_dt[2].'-'.$ex_ful_en_dt[0].'-'.$ex_ful_en_dt[1];

		$fundata       = array('fund_start_dt'=>$ex_ful_st_dt,'fund_end_dt'=>$ex_ful_en_dt);
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function getFundraiserContactOpenAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->fundraiser_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_contact_edit_modal', $data, true);
	}

	public function fundraiserContactEditAjax(){
		$fundata       = array('fund_fname'      => $this->input->post('fund_fname'),
							   'fund_lname'      => $this->input->post('fund_lname'),
		                       'fund_email'      => $this->input->post('fund_email'),
                               'fund_contact'    => $this->input->post('fund_contact'));
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function getFundraiserBankDetailOpenAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->fundraiser_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_bankdetails_edit_modal', $data, true);
	}

	public function fundraiserBankDetailsEditAjax(){
		$fundata       = array('fund_bank_name'  		=> $this->input->post('fund_bank_name'),
							   'fund_routing_number'    => $this->input->post('fund_routing_number'),
							   'fund_account_number'	=> $this->input->post('fund_account_number'));
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}

	public function getFundraiserDesOpenAjax(){
		$slug=$this->input->post('fslug');        
		$data['fundraiserInfo']      = $this->fundraiser_model->fundraiserinfo($slug);  
		echo $this->load->view('view_fundraiser_desc_edit_modal', $data, true);
	}

	public function fundraiserDesEditAjax(){
		$fundata       = array('fund_des' => $this->input->post('fund_des'));
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}
	
	public function fundraiserEmailCheckerAjax(){
		$where = array('fund_email'=>$this->input->post('fund_email')); 
		$query=$this->fundraiser_model->fundraiser_check('tbl_fundraiser',$where);
		if($query>0){  echo 'false'; }
		else{ echo 'true'; }
	}

	public function fundraiserEmailCheckerEditAjax(){
		$fund_email = $this->input->post('fund_email');
		$fund_id    = $this->session->userdata('fundraiser_id'); 
		$query=$this->fundraiser_model->fundraiser_check_edit('tbl_fundraiser',$fund_email,$fund_id);		
		if($query>0){  echo 'false'; }
		else{ echo 'true'; }
	}
	
	public function getFundraiserFetImgOpenAjax(){
	    $slug=$this->input->post('fslug');        
        $data['fundraiserInfo']      = $this->fundraiser_model->fundraiserinfo($slug);  
        echo $this->load->view('view_fundraiser_feature_img_edit_modal', $data, true);
	}
	
	public function getFundraiserFetImgOpenAddAjax(){
	    $slug=$this->input->post('fslug');        
        $data['fundraiserInfo']      = $this->fundraiser_model->fundraiserinfo($slug);  
        echo $this->load->view('view_fundraiser_feature_img_add_modal', $data, true);
	}
	
	public function fundraiserFeatureAddAjax(){
		$fund_id        = $_POST['hid_fid'];
		$media_type     = $_POST['hid_mtype'];
		
		if($media_type == 'I'){
		$fund_image     = isset($_FILES['file']['name'])?$_FILES['file']['name']:"";
		if($fund_image!="")
		{
			$config_img['upload_path']   = './assets/fundraiser_feature_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
			{ 
				$fpath1  = $data_img['file_name'];	
				$fpath1_ex = explode(".",$fpath1);
				$fpath1  = $fpath1_ex[0].'.'.strtolower($fpath1_ex[1]);
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/fundraiser_feature_image/'.$fpath1;
				$config_resize['new_image']       = './assets/fundraiser_feature_image/thumb/'.$fpath1;
				$config_resize['height']          = 150;  //600
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$fund_feature_image = $fpath1;
			}            
		}
		$fundata    = array('fund_feature_image'=>$fund_feature_image,'media_type'=>$media_type,'fund_id'=>$this->input->post('hid_fid'));
		$return = $this->db->insert('tbl_rel_fund_feature_images', $fundata);
		//$data['fslug']=$fundraiserURL;
		}
		else
		{
		$return = $this->db->insert('tbl_rel_fund_feature_images', array('fund_id'=>$this->input->post('hid_fid'),'media_type'=>$media_type,' 	fund_feature_image'=>$this->input->post('youtube_fil')));	
		}
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}
	
	public function fundraiserFeatureEditAjax()
	{
		$fund_id        = $_POST['hid_fid'];
		$fund_image     = isset($_FILES['file']['name'])?$_FILES['file']['name']:"";    
		$arr=$this->fundraiser_model->view('tbl_fundraiser',array('id'=>$fund_id));
		if($fund_image!="")
		{
			if(!empty($arr)){
				if(isset($arr[0]['fund_feature_image']) && $arr[0]['fund_feature_image']!=""){
					$tfet_img = explode(".",$arr[0]['fund_feature_image']);
					if(file_exists("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1])){
					  unlink("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1]);
					}
					if(file_exists("./assets/fundraiser_feature_image/".$arr[0]['fund_feature_image'])){
					  unlink("./assets/fundraiser_feature_image/".$arr[0]['fund_feature_image']);
					}
				}
			}
			/* Create the config for upload library */
			/*$config_img['upload_path']   = './assets/fundraiser_feature_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);  
			$this->upload->initialize($config_img);
			$this->upload->do_upload('file');
			$data_img = $this->upload->data();            
			if($data_img['is_image'] == 1)
			{
				$fpath1     = $data_img['file_name'];
				$this->createThumb('./assets/fundraiser_feature_image/thumb/',$fpath1,$data_img,120,120);
				$fund_feature_image = $fpath1;
			}*/ 
			$config_img['upload_path']   = './assets/fundraiser_feature_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
			{ 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/fundraiser_feature_image/'.$fpath1;
				$config_resize['new_image']       = './assets/fundraiser_feature_image/thumb/'.$fpath1;
				$config_resize['height']          = 150;  //600
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$fund_feature_image = $fpath1;
			}            
		}
		else{
			$fund_feature_image = $arr[0]['fund_image'];
		}
		$fundata    = array('fund_feature_image'            => $fund_feature_image);
		$return = $this->db->update('tbl_fundraiser', $fundata, array('id'=>$this->input->post('hid_fid')));
		//$data['fslug']=$fundraiserURL;
		if($return){
			$data['msg']=1;
		}
		else{
			$data['msg']=0;
		}
		echo json_encode($data);
	}
	
	public function delMedia()
	{
		if(isset($_REQUEST['hid_fid']) && $_REQUEST['hid_fid']!="") 
		{
			$fslug = $_REQUEST['fslug'];
			$img = $this->fundraiser_model->view('tbl_rel_fund_feature_images', array('id'=>$_REQUEST['hid_fid']));
			if(isset($img[0]['media_type']) && $img[0]['media_type']=="I"){
				if(isset($img[0]['fund_feature_image']) && $img[0]['fund_feature_image']!=""){
					$tfet_img = explode(".",$img[0]['fund_feature_image']);
					if(file_exists("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1])){
					  unlink("./assets/fundraiser_feature_image/thumb/".$tfet_img[0]."_thumb.".$tfet_img[1]);
					}
					if(file_exists("./assets/fundraiser_feature_image/".$img[0]['fund_feature_image'])){
					  unlink("./assets/fundraiser_feature_image/".$img[0]['fund_feature_image']);
					}
				}
			}
			$this->db->delete('tbl_rel_fund_feature_images', array('id'=>$_REQUEST['hid_fid']));
			$this->session->set_userdata('base_msg', "Successfully deleted");
			redirect($fslug."/admin/fundraiser");
		}
	}
	
	/*-----------------------------------Fundraiser Profile Section End---------------------------------*/
	
	/*-----------------------------------Manage Donations Section Start---------------------------------*/
	public function managedonation()
	{
		if($this->input->post('hid_id')!=""){
			$this->fundraiser_model->remove('tbl_donation', array('id'=>$this->input->post('hid_id')));
			$this->fundraiser_model->remove('tbl_rel_donation_reward', array('donation_id'=>$this->input->post('hid_id')));
		}
		$recset = $this->fundraiser_model->donationinfo($this->session->userdata('fundraiser_id'));
		$data['info'] = $recset;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_manage_donation',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function getDonationOpenAjax()
	{
		$did 		 = $this->input->post('fslug');
		$data['did'] = $did;
		if($did!=0){
		$data['info']= $this->fundraiser_model->view('tbl_donation', array('id'=>$did));	
		}else{
		$data['info']= array();	
		}
		$data['rel_info']= $this->fundraiser_model->rewadrs_array($did);
		$data['rewards'] = $this->fundraiser_model->view('tbl_rewards', array('status'=>1));
		echo $this->load->view('view_fundraiser_add_donation_modal', $data, true);
	}
	
	public function checkDonation()
	{
		$did			= $this->input->post('hid_did');
		$donate_start	= $this->input->post('donate_start');
		$donate_end		= $this->input->post('donate_end');
		$fid			= $this->session->userdata('fundraiser_id');
		$return = $this->fundraiser_model->donationCheck($fid,$did,$donate_start,$donate_end);
		if($return == 0){
			$data['valid']=1;
			$data['msg']='';
		}
		else{
			$data['valid']=0;
			$data['msg']="This range is already in our system!!";
		}
		echo json_encode($data);
	}
	
	public function fundraiserDonationAddEditAjax()
	{
		if($this->input->post('hid_did')==0)
		{
			
			$fundata = array('donate_start'	=> $this->input->post('donate_start'),
							 'donate_end'	=> $this->input->post('donate_end'),
							 'fund_id'		=> $this->session->userdata('fundraiser_id'));
			$return  = $this->fundraiser_model->add('tbl_donation', $fundata);
			$lastId  = $this->fundraiser_model->last_insert_id();
			$rewards = $this->input->post('reward');
			foreach($rewards as $rewa)
			{
			$fundata1 = array('donation_id'	=> $lastId,'reward_id'	=> $rewa);
			$return1  = $this->fundraiser_model->add('tbl_rel_donation_reward', $fundata1);	
			}
			$msg = "Successfully Added !!";
		}
		else
		{
			$fundata = array('donate_start'	=> $this->input->post('donate_start'),
							 'donate_end'	=> $this->input->post('donate_end'),
							 'fund_id'		=> $this->session->userdata('fundraiser_id'));
			$return  = $this->fundraiser_model->edit('tbl_donation', $fundata, array('id'=>$this->input->post('hid_did')));
			$return  = $this->fundraiser_model->remove('tbl_rel_donation_reward', array('donation_id'=>$this->input->post('hid_did')));
			$lastId  = $this->input->post('hid_did');
			$rewards = $this->input->post('reward');
			foreach($rewards as $rewa)
			{
			$fundata1 = array('donation_id'	=> $lastId,'reward_id'	=> $rewa);
			$return1  = $this->fundraiser_model->add('tbl_rel_donation_reward', $fundata1);	
			}
			$msg = "Successfully Updated !!";
		}
		if($return){
			$data['valid']=1;
			$data['msg']=$msg;
		}
		else{
			$data['valid']=0;
			$data['msg']="Error occured while processing your request!";
		}
		echo json_encode($data);
	}
	/*-----------------------------------Manage Donations Section End---------------------------------*/
	
	/*-----------------------------------Manage Rewards Section Start---------------------------------*/
	public function managereward()
	{
		if($this->input->post('hid_id')!=""){
			$this->db->delete('tbl_rewards', array('id'=>$this->input->post('hid_id')));
		}
		$recset = $this->fundraiser_model->reward_listing_where($this->session->userdata('fundraiser_id'));
		$data['info'] = $recset;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_manage_rewards',$data);
		$this->load->view('../../includes_fundraiser/footer');
	}
	
	public function getRewardOpenAjax()
	{
		//$data = array();
		$rid 		 = $this->input->post('fslug');
		$data['rid'] = $rid;
		if($rid!=0){
		$data['info']= $this->fundraiser_model->view('tbl_rewards', array('id'=>$rid));	
		}else{
		$data['info']= array();	
		}
		echo $this->load->view('view_fundraiser_add_reward_modal', $data, true);
	}
	
	public function fundraiserRewardAddEditAjax()
	{
		if($this->input->post('hid_rid')==0)
		{
			$fundata = array('reward_name'  	=> $this->input->post('reward_name'),
							 'reward_info'      => $this->input->post('reward_info'),
							 'fund_id'		    => $this->session->userdata('fundraiser_id'));
			$return  = $this->db->insert('tbl_rewards', $fundata);
			$msg = "Successfully Added !!";
		}
		else
		{
			$fundata = array('reward_name'  	=> $this->input->post('reward_name'),
						     'reward_info'    => $this->input->post('reward_info'));
			$return  = $this->db->update('tbl_rewards', $fundata, array('id'=>$this->input->post('hid_rid')));
			$msg = "Successfully Updated !!";
		}
		if($return){
			$data['valid']=1;
			$data['msg']=$msg;
		}
		else{
			$data['valid']=0;
			$data['msg']="Error occured while processing your request!";
		}
		echo json_encode($data);
	}
	/*-----------------------------------Manage Rewards Section End---------------------------------*/
	
	public function settings()
	{
		$fund_pass			= $this->session->userdata('fundraiser_password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$fund_passw 		= $this->encrypt->decode($fund_pass,$encrypt_key);
		$data["fund_dpass"]	= $fund_passw;
		$data["fund_epass"]	= $fund_pass;
		$this->load->view('../../includes_fundraiser/header');        
		$this->load->view('view_settings',$data);
		$this->load->view('../../includes_fundraiser/footer');    
	}
	
	public function ShowHidePassword()
	{
		$stat = $this->input->post("term");
		if(strtolower($stat)=="show")
		{
		$fund_pass			= $this->session->userdata('fundraiser_password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$fund_passw 		= $this->encrypt->decode($fund_pass,$encrypt_key);
		$data["fund_pass"]	= $fund_passw;	
		$result				= array("spassword"=>$fund_passw,"btext"=>"Hide");
		}
		else
		{
		$fund_pass			= $this->session->userdata('fundraiser_password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$fund_passw 		= $this->encrypt->decode($fund_pass,$encrypt_key);
		$fund_passw			= str_repeat("*",strlen($fund_passw));;	
		$result				= array("spassword"=>$fund_passw,"btext"=>"Show");
		}
		echo json_encode($result);
	}
	
	public function ajaxSettings()
	{
        $fund_id			= $this->session->userdata('fundraiser_id');
        $fund_email			= $_POST['fund_email'];
        $fund_pass			= $_POST['fund_pass'];
        $fund_pass_confirm	= $_POST['fund_pass_confirm'];
		$data = array();
		if($fund_pass==$fund_pass_confirm)
		{
			$check = $this->fundraiser_model->view_check('tbl_fundraiser',array('id'=>$fund_id,'fund_email'=>$fund_email));
			if($check==1)
			{
				$encrypt_key = $this->config->item('encryption_key');  
				$fund_passw = $this->encrypt->encode($fund_pass,$encrypt_key);
			
				$fund_data = array('fund_pass' => $fund_passw);
				$return = $this->fundraiser_model->edit('tbl_fundraiser', $fund_data, array('id'=>$fund_id,'fund_email'=>$fund_email));
				
				if($return)
				{
					$this->session->set_userdata('fundraiser_password',$fund_passw);
					$data['valid']=1;
					$data['msg']="Password Changed Successfully";
				}
				else
				{
					$data['valid']=0;
					$data['msg']="Error Occured while processing your request.";
				}
			}
			else
			{
				$data['valid']=0;
				$data['msg']="Email address not found in our system.";
			}
		}
		else
		{
			$data['valid']=0;
            $data['msg']="Confirm Password is not matching.";
		}
        echo json_encode($data);
	}
	
	private function createThumb($path, $filename,$data,$w, $h=0, $crop=false){
    
        $config['image_library']    = "gd2";      
        $config['source_image']     = $data['full_path'];      
        $config['create_thumb']     = TRUE;      
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['new_image']    = $path.$filename;


        $width= $data['image_width'];
        $height= $data['image_height'];

        /*if($h!=0){
            $newwidth = $w;
            $newheight = $h;
        }else{
            $newwidth = $w;
            $fraction_amt = $w/$original_width;
            $newheight = intval($original_height*$fraction_amt);
        }*/
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }

        $config['width']            = $newwidth; 
        $config['height']           = $newheight;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize()){
        return $this->image_lib->display_errors();
        } 
        $this->image_lib->clear();
        return false; 
	}
	
	public function asktoinvite($fslug,$pslug)
	{
		if($fslug!='' && $pslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->fundraiser_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= date("F j, Y",strtotime($fund_details[0]['fund_start_dt']));
			$fundraiser_end		= date("F j, Y",strtotime($fund_details[0]['fund_end_dt']));
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_org_goal'];
			$player_goal		= $fund_details[0]['fund_individual_goal'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			if($fund_details[0]['fund_image']==""){
				$fundimg 		=  base_url()."assets/images/noimage-150x150.jpg";   
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		=  base_url()."assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1];   
			}
			
			$player_details		= $this->fundraiser_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_id			= $player_details[0]['id'];
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_email		= $player_details[0]['player_email'];
			$player_password	= $player_details[0]['player_pass'];
			$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
			$player_login_url	= base_url('admin/login');
			$start_fundraiser	= base_url("start-fundraiser");

			$template = $this->fundraiser_model->view('tbl_mail_template',array('template_slug'=>'asking-to-invite-donors-to-player-from-fundraiser'));
			$html_header = '<!DOCTYPE html>
			<html lang="en">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>ThanksForSupporting</title>
			<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
			</head>
			<body>
			<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato; font-size: 20px;">
			<tr>
			<td align="center"><table bgcolor="#fff" border="0" width="700px" cellpadding="0" cellspacing="0">
			<tr>
			<td style="padding:0 30px;"><table border="0" width="640" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="http://thanksforsupporting.com/apps/assets/images/logo_thanks-for-supporting.png" /></td>
			<td style="padding:0 10px;"><img src="'.$fundimg.'"  style="width: 100px;height: 100px;border-radius: 50%;"/></td>
			<td><strong>'.$fundraiser_name.'</strong> <br>
			<div style="font-size: 14px;">'.$fundraiser_slog.'</div></td>
			</tr>
			</table></td>
			</tr>
			</table></td>
			</tr>
			</table>';
			$html_footer = '<table bgcolor="#f0f0f0" border="0" width="100%" cellpadding="0" cellspacing="0" style="font-family:lato;">
			<tr>
			<td style="padding:5px 30px 30px; text-align: center; color:#9f9f9f; font-size: 16px"> This email was sent to you because we want to make the world a better place.<br/>
			You can choose to opt out of future opportunities to help others by clicking here. </td>
			</tr>
			</table>
			</td>
			</tr>
			</table>
			</body>
			</html>'.'<img src="'.base_url('getimagefund/'.$player_id).'" style="display:none;"/>';

			$template_body		= html_entity_decode($template[0]["template_content"]);
			$template_subject	= $template[0]["template_subject"];
			$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');
			$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_bgcolor);
			$email_content		= $html_header.str_replace($search_key,$search_val,$template_body).$html_footer;
			//echo $email_content;die;
			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "smtp";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
			$this->email->to($player_email); 
			$this->email->subject('ThanksForSupporting - '.$template_subject); 
			$this->email->message($email_content);
			$bval=$this->email->send();	
			if($bval){
			$this->fundraiser_model->edit('tbl_player',array("invitation"=>1,"invitation_dt"=>date("Y-m-d H:i:s")),array('player_slug_name'=>$pslug));
			}
			$response = "Invitation sent";
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_flashdata('msgg',$response); 
		redirect($_SERVER['HTTP_REFERER']);
		//redirect(base_url($fslug.'/admin/participants'));
	}
	
	public function remindtoinvite($fslug,$pslug)
	{
		if($fslug!='' AND $pslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->fundraiser_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_start	= $fund_details[0]['fund_start_dt'];
			$fundraiser_end		= $fund_details[0]['fund_end_dt'];
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
			$selected_color		= $fund_details[0]['fund_color_rgb'];
			
			$player_details		= $this->fundraiser_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_goal		= $player_details[0]['player_goal'];
			$player_email		= $player_details[0]['player_email'];
			$player_password	= $player_details[0]['player_pass'];
			$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
			$player_login_url	= base_url('admin/login');
			$start_fundraiser	= base_url("start-fundraiser");

			$template = $this->fundraiser_model->view('tbl_mail_template',array('template_slug'=>'reminder-to-invite-donors-to-player-from-fundraiser'));

			$template_body		= html_entity_decode($template[0]["template_content"]);
			$template_subject	= $template[0]["template_subject"];
			$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[selected_color]');
			$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$selected_color);
			$email_content		= str_replace($search_key,$search_val,$template_body);

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "smtp";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
			$this->email->to($player_email); 
			$this->email->subject('ThanksForSupporting - '.$template_subject); 
			$this->email->message($email_content);
			$bval=$this->email->send();
			if($bval){
			$this->fundraiser_model->edit('tbl_player',array("reminder"=>1,"reminder_dt"=>date("Y-m-d H:i:s")),array('player_slug_name'=>$pslug));
			}
			$response = "Reminder sent";
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_flashdata('msgg',$response); 
		redirect($_SERVER['HTTP_REFERER']);
		//redirect(base_url($fslug.'/admin/participants'));
	}
	
	public function secremindtoinvite($fslug,$pslug)
	{
		if($fslug!='' && $pslug!='')
		{
			$encrypt_key 		= $this->config->item('encryption_key');
			$fund_details		= $this->fundraiser_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fund_email 		= $fund_details[0]["fund_email"];
			$fundraiser_id		= $fund_details[0]['id'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			//$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_start	= $fund_details[0]['fund_start_dt'];
			$fundraiser_end		= $fund_details[0]['fund_end_dt'];
			$fundraiser_email	= $fund_details[0]['fund_email'];
			$fundraiser_no		= $fund_details[0]['fund_contact'];
			$fundraiser_goal	= $fund_details[0]['fund_individual_goal'];
			$selected_color		= $fund_details[0]['fund_color_rgb'];
			
			$player_details		= $this->fundraiser_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_goal		= $player_details[0]['player_goal'];
			$player_email		= $player_details[0]['player_email'];
			$player_password	= $player_details[0]['player_pass'];
			$player_pass		= $this->encrypt->decode($player_password,$encrypt_key);
			$player_login_url	= base_url('admin/login');
			$start_fundraiser	= base_url("start-fundraiser");

			$template = $this->fundraiser_model->view('tbl_mail_template',array('template_slug'=>'2nd-reminder-to-invite-donors-to-player-from-fundraiser'));

			$template_body		= html_entity_decode($template[0]["template_content"]);
			$template_subject	= $template[0]["template_subject"];
			$search_key 		= array('[PlayerFirst]','[StartDate]','[EndDate]','[FundraiserGoal]','[PlayerGoal]','[PlayerEmail]','[Password]','[FundraiserName]','[FundraiserEmailAddress]','[FundraiserPhoneNumber]','[PlayerLoginUrl]','[StartFundraiser]','[selected_color]');
			$search_val			= array($player_first,$fundraiser_start,$fundraiser_end,$fundraiser_goal,$player_goal,$player_email,$player_pass,$fundraiser_name,$fundraiser_email,$fundraiser_no,$player_login_url,$start_fundraiser,$selected_color);
			$email_content		= str_replace($search_key,$search_val,$template_body);

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "smtp";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);   
			$this->email->from('login@thanksforsupporting.com',$fundraiser_name);  
			$this->email->to($player_email); 
			$this->email->subject('ThanksForSupporting - '.$template_subject); 
			$this->email->message($email_content);
			$bval=$this->email->send();
			if($bval){
			$this->fundraiser_model->edit('tbl_player',array("sec_reminder"=>1,"sec_reminder_dt"=>date("Y-m-d H:i:s")),array('player_slug_name'=>$pslug));
			}
			$response = "2nd Reminder sent";
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_flashdata('msgg',$response); 
		redirect(base_url($fslug.'/admin/participants'));
	}
		
	public function logout($fslug)
	{
		$this->session->unset_userdata('fundraiser_id');
		$this->session->unset_userdata('fundraiser_username');
		$this->session->unset_userdata('fundraiser_urlname');
		$this->session->unset_userdata('fundraiser_password');
		$this->session->unset_userdata('fundraiser_contact_email');
		$this->session->set_userdata('err_msg', 'Successfully Logout !');
		redirect('admin/login');
	}
	
	public function contact_submit($fslug,$pslug,$player_details){
		if(isset($_POST['con_submit']))
		{
			$template	= $this->fundraiser_model->view('tbl_mail_template',array('template_slug'=>'request-for-donation-to-donor-from-player'));
			$subject	= $template[0]["template_subject"];
			$header		= $template[0]["template_header"];
			$footer		= $template[0]["template_footer"];
			$body		= $header;
			$body		.= html_entity_decode($template[0]["template_content"]);
			$body		.= '<a href="'.base_url('unsubscribe/[contact_id]').'">Unsubscribe</a>';
			
			$body		.= $footer;
			$contact_email = $this->input->post('contact_email');
			$contact_li = array(
							'contact_email'	=> $contact_email,
							'contact_added_by'=> $pslug,
							'created'	=> date("Y-m-d H:i:s")
							);
							
			$return = $this->fundraiser_model->add('tbl_player_contacts', $contact_li);
			//echo "<pre>";print_r($contact_li); print_r($return); echo "</pre>"; exit; 
			$contact_li['id'] = $this->fundraiser_model->last_insert_id();
			
			/*--------fundraiser-------------*/
			$fund_details		= $this->fundraiser_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fundraiser_id		= $fund_details[0]['id'];
		    //$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			
			if($fund_details[0]['fund_image']==""){
				$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
			}
			/*-------------------------------*/
			/*-------------player------------*/
			
			$player_id			= $player_details[0]['id'];
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_email		= $player_details[0]['player_email'];
			
			if($player_details[0]['player_image']==""){
			$player_img			=  base_url("assets/images/noimage-150x150.jpg");   
			}else{
			$playerTimg			= explode(".",$player_details[0]['player_image']);	
			$player_img			=  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
			}
			
			$player_name		= $player_first.' '.$player_last;
			$start_fundraiser	= base_url("start-fundraiser");
				$contact_id			= $contact_li["id"];
				$email				= $contact_li["contact_email"];
				
				$indata = array(
					'from_id'		=> $player_id,
					'to_id'			=> $contact_id,
					'from_email'	=> $player_email,
					'to_email'		=> $email,
					'sent'			=> '1',
					'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert				= $this->fundraiser_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->fundraiser_model->last_insert_id();
				
				$player_url			= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_share_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_support_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				
				$keywords			= array('[contact_id]','[FundraiserImage]','[FundraiserName]','[FundraiserSlogan]','[PlayerImage]','[PlayerName]','[PlayerURL]','[PlayerShareLink]','[UseOfDonations]','[PlayerFirst]','[PlayerLast]','[SupportUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');

				$values				= array($contact_id,$fundimg,$fundraiser_name,$fundraiser_slog,$player_img,$player_name,$player_support_url,$player_share_url,$fundraiser_name,$player_first,$player_last,$player_support_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_fontcolor);

				$mail_content = str_replace($keywords,$values,$body).'<img src="'.base_url('getimage/'.$report_id).'" style="display:none;"/>';
				
				$config 			= array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('support@thanksforsupporting.com',$player_name);  
				$this->email->to($email); 
				$this->email->subject('ThanksForSupporting - '.$subject); 
				$this->email->message($mail_content);
				$bval=$this->email->send();	
				if($bval){
				$this->fundraiser_model->edit('tbl_player_contacts',array("invitation"=>1),array("id"=>$contact_li["id"],'contact_added_by'=>$pslug));
				
				}
			
		}
		}
		
	public function invitetoall($fund_details,$player_details,$fslug,$pslug)
	{
		$response = '';		
	$action = $this->input->post('action');
	$status = $this->input->post('status');
	$contact_id = $this->input->post('contact_id');
	
			/*--------fundraiser-------------*/
			
			$fundraiser_id		= $fund_details[0]['id'];
		    //$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			
			if($fund_details[0]['fund_image']==""){
				$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
			}
			/*-------------------------------*/
			/*-------------player------------*/
			
			$player_id			= $player_details[0]['id'];
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_email		= $player_details[0]['player_email'];
			
			
			if($player_details[0]['player_image']==""){
			$player_img			=  base_url("assets/images/noimage-150x150.jpg");   
			}else{
			$playerTimg			= explode(".",$player_details[0]['player_image']);	
			$player_img			=  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
			}
			
			$player_name		= $player_first.' '.$player_last;
			$start_fundraiser	= base_url("start-fundraiser");
			/*------------------------------*/
			
			switch($action){
				case 'status_change':
					$this->fundraiser_model->edit('tbl_player_contacts',array('status'=>$status),array('id'=>$contact_id));
				break;				
				default:
			$template	= $this->fundraiser_model->view('tbl_mail_template',array('template_slug'=>'request-for-donation-to-donor-from-player'));
			$subject	= $template[0]["template_subject"];
			$header		= $template[0]["template_header"];
			$footer		= $template[0]["template_footer"];
			$body		= $header;
			$body		.= html_entity_decode($template[0]["template_content"]);
			$body		.= '<a href="'.base_url('unsubscribe/[contact_id]').'">Unsubscribe</a>';
			$body		.= $footer;
			
			$where = array('contact_added_by'=>$pslug);
			
			if($this->input->post('contact_id')):
			$where['id'] = $this->input->post('contact_id');
			else:
			$where['invitation'] = 0;
			endif;
			
			$where['status'] = 1;
			
			$contact_list = $this->fundraiser_model->view('tbl_player_contacts',$where);
			foreach($contact_list as $contact_li){
				$contact_id			= $contact_li["id"];
				$email				= $contact_li["contact_email"];
				
				$indata = array(
					'from_id'		=> $player_id,
					'to_id'			=> $contact_id,
					'from_email'	=> $player_email,
					'to_email'		=> $email,
					'sent'			=> '1',
					'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert				= $this->player_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->player_model->last_insert_id();
				
				$player_url			= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_share_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_support_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				
				$keywords			= array('[contact_id]','[FundraiserImage]','[FundraiserName]','[FundraiserSlogan]','[PlayerImage]','[PlayerName]','[PlayerURL]','[PlayerShareLink]','[UseOfDonations]','[PlayerFirst]','[PlayerLast]','[SupportUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');

				$values				= array($contact_id,$fundimg,$fundraiser_name,$fundraiser_slog,$player_img,$player_name,$player_support_url,$player_share_url,$fundraiser_name,$player_first,$player_last,$player_support_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_fontcolor);

				$mail_content = str_replace($keywords,$values,$body).'<img src="'.base_url('getimage/'.$report_id).'" style="display:none;"/>';
				
				$config 			= array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('support@thanksforsupporting.com',$player_name);  
				$this->email->to($email); 
				$this->email->subject('ThanksForSupporting - '.$subject); 
				$this->email->message($mail_content);
				$bval=$this->email->send();	
				if($bval){
				$this->fundraiser_model->edit('tbl_player_contacts',array("invitation"=>1),array("id"=>$contact_li["id"],'contact_added_by'=>$pslug));
				
				}
			}
			$this->fundraiser_model->edit('tbl_player',array("invited"=>1),array('player_slug_name'=>$pslug));
			$response = "Invitation sent";				
			break;
		  }
	
	
	
		$this->session->set_userdata('succ_msg', $response);
		
		
	}
	
}
