<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkplayer extends CI_controller{

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$id = $this->session->userdata('player_id'); 
		if(!empty($id)){
			redirect($this->uri->segment(1).'/'.$this->uri->segment(2).'/admin/home');
		}
	}
	   
    public function index($fslug='',$pslug=''){
		if($fslug!='' AND $pslug!='')
		{
			redirect(base_url("admin/login"));	
		}
		else
		{
			redirect(base_url('admin/login'));	
		}
		
	}
	
	public function login($fslug,$pslug)
	{
		redirect(base_url('admin/login'));
		$a=$this->session->userdata('err_msg');
		if(isset($a)){
		   $data['err_msg']=$a;
		   $this->session->unset_userdata('err_msg');
	    }else{
		   $data['err_msg']=0;
		}
		$data['fslug'] = $fslug;
		$data['pslug'] = $pslug;
		$this->load->view('../../includes_player/header');
		$this->load->view('view_login',$data);
		$this->load->view('../../includes_player/footer');
	}
	
	public function playerlogin($fslug,$pslug)
	{
		$this->load->model('player/player_model');
		$encrypt_key = $this->config->item('encryption_key');      
		$username=$this->input->post('username');
		
		$queryf=$this->player_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
		if(count($queryf)>0)
		{
			$fundraiser_id = $queryf[0]['id'];  
			$fundraiser_slug = $queryf[0]['fund_slug_name'];  
			$where=array('player_email' => $username);
			$query=$this->player_model->encry_login_check('tbl_player',$where);
			$this->session->unset_userdata('player_id');
			//print_r($query);
			if(count($query)>0)
			{
				$dpass = 	$query[0]['player_pass'];  
				//echo $this->input->post('password').'=='.$this->encrypt->decode($dpass,$encrypt_key);die;
				if($this->input->post('password')==$this->encrypt->decode($dpass,$encrypt_key)){
					
					$urlname = $query[0]['player_slug_name'];
					$this->session->set_userdata('player_id', $query[0]['id']);
					$this->session->set_userdata('fundraiser_id', $fundraiser_id);
					$this->session->set_userdata('player_fullname', $username);
					$this->session->set_userdata('player_urlname', $urlname);
					$this->session->set_userdata('fundraiser_urlname', $fundraiser_slug);
					$this->session->set_userdata('player_username', $query[0]['player_email']);
					$this->session->set_userdata('player_password', $query[0]['player_pass']);
					$this->session->set_userdata('player_contact_email', $query[0]['player_email']);
					//echo "player/".$urlname."/home";die;
					redirect($fslug.'/'.$pslug."/admin/home");
				}else{
				  $this->session->set_userdata('err_msg', 1);	
				  redirect(base_url("admin/login"));
				}
			}
			else
			{
				$this->session->set_userdata('err_msg', 1);	
				redirect(base_url("admin/login"));
			}
		}
		else
		{
			$this->session->set_userdata('err_msg', 1);	
			redirect(base_url("admin/login"));
		}
	}

}