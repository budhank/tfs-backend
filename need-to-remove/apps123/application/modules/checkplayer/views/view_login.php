<script src="<?php echo base_url()?>assets/player/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/player/js/additional-methods.js"></script>
<div id="page-wrapper">
        <div class="mainbox login_frm">
            <div class="panel panel-info">                
                <div class="panel-heading">
                    <div class="panel-title">Sign In</div>
                </div>
                
                    <?php
                    if(isset($err_msg) && $err_msg==1){
				    echo '<div class="err_msg_div">Username or Password is wrong !</div>';
				    unset($err_msg);
                    }                    
                    ?>
			    
                
                <div class="panel-body">
                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                    <form id="loginform" name="loginform" class="form-horizontal" role="form" method="post" action="<?php echo base_url($fslug.'/'.$pslug.'/login/playerlogin');?>">
                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="username" type="email" class="form-control required" name="username" value="" placeholder="Email">
                        </div>
                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input id="password" type="password" class="form-control required" name="password" placeholder="password">
                        </div>
                        <div style="margin-top:10px" class="form-group">
                            <div class="col-sm-12 controls">
                                <!--<a id="" href="dashboard.html" class="btn2">Login</a> -->                                
                                <input type="submit" name="login_btn" id="login_btn" class="btn2 required" value="Login"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
   $("#loginform").validate(); 
});
</script>