<div id="page-wrapper">
    <div class="full_top_wrp bg_wht">
        <ul class="breadcrumb">
            <li class="active">Fundraisers</li>
        </ul>
        <span class="msg_div">
    <?php
        $msg = $this->session->userdata('succ_msg');
        if(!empty($msg)){
         echo $msg;    
         $this->session->unset_userdata('succ_msg');    
        }
        ?>
    </span> </div>
    <div class="container-fluid">
        <div class="bg_wht mar_t_15 tot_pad">
            <div class="tit1_sec clearfix">
                <div class="left">
                    <h2>All Fundraisers</h2>
                </div>
				 <div class="left">
                   <form name="frmSearch" action="<?php echo base_url();?>admin/fundraisers" method="post">

	<input type="text" name="keyword" value=""><input type="submit" name="keyword_search" value="Search">
</form>
                </div>
                <a href="<?php echo base_url()?>admin/create_fundraiser" class="btn_round pull-right">Add new Fundraiser</a> </div>
            <div class="donations_tbl">
                <table class="table table-hover table-condensed data-short">
                    <thead>
					<?php //echo "<pre>"; print_r($_POST);?>
                        <tr>
                            <th style="width:48%"> 
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='fund_username'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','fund_username')" class="asc">Fundraiser</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','fund_username')" class="desc">Fundraiser</a>
								<?php endif;?>							
							</th>
                            <th style="width:10%; text-align:right;">
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='creation_date'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','creation_date')" class="asc">Created</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','creation_date')" class="desc">Created</a>
								<?php endif;?>
							</th>
                            <th style="width:10%; text-align:right;">
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='fund_start_dt'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','fund_start_dt')" class="asc">Start</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','fund_start_dt')" class="desc">Start</a>
								<?php endif;?>
							</th>
                            <th style="width:10%; text-align:right;">
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='fund_end_dt'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','fund_end_dt')" class="asc">End</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','fund_end_dt')" class="desc">End</a>
								<?php endif;?>
							</th>
                            <th style="width:10%; text-align:right;">
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='fund_status'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','fund_status')" class="asc">Status</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','fund_status')" class="desc">Status</a>
								<?php endif;?>
							</th>
                            <th style="width:8%; text-align:right;">
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='totalparticiapte'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','totalparticiapte')" class="asc">Partipants</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','totalparticiapte')" class="desc">Partipants</a>
								<?php endif;?>
							</th>
                            <th style="width:8%; text-align:right;">
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='raised_amt'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','raised_amt')" class="asc">Donations</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','raised_amt')" class="desc">Donations</a>
								<?php endif;?>
							</th>
                            <th style="width:8%; text-align:right;">
								<?php if(isset($_POST['order_by']) && $_POST['order_by']!='asc' && isset($_POST['title']) && $_POST['title']=='totalinvites'):?>
								<a href="javascript:void(0);" onclick="Fundraiser('asc','totalinvites')" class="asc">Invites</a>
								<?php else:?>
								<a href="javascript:void(0);" onclick="Fundraiser('desc','totalinvites')" class="desc">Invites</a>
								<?php endif;?>								
							</th>



                           <!-- <th class="text-right sort-filter-ico"> <a href="#" style="color: #333"><i class="fa fa-flask"></i>&nbsp;Filter</a> <a href="#" style="color: #333"><i class="fa fa-list-ul"></i>&nbsp;Sort</a> </th>
-->                        </tr>
                    </thead>
                    <tbody id="b_content">
                        <?php
                        //print_r($rec_listing);
                        if(count($rec_listing)>0){ ?>
                        <?php foreach($rec_listing as $val){?>
                        <?php 
                           if($val['fund_image']==""){
                            $proimg =  base_url()."assets/images/noimage-150x150.jpg";   
                           }else{
                            $ex_timg = explode(".",$val['fund_image']);      
                            $proimg =  base_url()."assets/fundraiser_image/thumb/".$ex_timg[0].'_thumb.'.$ex_timg[1];     
                           }   
						$org_goal = $val['fund_org_goal'];
						$raised_amt = $val['raised_amt'];
						$remaining_goal = ($org_goal - $raised_amt);
                        ?>
                        <tr>
                            <td data-th="Name"><span><img src="<?php echo $proimg;?>" alt=""></span>
                                <div style="display:table-cell; padding-left:10px;"> <a href="<?php echo base_url();?>admin/fundraiser/<?php echo $val['fund_slug_name'];?>/information"><strong class="nopad_L">
                                <?php echo $val['fund_username'];?></strong></a><br>
                                    <?php echo $val['fund_fname']." ".$val['fund_lname']?><br>
                                    <?php echo $val['fund_city']?>,<?php echo $val['fund_state']?><br>
                                    <a  class="grn2"href="<?php echo base_url();?>admin/fundraiser/<?php echo $val['fund_slug_name'];?>/information">Edit </a> | <a class="grn2" href="<?php echo base_url('admin/paymentdetails/'.$val['fund_slug_name']); ?>">Donations </a> | <a class="grn2" href="<?php echo base_url('admin/fundraiser/'.$val['fund_slug_name'].'/sendtomailtofunraiser'); ?>">Invoice </a> | <a class="grn2" href="<?php echo base_url('admin/fundraiser/'.$val['fund_slug_name'].'/paymentinformation/add'); ?>">Payment </a> | <a class="grn2" href="<?php echo base_url('admin/fundraiser/'.$val['fund_slug_name'].'/settings'); ?>">Login </a>| <a class="grn2" target="_blank" href="<?php echo base_url('donate/personal-info/'.$val['fund_slug_name'].''); ?>">Webpage </a>| <a class="grn2" href="javascript:void(0);" onclick="Delete(<?php echo $val['id'];?>)">Delete </a>
                                     </div>
									<!-- <a class="grn2" href="javascript:void(0);" onclick="Delete(<?php //echo $val['id'];?>)">Delete </a>-->
                            </td>
                            <td data-th="Org Goal" style="text-align:right;">
                                <?php echo date('M j\, Y',strtotime($val['creation_date'])) ?>
                            </td>
                            <td data-th="Raised" style="text-align:right;">
								<?php 
									if($val['fund_start_dt']!='0000-00-00'){ 
									echo date('m/d/Y',strtotime($val['fund_start_dt'])); 
									}else{ 
									echo "";
									} 
								?>
							</td>
                            <td data-th="Remaining" style="text-align:right;">
								<?php 
									if($val['fund_end_dt']!='0000-00-00'){ 
										echo date('m/d/Y',strtotime($val['fund_end_dt']));
										}else{ 
									echo "";
									} 
								?>
							</td>
                            <td data-th="Status" style="text-align:right;"><?php echo ($val['fund_status']==0)?'Inactive':'Active';?></td>
                            <td data-th="Status" style="text-align:center;"><a class="grn2" href="<?php echo base_url();?>admin/fundraiser/<?php echo $val['fund_slug_name'];?>/report/list"><?php  if($val['totalparticiapte']) echo $val['totalparticiapte']; else echo "0";?></a></td>
                            <td data-th="Status" style="text-align:center;">$<?php  if($val['raised_amt']) echo $val['raised_amt']; else echo "0";?></td>
                            <td data-th="Status"  style="text-align:right;" >
							<a class="grn2" href="<?php echo base_url();?>admin/fundraiser/<?php echo $val['fund_slug_name'];?>/report/list/invited"><?php
							if($val['totalinvites']) echo $val['totalinvites']; else echo "0";
							?>
							</a>
							</td>
                            <!--<td data-th="Status"  style="text-align:right;" ><a class="grn2" href="<?php /*echo base_url();*/?>admin/fundraiser/<?php /*echo $val['fund_slug_name'];*/?>/report/list">Report</a></td>-->

                        </tr>
                        <?php } } else { ?>
                        <tr>
                            <td colspan="6" style="text-align:center;">No records found!!</td>
                        </tr>
                        <?php } ?>
                    </tbody>

                    <!--<tr>
                            <td colspan="5" class="text-center"><a class="grn2" href="#">Load more</a></td>
                        </tr>-->
                    <tfoot id="f_content">
                        <?php
                            if(count($rec_listing) >= $offset)
                            {
                            ?>
                            <tr>
                                <td colspan="6" class="text-center"><a class="grn2" id="ldmore" href="javascript:void(0);" onclick="loadmore();">Load more</a></td>
                            </tr>
                            <?php
                            }
                            else
                            {
                            ?>
                                <tr>
                                    <td colspan="6" class="text-center"><a class="grn2" href="#">That's All</a></td>
                                </tr>
                                <?php
                            }
                            ?>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<form name="frm_opts" action="<?php echo base_url();?>admin/fundraisers" method="post">
	<input type="hidden" name="mode" value="">
	<input type="hidden" name="row_id" value="">
</form>
<form name="frm_opts1" action="<?php echo base_url();?>admin/fundraisers" method="post">
	<input type="hidden" name="mode" value="">
	<input type="hidden" name="order_by" value="">
	<input type="hidden" name="title" value="">
</form>
<script type="text/javascript">
    var cnt = 0;

    function loadmore() {
        cnt++;
        $.ajax({
            url: '<?php echo base_url();?>admin/fundraiserloadmore',
            type: 'POST',
            data: 'limit=' + cnt,
            dataType: "text",
            beforeSend: function() {
                $('#ldmore').text('loading...');
            },
            success: function(results) {
                if (results != 0) {
                    $('#ldmore').text('Load more');
                    $('#b_content').append(results);
                } else {
                    $('#f_content').html('<tr><td colspan="6" class="text-center"><a class="grn2" href="#">That\'s All</a></td></tr>');
                }
            }
        });
    }
function Delete(ID){
	var r = confirm("Are you sure you want to delete this record?");
	if (r == true) {
		document.frm_opts.mode.value='del';
		document.frm_opts.row_id.value=ID;
		document.frm_opts.submit();
	} 
}
function Fundraiser(order,title){
	if(order == 'asc'){
		$('.'+title+'_asc').hide();
		$('.'+title+'_desc').show();
	}else{
		$('.'+title+'_asc').show();
		$('.'+title+'_desc').hide();
	}
		document.frm_opts1.mode.value='search';
		document.frm_opts1.order_by.value=order;
		document.frm_opts1.title.value=title;
		document.frm_opts1.submit();
}

</script>
