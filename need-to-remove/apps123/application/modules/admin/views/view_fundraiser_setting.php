<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<div id="page-wrapper">
    <div class="full_top_wrp bg_wht">
        <ul class="breadcrumb">
            <li>All fundraisers</li>
            <li class="active"><?php echo $fundraiserInfo[0]['fund_username'];?></li>
        </ul>
        <input type="hidden" name="fund_id" id="fund_id" value="<?php echo $fund_id;?>" />
        <?php include(APPPATH.'modules/admin/views/view_top_section.php');?>
        <div class="tab_mnu">
            <ul>
                <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>">PROFILE</a></li>
                <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/players">PLAYERS</a></li>
                <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/maganage-rewards">MANAGE REWARDS</a></li>
                <li class="active"><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/settings">SETTINGS</a></li>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <div class="bg_wht mar_t_15 tot_pad">
            <div class="tit1_sec clearfix">
                <div class="left">
                    <h2>Settings</h2>
                </div>
            </div>
            <?php
			$fstatus = $fundraiserInfo[0]['fund_status'];
			if($fstatus == 1)
			{
				$status = "Disapprove Profile";
				$sts = 0;
			}
			else
			{
				$status = "Approve Profile";
				$sts = 1;
			}
            ?>
            <div class="total_info">
                <p><a href="javascript:void(0)" id="btshow2" class="grn2">Show Login name/email</a>
                    <span id="pass_str2" style="margin-left:10px;"><?php echo str_repeat("*",strlen($fundraiserInfo[0]['fund_email']));?></span></p>
				<p><a href="javascript:void(0)" id="btshow" class="grn2">Show Password</a> 
				<span id="pass_str" style="margin-left:10px;"><?php echo str_repeat("*",strlen($dpass));?></span></p>
                <p><a href="javascript:void(0)" onclick="changestatus(<?php echo $fundraiserInfo[0]['id']; ?>,<?php echo $sts;?>)" class="grn2"><?php echo $status; ?></a></p>
                <p><a href="#" class="grn2">Delete Profile</a></p>
            </div>
        </div>
    </div>



</div>
<script type="text/javascript">
$(document).ready(function(){ 
	$( "#btshow" ).click(function(){
		$.ajax({
			url: "<?php echo base_url();?>admin/FundShowHidePassword",
			type: "POST",
			data: {term:$( "#btshow" ).text(),fund_id:$( "#fund_id" ).val()},
			dataType: "json",
			success: function ( results ) {
			$('#pass_str').html(results.spassword);
			$('#btshow').text(results.btext);
			}
		});
		return false;
	});

    $( "#btshow2" ).click(function(){
        $.ajax({
            url: "<?php echo base_url();?>admin/FundShowHideEmail",
            type: "POST",
            data: {term:$( "#btshow2" ).text(),fund_id:$( "#fund_id" ).val()},
            dataType: "json",
            success: function ( results ) {
                $('#pass_str2').html(results.spassword);
                $('#btshow2').text(results.btext);
            }
        });
        return false;
    });
});
</script> 
<script type="text/javascript">
function openProfile() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/getFundraiserInfoOpenAjax",
            data: "fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
            dataType: "html",
            beforeSend: function() {
                $("#fundInfoModal").modal('show');
                $('#div_loading').show();
            },
            success: function(response) {
                $('#div_loading').hide();
                $("#fundInfoModal").html(response);
                $("#fundInfoModal").modal('show');
            }
        });
    }

function changestatus(fid,sts)
{
	
var r = confirm("Are you sure to change the status?");
if (r == true) 
{
    $.ajax({
	type      :"POST",
	url       :"<?php echo base_url(); ?>admin/fundPlayerStatus",
	data      :{fid:fid,sts:sts},
	dataType  :"json",
	success: function(response){ 
		if(response.valid == 1 )
		{
			location.reload();	
		}
		else
		{
			alert(response.msg);	
		}
	}
    });
} 
    else {}    
}    

</script>
<div id="fundInfoModal" class="modal fade" role="dialog"></div>
