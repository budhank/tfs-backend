<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>

<div id="page-wrapper">
  <div class="full_top_wrp bg_wht">
    <ul class="breadcrumb">
      <li>All fundraisers</li>
      <li class="active"> <?php echo $fundraiserInfo[0]['fund_username'];?> </li>
    </ul>
    <input type="hidden" name="fund_id" id="fund_id" value="<?php echo $fund_id;?>" />
    <?php include(APPPATH.'modules/admin/views/view_top_section.php');?>
    <div class="tab_mnu">
      <ul>
        <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>">PROFILE</a></li>
        <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/players">PLAYERS</a></li>
        <li class="active"><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/maganage-rewards">MANAGE REWARDS</a></li>
        <!--<li><a href="#">EMAIL TEMPLATE</a></li>-->
        <li><a href="<?php echo base_url()?>admin/fundraiser/<?php echo $slug;?>/settings">SETTINGS</a></li>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="tit1_sec clearfix">
        <div class="left">
          <h2>Donations & Rewards</h2>
        </div>
        <a href="javascript:void(0)" class="btn_round pull-right" onclick="openNewRec(0);">Add new Donation amount</a>
      </div>
      <div class="donations_tbl">
        <table class="table table-hover table-condensed">
          <thead>
            <tr>
              <th style="width:30%">Donation amount</th>
              <th style="width:50%">Reward</th>
              <th style="width:20%" class="text-right">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <tbody>
			<?php
            if(count($doninfo)>0)
            {
                foreach($doninfo as $val)
                {
            	?>
                <tr>
                    <td data-th="Donation amount">
                    <b>$<?php echo $val['donate_start'];?> - $<?php echo $val['donate_end'];?></b>
                    </td>
                    <td data-th="Reward"><?php echo $val['reward_name'];?></td>
                    <td data-th="" class="text-right">
                        <a href="javascript:void(0)" class="grn2" onclick="openNewRec(<?php echo $val['id'];?>);">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="javascript:void(0)" class="grn2" onclick="removeRec(<?php echo $val['id'];?>,'d');">Remove</a>
                    </td>
                </tr>
                <?php
                }
            }
            else
            {
            ?>
                <tr><td colspan="3" style="text-align: center;">No records found!!</td></tr>
            <?php
            }
            ?>
			 </tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="tit1_sec clearfix">
        <div class="left">
          <h2>Rewards</h2>
        </div>
        <a href="javascript:void(0)" class="btn_round pull-right" onclick="openNewRecRew(0);">Add new Record</a>
      </div>
      <div class="donations_tbl">
        <table class="table table-hover table-condensed">
          <thead>
            <tr>
              <th style="width:25%">Reward name</th>
              <th style="width:40%">Reward info</th>
              <th class="text-right">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <?php
			if(count($rewardinfo)>0)
			{
				foreach($rewardinfo as $val)
				{
					$str_rem_link = '';
					$get_rel_don_data = $this->admin_model->has_rewords_rel_donation($val['id']);
					if($get_rel_don_data > 0)
				 	{
					 	$str_rem_link = '<a href="javascript:void(0)" class="grn2" data-toggle="tooltip" data-placement="left" data-original-title="Delete donation records first!!">Remove</a>';
				 	}
					else
					{
						$str_rem_link = '<a href="javascript:void(0)" class="grn2" onclick="removeRec('.$val['id'].',\'r\');">Remove</a>';
					}
			   ?>
				<tr>
					<td data-th="Reward Name"><?php echo $val['reward_name'];?></td>
					<td data-th="Reward Info"><?php echo $val['reward_info'];?></td>
					<td data-th="" class="text-right">
						<a href="javascript:void(0)" class="grn2" onclick="openNewRecRew(<?php echo $val['id'];?>);">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<?php echo $str_rem_link;?>
					</td>
				</tr>
				<?php
				}
			}
			else
			{
			?>
				<tr><td colspan="3" style="text-align: center;">No records found!!</td></tr>
			<?php
			}
			?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<form name="frm_rem" id="frm_rem" action="<?php echo base_url()?>admin/fundraiser/<?php echo $fundraiserInfo[0]['fund_slug_name'];?>/maganage-rewards" method="post">
<input type="hidden" id="hid_id" name="hid_id" value="">
<input type="hidden" id="hid_md" name="hid_md" value="">
</form>
<script type="text/javascript">
function openProfile()
{
$.ajax({
            type      :"POST",
            url       :"<?php echo base_url(); ?>admin/getFundraiserInfoOpenAjax",
            data      :"fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
            dataType  :"html",
            beforeSend: function(){
                $("#fundInfoModal").modal('show');
				$('#div_loading').show();
            },
            success: function(response){
				$('#div_loading').hide();               
                $("#fundInfoModal").html(response);
                $("#fundInfoModal").modal('show');               
            }
    });
}

function openNewRec(did)
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/getDonationOpenAjax",
		data      :"did="+did+"&fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
		dataType  :"html",
		beforeSend: function(){
			$("#donAddModal").modal('show');
			$('#div_loading').show();
		},
		success: function(response){ 
			$('#div_loading').hide();               
			$("#donAddModal").html(response);
			$("#donAddModal").modal('show');               
		}
	});	
}

function openNewRecRew(rid)
{
  if(rid==0)
	{
		$.ajax({
            type      :"POST",
            url       :"<?php echo base_url(); ?>admin/getRewardOpenAjax",
            data      :"rid="+rid+"&fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
            dataType  :"html",
            beforeSend: function(){
                $("#rewAddModal").modal('show');
                $('#div_loading').show();
            },
            success: function(response){ 
			    $('#div_loading').hide();               
                $("#rewAddModal").html(response);
                $("#rewAddModal").modal('show');               
            }
    	});	
	}
	else{
		$.ajax({
            type      :"POST",
            url       :"<?php echo base_url(); ?>admin/getRewardOpenAjax",
            data      :"rid="+rid+"&fslug=<?php echo $fundraiserInfo[0]['fund_slug_name'];?>",
            dataType  :"html",
            beforeSend: function(){
                $("#rewAddModal").modal('show');
                $('#div_loading').show();
            },
            success: function(response){ 
			    $('#div_loading').hide();               
                $("#rewAddModal").html(response);
                $("#rewAddModal").modal('show');               
            }
    	});
	}
}

function removeRec(v,md)
{
	var r = confirm("Are you sure to delete this?");
	if (r == true) 
	{
		$("#hid_id").val(v);
		$("#hid_md").val(md);
		$("#frm_rem").submit();
	}
}
</script>
<div id="fundInfoModal" class="modal fade" role="dialog"></div>
<div id="donAddModal" class="modal fade" role="dialog"></div>
<div id="rewAddModal" class="modal fade" role="dialog"></div>
