<script src="<?php echo base_url('assets/admin/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/admin/js/additional-methods.js')?>"></script>
<script src="<?php echo base_url('assets/admin/js/raphael-min.js')?>"></script>
<script src="<?php echo base_url('assets/admin/js/morris-0.4.1.min.js')?>"></script>

<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		<li><a href="<?php echo base_url('admin/fundraiser/'.$fslug); ?>">All fundraisers</a></li>
		<li><a href="<?php echo base_url('admin/fundraiser/'.$fslug.'/players'); ?>"><?php echo $fundraiserInfo[0]['fund_username'];?></a></li>
		<li class="active"><?php echo $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]; ?></li>
		</ul>
		<?php include(APPPATH.'modules/admin/views/view_top_section_player.php');?>
		<div class="tab_mnu">
			<ul>
				<li class="active"><a href="<?php echo base_url('admin/fundraiser/'.$fslug.'/players/summary/'.$pslug);?>">SUMMARY</a></li>
				<li><a href="<?php echo base_url('admin/fundraiser/'.$fslug.'/players/donors/'.$pslug);?>">DONATION/DONORS</a></li>
				<li><a href="<?php echo base_url('admin/fundraiser/'.$fslug.'/players/emailreportgraph/'.$pslug);?>">EMAIL SHARE REPORTS</a></li>
				<li><a href="<?php echo base_url('admin/fundraiser/'.$fslug.'/players/setting/'.$pslug);?>">SETTINGS</a></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Donations/Donors</h2></div>
			</div>
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:20%">Donor Name</th>
							<th style="width:20%">Amount</th>
							<th style="width:20%">Message</th>
							<th style="width:20%"></th>
							<th style="width:20%" class="text-right">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					if(count($donors_list)>0)
					{
					foreach($donors_list as $donors_li)
					{
					?>
						<tr>
							<td data-th="Donor Name"><?php echo $donors_li["donor_fname"].' '.$donors_li["donor_lname"];?></td>
							<td data-th="Amount"><strong>$<?php echo $donors_li["donation_amt"]; ?></strong></td>
							<td data-th="Message"></td>
							<td data-th="Mail">
							<a href="<?php echo base_url('admin/donationrequest/'.$donors_li["id"]);?>" style="text-decoration:underline;">Request for donation</a><br/><br/>
							<a href="<?php echo base_url('admin/reminddonationrequest/'.$donors_li["id"]);?>" style="text-decoration:underline;">Reminder request for donation</a>
							</td>
							<td data-th="" class="text-right">
							<small><?php echo date("j-M-Y H:i:s A",strtotime($donors_li["donation_date"]));?></small>
							</td>
						</tr>
					<?php
					}
					}
					else
					{
					?>
						<tr>
							<td colspan="5" class="text-center">No Donation Found</td>
						</tr>
					<?php	
					}
					?>
					</tbody>
					<tfoot>
					<?php 
					if(count($donors_list)>0)
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<a class="grn2" href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/donors/<?php echo $pslug;?>">View all</a>
							</td>
						</tr>
					<?php	
					}
					?>
					</tfoot>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="morris-area-chart_sec bg_wht mar_t_15 tot_pad">
					<div class="tit2_sec clearfix">
						<div class="left"><h3>Email Shares Report</h3></div>
						<div class="right"><a href="#" class="grn2">View full report</a></div>
					</div>
					<div class="sh_day_info">
						<h3>Last 15 Days- Shared 50 emails</h3>
						<ul>
							<li>
								<img src="<?php echo base_url()?>assets/images/sh_day_info1.jpg" alt=""/>
								<p>Sent</p>
							</li>
							<li>
								<img src="<?php echo base_url()?>assets/images/sh_day_info3.jpg" alt=""/>
								<p>Unopened</p>
							</li>
							<li>
								<img src="<?php echo base_url()?>assets/images/sh_day_info1.jpg" alt=""/>
								<p>Opened</p>
							</li>
							<li>
								<img src="<?php echo base_url()?>assets/images/sh_day_info2.jpg" alt=""/>
								<p>Donated</p>
							</li>
							
						</ul>
					</div>
					<div class="morris-area-chart_inr">
						<div id="morris-area-chart"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<div id="mail_pro" class="modal fade" role="dialog"></div>
<div id="send_email_donations" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function openProfile()
{
$.ajax({
            type      :"POST",
            url       :"<?php echo base_url(); ?>admin/getPlayerModalAjax",
            data      :"pslug=<?php echo $pslug;?>",
            dataType  :"html",
            beforeSend: function(){
				$('#div_loading').show();
                $("#edit_pro").modal('show');
            },
            success: function(response){
				$('#div_loading').hide();
                $("#edit_pro").html(response);
                $("#edit_pro").modal('show');               
            }
    });
}
function openSendmail()
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/getSendMailModalAjax",
		data      :"pslug=<?php echo $pslug;?>",
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#send_email_donations").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#send_email_donations").html(response);
			$("#send_email_donations").modal('show');               
		}
    });
}
function thanksent(ref)
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/getMailStructure",
		data      :"pslug=<?php echo $pslug;?>&donor="+ref.rel,
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#mail_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#mail_pro").html(response);
			$("#mail_pro").modal('show');               
		}
    });
}
jQuery(document).ready(function($){
	// Morris Line
	Morris.Line({
		element: 'morris-area-chart',
		data: [
			{ y: '2010', a: 100},
			{ y: '2011', a: 75},
			{ y: '2012', a: 50},
			{ y: '2013', a: 75},
			{ y: '2014', a: 50},
			{ y: '2015', a: 75},
			{ y: '2016', a: 100}
		],
		xkey: 'y',
		ykeys: ['a'],
		labels: ['Series A']
	});
	
});
</script>
