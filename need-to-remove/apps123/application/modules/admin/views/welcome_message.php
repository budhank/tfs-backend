<div class="main-body container">
            <div class="page-heading">
              <h1>Dashboard</h1>
            </div>
            <div class="fc-list-wrapper primary-nav"> 
              <h3 class="fc-list"><a href="<?php echo base_url()?>admin/fundraiser-coordinator">Fundraising Coordinators</a></h3>
              <h3 class="fc-list"><a href="<?php echo base_url()?>admin/fundraisers">Fundraisers</a></h3>  
              <h3 class="fc-list"><a href="<?php echo base_url()?>admin/business">Businesses</a></h3>  
              <h3 class="fc-list"><a href="<?php echo base_url(); ?>admin/template-master">Templates</a></h3>  
              <h3 class="fc-list"><a href="<?php echo base_url(); ?>admin/printers">Printers</a></h3>  
              <h3 class="fc-list"><a href="<?php echo base_url(); ?>admin/agreement">Agreement</a></h3>  
              <h3 class="fc-list"><a href="<?php echo base_url(); ?>admin/changepassword">Change Password</a></h3> 
              <h3 class="fc-list"><a href="<?php echo base_url(); ?>admin/logout">Log Out</a></h3>
            </div>
</div>