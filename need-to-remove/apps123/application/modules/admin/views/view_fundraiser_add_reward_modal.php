<form name="frm_reward_add" id="frm_reward_add" method="post" enctype="multipart/form-data" action="">
   <input type="hidden" name="hid_rid" id="hid_rid" value="<?php echo $rid;?>">
   <input type="hidden" name="hid_fid" id="hid_fid" value="<?php echo $fid;?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="tit1_sec clearfix">
					<div class="left"><h2>Add/Edit Reward</h2></div>
					<div class="ms" style="display:none;"></div>
				</div>
				
				<div class="add_plr_wrp">
					<div class="row">
						<div class="col-xs-12">
							<div class="frm edit_don_am">
								<div class="form-group">
									<label for="reward-name">Reward name</label>
									<input type="text" name="reward_name" id="reward_name" class="form-control required" value="<?php echo (!empty($info) && $info[0]['reward_name']!="")?$info[0]['reward_name']:'';?>"/>
								</div>
								<div class="form-group">
									<label for="reward-info">Reward info</label>
									<textarea name="reward_info" id="reward_info" class="form-control required"><?php echo (!empty($info) && $info[0]['reward_info']!="")?$info[0]['reward_info']:'';?></textarea>
								</div>
							</div>
							<hr>
						</div>
					</div>
					<div class="frm_btn_grp">
						<button type="submit" name="Save" class="btn_round">Save</button>
						<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$( document ).ready( function ( $ ){
		$( "#frm_reward_add" ).validate({
			submitHandler: function ( form ) {

				$.ajax( {
					url: '<?php echo base_url();?>admin/fundraiserRewardAddEditAjax',
					type: 'POST',
					data: $( '#frm_reward_add' ).serializeArray(),     
					dataType: "json",
					beforeSend: function () {
						//$('#infocontent').html('<div style="padding:5px;">loading...</div>');
					},
					success: function ( results ) {
						if ( results.valid == 1 ) {
							$( '.ms' ).html( results.msg );
							$( '.ms' ).show();
							setTimeout( function () {
								location.reload();
							}, 3000 );
						} else {
							$( '.ms' ).html( results.msg );
						}
					}
				});
			}
		});
	});
</script>