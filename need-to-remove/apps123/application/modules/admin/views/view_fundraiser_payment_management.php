<div id="page-wrapper">
  <div class="full_top_wrp bg_wht">
    <ul class="breadcrumb">
      <li class="active">Manage Payments</li>
    </ul>
  </div>
  <div class="container-fluid">
    <div class="bg_wht mar_t_15 tot_pad">
      <div class="tit1_sec clearfix">
        <div class="left">
          <h2>All Fundraisers</h2>
        </div>
      </div>
      <div class="donations_tbl">
        <table class="table table-hover table-condensed">
          <thead>
            <tr>
              <th style="width:40%">Fundraiser</th>
              <th style="width:8%;text-align:right;">Donners</th>
              <th style="width:8%;text-align:right;">Donations</th>
              <th style="width:8%;text-align:right;">Fees</th>
              <th style="width:8%;text-align:right;">Net</th>
                <th style="width:8%;text-align:right;">Invoice</th>
                <th style="width:8%;text-align:right;">Paid</th>
                <th style="width:8%;text-align:right;">Details</th>

              <!--<th style="width:22%" class="text-right">
			  <a href="#" style="color: #333"><i class="fa fa-flask">&nbsp;Filter</i></a>&nbsp;&nbsp; 
			  <a href="#" style="color: #333"><i class="fa fa-list-ul">&nbsp;Sort</i></a> 
			  </th>-->
            </tr>
          </thead>
          <tbody>
			<?php
			if(count($fundraiser_collects)>0)
			{
			foreach($fundraiser_collects as $collects)
			{
			if($collects['fund_image']=="")
			{
			$proimg =  base_url("assets/images/noimage-150x150.jpg");   
			}
			else
			{
			$protimg = explode(".",$collects['fund_image']);   
			$proimg =  base_url("assets/fundraiser_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]);   
			} 
			$fund_id	= $collects['fund_id'];
			$deduct_amt	= $settings[0]["deductable_amt"];
			$less_per	= $settings[0]["less_percent"];
			$collected	= $collects['raised_amt'];
			$donors		= $collects['donors'];
			$deductable	= ($donors*$deduct_amt);
			$lessamt	= (($collects['raised_amt']*$less_per)/100);
			$deduct		= ($deductable+$lessamt);
			$transferable= ($collected-$deduct);
			?>
			<tr>
				<td data-th="Fundraiser Name"><span><img src="<?php echo $proimg;?>" alt=""></span>
				<div style="display:table-cell; padding-left:10px;">
                    <a href="<?php echo base_url('admin/fundraiser/'.$collects['fund_slug_name']);?>">
                        <strong class="nopad_L"><?php echo $collects['fund_username'];?></strong></a>
                    <br>
                    <?php echo $collects['fund_fname']." ".$collects['fund_lname']?><br>
                    <?php echo $collects['fund_city']?>,<?php echo $collects['fund_state']?><br>
                    <a  class="grn2"href="<?php echo base_url('admin/fundraiser/'.$collects['fund_slug_name']);?>/information">Edit </a> | <a class="grn2" href="<?php echo base_url('admin/paymentdetails/'.$collects['fund_slug_name']); ?>">Donations </a> | <a class="grn2" href="<?php echo base_url('admin/fundraiser/'.$collects['fund_slug_name'].'/sendtomailtofunraiser'); ?>">Invoice </a> | <a class="grn2" href="<?php echo base_url('admin/fundraiser/'.$collects['fund_slug_name'].'/paymentinformation/add'); ?>"">Payment </a>| <a class="grn2" href="<?php echo base_url('admin/fundraiser/'.$collects['fund_slug_name'].'/settings'); ?>">Login </a>| <a class="grn2" target="_blank" href="<?php echo base_url('donate/personal-info/'.$collects['fund_slug_name'].''); ?>">Webpage </a>
				</div>
				</td>
				<td class="text-right"><?php echo ($donors ? $donors : "0"); ?></td>
                <td class="text-right"><a class="grn2" target="_blank" href="<?php echo base_url('admin/paymentdetails/'.$collects['fund_slug_name']); ?>" ><span class="dlr">$</span><?php echo number_format($collected,2); ?></a></td>
				<td class="text-right"><span class="dlr">$</span><?php echo number_format($deduct,2); ?></td>
				<td class="text-right"><span class="dlr">$</span><?php echo number_format($transferable,2); ?></td>
				<td class="text-right"><a class="grn2" href="<?php echo base_url('admin/fundraiser/'.$collects['fund_slug_name'].'/sendtomailtofunraiser'); ?>">Invoice </a></td>

                <td class="text-right">
                    <?php

                    if($collects['transferStatus'])
                        echo date('m/d/Y',strtotime($collects['transfer_date']));
                    else
                        echo "Unpaid";
                    ?>
                </td>

                <td class="text-right">
                    <?php

                    if($collects['transferStatus'])
                        echo '<a class="grn2"  href="'.base_url('admin/fundraiser/'.$collects['fund_slug_name']).'/paymentinformation/add">View</a>';
                    else
                        echo '<a class="grn2"  href="'.base_url('admin/fundraiser/'.$collects['fund_slug_name']).'/paymentinformation/add">Record</a>'
                   ?>


                </td>


				<!--<td class="text-right">
				<a href="<?php /*echo base_url('admin/paymentdetails/'.$collects['fund_slug_name']);*/?>" class="grn2" target="_blank">
				View Donations 
				<i class="fa fa-angle-right"></i>
				</a>
				<?php /*if($collects['transferStatus']==0){ */?>
				<form action="<?php /*echo base_url('admin/fundtransfer');*/?>" method="POST" style="display:inline-block;">
				<input type="hidden" name="fundraiser_id" value="<?php /*echo $fund_id ;*/?>"/>
				<input type="hidden" name="total_collection" value="<?php /*echo $collected ;*/?>"/>
				<input type="hidden" name="less_percent" value="<?php /*echo $less_per ;*/?>"/>
                    <input type="hidden" name="less_per_amt" value="<?php /*echo $lessamt ;*/?>"/>
				<input type="hidden" name="total_deduction" value="<?php /*echo $deduct ;*/?>"/>
				<input type="hidden" name="transfer_amt" value="<?php /*echo $transferable ;*/?>"/>
				<button type="submit" class="grn2 btn-transfer">
				Transfer Amount <i class="fa fa-angle-right"></i> 
				</button>
				</form>
				<?php /*} */?>
				</td>-->
            </tr>
			<?php
			}
			}
			?>
          </tbody>
		<!--<tfoot>
				<tr>
					<td colspan="5" class="text-center"><a class="grn2" href="#">Load more</a></td>
				</tr>
			</tfoot>-->
        </table>
      </div>
    </div>
  </div>
</div>
