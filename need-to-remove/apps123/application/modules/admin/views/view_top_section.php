<div class="top_pro_wrp">
  <div class="clearfix">
	<?php 
	if($fundraiserInfo[0]['fund_image']==""){
	$proimg =  base_url()."assets/images/noimage-150x150.jpg";   
	}else{
	$protimg = explode(".",$fundraiserInfo[0]['fund_image']);   
	$proimg =  base_url()."assets/fundraiser_image/thumb/".$protimg[0].'_thumb.'.$protimg[1];   
	} 
	$org_goal = $fundraiserInfo[0]['fund_org_goal'];
	$raised_amt = $fundraiserInfo[0]['raised_amt'];
	$remaining_goal = ($org_goal - $raised_amt);
	?>
    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
      <div class="top_pro">
        <div class="top_pro_img"><img src="<?php echo $proimg;?>" alt=""></div>
        <div class="top_pro_con">
          <h4> <?php echo $fundraiserInfo[0]['fund_username']?> </h4>
          <p> <?php echo $fundraiserInfo[0]['fund_city'];?>, <?php echo $fundraiserInfo[0]['state_ini'];?> </p>
        </div>
      </div>
    </div>
    <div class="col-md-2 col-sm-12 col-xs-12">
      <div class="top_pro_prc">
        <h4><span class="dlr">$</span> <?php echo number_format($org_goal,2,'.',',');?> </h4>
        <p>Org Goal</p>
      </div>
    </div>
    <div class="col-md-2 col-sm-12 col-xs-12">
      <div class="top_pro_prc">
        <h4><span class="dlr">$</span><?php echo number_format($raised_amt,2,'.',',');?></h4>
        <p>Raised</p>
      </div>
    </div>
    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
      <div class="top_pro_prc">
        <h4><span class="dlr">$</span><?php echo number_format($remaining_goal,2,'.',',');?></h4>
        <p>Remaining</p>
      </div>
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 text-right">
      <div class="top_pro_donation"> <a href="javascript:void(0)" class="grn2 mar_t_15" onclick="openProfile();">Edit info</a> </div>
    </div>
  </div>
  
  
</div>
