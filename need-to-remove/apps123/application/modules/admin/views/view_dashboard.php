<div id="page-wrapper">
    <div class="container-fluid mar_t_15">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="bg_wht mar_t_15 tot_pad text-center">
                    <div class="dsb-title">
                        <h5>Amount Raised</h5>
                    </div>
                    <div class="dsb-icons"><img src="<?php echo base_url('assets/admin/images/amount_icon.jpg'); ?>" /></div>
                    <div class="dsb-price">
                        <h2>$<?php echo number_format($amount_raised,2);?></h2>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="dsb-title">
                        <h5>Fundraisers</h5>
                    </div>
                    <div class="dsb-icons"><img src="<?php echo base_url('assets/admin/images/fundraisers_icon.jpg'); ?>" /></div>
                    <div class="dsb-price">
                        <h2><?php echo number_format($count_fundraiser);?></h2>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="dsb-title">
                        <h5>Participants</h5>
                    </div>
                    <div class="dsb-icons"><img src="<?php echo base_url('assets/admin/images/participants_icon.jpg'); ?>" /></div>
                    <div class="dsb-price">
                        <h2><?php echo number_format($count_player);?></h2>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="dsb-title">
                        <h5>Donors</h5>
                    </div>
                    <div class="dsb-icons"><img src="<?php echo base_url('assets/admin/images/donors_icon.jpg'); ?>" /></div>
                    <div class="dsb-price">
                        <h2><?php echo number_format($count_donors);?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="mar_t_15 tot_pad bg_wht">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="dsb-title">
                        <h5>Donations</h5>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="text-right pie-chart-graph">
                        <ul>
                            <li><a class="" href="<?php echo site_url('admin/home/day');?>">Day</a></li>
                            <li><a class="active-tab" href="<?php echo site_url('admin/home/week');?>">Week</a></li>
                            <li><a class="" href="<?php echo site_url('admin/home/month');?>">Mounth</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="mar_t_15">
                        <div id="resizable" style="height: 370px;">
                            <div id="chartContainer1" style="height: 100%; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 dw-lt">
                <div class="bg_wht mar_t_15 tot_pad setup-steps">
                    <div class="nw-admin">
                        <h3 class="ptitle">Fundraising Averages</h3>
                    </div>
                    <ul class="nav tab-title-left">

                        <li class="active green-check">
                            <div class="stp-info"><b>Amount Raised</b><span>Per Participant</span></div>
                            <div class="total-price text-right"><span>$</span><?php echo number_format($amount_raised/$count_player, 2);?></div>
                            <div class="price-summary text-right"><span><?php echo number_format($count_player);?></span>|<span>$<?php echo number_format($amount_raised,2);?></span> </div>

                        </li>
                        <li class=" green-check">
                            <div class="stp-info"><b>Donation Amount</b><span>Donation per Participant</span></div>
                            <div class="total-price text-right"><span>$</span><?php echo number_format($amount_raised/$count_player, 2);?></div>
                            <div class="price-summary text-right"><span><?php echo number_format($count_player);?></span>|<span>$<?php echo number_format($amount_raised,2);?></span> </div>
                        </li>
                        <li class=" green-check">

                            <div class="stp-info"><b>Donors</b><span>Contacts Invited | Participation % </span></div>
                            <div class="total-price text-right">37</div>
                            <div class="price-summary text-right"><span>68</span>|<span>55%</span> </div>
                        </li>
                        <li class=" green-check">
                            <div class="stp-info"><b>Participants</b><span>Invited | Participation %</span></div>
                            <div class="total-price text-right">8</div>
                            <div class="price-summary text-right"><span>14</span>|<span>57%</span> </div>
                        </li>

                    </ul>
                </div>
                <div class="bg_wht mar_t_15 tot_pad other-resources">
                    <div class="nw-admin">
                        <h3 class="ptitle">Leaderboard </h3>
                    </div>
                    <div class="db-content-table">
                        <div class="ta-participants">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2">Name</th>
                                        <th class="text-right">Raised</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width:30px;">
                                            <span class="pic"><img src="https://thanksforsupporting.com/apps/assets/images/noimage-150x150.jpg"></span>
                                        </td>
                                        <td class="fn-fname"><strong>Fundraiser Name</strong>
                                            <span>First Last</span><span>City, ST</span>

                                        </td>
                                        <td style="width:80px;">
                                            <div class="total-price text-right"><span>$</span>1,835</div>
                                            <div class="price-summary text-right"><span>88</span>participants | <span>$45</span> avg donation</div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="width:30px;">
                                            <span class="pic"><img src="https://thanksforsupporting.com/apps/assets/images/noimage-150x150.jpg"></span>
                                        </td>
                                        <td class="fn-fname"><strong>Fundraiser Name</strong>
                                            <span>First Last</span><span>City, ST</span>

                                        </td>
                                        <td style="width:80px;">
                                            <div class="total-price text-right"><span>$</span>1,835</div>
                                            <div class="price-summary text-right"><span>88</span>participants | <span>$45</span> avg donation</div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="width:30px;">
                                            <span class="pic"><img src="https://thanksforsupporting.com/apps/assets/images/noimage-150x150.jpg"></span>
                                        </td>
                                        <td class="fn-fname"><strong>Fundraiser Name</strong>
                                            <span>First Last</span><span>City, ST</span>

                                        </td>
                                        <td style="width:80px;">
                                            <div class="total-price text-right"><span>$</span>1,835</div>
                                            <div class="price-summary text-right"><span>88</span>participants | <span>$45</span> avg donation</div>
                                        </td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <div class="bg_wht mar_t_15 tot_pad other-resources">
                    <div class="nw-admin">
                        <h3 class="ptitle">Other Resources</h3>
                    </div>
                    <ul class="nav tab-title-left">
                        <li> <a href="#">
                                <div class="stp-info"><b>Announcement Email</b><span>Look for upcoming fundraiser email</span></div>
                                <div class="go-btn black">Go</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://thanksforsupporting.com/apps/donate/personal-info/test" target="_blank">
                                <div class="stp-info"><b>Fundraiser Webpage</b><span>View the fundraising webpage</span></div>
                                <div class="go-btn black">Go</div>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://thanksforsupporting.com/apps/donate/personal-info/test">
                                <div class="stp-info"><b>Share on Facebook</b><span>Post your fundraiser on Facebook</span></div>
                                <div class="go-btn black">Go</div>
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="col-sm-9">
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tit1_sec clearfix">
                                <h2>Active Fundraisers</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ta-participants">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Name</th>
                                            <th class="text-center">Start </th>
                                            <th class="text-center">End</th>
                                            <th class="text-right">Goal</th>
                                            <th class="text-right">Raised</th>
                                            <th class="text-right">Remaining</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($fundraisers_list) && count($fundraisers_list)>0){
                                            $sl = 1;
                                            foreach($fundraisers_list as $fundraisers){
                                                $total = $sl+1;
                                                if($fundraisers['fund_image']==""){
                                                    $image =  base_url()."assets/images/noimage-150x150.jpg";   
                                                   }else{
                                                    $ex_timg = explode(".",$fundraisers['fund_image']);      
                                                    $image =  base_url()."assets/fundraiser_image/thumb/".$ex_timg[0].'_thumb.'.$ex_timg[1];     
                                                   } 
                                                $total_goal[] = $fundraisers['fund_org_goal']; 
                                                $total_raised[] = $fundraisers['fund_individual_goal']; 
                                                $total_remaining[] = $fundraisers['fund_org_goal'] -  $fundraisers['fund_individual_goal'];
                                            ?>
                                        <tr>
                                            <td>
                                                <span class="pic">
                                                    <img src="<?php echo $image;?>"></span>
                                            </td>
                                            <td><strong class="plr-fname"><?php echo $fundraisers['fund_username'];?></strong><br>
                                                <?php echo $fundraisers['fund_fname'].' '.$fundraisers['fund_lname'];?><br>
                                                <?php echo $fundraisers['fund_city'];?>, <?php echo $fundraisers['fund_state'];?></td>
                                            <td class="text-center"><span class="invite"><?php echo date('d-m-Y',strtotime($fundraisers['fund_start_dt']));?></span>
                                            </td>
                                            <td class="text-center"><span class="invite"><?php echo date('d-m-Y',strtotime($fundraisers['fund_end_dt']));?></span>
                                            </td>
                                            <td class="text-right">$<?php echo $fundraisers['fund_org_goal'];?></td>
                                            <td class="text-right">$<?php echo $fundraisers['fund_individual_goal'];?></td>
                                            <td class="text-right text-danger">($<?php echo $fundraisers['fund_org_goal'] - $fundraisers['fund_individual_goal'];?>)</td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle btn-radius" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right dropdown-radius-bg" aria-labelledby="dropdownMenuButton">
                                                        <li><a href="#">Resend</a></li>
                                                        <li><a href="<?php echo base_url().'admin/fundraiser/'.$fundraisers['fund_slug_name'].'/information';?>">Edit</a></li>
                                                        <li><a target="_blank" href="<?php echo base_url().'donate/personal-info/'.$fundraisers['fund_slug_name'];?>">Webpage</a></li>
                                                        <li><a href="#">Contacts</a></li>
                                                        <li><a href="<?php echo base_url().'admin/paymentdetails/'.$fundraisers['fund_slug_name'];?>">Donations</a></li>
                                                        <li><a class="grn2" href="javascript:void(0);" onclick="Delete(<?php echo $fundraisers['id'];?>)">Delete </a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php }} ?>
                                        <tr>
                                            <th colspan="3">TOTALS</th>
                                            <th><?php echo $total;?></th>
                                            <th class="text-center">$<?php echo array_sum($total_goal);?></th>
                                            <th class="text-right">$<?php echo array_sum($total_raised);?></th>
                                            <th class="text-right">$<?php echo array_sum($total_remaining);?></th>
                                            <th></th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="bg_wht mar_t_15 tot_pad">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tit1_sec clearfix">
                                <div class="pull-left">
                                    <h2>Rep Leaderboard </h2>
                                </div>


                                <div class="pull-right">
                                    <input type='date' class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ta-participants">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Rep Name</th>
                                            <th class="text-center">Territory</th>
                                            <th class="text-center">Fundraisers</th>
                                            <th class="text-center">Raised</th>
                                            <th class="text-center">Goal%</th>
                                            <th class="text-center">Participants</th>
                                            <th class="text-right">Amount</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="pic">
                                                    <img src="<?php echo base_url('assets/admin/css/images/Step1Profile.png'); ?>"></span>
                                            </td>
                                            <td><strong class="plr-fname">Budhan2 Kundu</strong></td>
                                            <td class="text-center">17</td>
                                            <td class="text-center">356</td>
                                            <td class="text-center">$5,000</td>
                                            <td class="text-center">50%</td>
                                            <td class="text-center">45</td>
                                            <td class="text-right">$5,000</td>
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle btn-radius" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right dropdown-radius-bg" aria-labelledby="dropdownMenuButton">
                                                        <li><a href="#">View Details</a></li>
                                                        <li><a href="#">View Activity</a></li>
                                                        <li><a href="#">View Invoices</a></li>
                                                        <li><a href="#">Make Payment</a></li>
                                                        <li><a href="#">Delete Rep</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form name="frm_opts" action="<?php echo base_url();?>admin/home" method="post">
    <input type="hidden" name="mode" value="">
    <input type="hidden" name="row_id" value="">
</form>
        <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
		<script>
    window.onload = function() {

        // Construct options first and then pass it as a parameter
        var options1 = {
            animationEnabled: true,
            title: {
                //text: "Chart inside a jQuery Resizable Element"
            },
			axisX:{      
			valueFormatString: "DD-MMM-YYYY" ,
			labelAngle: -50,
			 
			},
            data: [{
                type: "column", //change it to line, area, bar, pie, etc
                legendText: "Try Resizing with the handle to the bottom right",
                showInLegend: true,
                dataPoints: [<?php 
				foreach($donation_day as $day){
				 echo '{x:'.$day['x'].',y:'.$day['y'].'}, ';
					}
					?>]
				}]
        };

        $("#resizable").resizable({
            create: function(event, ui) {
                //Create chart.
                $("#chartContainer1").CanvasJSChart(options1);
            },
            resize: function(event, ui) {
                //Update chart size according to its container size.
                $("#chartContainer1").CanvasJSChart().render();
            }
        });

    }
    function Delete(ID){
        var r = confirm("Are you sure you want to delete this record?");
        if (r == true) {
            document.frm_opts.mode.value='del';
            document.frm_opts.row_id.value=ID;
            document.frm_opts.submit();
        } 
    }
</script>
