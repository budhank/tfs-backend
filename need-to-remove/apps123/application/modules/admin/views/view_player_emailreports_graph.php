<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/raphael-min.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/morris-0.4.1.min.js"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		  <li><a href="<?php echo base_url(); ?>admin/fundraiser/<?php echo $fslug;?>">All fundraisers</a></li>
          <li><a href="<?php echo base_url(); ?>admin/fundraiser/<?php echo $fslug;?>/players"><?php echo $fundraiserInfo[0]['fund_username'];?></a></li>
		  <li class="active"><?php echo $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]; ?></li>
		</ul>
		<?php include(APPPATH.'modules/admin/views/view_top_section_player.php');?>
		<div class="tab_mnu">
			<ul>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/summary/<?php echo $pslug;?>">SUMMARY</a></li>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/donors/<?php echo $pslug;?>">DONATION/DONORS</a></li>
				<li class="active"><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/emailreportgraph/<?php echo $pslug;?>">EMAIL SHARE REPORTS</a></li>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/setting/<?php echo $pslug;?>">SETTINGS</a></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Email Share Reports</h2></div>
				<div class="right">
					<a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/emailreportgraph/<?php echo $pslug;?>" class="active">Graph View</a>
					<a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/emailreportlist/<?php echo $pslug;?>">list View</a>
				</div>
			</div>
			<div class="filter_sec">
				<h3>Filters:</h3>
				<div class="filter_sec_inner">
					<ul>
						<li>
							<label>From</label>
							<input type="text" class="datepicker" value="All Time" readonly>
						</li>
						<li>
							<label>To</label>
							<input type="text" class="datepicker" value="All Time" readonly>
						</li>
						<li>
							<label>Attributes</label>
							<a href="#" class="btn1">All</a>
						</li>
						<li>
							<label></label>
							<a href="#">Sent</a>
						</li>
						<li>
							<label></label>
							<a href="#">Unopened</a>
						</li>
						<li>
							<label></label>
							<a href="#">Opened</a>
						</li>
						<li>
							<label></label>
							<a href="#">Donated</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="total_info">
				<div class="row">
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Total Emails sent</p>
							<span><?php echo $report_chart["sent"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Unopened Emails</p>
							<span><?php echo $report_chart["unopen"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Emailed Opened</p>
							<span><?php echo $report_chart["open"];?></span>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<div class="total_info_con">
							<p>Donated</p>
							<span><?php echo $report_chart["donate"];?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="morris-area-chart_sec">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="morris-area-chart_inr">
							<div id="morris-area-chart"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<div id="send_email_donations" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function openProfile()
{
$.ajax({
            type      :"POST",
            url       :"<?php echo base_url(); ?>admin/getPlayerModalAjax",
            data      :"pslug=<?php echo $pslug;?>",
            dataType  :"html",
            beforeSend: function(){
				$('#div_loading').show();
                $("#edit_pro").modal('show');
            },
            success: function(response){
				$('#div_loading').hide();
                $("#edit_pro").html(response);
                $("#edit_pro").modal('show');               
            }
    });
}
function openSendmail()
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/getSendMailModalAjax",
		data      :"pslug=<?php echo $pslug;?>",
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#send_email_donations").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#send_email_donations").html(response);
			$("#send_email_donations").modal('show');               
		}
    });
}
jQuery(document).ready(function($){
	// Morris Line
	Morris.Line({
		element: 'morris-area-chart',
		data: [
			{ y: '2010', a: 100},
			{ y: '2011', a: 75},
			{ y: '2012', a: 50},
			{ y: '2013', a: 75},
			{ y: '2014', a: 50},
			{ y: '2015', a: 75},
			{ y: '2016', a: 100}
		],
		xkey: 'y',
		ykeys: ['a'],
		labels: ['Series A']
	});
	
});
</script>