<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<div id="page-wrapper">
  <div class="full_top_wrp bg_wht">
    <ul class="breadcrumb">
      <li class="active">Email Templates</li>
    </ul>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="bg_wht mar_t_15 tot_pad">
          <div class="tit2_sec clearfix">
            <div class="left">
              <h3>Template Design</h3>
            </div>
          </div>
          <div class="email-wrap">
			<?php
			/*if(count($template_list)>0)
			{
			$i=1;
			foreach($template_list as $template_li)
			{
			?>
			<div class="list-style"><?php echo $i.'. '.$template_li["template_name"]; ?> 
            	<div class="right">
                    <a class="grn2" onclick="viewTemplate(<?php echo $template_li["id"];?>);" href="javascript:void(0)"> <i class="fa fa-eye"></i> View</a> | 
                    <a class="grn2" onclick="openTemplateModal(<?php echo $template_li["id"];?>);" href="javascript:void(0)"><i class="fa fa-pencil"></i>Edit</a>
                </div>
			</div>
			<?php
			$i++;
			}
			}*/
			?>
			<table class="table">
			<thead>
			  <tr>
				<th width="400">Email Description</th>
				<th>Schedule</th>
				<th>Conditions</th>
				<th>&nbsp;</th>
			  </tr>
			</thead>
			<tbody>
			<?php
			if(count($template_list)>0)
			{
			$i=1;
			$template_for = '';
			foreach($template_list as $template_li)
			{
				if($template_li["template_for"]!=$template_for)
				{
				$template_for = $template_li["template_for"];
				echo '<tr><td colspan="4"><strong>'.$template_for.'<strong></td></tr>';	
				}
			?>
			  <tr>
				<td><?php echo $template_li["template_name"]; ?> </td>
				<td><?php echo $template_li["template_schedule"]; ?></td>
				<td><?php echo $template_li["template_condition"]; ?></td>
				<td>
				<div class="right">
					 <i class="fa fa-eye"></i>
                    <a class="grn2" onclick="viewTemplate(<?php echo $template_li["id"];?>);" href="javascript:void(0)"> View </a> | 
					<i class="fa fa-pencil"></i> 
                    <a class="grn2" onclick="openTemplateModal(<?php echo $template_li["id"];?>);" href="javascript:void(0)"> Edit </a>
                </div>
				</td>
			  </tr>
			<?php
			$i++;
			}
			}
			?>
			</tbody>
			</table>
          </div>
		</div>
      </div>
      
    </div>
  </div>
</div>

<div id="templateModal"  class="modal fade" role="dialog"></div>
<div id="templateModal1"  class="modal fade" role="dialog"></div>
<script type="text/javascript">
function viewTemplate(str)
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/viewtemplateModal",
		data      :"template_id="+str,
		dataType  :"html",
		beforeSend: function(){
			$("#templateModal1").modal('show');
			$('#div_loading').show();
		},
		success: function(response){ 
			$('#div_loading').hide();               
			$("#templateModal1").html(response);
			$("#templateModal1").modal('show');               
		}
	});	
}
function openTemplateModal(str)
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/opentemplateModal",
		data      :"template_id="+str,
		dataType  :"html",
		beforeSend: function(){
			$("#templateModal").modal('show');
			$('#div_loading').show();
		},
		success: function(response){ 
			$('#div_loading').hide();               
			$("#templateModal").html(response);
			$("#templateModal").modal('show');               
		}
	});	
}
</script>