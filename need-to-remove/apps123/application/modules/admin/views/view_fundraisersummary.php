<div id="page-wrapper">

    <div class="container-fluid">
        <div class="bg_wht mar_t_15 tot_pad">
            <div class="tit1_sec clearfix">
                <div class="left"><h2>Fundraiser summary for : <?php echo $fundraiserInfo[0]['fund_username'] ?></h2>
                </div>
            </div>
            <div class="add_plr_wrp mar_t_15">

                <div class="row">
                    <?php
                    echo $content;
                    ?>
                </div>
                <div class="row">

                <table style="font-family:lato;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f0f0f0">
                    <tr>
                        <td align="center">
                    <input type="button" onclick="window.location.href='<?php echo base_url('admin/fundraiser/'.$slug.'/sendtomailtofunraiser?emailtoclient=1'); ?>'" name="submit" value="Email to Client" class="btn_round"/>
                    <a href="<?php echo base_url() ?>admin/managepayment" class="undr_lin">Cancel</a>
                        </td>
                    </tr>
                </table>
                </div>
                <hr>


            </div>
        </div>
    </div>
</div>
<style>
    p{
        padding: 0!important;
    }

</style>
<script src="<?php echo base_url() ?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url() ?>assets/admin/js/additional-methods.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#transfer_date").datepicker({
            dateFormat: 'mm-dd-yy',
        });

        $("#playeraddedit").validate({});
    });
</script>