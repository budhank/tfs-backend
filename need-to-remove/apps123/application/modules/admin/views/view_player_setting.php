<script src="<?php echo base_url()?>assets/admin/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/additional-methods.js"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		  <li><a href="<?php echo base_url(); ?>admin/fundraiser/<?php echo $fslug;?>">All fundraisers</a></li>
          <li><a href="<?php echo base_url(); ?>admin/fundraiser/<?php echo $fslug;?>/players"><?php echo $fundraiserInfo[0]['fund_username'];?></a></li>
		  <li class="active"><?php echo $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"]; ?></li>
		</ul>
		<?php include(APPPATH.'modules/admin/views/view_top_section_player.php');?>
		<div class="tab_mnu">
			<ul>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/summary/<?php echo $pslug;?>">SUMMARY</a></li>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/donors/<?php echo $pslug;?>">DONATION/DONORS</a></li>
				<li><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/emailreportgraph/<?php echo $pslug;?>">EMAIL SHARE REPORTS</a></li>
				<li class="active"><a href="<?php echo base_url();?>admin/fundraiser/<?php echo $fslug;?>/players/setting/<?php echo $pslug;?>">SETTINGS</a></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Settings</h2></div>
			</div>
			<div class="total_info">
			<?php
			$fundraiser_id = $fundraiserInfo[0]['id'];
			$player_id = $player_details[0]["id"];
			$player_status = $player_details[0]["player_status"];
			if($player_status == 1)
			{
				$status = "Deactivate Profile";
				$sts = 0;
			}
			else
			{
				$status = "Activate Profile";
				$sts = 1;
			}
			?>
				<p><a href="javascript:void(0)" id="btshow" class="grn2">Show Password</a> 
				<span id="pass_str" style="margin-left:10px;"><?php echo str_repeat("*",strlen($dpass));?></span></p>
				<p><a href="javascript:void(0)" onclick="changestatus(<?php echo $fundraiser_id; ?>,<?php echo $player_id; ?>,<?php echo $sts;?>)" class="grn2"><?php echo $status; ?></a></p>
				<p><a href="#" class="grn2">Delete Profile</a></p>
			</div>
		</div>
	</div>
</div>
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<div id="send_email_donations" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
$(document).ready(function(){ 
	$( "#btshow" ).click(function(){
		$.ajax({
			url: "<?php echo base_url();?>admin/PlayShowHidePassword",
			type: "POST",
			data: {term:$( "#btshow" ).text(),player_id:<?php echo $player_id; ?>},
			dataType: "json",
			success: function ( results ) {
			$('#pass_str').html(results.spassword);
			$('#btshow').text(results.btext);
			}
		});
		return false;
	});
});
</script> 
<script type="text/javascript">
function openProfile()
{
$.ajax({
	type      :"POST",
	url       :"<?php echo base_url(); ?>admin/getPlayerModalAjax",
	data      :"pslug=<?php echo $pslug;?>",
	dataType  :"html",
	beforeSend: function(){
		$('#div_loading').show();
		$("#edit_pro").modal('show');
	},
	success: function(response){
		$('#div_loading').hide();
		$("#edit_pro").html(response);
		$("#edit_pro").modal('show');               
	}
    });
}
function openSendmail()
{
	$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>admin/getSendMailModalAjax",
		data      :"pslug=<?php echo $pslug;?>",
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#send_email_donations").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#send_email_donations").html(response);
			$("#send_email_donations").modal('show');               
		}
    });
}
function changestatus(fid,pid,sts)
{
	$.ajax({
	type      :"POST",
	url       :"<?php echo base_url(); ?>admin/playerStatus",
	data      :{fid:fid,pid:pid,sts:sts},
	dataType  :"json",
	success: function(response){ 
		if(response.valid == 1 )
		{
			location.reload();	
		}
		else
		{
			alert(response.msg);	
		}
	}
    });
}

</script>