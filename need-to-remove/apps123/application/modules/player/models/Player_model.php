<?php
class Player_model extends CI_Model {
	public function view_all($table_name)
    {
		$query = $this->db->get($table_name);
		return $query->result_array(); 	
    }
	public function view_limit($table,$where='',$limit,$offset)
    {
		$this->db->select('*');
		$this->db->from($table);
		if($where!='')
		{
		$this->db->where($where);	
		}
		$this->db->limit($offset,$limit);	
		
		$query = $this->db->get();
		return $query->result_array(); 	
    }
	public function view($table_name, $where)
    {
		$query = $this->db->get_where($table_name, $where);
		return $query->result_array(); 
    }
	public function view_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}
	public function add($table_name, $data)
    {
		return $this->db->insert($table_name, $data); 
	}  
	public function edit($table_name, $data, $where)
    {
		return $this->db->update($table_name, $data, $where);
	}
	public function remove($table_name, $where)
    {
		return $this->db->delete($table_name, $where); 
	}
	public function truncate($table_name)
    {
		return $this->db->truncate($table_name);
	}
	public function last_insert_id()
	{
		return $this->db->insert_id();
	}
	public function last_qry()
	{
		echo $this->db->last_query();
	}	
	public function getStartdate($slug){
		$sql = "SELECT * FROM tbl_fundraiser WHERE fund_slug_name='".$slug."'";
		$query = $this->db->query($sql);
		$r = $query->result_array();
		$rr = ($query->num_rows() > 0)?$r[0]["fund_start_dt"]:'';
		return $rr;
	}
	public function login_check($table,$where){
		   $this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query;
	}	
	public function encry_login_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function func_common_listing($table,$field,$ord)
	{
        $this->db->from($table);
        if($field!='' && $ord!=''){
        $this->db->order_by($field, $ord);
        }
        $query = $this->db->get(); 
        return $query->result_array();
	}
	public function fundraiser_check($table_name, $where)
	{
		$query = $this->db->get_where($table_name, $where);
		return ($query->num_rows() > 0)?$query->num_rows():0;
	}

	public function fundraiser_check_edit($table_name, $femail, $fid)
	{
		$sql   = "SELECT * FROM $table_name WHERE fund_email='".$femail."' AND id!=".$fid;
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}

	public function fundraiser_list()
	{
		$sql = "SELECT tbl_fundraiser.*, tbl_mst_state.name as state_ini FROM tbl_fundraiser LEFT JOIN tbl_mst_state ON tbl_mst_state.state_initial=tbl_fundraiser.fund_state ORDER BY fund_fname ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function donors_list($pslug,$limit)
	{
		$sql = "SELECT * FROM tbl_donors_payment WHERE player_slug = '".$pslug."' ORDER BY donation_date DESC LIMIT ".$limit." ";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}

	public function isUrlExists($tname,$slug)
	{
		$sql   = "SELECT * FROM $tname WHERE fund_slug_name LIKE '".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}

	public function isUrlExistsEditMode($tname,$slug,$fid)
	{
		$sql   = "SELECT * FROM $tname WHERE fund_slug_name LIKE '".$slug."' AND id!=".$fid;
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}

	public function fundraiserinfo($slug)
	{
		//$sql   = "SELECT * FROM tbl_fundraiser WHERE fund_slug_name='".$slug."'";
		$sql = "SELECT tbl_fundraiser.*, tbl_mst_state.name as state_ini FROM tbl_fundraiser LEFT JOIN tbl_mst_state ON tbl_mst_state.state_initial=tbl_fundraiser.fund_state WHERE tbl_fundraiser.fund_slug_name='".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	
	public function isPlayerUrlExists($tname,$slug)
	{
		$sql   = "SELECT * FROM $tname WHERE player_slug_name LIKE '".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?1:0;
	}
	
	public function playersInfo($slug)
	{
		$sql   = "SELECT tp.*, tdnp.pay_id, tdnp.fund_id, tdnp.amount raised_amt
		FROM tbl_player tp
		LEFT JOIN (
		SELECT id as pay_id,fund_id,player_id ,IFNULL(SUM(donation),0.00) amount FROM tbl_donors_payment GROUP BY player_id
		) tdnp ON tdnp.player_id=tp.id 
		WHERE tp.player_slug_name='".$slug."' 
		GROUP BY tp.id
		ORDER BY player_fname ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function total_player_fund($slug)
	{
		$sql="SELECT *, IFNULL(SUM(donation),0.00) raised_amt FROM tbl_donors_payment WHERE player_slug='".$slug."'";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0)?$query->result_array():array();
	}
	public function email_reports($pid)
	{
		$result		= array();
		/*-----------------------------------------SENT SQL-----------------------------------------------------------*/
		$sent_sql	= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1";
		$sent_qry	= $this->db->query($sent_sql);
		$sent_cnt	= ($sent_qry->num_rows() > 0)?$sent_qry->num_rows():0;
		$result['sent']=$sent_cnt;
		/*---------------------------------------UNOPEN SQL-----------------------------------------------------------*/
		$unopen_sql	= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1 AND opened=0";
		$unopen_qry	= $this->db->query($unopen_sql);
		$unopen_cnt	= ($unopen_qry->num_rows() > 0)?$unopen_qry->num_rows():0;
		$result['unopen']=$unopen_cnt;
		/*-----------------------------------------OPEN SQL-----------------------------------------------------------*/
		$open_sql	= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1 AND opened=1";
		$open_qry	= $this->db->query($open_sql);
		$open_cnt	= ($open_qry->num_rows() > 0)?$open_qry->num_rows():0;
		$result['open']=$open_cnt;
		/*-----------------------------------------VISIT SQL-----------------------------------------------------------*/
		$visit_sql	= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1 AND opened=1 AND visited=1";
		$visit_qry	= $this->db->query($visit_sql);
		$visit_cnt	= ($visit_qry->num_rows() > 0)?$visit_qry->num_rows():0;
		$result['visit']=$visit_cnt;
		/*----------------------------------------DONATE SQL-----------------------------------------------------------*/
		$donate_sql	= "SELECT id FROM tbl_email_share_reports WHERE from_id='".$pid."' AND sent=1 AND opened=1 AND donated=1";
		$donate_qry	= $this->db->query($donate_sql);
		$donate_cnt	= ($donate_qry->num_rows() > 0)?$donate_qry->num_rows():0;
		$result['donate']=$donate_cnt;

		return $result;
	}
}
?>