<script src="<?php echo base_url()?>assets/fundraiser/js/jquery.validate.js"></script>
<script src="<?php echo base_url()?>assets/fundraiser/js/additional-methods.js"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
		  <li class="active">Profile</li>
		</ul>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Profile</h2></div>
				<a href="javascript:void(0)" class="right grn2" onclick="openProfile();" style="border: none; margin-top: 5px;" >Edit Profile</a>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="top_pro_wrp">
			<div class="clearfix bg_wht tot_pad">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mar-b15">
					<div class="top_pro">
						<?php
						if($player_details[0]['player_image']==""){
						$proimg =  base_url()."assets/images/noimage-150x150.jpg";   
						}else{
						$proimg =  base_url()."assets/player_image/thumb/".$player_details[0]['player_image'];   
						}
						?>
						<div class="top_pro_img">
						<img src="<?php echo $proimg; ?>" alt="">
						</div>
						<div class="top_pro_con">
							<h4><?php echo ucfirst($player_details[0]["player_fname"]).' '.ucfirst($player_details[0]["player_lname"]); ?></h4>
							<p><?php echo ucfirst($player_details[0]["player_title"]); ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-12 mar-b20"><h5>Contact Details</h5></div>
				<div class="col-md-4 col-sm-4 col-xs-12 mar-b15">
					<div class="top_pro_prc">
						<h4><span class="glyphicon glyphicon-usd"></span> <?php echo $player_details[0]["player_goal"]; ?></h4>
						<p>Goal</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 mar-b15">
					<div class="top_pro_prc">
						<h4 class="grn1"><span class="glyphicon glyphicon-usd"></span> <?php echo "NIL"; ?></h4>
						<p>Raised</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mar-b15">
					<div class="top_pro_prc">
						<h4 class="txt-red"><span class="glyphicon glyphicon-usd"></span> <?php echo "NIL"; ?></h4>
						<p>Remaining</p>
					</div>
				</div>
				<div class="col-lg-12 mar-b15 mar_t_15">
					<p>Email Address<br><?php echo $player_details[0]["player_email"]; ?></p>
				</div>
			</div>
		</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function openProfile()
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>player/getPlayerModalAjax",
		data      :"pslug=<?php echo $pslug;?>",
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#edit_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#edit_pro").html(response);
			$("#edit_pro").modal('show');               
		}
    });
}
</script>