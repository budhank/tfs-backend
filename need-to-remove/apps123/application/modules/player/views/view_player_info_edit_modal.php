<form name="frm_popup_edit" id="frm_popup_edit" method="post" enctype="multipart/form-data" action=""> 
<input type="hidden" name="hid_pid" id="hid_pid" value="<?php echo $playerInfo[0]['id'];?>"/>
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left">
				<h2>Edit Profile</h2>
				<div class="ms" style="display:none;"></div>
				</div>
			</div>
			<div class="add_plr_wrp">
				<div class="row">
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="frm">
							<div class="form-group">
								<label>Email address(Login id)</label>
								<input type="email" class="form-control" id="player_email" name="player_email" value="<?php echo $playerInfo[0]['player_email'];?>" readonly/>
							</div>
							<div class="form-group">
								<label>First Name</label>
								<input type="text" class="form-control" id="player_fname" name="player_fname" value="<?php echo $playerInfo[0]['player_fname'];?>" required/>
							</div>
							<div class="form-group">
								<label>Last Name</label>
								<input type="text" class="form-control" id="player_lname" name="player_lname" value="<?php echo $playerInfo[0]['player_lname'];?>" required/>
							</div>
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-12 text-center">
						<label>Picture</label>
						<?php
						if($playerInfo[0]['player_image']==""){
						$proimg =  base_url()."assets/player/images/pro_pic1.jpg";   
						}else{
						$protimg = explode(".",$playerInfo[0]['player_image']);   
						$proimg =  base_url()."assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]; 
						} 
						?>
						<div class="upld_img" style="margin:auto;">
							<img id="uploadPreview1" src="<?php echo $proimg; ?>" />
						</div>
						<div class="chng_up_file_wrp">
							<input class="chng_up_file" id="uploadImage1" placeholder="Change" type="file" name="proimage" onchange="PreviewImage(1);" />
							<a href="JavaScript:Void(0);">Change or remove</a>
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
					<input type="submit" name="submit" value="Save" class="btn_round"/>
					<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
			</div>
      </div>
    </div>
</div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	$("#frm_popup_edit").validate({
        submitHandler: function(form) {
        var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0]); 
        formData.append('player_email', $('#player_email').val());
        formData.append('player_fname', $('#player_fname').val());
        formData.append('player_lname', $('#player_lname').val()); 
        formData.append('hid_pid', $('#hid_pid').val()); 
            
        $.ajax({
        url:'<?php echo base_url();?>player/admin/playerInfoEditAjaxp',
        type:'POST',
        data: formData,    
        contentType: false,          
        processData:false,     
        dataType:"json",
        beforeSend: function(){
           //$('#infocontent').html('<div style="padding:5px;">loading...</div>');
        },
        success:function(results){
            if(results.msg==1){
            $('.ms').html('Successfully updated!!');
            $('.ms').show();
            setTimeout(function() {            
                location.reload();
                }, 3000);
            }
            else{
                $('.ms').html('Updation is failured');
				$('.ms').show();
            }            
        }
       });
      }
	});   
});
</script>    
