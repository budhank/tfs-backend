<script src="<?php echo base_url('assets/player/js/jquery.validate.js');?>"></script>
<script src="<?php echo base_url('assets/player/js/additional-methods.js');?>"></script>
<script src="<?php echo base_url('assets/player/js/raphael-min.js');?>"></script>
<script src="<?php echo base_url('assets/player/js/morris-0.4.1.min.js');?>"></script>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<div class="top_pro_wrp">
			<div class="clearfix">
				<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
					<div class="top_pro">
						<?php
						if($player_details[0]['player_image']==""){
							$proimg =  base_url("assets/images/noimage-150x150.jpg");   
						}else{
							$protimg = 	explode(".",$player_details[0]['player_image']);	
							$proimg  =  base_url("assets/player_image/thumb/".$protimg[0].'_thumb.'.$protimg[1]);   
						}
						?>
						<div class="top_pro_img">
						<img src="<?php echo $proimg; ?>" alt="">
						</div>
						<div class="top_pro_con">
							<h4><?php echo ucfirst($player_details[0]["player_fname"]).' '.ucfirst($player_details[0]["player_lname"]); ?></h4>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class="top_pro_prc">
						<h4><span class="glyphicon glyphicon-usd"></span> <?php echo number_format($player_details[0]["player_goal"]); ?></h4>
						<p>Goal</p>
					</div>
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class="top_pro_prc">
						<h4 class="grn1"><span class="glyphicon glyphicon-usd"></span> <?php echo number_format($player_details[0]["raised_amt"]); ?></h4>
						<p>Raised</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
					<div class="top_pro_prc">
						<h4 class="txt-red"><span class="glyphicon glyphicon-usd"></span> <?php echo number_format(($player_details[0]["player_goal"]-$player_details[0]["raised_amt"])); ?></h4>
						<p>Remaining</p>
					</div>
				</div>
				<div class="col-lg-3 col-xs-12 text-right">
					<div class="top_pro_donation">
						<!--a href="#" class="btn_round" data-toggle="modal" data-target="#send_email_donations">Send Emails for Donation</a>
						<br-->
						<a href="javascript:void(0)" class="grn2" onclick="openProfile();">Edit profile</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Donations/Donors</h2></div>
			</div>
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:20%">Donor Name</th>
							<th style="width:20%">Amount</th>
							<th style="width:20%">Message</th>
							<th style="width:20%">Thanks Mail</th>
							<th style="width:20%" class="text-right">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					if(count($donors_list)>0)
					{
					foreach($donors_list as $donors_li)
					{
					?>
						<tr>
							<td data-th="Donor Name"><?php echo $donors_li["donor_fname"].' '.$donors_li["donor_lname"];?></td>
							<td data-th="Amount"><strong>$<?php echo $donors_li["donation_amt"]; ?></strong></td>
							<td data-th="Message"></td>
							<td data-th="Mail">
							<a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($donors_li["id"]);?>" style="text-decoration:underline;">Send a Thank you Email</a>
							</td>
							<td data-th="" class="text-right">
							<small><?php echo date("d-m-Y H:i:s A",strtotime($donors_li["donation_date"]));?></small>
							</td>
						</tr>
					<?php
					}
					}
					else
					{
					?>
						<tr>
							<td colspan="5" class="text-center">No Donation Found</td>
						</tr>
					<?php	
					}
					?>
					</tbody>
					<tfoot>
					<?php 
					if(count($donors_list)>0)
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<a class="grn2" href="<?php echo base_url($fslug.'/'.$pslug.'/admin/donors');?>">View all</a>
							</td>
						</tr>
					<?php	
					}
					?>
					</tfoot>
				</table>
			</div>
		</div>
		
			<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="bg_wht mar_t_15 tot_pad">
				<div class="morris-area-chart_sec bg_wht tot_pad">
					<div class="tit2_sec clearfix">
						<div class="left"><h3>Email Shares Report</h3></div>
						<div class="right"><a href="#" class="grn2">View full report</a></div>
					</div>
					<div class="sh_day_info">
						<h3>Last 15 Days- Shared 50 emails</h3>
						<ul>
							<li>
								<img src="<?php echo base_url('assets/images/sh_day_info1.jpg')?>" alt="">
								<p>Sent</p>
							</li>
							<li>
								<img src="<?php echo base_url('assets/images/sh_day_info3.jpg')?>" alt="">
								<p>Unopened</p>
							</li>
							<li>
								<img src="<?php echo base_url('assets/images/sh_day_info1.jpg')?>" alt="">
								<p>Opened</p>
							</li>
							<li>
								<img src="<?php echo base_url('assets/images/sh_day_info2.jpg')?>" alt="">
								<p>Donated</p>
							</li>
							
						</ul>
					</div>
					<div class="morris-area-chart_inr">
						<div id="morris-area-chart"></div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<!-- Send Donation Email Popup -->
<div id="send_email_donations" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="tit1_sec clearfix">
					<div class="left">
					<h2>Send Emails for Donation</h2>
					<div class="ms" style="display:none;"></div>
					</div>
				</div>
				<div class="add_plr_wrp">
					<form id="sendfordonations" name="sendfordonations" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-8 col-sm-9 col-xs-12">
							<div class="frm">
								<div class="form-group">
									<label>Enter Email Address </label> <span> (Separate them by comma " , " )</span>
									<textarea id="email_list" name="email_list" rows="4" class="form-control myclass"></textarea>
								</div>
								<div class="form-group">
									<label>Upload email list(CSV Only)</label>
									<input type="file" id="email_csv" name="email_csv" class="myclass"/>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="frm_btn_grp">
						<button type="submit" id="esubmit" name="esubmit" class="btn_round">Send Emails</button>
						<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Send Donation Email Popup ends -->
<!-- Edit Profile Popup -->
<div id="edit_pro" class="modal fade" role="dialog"></div>
<div id="mail_pro" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function openProfile()
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>player/admin/getPlayerModalAjaxp",
		data      :"pslug=<?php echo $pslug;?>",
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#edit_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#edit_pro").html(response);
			$("#edit_pro").modal('show');               
		}
    });
}
function thanksent(ref)
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>player/admin/getMailStructurep",
		data      :"pslug=<?php echo $pslug;?>&donor="+ref.rel,
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#mail_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#mail_pro").html(response);
			$("#mail_pro").modal('show');               
		}
    });
}
jQuery(document).ready(function($){

	$.validator.addMethod('oneMust', function (value, element, params) {
	return (($('#' + params[0]).val()!='' && $('#' + params[1]).val()=='') || ($('#' + params[0]).val()=='' && $('#' + params[1]).val()!=''));
	}, "you must choose one of these option");
	
	$.validator.addMethod('notBoth', function (value, element, param){
	return this.optional(element) || ($(element).is(':filled') && $('[name="' + param + '"]').is(':blank'));
	}, "you must leave one of these blank");
	
	$("#sendfordonations").validate({

		rules:{
			email_list: {
				oneMust: ['email_list','email_csv'],
				notBoth: 'email_csv'
			},
			email_csv: {
				oneMust: ['email_list','email_csv'],
				notBoth: 'email_list'
			}
		},
		groups: {
			mygroup: 'email_list email_csv'
		},
		errorPlacement: function (error, element) {
			if ($(element).hasClass('myclass')) {
				error.insertAfter('#email_csv');
			} else {
				error.insertAfter(element);
			}
		},
        submitHandler: function(form) {
			
        var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0]); 
        formData.append('email_list', $('#email_list').val()); 
			$.ajax({
				url:"<?php echo base_url('player/admin/donationemailsendp');?>",
				type:"POST",
				data: formData,    
				contentType: false,          
				processData:false,     
				dataType:"json",
				beforeSend: function(){
				//$('#infocontent').html('<div style="padding:5px;">loading...</div>');
				},
				success:function(results){
				if(results.valid==1){
				$('.ms').html(results.msg);
				$('.ms').show();
				setTimeout(function() {            
					location.reload();
					}, 3000);
				}
				else{
					$('.ms').html('Process is failured');
					$('.ms').show();
				}            
				}
			});
		}
	});
	Morris.Line({
		element: 'morris-area-chart',
		data: [
			{ y: '2006', a: 100},
			{ y: '2007', a: 75},
			{ y: '2008', a: 50},
			{ y: '2009', a: 75},
			{ y: '2010', a: 50},
			{ y: '2011', a: 75},
			{ y: '2012', a: 100}
		],
		xkey: 'y',
		ykeys: ['a'],
		labels: ['Series A']
	});
});
</script>