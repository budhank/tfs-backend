<script src="<?php echo base_url('assets/player/js/jquery.validate.js');?>"></script>
<script src="<?php echo base_url('assets/player/js/additional-methods.js');?>"></script>
<script src="<?php echo base_url('assets/player/js/raphael-min.js');?>"></script>
<!--<script src="<?php //echo base_url('assets/player/js/morris-0.4.1.min.js');?>"></script>-->
<style>
.nw-db .price_sec ul > li {
    width: 30%;}
.nw-db .price_sec .p__bar_sec {
    padding: 0;
}
.p-edt a{
display: table;
margin: 15px auto 0;
}
.addcontact a.btn34{ display:block; margin-bottom: 5px;padding:2px 20px;}
.nw-db .ptitle{ font-size:14px; border-bottom:1px solid #000; margin-bottom:15px; text-align:left; overflow:hidden;}
.nw-db .dw-lt{text-align:center; padding-right:0}
.p-vw a{ color:#5295e3;}
.top_procon h4{ font-weight:normal; font-family:robotoregular; margin-top:5px;font-size: 22px; }
.top_usr_con h4{ font-size:16px;font-weight:normal; font-family:robotoregular;margin-top:10px;}
.top_usr_con h5{ font-size:13px;font-weight:normal; font-family:robotoregular;margin-top:2px;}
.notif{color:#a6a6a6; font-size:12px; text-align:center;}
.nw-db .table > tbody + tbody {
    border-top:none;
}
.nw-db .table > tfoot > tr > td,
.nw-db .table > tfoot > tr > th{
    border-top: 2px solid #eaeaea !important;
}
.nw-db .table > thead > tr > th {
    border-bottom: 2px solid #eaeaea;
}
.btn37{background-color: #385699;

padding: 10px 25px;

border-radius: 5px;

color: #fff;

font-weight: normal;

font-size: 18px !important;

border: 1px solid #385699 !important;

cursor: pointer;}
.nw-db .btn-grp{ max-width:70%; margin:0 auto 5px; overflow:hidden;}
</style>
<div id="page-wrapper" class="nw-admin nw-db">
    <div class="container-fluid p-gap2-0">
  
 </div>
     <div class="container-fluid">
     <div class="row">
			<div class="col-sm-3 dw-lt"> 
				<div class="bg_wht mar_t_15 tot_pad">
				<?php
				//print_r($fund_details);
				$fund_start_dt = $fund_details[0]['fund_start_dt'];
				$fund_end_dt = $fund_details[0]['fund_end_dt'];
				if($fund_details[0]['fund_image']==""){
				$fundraiser_image =  base_url("assets/images/noimage-150x150.jpg");   
				}else{
				$playerTimg = explode(".",$fund_details[0]['fund_image']);	
				$fundraiser_image  =  base_url("assets/fundraiser_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
				}
				?>
				<div class="top_usr_img">
				<img src="<?php echo $fundraiser_image; ?>" alt="">
				</div>
				<div class="top_usr_con">
				<h4><?php 
				//
				//echo ucfirst($fund_details[0]["fund_fname"]).' '.ucfirst($fund_details[0]["fund_lname"]);
				echo ucfirst($fund_details[0]["fund_username"]);
				?></h4>
				<span><?php echo $fund_details[0]["fund_slogan"]; ?></span>
				</div>
				</div>
			</div>
<div class="col-sm-9">
     
     <?php
$fund_enddate		=	$fund_details[0]["fund_end_dt"];
$today_date			=	date("Y-m-d");
//$goal				=	$player_details[0]["player_goal"];	
$goal				=	$fund_details[0]["fund_individual_goal"];
if(count($total_payments)>0){
$raised				=	$total_payments[0]["raised_amt"];
}else{
$raised				=	0;	
}	

$remaining			=	$goal - $raised;

if($goal > 0){
$average_percent	=	ceil((($raised/$goal)*100));	
}else{
$average_percent	=	0;	
}
$ended_days_in 		=	ceil(strtotime($fund_enddate)-strtotime($today_date));
if($ended_days_in < 0)
{	
$campaign_msg		=	"<p>Campaign has ended</p>";
}
elseif($ended_days_in == 0)
{	
$campaign_msg		=	"<p>Campaign ends today</p>";	
}
elseif($ended_days_in == 1)
{	
$campaign_msg		=	"<p>Campaign ends tomorrow</p>";
}
else
{
$campaign_days_left =	ceil((strtotime($fund_enddate)-strtotime($today_date))/86400);
$campaign_msg		=	"<p>Campaign ends in ".$campaign_days_left." days</p>";	
}
?>
    <div class="bg_wht mar_t_15 tot_pad">
    <div class="price_sec">
    <ul>
    <li>
    <h1>$<?php echo number_format($goal);?></h1>
    <p>goal</p>
    </li>
     <li>
    <h1>$<?php echo number_format($raised);?></h1>
    <p>raised</p>
    </li>
    <li class="active">
	<h1>
	<?php 
	if($remaining < 0)
	{
	echo "$0";	
	}
	else
	{
	echo "$".number_format($remaining);	
	}
	?>
	</h1>
    <p>remaning</p>
    </li>
    </ul>
    <div class="p__bar_sec">
    <p><?php echo $average_percent; ?>% Funded</p>
    <div class="progress">
	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $average_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $average_percent; ?>%;"> </div>
    </div>
    <?php echo $campaign_msg; ?>
    </div>
    </div>
    </div>
		
</div>
</div>
      <div class="row">
      <div class="col-sm-3 dw-lt">
      <div class="bg_wht  mar_t_15 tot_pad"><div class="top_pro">
						<?php
						if($player_details[0]['player_image']==""){
							$playerimg =  base_url("assets/images/noimage-150x150.jpg");   
						}else{
							$playerTimg = explode(".",$player_details[0]['player_image']);	
							$playerimg  =  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
						}
						//echo "<pre>"; print_r($player_details);
						?>
                        <h3 class="ptitle">Your Profile</h3>
						<div class="top_proimg">
						<img src="<?php echo $playerimg; ?>" alt="">
						</div>
						<div class="top_procon">
							<h4><?php echo ucfirst($player_details[0]["player_fname"]).' '.ucfirst($player_details[0]["player_lname"]); ?></h4>
							<!--<p><?php //echo ucfirst($player_details[0]["player_title"]); ?></p>-->
						</div>
                        <div class="p-edt">
						<button type="button" class="btn btn-info text-center btn34" data-toggle="modal" data-target="#myModalProfile">Edit Profile</button>
						
						</div>
                        <div class="p-vw mar_t_15"><a href="<?php echo base_url($fslug.'/'.$pslug);?>" target="_blank"> View Your Webpage</a></div>
                        </div>
					</div>
         <div class="bg_wht  mar_t_15 tot_pad"><div class="top_pro">
           <h3 class="ptitle">Promote on Facebook</h3>
           <div class="top_usr_img">
						<img src="<?php echo base_url(); ?>assets/images/facebook-icon.png" alt="" width="100">
						</div>
                        <div class="p-edt "><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url($fslug.'/'.$pslug);?>" target="_blank" class="text-center btn34">Share On Facebook</a></div>
         </div>            
        
        </div>
        <div class="bg_wht  mar_t_15 tot_pad"><div class="top_pro">
           <h3 class="ptitle">Login Credentials</h3>
           <div class="top_usr_img ">
						<div class="row text-left"><div class="col-sm-4 ">Login</div><div class="col-sm-8"><?php echo $player_details[0]["player_email"]; ?></div></div>
                        <div class="row text-left"><div class="col-sm-4 ">Password</div>
						<div class="col-sm-8">
						<?php
							$encrypt_key = $this->config->item('encryption_key');  
							$player_passw = $this->encrypt->decode($player_details[0]["player_pass"],$encrypt_key);
							echo $player_passw;
						?>
						</div>
						</div>
						</div>
                        <div class="p-edt ">
						<a href="#" class="text-center btn34" data-toggle="modal" data-target="#edit_login_credential">Change Password</a>
						</div>
         </div>            
        
        </div> </div>
        
                    <div class="col-sm-9"><div class="bg_wht  mar_t_15 tot_pad">
                     <h3 class="ptitle">People You’ve Invited 
						 <div class="pull-right addcontact">
						  <button type="button" class="btn btn-info text-center btn34" data-toggle="modal" data-target="#myModalContact">Add Contact</button>
						 </div>
                     </h3>
                    
                   
                            
                            <table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:20%">Name</th>
							<th style="width:21%">Email</th>
							<th style="width:15%">Invited</th>
							<th style="width:15%">Reminder*	</th>
                            <th style="width:15%"> Last Chance*</th>
                            <th style="width:12%"></th>
							<th style="width:2%" class="delete_btn" ></th>
						</tr>
					</thead>

					<tbody>
<?php
$doneteds = array();
if(count($contact_list)){								
foreach($contact_list as $contact_li){
	$countSent = 0;
	$countDoneted = 0;
	$share_reports = $this->player_model->view('tbl_email_share_reports',array('to_id'=>$contact_li["id"]));
	if(count($share_reports))
		$countSent = count($share_reports) - 1;	
	
	$donors_payment = $this->player_model->view('tbl_donors_payment',array('donor_email'=>$contact_li["contact_email"]));
	if(count($donors_payment)){
		$doneteds[$donors_payment[0]['donor_email']] = $donors_payment[0];
		$countDoneted = count($donors_payment);
	}
?>
                    <tr>
							<td><?php echo $contact_li["contact_name"]; ?></td>
							<td><?php echo $contact_li["contact_email"]; ?></td>
							<td>
							<?php if($countDoneted!=0):?>
							Donated
							<?php elseif($contact_li["status"]==3):?>
							Opted Out
							<?php else:?>							
							<?php echo (count($share_reports))? date('m-d-Y',strtotime($share_reports[0]["sent_time"])):''; ?>
							<?php endif;?>							
							</td>
							<td>
							<?php if($countDoneted!=0):?>
							Donated
							<?php elseif($contact_li["status"]==3):?>
							Opted Out
							<?php else:?>	
							<span <?php if($contact_li["status"]==1):?>style="color:#cdcdcd;"<?php endif;?>><?php echo date('m-d-Y',strtotime('+5 days', strtotime($share_reports[0]["sent_time"]))); ?></span>
							<?php endif;?>
							</td>
                            <td>
							<?php if($countDoneted!=0):?>
							Donated
							<?php elseif($contact_li["status"]==3):?>
							Opted Out
							<?php else:?>	
							<span <?php if($contact_li["status"]==1):?>style="color:#cdcdcd;"<?php endif;?>><?php echo date('m-d-Y',strtotime('-3 days', strtotime($fund_end_dt))); ?></span>
							<?php endif;?>							
							</td>
                            <td>
								<?php
								switch($contact_li["status"]){
								case '2':
								?>
								<button type="button" class="btn-invyt btn-ste">Stopped</button>
								<?php
								break; 
								case '3':
								?>
								Opted Out
								<?php
								break;
								default:
								?>
							<?php if($countDoneted==0):?>
							<form action="<?php echo base_url($fslug.'/'.$pslug.'/admin/invitetoall');?>" method="POST">
							<input type="hidden" name="contact_id" value="<?php echo $contact_li["id"]; ?>"/>
							<input type="hidden" name="goto" value="same"/>
							<button type="submit" class="btn-invyt btn-ste">
							<?php if(count($share_reports)==0):?>Invite<?php endif;?>
							<?php if(count($share_reports)>0):?>Re-invite<?php endif;?>							
							</button>
							</form>
							<?php else:?>
							<button type="button" class="btn-invyt btn-black" >Donated</button>
							<?php endif;?>	
							
								<?php
								break;
							}
							?>	
							</td>
							<td class="text-right">
							<?php if($contact_li["status"]==1 && $countDoneted==0):?>
							<form action="<?php echo base_url($fslug.'/'.$pslug.'/admin/invitetoall');?>" method="POST">
							<input type="hidden" name="contact_id" value="<?php echo $contact_li["id"]; ?>"/>
							<input type="hidden" name="goto" value="same"/>
							<input type="hidden" name="action" value="status_change"/>
							<input type="hidden" name="status" value="2"/>
							<button type="submit" class="btn btn-danger">
							<i class="fa fa-times" aria-hidden="true"></i>						
							</button>
							</form>
							<?php endif;?>
							</td>
						</tr>
						 <?php 
						 }
					}else{
							?>
                      <tr>
							<td colspan="7" class="text-center"><p>You haven’t invited any of your contacts to donate yet.
                            <br/>Invite your contacts to start receiving donations.</p>
                            <button type="button" class="text-center btn34" data-toggle="modal" data-target="#myModalContact">
							Invite Your Contacts</button>
							</td>							
						</tr>
							<?php 
							}
							?>
                    </tbody>
                    
                    <tfoot class="notif">
					<tr><td colspan="7">*Up to two reminder emails will be sent to contacts who have NOT donated prior to the scheduled reminder date. You may send a reminder manually at anytime by clicking on the “Email Re-Invite” button. Doing so will override the next scheduled reminder.
					</tfoot>
</td></tr></table>
						 
                            
                    </div>
                    
                    <div class="bg_wht  mar_t_15 tot_pad">
                     <h3 class="ptitle">People Who Have Donated</h3>
                   
                       <div class="donations_tbl">
					   
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:20%">Name</th>
							<th style="width:20%">Email</th>
							<th style="width:20%">Date</th>
							<th style="width:15%" class="text-center">Amount</th>
							<th style="width:25%" class="text-right">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					if(count($donors_list)>0)
					{
						$total = 0;
					foreach($donors_list as $donors_li)
					{
						//print_r($donors_li);
						$total += $donors_li["donation"];
					?>
						<tr>
							<td data-th="Donor Name"><?php echo $donors_li["donor_fname"].' '.$donors_li["donor_lname"];?></td>
							<td data-th="Donor Email"><?php echo $donors_li["donor_email"];?></td>
							<td data-th="Date"><?php echo date("m-d-Y h:i A",strtotime($donors_li["donation_date"]));?></td>
							<td data-th="Amount" class="text-right"><strong>$<?php echo $donors_li["donation"]; ?></strong></td>
							
							<td data-th="Mail" class="text-right">
							<?php if($donors_li["is_thankyou_email"]==1):?>
							<a href="javascript:void(0);" class="btn-invyt btn-black"><i class="fa fa-check" aria-hidden="true"></i> Thank you Email sent</a>
							<?php else:?>
							<a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($donors_li["id"]);?>"  class="btn-invyt btn-ste">Send a Thank you Email</a>
							<?php endif;?>
							</td>
							
						</tr>
					<?php
					}
					?>
					<tr><td colspan="3"><strong>TOTAL DONATIONS</strong></td><td class="text-right" ><strong>$<?php echo number_format($total,2);?></strong></td><td></td></tr>
					<?php
					}
					else
					{
					?>
						<tr>
							<td colspan="6" class="text-center">
                            <p>You haven’t received any donations yet. <br/>
Invite your contacts via Email and Facebook to donate.</p>
  <div  class="btn-grp"> 
                                          <div  class="col-sm-6 text-center">   <button type="button" class="text-center btn34" data-toggle="modal" data-target="#myModalContact">Invite Your Contacts</button> </div>
                                           <div  class="col-sm-6 text-center">    
                                          
                                           <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url($fslug.'/'.$pslug);?>" target="_blank" class="text-center btn37" style="display: inline-block;">Share on Facebook</a></div>
                                           </div>
                                            
</td>
							
						</tr>
					<?php	
					}
					?>
					</tbody>					
				</table>
			</div>     
                    </div>
                    
                    </div></div>
     
     </div>
</div> <!-- #page-wrapper ends -->


<!-- Modal -->
<div id="myModalProfile" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Your Profile</h4>
      </div>
      <div class="modal-body">
       <h2>Step 1 - Verify Your Info</h2>
                  <div class="lt-form-wrap m-gap2-15 participants">
                    <form action="<?php echo base_url($fslug."/".$pslug."/admin/home");?>" method="POST" id="frm_information_add" name="frm_information_add" enctype="multipart/form-data">
                      <div class="form-group">
                        <label class="btn-block txt-yellow">First Name</label>
                        <input type="text" class="form-control required" id="player_fname" name="player_fname" value="<?php echo $player_details[0]["player_fname"];?>">
                      </div>
                      <div class="form-group">
                        <label class="btn-block txt-yellow">Last Name</label>
                        <input type="text" class="form-control required" id="player_lname" name="player_lname" value="<?php echo $player_details[0]["player_lname"];?>">
                      </div>
                      <div class="form-group">
						<?php
						
						if($player_details[0]['player_image']==""){
							$playerimg =  base_url("assets/images/noimage-150x150.jpg");   
						}else{
							$playerTimg = explode(".",$player_details[0]['player_image']);	
							$playerimg  =  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
						}
						?>
                        <label class="btn-block txt-yellow">Profile Image</label>
                        <span class="pic large">
						<img src="<?php echo $playerimg;?>" alt="Photo" id="player_img"/>
						</span>
                        <div class="clearfix"></div>
                        <label for="profile_img" class="file-upload text-center btn34">Upload</label>
                        <input id="profile_img" name="profile_img" type="file" class="do-not-ignore" style="display:none;"/>
                        <div>(jpg, jpeg, png, gif)</div>
                      </div>
                      <div class="sec-line"></div>
                      <div class="btn-gp text-center m-gap2-25">
                        <input type="submit" name="info_submit" class="btn33" value="Next">
                      </div>
                    </form>
                  </div>
                
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModalContact" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Contact</h4>
      </div>
      <div class="modal-body">
        <div class="add-new-email-row">
							<form action="<?php echo base_url($fslug."/".$pslug."/admin/contacts");?>" method="POST" id="frm_contact_add" name="frm_contact_add" enctype="multipart/form-data" class="add-email-form">
                            <div class="col-xs-6 email-input">
							<input type="email" class="form-control required email" id="contact_email" name="contact_email" placeholder="Enter Email Address" required>
							</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 email-input-action text-center">
							  <button id="con_submit" name="con_submit" type="submit" class="btn34 btn-add-email">Add</button>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 email-input-action text-center">
                              <!--<button id="cus_mail" name="cus_mail" type="button" class="btn34 btn-add-email">Send Custom Mail</button>-->
                            </div>
							</form>
                          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Edit Login Credential -->
<div id="edit_login_credential" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
			<div class="tit1_sec clearfix">
				<div class="left">
				<h2>Edit Login Credential</h2>
				<div class="ms" style="display:none;"></div>
				</div>
			</div>
			<div class="add_plr_wrp">
				<form method="post" name="changesettings" id="changesettings">
				<div class="row">
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="frm">
							<div class="form-group">
								<label>Change password for</label>
								<input type="text" class="form-control" id="player_email" name="player_email" value="<?php echo $this->session->userdata('player_contact_email');?>" readonly/>
							</div>
							<div class="form-group">
								<label for="new-pass">New password</label>
								<input type="password" class="form-control" id="player_pass" name="player_pass" required/>
							</div>
							<div class="form-group">
								<label for="re-pass">Retype new password</label>
								<input type="password" class="form-control" id="player_pass_confirm" name="player_pass_confirm" required/>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="frm_btn_grp">
				<input type="submit" class="btn_round" name="change_password" value="Save"/>
				<a href="#" class="undr_lin" data-dismiss="modal">Cancel</a>
				</div>
				</form>
			</div>
      </div>
    </div>
  </div>
</div>
<!-- Edit Login Credential ends -->  

<div id="mail_pro" class="modal fade" role="dialog"></div>
<script type="text/javascript">
function openProfile()
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>player/admin/getPlayerModalAjaxp",
		data      :"pslug=<?php echo $pslug;?>",
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#edit_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#edit_pro").html(response);
			$("#edit_pro").modal('show');               
		}
    });
}
function thanksent(ref)
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>player/admin/getMailStructurep",
		data      :"pslug=<?php echo $pslug;?>&donor="+ref.rel,
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#mail_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#mail_pro").html(response);
			$("#mail_pro").modal('show');               
		}
    });
}
jQuery(document).ready(function($){

	$.validator.addMethod('oneMust', function (value, element, params) {
	return (($('#' + params[0]).val()!='' && $('#' + params[1]).val()=='') || ($('#' + params[0]).val()=='' && $('#' + params[1]).val()!=''));
	}, "you must choose one of these option");
	
	$.validator.addMethod('notBoth', function (value, element, param){
	return this.optional(element) || ($(element).is(':filled') && $('[name="' + param + '"]').is(':blank'));
	}, "you must leave one of these blank");
	
	$("#sendfordonations").validate({

		rules:{
			email_list: {
				oneMust: ['email_list','email_csv'],
				notBoth: 'email_csv'
			},
			email_csv: {
				oneMust: ['email_list','email_csv'],
				notBoth: 'email_list'
			}
		},
		groups: {
			mygroup: 'email_list email_csv'
		},
		errorPlacement: function (error, element) {
			if ($(element).hasClass('myclass')) {
				error.insertAfter('#email_csv');
			} else {
				error.insertAfter(element);
			}
		},
        submitHandler: function(form) {
			
        var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0]); 
        formData.append('email_list', $('#email_list').val()); 
			$.ajax({
				url:"<?php echo base_url('player/admin/donationemailsendp');?>",
				type:"POST",
				data: formData,    
				contentType: false,          
				processData:false,     
				dataType:"json",
				beforeSend: function(){
				//$('#infocontent').html('<div style="padding:5px;">loading...</div>');
				},
				success:function(results){
				if(results.valid==1){
				$('.ms').html(results.msg);
				$('.ms').show();
				setTimeout(function() {            
					location.reload();
					}, 3000);
				}
				else{
					$('.ms').html('Process is failured');
					$('.ms').show();
				}            
				}
			});
		}
	});

		$("#add-email").click(function(){
      $(".add-remove-email-row").prepend("<div class='add-new-email-row'><div class='col-xs-6 email-input'>abc@abcd.com</div><div class='col-xs-4 col-sm-3 col-md-2 email-input-action text-center'><span style='cursor:pointer;' class='del-email'>X</span></div></div>");
    });

    $(".add-remove-email-row").on("click", ".del-email", function(){
      $(this).closest(".add-new-email-row").remove();
    });

    $(".btn-invyt").click(function(){
      var txt1 = '<p><img src="http://thanksforsupporting.com/apps/assets/player/images/icon-successful.png" alt="Successful"></p><p style="font-size:24px;font-weight:bold;">Congratulations</p><p>Your contacts have been invited</p>';
      $("#invyt-stat").html(txt1);
    });

});


</script>
