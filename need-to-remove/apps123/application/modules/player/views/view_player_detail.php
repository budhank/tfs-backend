<script src="<?php echo base_url('assets/player/js/jquery.validate.js');?>"></script>
<script src="<?php echo base_url('assets/player/js/additional-methods.js');?>"></script>
<script src="<?php echo base_url('assets/player/js/raphael-min.js');?>"></script>
<!--<script src="<?php //echo base_url('assets/player/js/morris-0.4.1.min.js');?>"></script>-->
<div id="page-wrapper" class="nw-admin">
    <div class="container-fluid p-gap2-0">
      <div class="sec-title">Setup Guide</div>
    </div>
    <div class="container-fluid">
      <div class="bg_wht mar_t_15 tot_pad"> 
        <div class="row">
          <div class="col-xs-12">
            <p class="p-gap2-20">Complete the Setup Steps below to get your fundraiser up and running in less than 5 minutes.</p>
          </div>
        </div>
      </div>
      <div class="tab-section mar_t_15 ">
        <ul class="nav tab-title">
          <li <?php echo ($active_tab=='home')? 'class="active"': ''?>>
            <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/home');?>"><b>Step 1</b><span>Profile Image</span></a>
          </li>
          <li <?php echo ($active_tab=='contacts')? 'class="active"': ''?>>
            <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/contacts');?>"><b>Step 2</b><span>Contacts</span></a>
          </li>
          <li <?php echo ($active_tab=='invite')? 'class="active"': ''?>>
            <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/invite');?>"><b>Step 3</b><span>Invite</span></a>
          </li>
          <li <?php echo ($active_tab=='checkoutweb')? 'class="active"': ''?>>
            <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/checkoutweb');?>"><b>Step 4</b><span>Your Webpage</span></a>
          </li>
        </ul>
        <div class="bg_wht tot_pad">
          <div class="tab-content">
            <div id="step1" class="tab-pane fade <?php echo ($active_tab=='home')? 'in active': ''?>">
              <div class="row">
                <div class="col-sm-8 lt-form-sec">
                  <h2>Step 1 - Verify Your Info</h2>
                  <div class="lt-form-wrap m-gap2-15 participants">
                    <form action="<?php echo base_url($fslug."/".$pslug."/admin/home");?>" method="POST" id="frm_information_add" name="frm_information_add" enctype="multipart/form-data">
                      <div class="form-group">
                        <label class="btn-block txt-yellow">First Name</label>
                        <input type="text" class="form-control required" id="player_fname" name="player_fname" value="<?php echo $player_details[0]["player_fname"];?>">
                      </div>
                      <div class="form-group">
                        <label class="btn-block txt-yellow">Last Name</label>
                        <input type="text" class="form-control required" id="player_lname" name="player_lname" value="<?php echo $player_details[0]["player_lname"];?>">
                      </div>
                      <div class="form-group">
						<?php
						if($player_details[0]['player_image']==""){
							$playerimg =  base_url("assets/images/noimage-150x150.jpg");   
						}else{
							$playerTimg = explode(".",$player_details[0]['player_image']);	
							$playerimg  =  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
						}
						?>
                        <label class="btn-block txt-yellow">Profile Image</label>
                        <span class="pic large">
						<img src="<?php echo $playerimg;?>" alt="Photo" id="player_img"/>
						</span>
                        <div class="clearfix"></div>
                        <label for="profile_img" class="file-upload text-center btn34">Upload</label>
                        <input id="profile_img" name="profile_img" type="file" class="do-not-ignore"/>
                        <div>(jpg, jpeg, png, gif)</div>
                      </div>
                      <div class="sec-line"></div>
                      <div class="btn-gp text-center m-gap2-25">
                        <input type="submit" name="info_submit" class="btn33" value="Next">
                      </div>
                    </form>
                  </div>
                </div>
                <div  class="col-sm-4">
                  <h2 class="text-center">Description</h2>
                  <p>Verify your first and last name are spelled correctly.</p>
                  <p>Upload an image of yourself to be displayed in your emails and your webpage.</p>
                  <p>When finished, click the Next button to continue.</p>
                </div>
              </div>
            </div>
            <div id="step2" class="tab-pane fade <?php echo ($active_tab=='contacts')? 'in active': ''?>">
              <div class="row">
                <div class="col-sm-8 lt-form-sec">
                  <h2 class="flex-wrapper">Step 2 - Enter your Contacts
					<form action="<?php echo base_url($fslug.'/'.$pslug.'/admin/bulk_contact_upload');?>" method="POST" id="frm_upload_info" name="frm_upload_info" enctype="multipart/form-data" class="add-email-form">
                    <label for="bulk_contacts" class="file-upload text-center btn33">
					Upload File<span class="btn-block">(CSV,Excel)</span>
					</label>
                    <input id="bulk_contacts" name="bulk_contacts" type="file" class="do-not-ignore"/>
					<input type="submit" id="bulk_submit" name="bulk_submit" value="Next" style="display:none;">
					</form>
                  </h2>
				<?php
				if($this->session->userdata('up_msg')){
				echo '<br /><p class="text-success">&nbsp;&nbsp;&nbsp;'.$this->session->userdata('up_msg').'</p>';
				$this->session->unset_userdata('up_msg');
				}
				?>
                  <div class="lt-form-wrap m-gap2-15">
                      <div style="min-height:300px;">
						<div class="add-new-email-row head txt-yellow">
                          <div class="col-xs-6 email-input">Email Address</div>
                          <div class="col-xs-4 col-sm-3 col-md-2 email-input-action text-center">Delete</div>
                        </div>
						<div class="add-remove-email-row">
                          <div class="add-new-email-row">
							<form action="<?php echo base_url($fslug."/".$pslug."/admin/contacts");?>" method="POST" id="frm_contact_add" name="frm_contact_add" enctype="multipart/form-data" class="add-email-form">
                            <div class="col-xs-6 email-input">
							<input type="text" class="form-control required email" id="contact_email" name="contact_email">
							</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 email-input-action text-center">
							  <button id="con_submit" name="con_submit" type="submit" class="btn34 btn-add-email">Add</button>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 email-input-action text-center">
                              <!--<button id="cus_mail" name="cus_mail" type="button" class="btn34 btn-add-email">Send Custom Mail</button>-->
                            </div>
							</form>
                          </div>
						  <?php
							if(count($contact_list)){
								foreach($contact_list as $contact_li){
							?>
						  <div class="add-new-email-row">
							  <div class="col-xs-6 email-input"><?php echo $contact_li["contact_email"]; ?></div>
							  <div class="col-xs-4 col-sm-3 col-md-2 email-input-action text-center">
							  <!-- <a href="jsvascript:void(0);" onclick="delContact(<?php echo $contact_li["id"];?>)"><span style="cursor:pointer;" class="del-email">X</span></a> -->
							  </div>
						  </div>
						  <?php
								}
							}
							?>
                        </div>
                      </div>
					
                      <div class="sec-line"></div>
                      <div class="btn-gp text-center m-gap2-25">
                        <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/home');?>" class="btn35">Back</a>
                        <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/invite');?>" class="btn33" style="text-decoration:none;">Next</a>
                      </div>
                  </div>
                </div>
                <div  class="col-sm-4">
                  <h2 class="text-center">Description</h2>
                  <p>Enter the email addresses of your contacts that you would like to invite to donate to your fundraiser.</p>
                  <p>You may ONLY invited people who are 18 years or older to contribute.</p>
                  <p>To save time, you may uplaod a csv or spreadsheet file if you have one.</p>
                  <p>When finished, click the Next button at the bottom to save your information and continue to the next step.</p>
                </div>
              </div>
            </div>
			<div id="step3" class="tab-pane fade <?php echo ($active_tab=='invite')? 'in active': ''?>">
              <div class="row">
                <div class="col-sm-8 lt-form-sec">
                  <h2>Step 3 - Invite Your Contacts</h2>
                  <div class="lt-form-wrap m-gap2-25">
                      <div style="min-height:300px;">
					   <?php
							if(count($non_invited_contact_details)){
								?>
								<h2>Contacts not yet invited</h2>
								<?php
								foreach($non_invited_contact_details as $non_invited_contact){
							?>
						  <div class="add-new-email-row " style="margin-top:15px;">
                          <div style="line-height: 25px;margin-right: 15px;" class="email-input col-sm-2">
							  <?php echo date('Y-m-d',strtotime($non_invited_contact["created"])); ?>
                              </div>
                          <div style="line-height: 25px;margin-right: 15px;" class="email-input col-sm-7">
							  <?php echo $non_invited_contact["contact_email"]; ?></div>
							  
						  </div>
						  <?php
								}
								?>
				<div class="lt-form-wrap text-center" id="invyt-stat">
				<form action="<?php echo base_url($fslug.'/'.$pslug.'/admin/invitetoall');?>" method="POST">
				<input type="hidden" name="goto" value="same"/>
				<button type="submit" class="btn-invyt btn34">Invite Your Contacts</button>
				</form>
				</div>
								
								<?php
							}
							?>
					  
						<?php
						$date	 = strtotime(date("Y-m-d"));
						$st_date = strtotime($start_date);
						
						if($date >= $st_date){
						if($this->session->userdata('succ_msg')){
						?>
						<div class="lt-form-wrap text-center" id="invyt-stat">
							<div class="text-center stp-img">
							<img src="<?php echo base_url('assets/player/images/icon-successful.png');?>" alt="Successful"/>
							</div>
							<p style="font-size:24px;font-weight:bold;">Congratulations</p>
							<p>Your contacts have been invited</p>
						</div>
						<?php
						$this->session->unset_userdata('succ_msg');
						}
						}else{
						?>
						<div class="lt-form-wrap text-center" id="invyt-stat">
						<p style="font-size:24px;font-weight:bold;">Sorry!</p>
						<p>You cannot invite your contacts until the campaign starts</p>
						</div>
						<?php
						}
						?>
						 <?php
							if(count($invited_contact_details)){
								?>
								<h2>Contacts already invited</h2>
								<?php
								foreach($invited_contact_details as $invited_contact){
							?>
						  <div class="add-new-email-row " style="margin-top:15px;">
                          <div style="line-height: 25px;margin-right: 15px;" class="email-input col-sm-2">
							  <?php echo date('Y-m-d',strtotime($invited_contact["created"])); ?>
                              </div>
                          <div style="line-height: 25px;margin-right: 15px;" class="email-input col-sm-7">
							  <?php echo $invited_contact["contact_email"]; ?></div>
							  <span class="email-input-action">
							  <form action="<?php echo base_url($fslug.'/'.$pslug.'/admin/invitetoall');?>" method="POST">
							<input type="hidden" name="contact_id" value="<?php echo $invited_contact["id"]; ?>"/>
							<input type="hidden" name="goto" value="same"/>
							<button type="submit" class="btn-invyt btn3">Re Invite</button>
							</form>
							  </span>
						  </div>
						  <?php
								}
							}
							?>
						
						
						
                        <div class="sec-line m-top-60"></div>
                        <div class="btn-gp text-center m-gap2-25">
                          <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/contacts');?>" class="btn35">Back</a>
                          <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/checkoutweb');?>" class="btn33" style="text-decoration:none;">Next</a>
                        </div>
                      </div>
                  </div>
                  
                </div>
				<div  class="col-sm-4">
				<h2 class="text-center">Description</h2>
				<p>Click on the Invite Your Contacts button to have the system send out emails to each of the Contacts you entered.</p>
				<p>The email will include a link to your webpage where they can donate to your cause.</p>
				</div>
              </div>
            </div>
			<div id="step4" class="participants tab-pane fade <?php echo ($active_tab=='checkoutweb')? 'in active': ''?>">
              <div class="row">
                <div class="col-sm-8 lt-form-sec">
                  <h2>Step 4 - Your Webpage</h2>
                  <div class="lt-form-wrap m-gap2-25">
                      <div class="text-center" style="min-height:300px;">
						<?php
						if($player_details[0]['player_image']==""){
						$playerimg =  base_url("assets/images/noimage-150x150.jpg");   
						}else{
						$playerTimg = explode(".",$player_details[0]['player_image']);	
						$playerimg  =  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
						}
						?>
                        <span class="pic large"><img src="<?php echo $playerimg; ?>" alt="Photo"></span>
                        <p style="font-size:24px;">Checkout Your Webpage</p>
                        <div class="lt-form-wrap">
                            <a href="<?php echo base_url($fslug.'/'.$pslug);?>" target="_blank" class="btn34">View Your Webpage</a>
                        </div>
                        <div class="sec-line m-top-60"></div>
                        <div class="btn-gp text-center m-gap2-25">
                          <a href="<?php echo base_url($fslug.'/'.$pslug.'/admin/invite');?>" class="btn35">Back</a>
                        </div>
                      </div>
                  </div>
                </div>
                <div  class="col-sm-4">
                  <h2 class="text-center">Description</h2>
                  <p>We’ve taken your personal information (along with the information provided from your organization) to create a personal webpage for you.</p>
                  <p>Your contacts will donate to your fundraiser from your personal webpage.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div> <!-- #page-wrapper ends -->
<?php
if($active_tab=='home'){
?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$.validator.setDefaults({ ignore: ':hidden:not(.do-not-ignore)' });
	$("#frm_information_add").validate({
		rules:{
			profile_img: {
				extension: 'png|jpe?g|gif'
			}
		},
		messages:{
			profile_img: {
				extension: 'Please upload valid file type'
			}
		}
	});
	$("#profile_img").change(function () {
	var stss = true;
	$('.crr').remove();
	var img = $("#profile_img").val();
	if(img != ''){
		var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		$('#profile_img').after('<label for="profile_img" generated="true" class="crr error">Please upload valid file type.</label>');
		}else{
		stss = true;	
		}
	}else{
		$('#profile_img').after('<label for="profile_img" generated="true" class="crr error">This field is required.</label>');
		stss = false;
	}
	if(stss==true){
		var formData = new FormData();
		formData.append('profile_img', $('#profile_img')[0].files[0]);  	      
		
		$.ajax({
		url:"<?php echo base_url($fslug.'/'.$pslug.'/admin/upload_player_logo');?>", 
		type:'POST',
		data: formData,    
		contentType: false,            
		processData:false,     
		dataType:"json",
		beforeSend: function(){
		   //$('#div_loading').html('<div class="loading_inn">loading...</div>');
		},
		success:function(results){
			if(results.valid==1){
				$("#player_img").attr('src',results.src);
			}else{
				$('#profile_img').after('<label for="profile_img" generated="true" class="crr error">'+results.msg+'</label>');
			}
		}
		});
	}
	});
});
</script>
<?php 
}
?>
<?php
if($active_tab=='contacts'){
?>
<div id="cusMailModal" class="modal fade" role="dialog"></div>
<script type="text/javascript">
jQuery(document).ready(function($){
	$.validator.setDefaults({ ignore: ':hidden:not(.do-not-ignore)' });
	$("#frm_contact_add").validate();
	$("#bulk_contacts").change(function (e) {
		$('#frm_upload_info').validate({
			rules: {
				bulk_contacts:{
				extension: "csv|xls|csvx|xlsx"
				}
			},
			messages:{
				bulk_contacts:{
				extension: "Please upload valid file type."
				}
			}
		});
		$('#bulk_submit').trigger('click');
	});
	$("#cus_mail").click(function (e) {
		$.ajax({
		type      :"POST",
		url       :"<?php echo base_url($fslug.'/'.$pslug.'/admin/getCustomMailAjax'); ?>",
		data      :"fslug=",
		dataType  :"html",
		success: function(response){        
			$("#cusMailModal").html(response);
			$("#cusMailModal").modal('show');               
		}
		});	
	});
});
function delContact(cid)
{
	var  ans=confirm("Do you want to Delete?");
	if(ans)
	{
		$.ajax({
			type 	:	"POST",
			url		:	"<?php echo base_url($fslug.'/'.$pslug.'/admin/del_contact'); ?>",
			data	:	{'contact_id':cid},
			dataType:	"JSON",
			success	:	function(response){
				if(response.valid==1){
					location.reload();
				}else{
					alert(response.msg);
				}
			}
		});
	}
}
</script>
<?php 
}
?>