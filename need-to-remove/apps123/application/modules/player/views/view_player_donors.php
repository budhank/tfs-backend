<script src="<?php echo base_url('assets/fundraiser/js/jquery.validate.js')?>"></script>
<script src="<?php echo base_url('assets/fundraiser/js/additional-methods.js')?>"></script>
<!-- Green Bar start-->
<?php
$fund_enddate		=	$fund_details[0]["fund_end_dt"];
$today_date			=	date("Y-m-d");
//$goal				=	$player_details[0]["player_goal"];	
$goal				=	$fund_details[0]["fund_individual_goal"];
if(count($total_payments)>0){
$raised				=	$total_payments[0]["raised_amt"];
}else{
$raised				=	0;	
}	

$remaining			=	$goal - $raised;

if($goal > 0){
$average_percent	=	ceil((($raised/$goal)*100));	
}else{
$average_percent	=	0;	
}
$ended_days_in 		=	ceil(strtotime($fund_enddate)-strtotime($today_date));
if($ended_days_in < 0)
{	
$campaign_msg		=	"<p>Campaign has ended</p>";
}
elseif($ended_days_in == 0)
{	
$campaign_msg		=	"<p>Campaign ends today</p>";	
}
elseif($ended_days_in == 1)
{	
$campaign_msg		=	"<p>Campaign ends tomorrow</p>";
}
else
{
$campaign_days_left =	ceil((strtotime($fund_enddate)-strtotime($today_date))/86400);
$campaign_msg		=	"<p>Campaign ends in ".$campaign_days_left." days</p>";	
}
?>
<div id="page-wrapper">
	<div class="full_top_wrp bg_wht">
		<ul class="breadcrumb">
			<li class="active">Donors/ Donations</li>
		</ul>
	</div>
	<div class="container-fluid">
    <div class="bg_wht mar_t_15 tot_pad">
    <div class="price_sec">
    <ul>
    <li>
    <h1>$<?php echo number_format($goal);?></h1>
    <p>goal</p>
    </li>
     <li>
    <h1>$<?php echo number_format($raised);?></h1>
    <p>raised</p>
    </li>
    <li class="active">
	<h1>
	<?php 
	if($remaining < 0)
	{
	echo "$0";	
	}
	else
	{
	echo "$".number_format($remaining);	
	}
	?>
	</h1>
    <p>remaning</p>
    </li>
    </ul>
    <div class="p__bar_sec">
    <p><?php echo $average_percent; ?>% Funded</p>
    <div class="progress">
	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $average_percent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $average_percent; ?>%;"> </div>
    </div>
    <?php echo $campaign_msg; ?>
    </div>
    </div>
    </div>
		<div class="bg_wht mar_t_15 tot_pad">
			<div class="tit1_sec clearfix">
				<div class="left"><h2>Donors/ Donations</h2></div>
			</div>
			<div class="donations_tbl">
				<table class="table table-hover table-condensed">
					<thead>
						<tr>
							<th style="width:20%">Donor Name</th>
							<th style="width:20%">Amount</th>
							<th style="width:20%">Message</th>
							<th style="width:20%">Thanks Mail</th>
							<th style="width:20%" class="text-right">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					if(count($donors_list)>0)
					{
					foreach($donors_list as $donors_li)
					{
					?>
						<tr>
							<td data-th="Donor Name"><?php echo $donors_li["donor_fname"].' '.$donors_li["donor_lname"];?></td>
							<td data-th="Amount"><strong>$<?php echo $donors_li["donation_amt"]; ?></strong></td>
							<td data-th="Message"></td>
							<td data-th="Mail">
							<a href="javascript:void(0);" onclick="thanksent(this)" rel="<?php echo base64_encode($donors_li["id"]);?>" style="text-decoration:underline;">Send a Thank you Email</a>
							</td>
							<td data-th="" class="text-right">
							<small><?php echo date("m-d-Y H:i:s A",strtotime($donors_li["donation_date"]));?></small>
							</td>
						</tr>
					<?php
					}
					}
					else
					{
					?>
						<tr>
							<td colspan="5" class="text-center">No Donation Found</td>
						</tr>
					<?php	
					}
					?>
					</tbody>
					<tfoot>
					<?php 
					if(count($donors_list)>10)
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<a class="grn2" href="#">+load more</a>
							</td>
						</tr>
					<?php	
					}
					else
					{
					if(count($donors_list)>10)
					{
					?>
						<tr>
							<td colspan="5" class="text-center">
							<span class="grn2">Thats all</span>
							</td>
						</tr>
					<?php
					}
					}
					?>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="mail_pro" class="modal fade" role="dialog"></div>
<!-- Edit Profile Popup ends -->
<script type="text/javascript">
function thanksent(ref)
{
$.ajax({
		type      :"POST",
		url       :"<?php echo base_url(); ?>player/admin/getMailStructurep",
		data      :"pslug=<?php echo $pslug;?>&donor="+ref.rel,
		dataType  :"html",
		beforeSend: function(){
			$('#div_loading').show();
			$("#mail_pro").modal('show');
		},
		success: function(response){
			$('#div_loading').hide();
			$("#mail_pro").html(response);
			$("#mail_pro").modal('show');               
		}
    });
}
</script>