<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player extends MY_PlayerController{

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('player_model');
	}

	public function index()
	{
		if($this->session->userdata('player_id')!="")
		{
			redirect($this->session->userdata('fundraiser_urlname').'/'.$this->session->userdata('player_urlname').'/admin/home');
		}
		else
		{
			redirect('admin/login');
		}
	}

	/*public function home($fslug,$pslug)
	{
		$player_details = array();
		$donor_details = array();
		if($pslug!='')
		{
			$player_details=$this->player_model->playersInfo($pslug);
			$donor_details=$this->player_model->donors_list($pslug,10);
		}
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_dashboard',$data);
		$this->load->view('../../includes_player/footer');
	}*/
	
	public function home($fslug,$pslug)
	{
        if($this->input->post('change_password') == 'Save')
		{
            $this->ajaxSettings();
            redirect($fslug.'/'.$pslug.'/admin/home');
        }
        
        if($this->input->post('info_submit'))
		{
			$player_fname = $this->input->post('player_fname');
			$player_lname = $this->input->post('player_lname');
			$upload_data = $this->upload_player_logo($fslug,$pslug,true);
			
			$playdata = array('player_fname'=>$player_fname,'player_lname'=>$player_lname);
			$this->player_model->edit('tbl_player', $playdata, array('player_slug_name'=>$pslug));
			
		}
        
		$player_details = array();
		$donor_details = array();
		if($pslug!='')
		{
			$player_details=$this->player_model->playersInfo($pslug);
			$donor_details=$this->player_model->donors_list($pslug,10);
			$data["total_payments"] = $this->player_model->total_player_fund($pslug);
			$data["fund_details"]		= $this->player_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$data["contact_list"]		= $this->player_model->view("tbl_player_contacts",array("contact_added_by"=>$pslug));
		}
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_new_dashboard',$data);
		$this->load->view('../../includes_player/footer');
	}
	
	
	public function testing($fslug,$pslug)
	{
		if($this->input->post('info_submit'))
		{
			$player_fname = $this->input->post('player_fname');
			$player_lname = $this->input->post('player_lname');
			$upload_data = $this->upload_player_logo($fslug,$pslug,true);
			
			$playdata = array('player_fname'=>$player_fname,'player_lname'=>$player_lname);
			$return = $this->player_model->edit('tbl_player', $playdata, array('player_slug_name'=>$pslug));
			if($return){
				redirect($fslug.'/'.$pslug.'/admin/contacts');
			}
		}
		$player_details = array();
		$donor_details = array();
		$contact_details = array();
		if($pslug!='')
		{
			$player_details=$this->player_model->view("tbl_player",array("player_slug_name"=>$pslug));
			$donor_details=$this->player_model->donors_list($pslug,10);
			$contact_details=$this->player_model->view("tbl_player_contacts",array("contact_added_by"=>$pslug));
		}
		$start_date = $this->player_model->getStartdate($fslug);
		$data["active_tab"] = 'home';
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["contact_list"] = $contact_details;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$data["start_date"] = $start_date;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_player_detail',$data);
		$this->load->view('../../includes_player/footer');
	}
	
	public function contact_tab($fslug,$pslug)
	{
		
		$player_details = array();
		$donor_details = array();
		$contact_details = array();
		if($pslug!='')
		{
			$player_details=$this->player_model->view("tbl_player",array("player_slug_name"=>$pslug));
			$donor_details=$this->player_model->donors_list($pslug,10);
			$contact_details=$this->player_model->view("tbl_player_contacts",array("contact_added_by"=>$pslug));
		}
		if(isset($_POST['con_submit']))
		{
			$template	= $this->player_model->view('tbl_mail_template',array('template_slug'=>'request-for-donation-to-donor-from-player'));
			$subject	= $template[0]["template_subject"];
			$header		= $template[0]["template_header"];
			$footer		= $template[0]["template_footer"];
			$body		= $header;
			$body		.= html_entity_decode($template[0]["template_content"]);
			$body		.= '<a href="'.base_url('unsubscribe/[contact_id]').'">Unsubscribe</a>';
			
			$body		.= $footer;
			$contact_email = $this->input->post('contact_email');
			$contact_li = array(
							'contact_email'	=> $contact_email,
							'contact_added_by'=> $pslug,
							'created'	=> date("Y-m-d H:i:s")
							);
			$return = $this->player_model->add('tbl_player_contacts', $contact_li);
			$contact_li['id'] = $this->player_model->last_insert_id();
			
			/*--------fundraiser-------------*/
			$fund_details		= $this->player_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fundraiser_id		= $fund_details[0]['id'];
		    //$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			
			if($fund_details[0]['fund_image']==""){
				$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
			}
			/*-------------------------------*/
			/*-------------player------------*/
			
			$player_id			= $player_details[0]['id'];
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_email		= $player_details[0]['player_email'];
			
			if($player_details[0]['player_image']==""){
			$player_img			=  base_url("assets/images/noimage-150x150.jpg");   
			}else{
			$playerTimg			= explode(".",$player_details[0]['player_image']);	
			$player_img			=  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
			}
			
			$player_name		= $player_first.' '.$player_last;
			$start_fundraiser	= base_url("start-fundraiser");
				$contact_id			= $contact_li["id"];
				$email				= $contact_li["contact_email"];
				
				$indata = array(
					'from_id'		=> $player_id,
					'to_id'			=> $contact_id,
					'from_email'	=> $player_email,
					'to_email'		=> $email,
					'sent'			=> '1',
					'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert				= $this->player_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->player_model->last_insert_id();
				
				$player_url			= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_share_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_support_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				
				$keywords			= array('[contact_id]','[FundraiserImage]','[FundraiserName]','[FundraiserSlogan]','[PlayerImage]','[PlayerName]','[PlayerURL]','[PlayerShareLink]','[UseOfDonations]','[PlayerFirst]','[PlayerLast]','[SupportUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');

				$values				= array($contact_id,$fundimg,$fundraiser_name,$fundraiser_slog,$player_img,$player_name,$player_support_url,$player_share_url,$fundraiser_name,$player_first,$player_last,$player_support_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_fontcolor);

				$mail_content = str_replace($keywords,$values,$body).'<img src="'.base_url('getimage/'.$report_id).'" style="display:none;"/>';
				
				$config 			= array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('support@thanksforsupporting.com',$player_name);  
				$this->email->to($email); 
				$this->email->subject('ThanksForSupporting - '.$subject); 
				$this->email->message($mail_content);
				$bval=$this->email->send();	
				if($bval){
				$this->player_model->edit('tbl_player_contacts',array("invitation"=>1),array("id"=>$contact_li["id"],'contact_added_by'=>$pslug));
				
				}
			
		}
		
		
		$start_date = $this->player_model->getStartdate($fslug);
		$data["active_tab"] = 'contacts';
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["contact_list"] = $contact_details;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$data["start_date"] = $start_date;
        redirect($fslug.'/'.$pslug.'/admin/home');
		//$this->load->view('../../includes_player/header');        
		//$this->load->view('view_player_detail',$data);
		//$this->load->view('../../includes_player/footer');
	}
	
	public function invite_tab($fslug,$pslug)
	{
		$player_details = array();
		$donor_details = array();
		$contact_details = array();
		if($pslug!='')
		{
			$player_details=$this->player_model->view("tbl_player",array("player_slug_name"=>$pslug));
			$donor_details=$this->player_model->donors_list($pslug,10);
			$contact_details=$this->player_model->view("tbl_player_contacts",array("contact_added_by"=>$pslug));
			$invited_contact_details=$this->player_model->view("tbl_player_contacts",array("contact_added_by"=>$pslug,"invitation"=>1));
			$non_invited_contact_details=$this->player_model->view("tbl_player_contacts",array("contact_added_by"=>$pslug,"invitation"=>0));
		}
		$start_date = $this->player_model->getStartdate($fslug);
		$data["active_tab"] = 'invite';
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["contact_list"] = $contact_details;
		$data["invited_contact_details"] = $invited_contact_details;
		$data["non_invited_contact_details"] = $non_invited_contact_details;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$data["start_date"] = $start_date;
        
        redirect($fslug.'/'.$pslug.'/admin/home');
        
		//$this->load->view('../../includes_player/header');        
		//$this->load->view('view_player_detail',$data);
		//$this->load->view('../../includes_player/footer');
	}
	
	public function checkoutweb_tab($fslug,$pslug)
	{
		$player_details = array();
		$donor_details = array();
		$contact_details = array();
		if($pslug!='')
		{
			$player_details=$this->player_model->view("tbl_player",array("player_slug_name"=>$pslug));
			$donor_details=$this->player_model->donors_list($pslug,10);
			$contact_details=$this->player_model->view("tbl_player_contacts",array("contact_added_by"=>$pslug));
		}
		$start_date = $this->player_model->getStartdate($fslug);
		$data["active_tab"] = 'checkoutweb';
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["contact_list"] = $contact_details;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$data["start_date"] = $start_date;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_player_detail',$data);
		$this->load->view('../../includes_player/footer');
	}
	
	public function bulk_contact_upload($fslug,$pslug)
	{
		$ext = pathinfo($_FILES['bulk_contacts']['name'],PATHINFO_EXTENSION);
		if($ext=='xls' || $ext=='xlsx')
		{
			$this->load->library('excel');
			//Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/)	 
			$configUpload['upload_path'] = FCPATH.'assets/uploads/excel/';
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '5000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('bulk_contacts');	
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension=$upload_data['file_ext'];    // uploded file extension
			
			$objReader		= PHPExcel_IOFactory::createReader('Excel2007');// For excel 2007 	  
			//Set to read only
			$objReader->setReadDataOnly(true);
			//Load excel file
			$objPHPExcel	= $objReader->load(FCPATH.'assets/uploads/excel/'.$file_name);		 
			$totalrows		= $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows available in excel      	 
			$objWorksheet	= $objPHPExcel->setActiveSheetIndex(0);
			//loop from first data untill last data
			for($i=2;$i<=$totalrows;$i++)
			{	
				$contact_email = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
				$contact_added_by = $pslug;
				
				$dt_array = array(
				'contact_email' => $contact_email,
				'contact_added_by' => $contact_added_by
				);
				$return = $this->player_model->add('tbl_player_contacts',$dt_array);
			}
			if($return){
				unlink('./assets/uploads/excel/'.$file_name); //File Deleted After uploading in database .
				$this->session->set_userdata('up_msg','Upload successfully');
			}
		}
		else
		{
			$dir_path = FCPATH.'assets/uploads/excel/';
			$configUpload['upload_path'] = $dir_path;
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '5000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('bulk_contacts');	
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension=$upload_data['file_ext'];    // uploded file extension
			
			$filepath=$dir_path.$file_name;
			$ntag='\r\n';
			$estag='\"';

			$sqlPl="LOAD DATA LOCAL INFILE '".addslashes($filepath)."' INTO TABLE `tbl_contact_exception` 
			FIELDS TERMINATED BY ',' 
			OPTIONALLY ENCLOSED BY '".$estag."' 
			LINES TERMINATED BY '".$ntag."' 
			IGNORE 1 LINES (contact_email, added_by)
			SET added_by = '".$pslug."'";
			$this->db->query($sqlPl);
			$qry = $this->db->query("SELECT * FROM tbl_contact_exception WHERE added_by='".$pslug."'");
			$qry_arr = $qry->result_array();
			foreach($qry_arr as $arr){
				$contact_email = $arr["contact_email"];
				$contact_added_by = $pslug;
				
				$dt_array = array(
				'contact_email' => $contact_email,
				'contact_added_by' => $contact_added_by
				);
				$return = $this->player_model->add('tbl_player_contacts',$dt_array);
				$this->player_model->remove('tbl_contact_exception',array('id'=>$arr["id"],'added_by'=>$pslug));
			}
			if($return){
				unlink('./assets/uploads/excel/'.$file_name); //File Deleted After uploading in database .
				$this->session->set_userdata('up_msg','Upload successfully');
			}
		}
		redirect($fslug.'/'.$pslug.'/admin/contacts');
	}
	
	public function invitetoall($fslug,$pslug)
	{
		$response = '';		
	$action = $this->input->post('action');
	$status = $this->input->post('status');
	$contact_id = $this->input->post('contact_id');
	
	
		if($fslug!='' && $pslug!='')
		{
			/*--------fundraiser-------------*/
			$fund_details		= $this->player_model->view('tbl_fundraiser',array('fund_slug_name'=>$fslug));
			$fundraiser_id		= $fund_details[0]['id'];
		    //$fundraiser_name	= $fund_details[0]['fund_fname'].' '.$fund_details[0]['fund_lname'];
			$fundraiser_name	= $fund_details[0]['fund_username'];
			$fundraiser_slog	= $fund_details[0]['fund_slogan'];
			$fundraiser_bgcolor	= $fund_details[0]['fund_color_rgb'];
			$fundraiser_fontcolor= $fund_details[0]['fund_font_color'];
			
			if($fund_details[0]['fund_image']==""){
				$fundimg 		= base_url("assets/images/noimage-150x150.jpg");  
			}else{
				$fundTimg 		= explode(".",$fund_details[0]['fund_image']);	
				$fundimg  		= base_url("assets/fundraiser_image/thumb/".$fundTimg[0].'_thumb.'.$fundTimg[1]);   
			}
			/*-------------------------------*/
			/*-------------player------------*/
			$player_details		= $this->player_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_id			= $player_details[0]['id'];
			$player_first		= $player_details[0]['player_fname'];
			$player_last		= $player_details[0]['player_lname'];
			$player_email		= $player_details[0]['player_email'];
			
			if($player_details[0]['player_image']==""){
			$player_img			=  base_url("assets/images/noimage-150x150.jpg");   
			}else{
			$playerTimg			= explode(".",$player_details[0]['player_image']);	
			$player_img			=  base_url("assets/player_image/thumb/".$playerTimg[0].'_thumb.'.$playerTimg[1]);   
			}
			
			$player_name		= $player_first.' '.$player_last;
			$start_fundraiser	= base_url("start-fundraiser");
			/*------------------------------*/
			
			switch($action){
				case 'status_change':
					$this->player_model->edit('tbl_player_contacts',array('status'=>$status),array('id'=>$contact_id));
				break;				
				default:
			$template	= $this->player_model->view('tbl_mail_template',array('template_slug'=>'request-for-donation-to-donor-from-player'));
			$subject	= $template[0]["template_subject"];
			$header		= $template[0]["template_header"];
			$footer		= $template[0]["template_footer"];
			$body		= $header;
			$body		.= html_entity_decode($template[0]["template_content"]);
			$body		.= '<a href="'.base_url('unsubscribe/[contact_id]').'">Unsubscribe</a>';
			$body		.= $footer;
			
			$where = array('contact_added_by'=>$pslug);
			
			if($this->input->post('contact_id')):
			$where['id'] = $this->input->post('contact_id');
			else:
			$where['invitation'] = 0;
			endif;
			
			$where['status'] = 1;
			
			$contact_list = $this->player_model->view('tbl_player_contacts',$where);
			foreach($contact_list as $contact_li){
				$contact_id			= $contact_li["id"];
				$email				= $contact_li["contact_email"];
				
				$indata = array(
					'from_id'		=> $player_id,
					'to_id'			=> $contact_id,
					'from_email'	=> $player_email,
					'to_email'		=> $email,
					'sent'			=> '1',
					'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert				= $this->player_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->player_model->last_insert_id();
				
				$player_url			= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_share_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				$player_support_url	= base_url($fslug.'/'.$pslug.'/support/'.$report_id);
				
				$keywords			= array('[contact_id]','[FundraiserImage]','[FundraiserName]','[FundraiserSlogan]','[PlayerImage]','[PlayerName]','[PlayerURL]','[PlayerShareLink]','[UseOfDonations]','[PlayerFirst]','[PlayerLast]','[SupportUrl]','[StartFundraiser]','[bgcolor]','[fontcolor]','[selected_color]');

				$values				= array($contact_id,$fundimg,$fundraiser_name,$fundraiser_slog,$player_img,$player_name,$player_support_url,$player_share_url,$fundraiser_name,$player_first,$player_last,$player_support_url,$start_fundraiser,$fundraiser_bgcolor,$fundraiser_fontcolor,$fundraiser_fontcolor);

				$mail_content = str_replace($keywords,$values,$body).'<img src="'.base_url('getimage/'.$report_id).'" style="display:none;"/>';
				
				$config 			= array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('support@thanksforsupporting.com',$player_name);  
				$this->email->to($email); 
				$this->email->subject('ThanksForSupporting - '.$subject); 
				$this->email->message($mail_content);
				$bval=$this->email->send();	
				if($bval){
				$this->player_model->edit('tbl_player_contacts',array("invitation"=>1),array("id"=>$contact_li["id"],'contact_added_by'=>$pslug));
				
				}
			}
			$this->player_model->edit('tbl_player',array("invited"=>1),array('player_slug_name'=>$pslug));
			$response = "Invitation sent";				
			break;
		  }
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_userdata('succ_msg', $response);
		if(isset($_POST["goto"])){
			if($_POST["goto"] == 'step2'){
				redirect($fslug.'/'.$pslug.'/admin/invite');
			}else{
				redirect($fslug.'/'.$pslug.'/admin/invite');
			}
		}else{
			redirect($fslug.'/'.$pslug.'/admin/invite');
		}
		
	}
	
	public function getCustomMailAjax($fslug,$pslug)
	{
		$template_details	= $this->player_model->view('tbl_mail_template',array("template_slug"=>'custom-email-to-contacts'));
        $data['fslug']		= $fslug;  
        $data['pslug']		= $pslug;  
        $data['templateInfo']= $template_details;  
        echo $this->load->view('view_custom_mail_modal', $data, true);
	}
	
	public function payagain($fslug,$pslug)
	{
		$response = '';
		if($fslug!='' && $pslug!='')
		{
			/*----------------player---------------*/
			$player_details		= $this->player_model->view('tbl_player',array('player_slug_name'=>$pslug));
			$player_name		= $player_details[0]['player_fname'].' '.$player_details[0]['player_lname'];
			/*-------------------------------------*/
			
			$template	= $this->player_model->view('tbl_mail_template',array('template_slug'=>'custom-email-to-contacts'));
			$subject	= $template[0]["template_subject"];
			$header		= $template[0]["template_header"];
			$body		= $template[0]["template_content"];
			$footer		= $template[0]["template_footer"];
			
			$mailcontent= $header.html_entity_decode($body).$footer;

			$contact_list = $this->player_model->view('tbl_player_contacts',array('contact_added_by'=>$pslug));
			foreach($contact_list as $contact_li){
				$email				= $contact_li["contact_email"];
				$config 			= array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);   
				$this->email->from('support@thanksforsupporting.com',$player_name);  
				$this->email->to($email); 
				$this->email->subject('ThanksForSupporting - '.$subject); 
				$this->email->message($mailcontent);
				$bval=$this->email->send();	
			}
			$response = "Mail sent";
		}else{
			$response = "Sufficient data not provided";
		}
		$this->session->set_userdata('up_msg', $response);
		redirect($fslug.'/'.$pslug.'/admin/contacts');
	}
	
	public function del_contact($fslug,$pslug)
	{
		
		$contact_id = $this->input->post('contact_id');
		if($fslug!='' AND $pslug!='')
		{
			$return = $this->player_model->remove('tbl_player_contacts',array('id'=>$contact_id,'contact_added_by'=>$pslug));
			if($return){
				$rr = array('valid'=>1,'msg'=>'Deleted Successfully');
			}else{
				$rr = array('valid'=>0,'msg'=>'Error Occured');
			}
		}else{
			$rr = array('valid'=>0,'msg'=>'Sufficient data not provided');
		}
		
		echo json_encode($rr);
	}
	
	public function upload_player_logo($fslug,$pslug,$is_update=false)
	{
		$player_image 	= isset($_FILES['profile_img']['name'])?$_FILES['profile_img']['name']:"";
	
		$arr			= $this->player_model->view('tbl_player',array('player_slug_name'=>$pslug));
		$res			= array('msg'=>'','valid'=>0);
		if($player_image!="")
		{
			if(!empty($arr))
			{
				if(isset($arr[0]['player_image']) && $arr[0]['player_image']!=""){
					if(file_exists("./assets/player_image/thumb/".$arr[0]['player_image'])){
					  unlink("./assets/player_image/thumb/".$arr[0]['player_image']);
					}
					if(file_exists("./assets/player_image/".$arr[0]['player_image'])){
					  unlink("./assets/player_image/".$arr[0]['player_image']);
					}
				}
			}
			
			$config_img['upload_path']   = './assets/player_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config_img['max_size']      = '100000';
			$this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('profile_img');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
			{ 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/player_image/'.$fpath1;
				$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
				$config_resize['height']          = 200;
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
			}
			$playerimg 		= explode(".",$player_image);   
			$playerimage	=  base_url("assets/player_image/thumb/".$playerimg[0].'_thumb.'.$playerimg[1]);
			$playdata = array('player_image'=> $player_image);
			$return = $this->player_model->edit('tbl_player', $playdata, array('player_slug_name'=>$pslug));
			if($return){
				$res = array('msg'=>'uploaded','valid'=>1,'src'=>$playerimage);
			}else{
				$res = array('msg'=>'Error occured','valid'=>0);
			}
		}
		if(!$is_update)
		echo json_encode($res);
	}
	/*-----------------------------------Player Section Start---------------------------------*/
	public function playerloadmore()
	{
		$offset = 10;
		$limit = $this->input->post('limit')*$offset;
		$players_list = $this->player_model->view_limit("tbl_player",'',$limit,$offset);
		$tbr='';
		if(count($players_list)>0)
		{
		foreach($players_list as $players_li)
		{
		$tbr.= '<tr>
				<td data-th="Player Name">
				<span><img src="'.base_url().'assets/fundraiser/images/pro_pic3.png" alt=""></span>
				<strong>
				<a href="'.base_url().'fundraiser/'.$this->session->userdata('fundraiser_urlname').'/players/profile/'.$players_li["player_slug_name"].'">'.$players_li["player_fname"].' '.$players_li["player_lname"].'</a>
				</strong>
				</td>
				<td data-th="Title">'.$players_li["player_title"].'</td>
				<td data-th="Goal">$'.$players_li["player_goal"].'</td>
				<td data-th="Raised">$'.$players_li["player_goal"].'</td>
				<td data-th="Remaining">$'.$players_li["player_goal"].'</td>
				<td data-th="" class="text-right"><small>'.date("F d, Y",strtotime($players_li["created"])).'</small></td>
			    </tr>';
		}
		echo $tbr;
		}
		else
		{
		echo 0;	
		}
	}

	public function playerDonors($fslug,$pslug)
	{
		$fund_details 	= array();
		$player_details = array();
		$donor_details 	= array();
		$total_payments = array();
		if($fslug!='' AND $pslug!='')
		{
			$fund_details = $this->player_model->view('tbl_fundraiser',array("fund_slug_name"=>$fslug));
			$player_details = $this->player_model->view('tbl_player',array("player_slug_name"=>$pslug));			
			if(count($player_details)>0)
			{
				$donor_details=$this->player_model->donors_list($pslug,999999);
				$total_payments	= $this->player_model->total_player_fund($pslug);
			}
		}
		$data["fund_details"] = $fund_details;
		$data["player_details"] = $player_details;
		$data["donors_list"] = $donor_details;
		$data["total_payments"] = $total_payments;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_player_donors',$data);
		$this->load->view('../../includes_player/footer');    
	}
	
	public function playerEmailReportsGraph($fslug,$pslug)
	{
		$player_details = array();
		$report_list = array();
		$player_id = 0;
		if($pslug!='')
		{
			$player_details = $this->player_model->playersInfo($pslug);
		}
		if(count($player_details)>0)
		{
			$report_list = $this->player_model->view('tbl_email_share_reports',array('from_id'=>$player_details[0]['id']));
			$player_id = $player_details[0]['id'];
		}
		$report_chart = $this->player_model->email_reports($player_id);
		$start_date = $this->player_model->getStartdate($fslug);
		$data["player_details"] = $player_details;
		$data['report_chart'] 	= $report_chart;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$data["start_date"] = $start_date;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_player_emailreports_graph',$data);
		$this->load->view('../../includes_player/footer');    
	}
	
	public function playerEmailReportsList($fslug,$pslug)
	{
		$player_details = array();
		$report_list = array();
		$player_id = 0;
		if($pslug!='')
		{
			$player_details = $this->player_model->playersInfo($pslug);
		}
		if(count($player_details)>0)
		{
			$report_list = $this->player_model->view('tbl_email_share_reports',array('from_id'=>$player_details[0]['id']));
			$player_id = $player_details[0]['id'];
		}
		$report_chart = $this->player_model->email_reports($player_id);
		$start_date = $this->player_model->getStartdate($fslug);
		$data["player_details"] = $player_details;
		$data['report_list'] 	= $report_list;
		$data['report_chart'] 	= $report_chart;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$data["start_date"] = $start_date;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_player_emailreports_list',$data);
		$this->load->view('../../includes_player/footer');    
	}
	
	public function getPlayerModalAjax()
	{
		$pslug=$this->input->post('pslug');  
		if($pslug!='')
		{
			$player_details=$this->player_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
        $data['playerInfo']      = $player_details;  
        echo $this->load->view('view_player_info_edit_modal', $data, true); 
	}
	
	public function playerInfoEditAjax()
	{
        $player_id			= $_POST['hid_pid'];
        $player_image		= isset($_FILES['file']['name'])?$_FILES['file']['name']:"";
        $player_email		= $_POST['player_email'];
        $player_fname		= $_POST['player_fname'];
        $player_lname		= $_POST['player_lname'];
    
        $arr=$this->player_model->view('tbl_player',array('id'=>$player_id));
        if($player_image!="")
        {
            if(!empty($arr))
			{
                if(isset($arr[0]['player_image']) && $arr[0]['player_image']!=""){
                    if(file_exists("./assets/player_image/thumb/".$arr[0]['player_image'])){
                      unlink("./assets/player_image/thumb/".$arr[0]['player_image']);
                    }
                    if(file_exists("./assets/player_image/".$arr[0]['player_image'])){
                      unlink("./assets/player_image/".$arr[0]['player_image']);
                    }
                }
            }
			
			$config_img['upload_path']   = './assets/player_image/';
			$config_img['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config_img['max_size']      = '100000';
            $this->load->library('upload', $config_img);
			$this->upload->initialize($config_img);			 
			$this->upload->do_upload('file');			
			$data_img = $this->upload->data(); 
			if($data_img['is_image'] == 1)
            { 
				$fpath1  = $data_img['file_name'];			
				$this->load->library('image_lib');
				$config_resize['image_library']   = 'gd2';	
				$config_resize['create_thumb']    = TRUE;
				$config_resize['maintain_ratio']  = TRUE;
				$config_resize['master_dim']      = 'height';
				$config_resize['quality']         = "100%";  
				$config_resize['source_image']    = './assets/player_image/'.$fpath1;
				$config_resize['new_image']       = './assets/player_image/thumb/'.$fpath1;
				$config_resize['height']          = 200;
				$config_resize['width']           = 1;
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize(); 
				$player_image = $fpath1;
			}
        }
        else
		{
            $player_image = $arr[0]['player_image'];
        }
    
        $playdata = array(
                          'player_fname'	=> $this->input->post('player_fname'),
                          'player_lname'	=> $this->input->post('player_lname'),
                          'player_image'	=> $player_image
                          );
        $return = $this->db->update('tbl_player', $playdata, array('id'=>$this->input->post('hid_pid')));
		
        if($return){
            $data['msg']=1;
        }
        else{
            $data['msg']=0;
        }
        echo json_encode($data);
	}
	
	public function donationemailsend()
	{
		$result 			= array();
		$email_csv  		= isset($_FILES['file']['name'])?$_FILES['file']['name']:"";
		$email_list 		= isset($_POST['email_list'])?$_POST['email_list']:"";
		$player_id			= $this->session->userdata('player_id');
		
		$player_details		= $this->player_model->view('tbl_player',array('id'=>$player_id));
		$template 			= $this->player_model->view('tbl_mail_template',array('template_slug'=>'donations-email-send'));
		$template_body		= html_entity_decode($template[0]["template_content"]);
		$template_subject 	= $template[0]["template_subject"];
		$player_slug		= $player_details[0]["player_slug_name"];
		$player_email		= $player_details[0]['player_email'];
		
		$fundraiser_id		= $player_details[0]['player_added_by'];
		$fundraiser_details	= $this->player_model->view('tbl_fundraiser',array('id'=>$fundraiser_id));
		$fundraiser_slug	= $fundraiser_details[0]['fund_slug_name'];
		
		if($email_csv!="")
		{
			$filename=$_FILES["file"]["tmp_name"];
			$file = fopen($filename, "r");
			
			while (($importdata = fgetcsv($file, 1000, ",")) !== FALSE)
			{
				$indata = array(
				'from_id'		=> $player_id,
				'from_email'	=> $player_email,
				'to_email'		=> $importdata[0],
				'sent'			=> '1',
				'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert 			= $this->player_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->player_model->last_insert_id();
				
				$player_url			= base_url($fundraiser_slug.'/'.$player_slug.'/support/'.$report_id);
				$email_content		= str_replace('[player URL]',$player_url,$template_body);
				
				$updata 			= array('email_content'	=> $email_content);
				$where				= array('id' => $report_id);
				$update				= $this->player_model->edit('tbl_email_share_reports',$updata,$where);
				
				$tracker_img		= '<img src="'.base_url().'getimage/'.$report_id.'" style="display:none;"/>';
				
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				$str = $email_content.$tracker_img;
				$this->email->initialize($config);   
				$this->email->from($player_email);  
				$this->email->to($importdata[0]); 
				$this->email->subject("Thanks for Supporting - ".$template_subject); 
				$this->email->message($str);
				$bval=$this->email->send();
			}
			
			fclose($file);
			$result = array('valid'=>1,'msg'=>'Email send for donations successfully');
		}
		else
		{
			$email_lists = explode(",", $email_list);
			foreach($email_lists as $email_li)
			{
				$indata = array(
					'from_id'		=> $player_id,
					'from_email'	=> $player_email,
					'to_email'		=> $email_li,
					'sent'			=> '1',
					'sent_time'		=> date('Y-m-d H:i:s')
				);
				$insert 			= $this->player_model->add('tbl_email_share_reports',$indata);
				$report_id			= $this->player_model->last_insert_id();
				
				$player_url			= base_url().$fundraiser_slug.'/'.$player_slug.'/support/'.$report_id;
				$email_content		= str_replace('[player URL]',$player_url,$template_body);
				
				$updata 			= array('email_content'	=> $email_content);
				$where				= array('id' => $report_id);
				$update				= $this->player_model->edit('tbl_email_share_reports',$updata,$where);
				
				$tracker_img		= '<img src="'.base_url().'getimage/'.$report_id.'" style="display:none;"/>';
				
				$config = array();
				$config['useragent']= "CodeIgniter";
				$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol'] = "smtp";
				$config['smtp_host']= "localhost";
				$config['smtp_port']= "25";
				$config['mailtype'] = 'html';
				$config['charset']	= 'iso-8859-1';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				
				$str = $email_content.$tracker_img;
				$this->email->initialize($config);   
				$this->email->from($player_email);  
				$this->email->to($email_li); 
				$this->email->subject("Thanks for Supporting - ".$template_subject); 
				$this->email->message($str);
				$bval=$this->email->send();
			}
			$result = array('valid'=>1,'msg'=>'Email send for donations successfully');
		}
		echo json_encode($result);
	}

	public function playerEmailCheckerAjax()
	{
		$where = array('player_email'=>$this->input->post('player_email')); 
		$query=$this->player_model->view_check('tbl_player',$where);
	   
		if($query>0)
		{
			echo 'false';
		}
		else
		{
			echo 'true';
		}
	}
	
	public function getMailStructure()
	{
		$pslug=$this->input->post('pslug');  
		$donor=$this->input->post('donor');  
		if($pslug!='')
		{
			$player_details=$this->player_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		if($donor!='')
		{
			$donor_details=$this->player_model->view('tbl_donors_payment',array("id"=>base64_decode($donor)));
		}
        $data['playerInfo']	= $player_details;  
        $data['donorInfo']	= $donor_details;  
        echo $this->load->view('view_player_mail_modal', $data, true); 
	}
	
	public function getMailfromReports()
	{
		$pslug=$this->input->post('pslug');
		$report=$this->input->post('report');  
		if($pslug!='')
		{
			$player_details=$this->player_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		if($report!='')
		{
			$report_details=$this->player_model->view('tbl_email_share_reports',array("id"=>base64_decode($report)));
		}
		$data['playerInfo']	= $player_details;  
		$data['reportInfo']	= $report_details;  
		echo $this->load->view('view_thanks_mail_modal', $data, true); 
	}
	
	public function sendingThanks()
	{
		$pid=$this->input->post('hid_pid'); 
		$did=$this->input->post('hid_did');
        if($pid!='' AND $did!='')
		{
			$player_details=$this->player_model->view('tbl_player',array("id"=>base64_decode($pid)));
			$player_name = $player_details[0]["player_fname"].' '.$player_details[0]["player_lname"];
			$player_email = $player_details[0]["player_email"];
			$donor_details=$this->player_model->view('tbl_donors_payment',array("id"=>base64_decode($did)));
			$donor_name = $donor_details[0]["donor_fname"].' '.$donor_details[0]["donor_lname"];
			$donor_email = $donor_details[0]["donor_email"];
			
			$this->player_model->edit('tbl_donors_payment',array('is_thankyou_email'=>1),array("id"=>base64_decode($did)));
			
			
			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "smtp";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;
			$str = "<p>Dear $donor_name,</p>
			<p>Thank you so much for your donation. I really appreciate you thinking of me and you couldn't have chosen a more perfect present for me. Again, thanks very much.</p>";
			$this->email->initialize($config);   
			$this->email->from($player_email);  
			$this->email->to($donor_email); 
			$this->email->subject("Thanks for Supporting"); 
			$this->email->message($str);
			$bval=$this->email->send();
			echo json_encode(array('msg'=>1));
		}
		else
		{
			echo json_encode(array('msg'=>0));
		}
	}
	
	public function sendThanksfromReports()
	{
		$rid=$this->input->post('hid_rid'); 
		if($rid!='')
		{
			$report_details	= $this->player_model->view('tbl_email_share_reports',array("id"=>base64_decode($rid)));
			$from_email	= $report_details[0]["from_email"];
			$to_email	= $report_details[0]["to_email"];
			$this->player_model->edit('tbl_email_share_reports',array("thanks"=>1,"thanks_datetime"=>date("Y-m-d H:i:s")),array("id"=>base64_decode($rid)));

			$config = array();
			$config['useragent']= "CodeIgniter";
			$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
			$config['protocol'] = "smtp";
			$config['smtp_host']= "localhost";
			$config['smtp_port']= "25";
			$config['mailtype'] = 'html';
			$config['charset']	= 'iso-8859-1';
			$config['newline']  = "\r\n";
			$config['wordwrap'] = TRUE;
			$str = "<p>Dear Friend,</p>
			<p>Thank you so much for your donation. I really appreciate you thinking of me and you couldn't have chosen a more perfect present for me. Again, thanks very much.</p>";
			$this->email->initialize($config);   
			$this->email->from($from_email);  
			$this->email->to($to_email); 
			$this->email->subject("Thanks for Supporting"); 
			$this->email->message($str);
			$bval=$this->email->send();
			echo json_encode(array('msg'=>1));
		}
		else
		{
			echo json_encode(array('msg'=>0));
		}
	}
	
	public function profile($fslug,$pslug)
	{
        $player_details = array();
		if($pslug!='')
		{
			$player_details=$this->player_model->view('tbl_player',array("player_slug_name"=>$pslug));
		}
		$data["player_details"] = $player_details;
		$data["fslug"] = $fslug;
		$data["pslug"] = $pslug;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_profile_detail',$data);
		$this->load->view('../../includes_player/footer');    
	}
	
	public function managerewards()
	{
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_manage_rewards');
		$this->load->view('../../includes_player/footer');    
	}
	
	/*-----------------------------------Manage Rewards Section End---------------------------------*/
	
	public function settings()
	{
		$player_pass			= $this->session->userdata('player_password');
		$encrypt_key 			= $this->config->item('encryption_key');  
		$player_passw 			= $this->encrypt->decode($player_pass,$encrypt_key);
		$data["player_dpass"]	= $player_passw;
		$data["player_epass"]	= $player_pass;
		$this->load->view('../../includes_player/header');        
		$this->load->view('view_settings',$data);
		$this->load->view('../../includes_player/footer');    
	}
	
	public function ShowHidePassword()
	{
		$stat = $this->input->post("term");
		if(strtolower($stat)=="show")
		{
		$player_pass		= $this->session->userdata('player_password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$player_passw 		= $this->encrypt->decode($player_pass,$encrypt_key);
		$data["player_pass"]= $player_passw;	
		$result				= array("spassword"=>$player_passw,"btext"=>"Hide");
		}
		else
		{
		$player_pass		= $this->session->userdata('player_password');
		$encrypt_key 		= $this->config->item('encryption_key');  
		$player_passw 		= $this->encrypt->decode($player_pass,$encrypt_key);
		$player_passw		= str_repeat("*",strlen($player_passw));;	
		$result				= array("spassword"=>$player_passw,"btext"=>"Show");
		}
		echo json_encode($result);
	}
	
	public function ajaxSettings()
	{
        $player_id				= $this->session->userdata('player_id');
        $player_email			= $_POST['player_email'];
        $player_pass			= $_POST['player_pass'];
        $player_pass_confirm	= $_POST['player_pass_confirm'];
		$data = array();
		if($player_pass==$player_pass_confirm)
		{
			$check = $this->player_model->view_check('tbl_player',array('id'=>$player_id,'player_email'=>$player_email));
			if($check==1)
			{
				$encrypt_key = $this->config->item('encryption_key');  
				$fund_passw = $this->encrypt->encode($player_pass,$encrypt_key);
			
				$fund_data = array('player_pass' => $fund_passw);
				$return = $this->player_model->edit('tbl_player', $fund_data, array('id'=>$player_id,'player_email'=>$player_email));
				
				if($return)
				{
					$this->session->set_userdata('player_password',$fund_passw);
					$data['valid']=1;
					$data['msg']="Password Changed Successfully";
				}
				else
				{
					$data['valid']=0;
					$data['msg']="Error Occured while processing your request.";
				}
			}
			else
			{
				$data['valid']=0;
				$data['msg']="Email address not found in our system.";
			}
		}
		else
		{
			$data['valid']=0;
            $data['msg']="Confirm Password is not matching.";
		}
        return json_encode($data);
	}
	
	private function createThumb($path, $filename,$data,$w, $h=0, $crop=false){
    
        $config['image_library']    = "gd2";      
        $config['source_image']     = $data['full_path'];      
        $config['create_thumb']     = TRUE;      
        $config['maintain_ratio']   = FALSE;
        $config['thumb_marker']     = '';
        $config['new_image']    = $path.$filename;


        $width= $data['image_width'];
        $height= $data['image_height'];

        /*if($h!=0){
            $newwidth = $w;
            $newheight = $h;
        }else{
            $newwidth = $w;
            $fraction_amt = $w/$original_width;
            $newheight = intval($original_height*$fraction_amt);
        }*/
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }

        $config['width']            = $newwidth; 
        $config['height']           = $newheight;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize()){
        return $this->image_lib->display_errors();
        } 
        $this->image_lib->clear();
        return false; 
	}
		
	public function logout($fslug,$pslug)
	{
		$this->session->unset_userdata('player_id');
		$this->session->unset_userdata('player_username');
		$this->session->unset_userdata('player_urlname');
		$this->session->unset_userdata('player_password');
		$this->session->unset_userdata('player_contact_email');
		$this->session->set_userdata('err_msg', 'Successfully Logout !');
		$this->session->set_userdata('player_logout', '1');
        
        $this->session->unset_userdata('fundraiser_id');
		$this->session->unset_userdata('fundraiser_username');
		$this->session->unset_userdata('fundraiser_urlname');
		$this->session->unset_userdata('fundraiser_password');
		$this->session->unset_userdata('fundraiser_contact_email');
		//$this->session->set_userdata('err_msg', 'Successfully Logout !');
        
		redirect('admin/login');
	}
}
