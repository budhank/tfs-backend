<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_AdminController extends CI_Controller {
 function __construct()
 {
	 parent::__construct();
	 $this->checkAdminLogin();
 }
 public function checkAdminLogin()
 {
	 
	 $id = $this->session->userdata('id');	 
	 if(empty($id)){
		 redirect('admin');
	 }
 }
 function _sendEmail($from,$sender_name,$to,$sub,$body){						      
	        $this->load->library('email');
		    $config['mailtype'] = 'html';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;	
			$this->email->initialize($config);
			$this->email->from($from, $sender_name);
			$this->email->to($to);	
			$this->email->subject($sub);
			$this->email->message($body);	
			$res = $this->email->send();			
			return $res;	
	}
}