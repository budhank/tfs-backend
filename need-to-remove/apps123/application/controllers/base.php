<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base extends CI_Controller{
	
  function Base(){
		parent::__construct();
		$this->load->model("base_model");
	   }
	   
   public function checkAllUser()
   {
	   $uname = $this->input->post('username');
	   $str	  = '';
	   $res   = $this->base_model->funcCheckAllUser($uname);
	   $res   = array_filter($res);   //Elimination empty values
	   $res   = array_values($res);   //Remaking the array 
	   //print_r($res);
	   if(count($res)>1){
		   /*$str	  .= '<select name="utype" class="form-control" required onchange="set_value(this.value)">';
		   $str	  .= '<option value="">Select</option>';
		   for($i=0;$i<count($res);$i++)
		   {
			 $str .= '<option value="'.$res[$i].'">'.$res[$i].'</option>'; 
		   }
		   $str	  .= '</select>';*/
		   //$data['oc']  = 'both';
		   $data['str']  = 'Admin';
	   }
	   elseif(count($res)==1){
		 $data['oc']   = 'single';  
		 $data['str']  = $res[0];
	   }
	   else{
		 $data['oc']   = 'no';  
		 $data['str']  = 'No username found!!';  
	   }
	   echo json_encode($data);
   }
	   
}
?>